package util;

public class CommUtil {
	
	public static int getIndexInIntArray(int[] arr, int val) {
		if (arr == null || arr.length == 0) {
			return 0;
		}
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == val) {
				return i;
			}
		}
		return 0;
	}

}
