package util;

import java.util.Iterator;

import normal.ui.UIBuilder;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/**
 * xml�??�?
 * 
 * 
 * @author <a href="mailto:whz-work@163.com">�?���?/a>
 * 
 * @since 1.0 Create on 2011-5-30
 */
public class XmlProcesser {
	 
	private static String file = "test/test.xml";

	@SuppressWarnings("unchecked")
	public static void processXml(String file) {

		Element rootElement = loadXML(file).getRootElement();
		if (rootElement.hasContent() && rootElement.hasMixedContent()) {
			Iterator<Element> iterator = rootElement.elementIterator();
			StringBuffer sb = new StringBuffer();
			StringBuffer valueSb = new StringBuffer();
			while (iterator.hasNext()) {
				
				Element element = iterator.next();
				if (element.getNodeType() == Node.ELEMENT_NODE) {

					Iterator<Element> childIterator = element.elementIterator();
					while (childIterator.hasNext()) {
						Element childElement = childIterator.next();

						if (childElement.getNodeType() == Node.ELEMENT_NODE) {
							try {
//								BeanUtils.setProperty(target, childElement.getName(), childElement.getTextTrim());
								sb.append(childElement.getTextTrim()).append(",");
								valueSb.append(childElement.attribute("value").getText()).append(",");
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
			System.out.println(sb.toString());
			System.out.println(valueSb.toString());
		}
	}

	/**
	 * ??��xml??��
	 * 
	 * @param file
	 * @return
	 */
	public static Document loadXML(String file) {
		SAXReader reader = new SAXReader();
		try {
			reader.setEncoding("utf8");
			return reader.read(UIBuilder.class.getClassLoader().getResource(file).getFile());
		} catch (Exception e) {
			e.printStackTrace();
			return DocumentFactory.getInstance().createDocument();
		}
	}
	
	public static void main(String[] args) {
		processXml(file);
	}
}
