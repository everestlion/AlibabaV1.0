package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

public class JDBC {
	
	private static final String driver = "com.mysql.jdbc.Driver"; 
	private static final String dbName = "kungfu_qq"; 
	public static final String userName = "kungfu_user"; 
	public static final String passwrod = "kunfudebug"; 
	public static final String url = "jdbc:mysql://192.168.22.2/" + dbName + "?useUnicode=true&autoReconnect=true&failOverReadOnly=false&characterEncoding=utf8&rewriteBatchedStatements=true";
	
	public static final String userName_l = "root"; 
	public static final String passwrod_l = "root"; 
	public static final String url_l = "jdbc:mysql://127.0.0.1/" + dbName + "?useUnicode=true&autoReconnect=true&failOverReadOnly=false&characterEncoding=utf8&rewriteBatchedStatements=true";
	
	private static Connection conn = null;
	private static PreparedStatement ps = null;
	
	public static void excute(String sql) {
		try {
			ps = (PreparedStatement) getConn().prepareStatement(sql); 
			ps.execute();
			System.out.println("!!!!done");	
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public static void main(String[] args) {
		try {
			Statement stmt = getConn().createStatement();
			ResultSet rst = stmt.executeQuery("show create table kf_player");
			while (rst.next()) {

//				System.out.println(new String(rst.getString(2).getBytes(), Charset.forName("UTF-8")));
				System.out.println(rst.getString(2));

			}

			rst.close();
			stmt.close();
			conn.close();

			System.out.println("end");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public static Connection getConn() {
		try {
			if(conn == null) {
				synchronized (JDBC.class) {
					if (conn == null) {
						Class.forName(driver);
//						conn = DriverManager.getConnection(url_l, userName_l, passwrod_l); 
						conn = DriverManager.getConnection(url_l, userName_l, passwrod_l); 
					}
				}
			}
           
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return conn;
	}
	
	public static PreparedStatement prepareStatement(String sql) throws SQLException {
		return (PreparedStatement)getConn().prepareStatement(sql);
	}
	
	public static void close() {
		try {
			if(ps != null) {
				ps.close();
				ps = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
