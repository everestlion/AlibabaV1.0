package normal.config;


public enum EquipType {
	/** 头盔 */
	Helmet(1, 201),

	/** 上身 */
	Upperbody(2, 202),

	/** 挂件 */
	Pendant(3, 203),

	/** 鞋子 */
	Shoes(4, 204),

	/** 武器 */
	Arms(5, 205),

	/** 护腕 */
	Wrist(6, 206),

	/** 项链 */
	Necklace(7, 207),

	/** 腰带 */
	Belt(8, 208),

	/** 戒指 */
	RingLeft(9, 209),

	RingRight(10, 209),
	/**
	 * 婚戒
	 */
	RingMarry(11, 211);

	private int position;
	private int goodTypeId;

	private EquipType(int position, int goodTypeId) {
		this.position = position;
		this.goodTypeId = goodTypeId;
	}

	public int position() {
		return position;
	}

	public int goodTypeId() {
		return goodTypeId;
	}

	public static EquipType convert2Type(int goodTypeId) {
		for (EquipType t : EquipType.values()) {
			if (t.goodTypeId() == goodTypeId)
				return t;
		}
		return null;
	}

	public static boolean isNOForgeEquiq(int goodTypeId) {
		return goodTypeId == 203 || goodTypeId == 211;
	}

}
