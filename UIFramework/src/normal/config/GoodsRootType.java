package normal.config;

public enum GoodsRootType {
	
	TOOL(1),
	EQUIP(2),
	TASK(3);
	
		
	private int value;
	
	private GoodsRootType(int value){
		this.value = value;
	}

	public int value() {
		return value;
	}
	
}
