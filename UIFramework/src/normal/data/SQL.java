package normal.data;
/**
 * �ֶ�˳��Ҫ�������ʾ˳��һ��
 * 
 * @author Everest
 *
 */
public class SQL {
	//goods
	public static final String GOODS_TABLE = "kfsys_goods_mode";
	public static final String GOODS_TABLE_FILEDS = " goodsId,name,icon,buyEnable,openOnShop,saleEnable,gold,vipScore,asset,maxStack";
	
	
	public static final String GET_ALL_GOODS_ = "SELECT * FROM " + GOODS_TABLE;
	public static final String GET_ALL_GOODS = "SELECT " + GOODS_TABLE_FILEDS + " FROM " + GOODS_TABLE;
	public static final String SEARCHBY_GOODS_ID = "SELECT " + GOODS_TABLE_FILEDS + " FROM " + GOODS_TABLE + " WHERE goodsId LIKE ?";
	public static final String GET_GOODS_BY_ID = "SELECT * FROM " + GOODS_TABLE + " WHERE goodsId = ?";
	public static final String SEARCHBY_GOODS_NAME = "SELECT " + GOODS_TABLE_FILEDS + " FROM " + GOODS_TABLE + " WHERE name LIKE ?";
	public static final String SEARCHBY_GOODS_TYPE = "SELECT " + GOODS_TABLE_FILEDS + " FROM " + GOODS_TABLE + " WHERE goodTypeId = ?";
	
	// task
	public static final String TASK_TABLE = "kfsys_task";
	public static final String TASK_TABLE_FIELDS = " taskId,name,description,type,classId,level,levelMax,color,sourceNpcId,targetNpcId,frontTaskId,acceptConditions,submitConditions,playerExp,playerAsset,playerGoldTicke,prestige,unionAsset,unionGest,unionDedicate,allRewards,oneRewards,acceptText,submitText,unCompleteText,storyOnSubmit,giveItem,timesLimit,fastPay,showInfo ";
	public static final String TASK_TABLE_FIELDS2 = " taskId,name,frontTaskId,level,levelMax,type,classId,sourceNpcId,targetNpcId";
	
	public static final String GET_ALL_TASK = "SELECT " + TASK_TABLE_FIELDS2 + " FROM " + TASK_TABLE;
	public static final String SEARCHBY_TASK_ID = "SELECT " + TASK_TABLE_FIELDS2 + " FROM " + TASK_TABLE + " WHERE taskId LIKE ?";
	public static final String SEARCHBY_TASK_NAME = "SELECT " + TASK_TABLE_FIELDS2 + " FROM " + TASK_TABLE + " WHERE name LIKE ?";
	public static final String GET_BY_TASK_TYPE = "SELECT " + TASK_TABLE_FIELDS2 + " FROM " + TASK_TABLE + " WHERE type = ?";
	public static final String GET_BY_TASK_CLASS = "SELECT " + TASK_TABLE_FIELDS2 + " FROM " + TASK_TABLE + " WHERE classId = ?";
	public static final String GET_TASK_BY_ID = "SELECT * FROM " + TASK_TABLE + " WHERE taskId = ?";

	
	//monster
	public static final String MONSTER_TABLE = "kfsys_monster";
	public static final String MONSTER_TABLE_FILEDS = "id,name,icon,type,level,hp,mp,attack,defense,hit,crit,tough,dodge,attackSpeed,assetR,asset,exp,boss,itemFallMax,itemFallData,talkR,talkData,moveSpeed,initiative,ai";
	public static final String MONSTER_TABLE_FILEDS2 = "id,name,level,icon,hp,type,boss";
	
	public static final String GET_ALL_MONSTER = "SELECT " + MONSTER_TABLE_FILEDS2 + " FROM " + MONSTER_TABLE;
	public static final String SEARCHBY_MONSTER_ID = "SELECT " + MONSTER_TABLE_FILEDS2 + " FROM " + MONSTER_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_MONSTER_NAME = "SELECT " + MONSTER_TABLE_FILEDS2 + " FROM " + MONSTER_TABLE + " WHERE name LIKE ?";
	public static final String GET_MONSTER_BY_ID = "SELECT * FROM " + MONSTER_TABLE + " WHERE id = ?";
	
	//monster deploy
	public static final String MONSTER_DEPLOY_TABLE = "kfsys_monster_deploy";
	public static final String MONSTER_DEPLOY_FIELDS = "id,name,sceneId,x,y,defaultMonsterId,monsterIds,storyOnSee,storyOnDead,storyOnTask,refreshApp";
	public static final String MONSTER_DEPLOY_FIELDS2 = "id,name,sceneId,x,y,defaultMonsterId,monsterIds";
	
	public static final String GET_ALL_MONSTER_DEPLOY = "SELECT " + MONSTER_DEPLOY_FIELDS2 + " FROM " + MONSTER_DEPLOY_TABLE;
	public static final String SEARCHBY_MONSTER_DEPLOY_ID = "SELECT " + MONSTER_DEPLOY_FIELDS2 + " FROM " + MONSTER_DEPLOY_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_MONSTER_DEPLOY_NAME = "SELECT " + MONSTER_DEPLOY_FIELDS2 + " FROM " + MONSTER_DEPLOY_TABLE + " WHERE name LIKE ?";
	public static final String GET_MONSTER_DEPLOY_BY_ID = "SELECT * FROM " + MONSTER_DEPLOY_TABLE + " WHERE id = ?";

	
	//drama
	public static final String DRAMA_TABLE = "kfsys_story";
	public static final String DRAMA_TABLE_FILEDS = "id,name,type,movie,dialogue";
	public static final String DRAMA_TABLE_FILEDS2 = "id,name,type,movie";
	public static final String GET_ALL_DRAMA = "SELECT " + DRAMA_TABLE_FILEDS2 + " FROM " + DRAMA_TABLE;
	public static final String SEARCHBY_DRAMA_ID = "SELECT " + DRAMA_TABLE_FILEDS2 + " FROM " + DRAMA_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_DRAMA_NAME = "SELECT " + DRAMA_TABLE_FILEDS2 + " FROM " + DRAMA_TABLE + " WHERE name LIKE ?";
	public static final String GET_DRAMA_BY_ID = "SELECT * FROM " + DRAMA_TABLE + " WHERE id = ?";
	
	//npc
	public static final String NPC_TABLE = " kfsys_npc ";
	public static final String NPC_TABLE_FIELDS = " id,name,deputyName,icon,sceneId,x,y,scriptIds,isHidden,isClick,extAttribute,forbidenArea,activeArea,basePoint,dialog  ";
	public static final String NPC_TABLE_FIELDS2 = " id,name,sceneId,icon,x,y,scriptIds,isClick";
	public static final String GET_ALL_NPC = "SELECT " + NPC_TABLE_FIELDS2 + " FROM " + NPC_TABLE;
	public static final String SEARCHBY_NPC_ID = "SELECT " + NPC_TABLE_FIELDS2 + " FROM " + NPC_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_NPC_NAME = "SELECT " + NPC_TABLE_FIELDS2 + " FROM " + NPC_TABLE + " WHERE name LIKE ?";
	public static final String GET_NPC_BY_ID = "SELECT * FROM " + NPC_TABLE + " WHERE id = ?";
	
	//scene
	public static final String SCENE_TABLE = " kfsys_scene ";
	public static final String SCENE_TABLE_FIELDS = " id,name,type,rootId,mapFile,icon,scriptOnEnter,scriptOnExit,defaultBirth,upOpen,downOpen,leftOpen,rightOpen,upTeleport,downTeleport,leftTeleport,rightTeleport,openKey,openParam1,openParam2,enterTips,liftTime,upScene,downScene,leftScene,rightScene,music";
	public static final String SCENE_TABLE_FIELDS2 = " id,name,rootId,icon,type,openKey";
	public static final String GET_ALL_SCENE = "SELECT " + SCENE_TABLE_FIELDS2 + " FROM " + SCENE_TABLE;
	public static final String SEARCHBY_SCENE_ID = "SELECT " + SCENE_TABLE_FIELDS2 + " FROM " + SCENE_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_SCENE_NAME = "SELECT " + SCENE_TABLE_FIELDS2 + " FROM " + SCENE_TABLE + " WHERE name LIKE ?";
	public static final String GET_SCENE_BY_ID = "SELECT * FROM " + SCENE_TABLE + " WHERE id = ?";
	
	//pendant
	public static final String PENDANT_TABLE = "kfsys_scene_pendant";
	public static final String PENDANT_TABLE_FIELDS = "id,name,type,sceneId,x,y,randX,randY,icon,taskId,gainId,gainNum,typeRefresh,refreshNum,intervalTime,clickTips,storyOnOpen";
	public static final String PENDANT_TABLE_FIELDS2 = "id,name,sceneId,x,y,icon,taskId,gainId,gainNum,typeRefresh,intervalTime";
	public static final String GET_ALL_PENDANT = "SELECT " + PENDANT_TABLE_FIELDS2 + " FROM " + PENDANT_TABLE;
	public static final String SEARCHBY_PENDANT_ID = "SELECT " + PENDANT_TABLE_FIELDS2 + " FROM " + PENDANT_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_PENDANT_NAME = "SELECT " + PENDANT_TABLE_FIELDS2 + " FROM " + PENDANT_TABLE + " WHERE name LIKE ?";
	public static final String GET_PENDANT_BY_ID = "SELECT * FROM " + PENDANT_TABLE + " WHERE id = ?";

	//undercity
	public static final String UNDERCITY_TABLE = "kfsys_undercity";
	public static final String UNDERCITY_TABLE_FIELDS = "id,name,type,level,levelMax,personType,sortLevel,awardId,icon,times,enterSceneId,exitSceneId,sceneIds,info";
	public static final String UNDERCITY_TABLE_FIELDS2 = "id,name,level,levelMax,type,icon,personType";
	public static final String GET_ALL_UNDERCITY = "SELECT " + UNDERCITY_TABLE_FIELDS2 + " FROM " + UNDERCITY_TABLE;
	public static final String SEARCHBY_UNDERCITY_ID = "SELECT " + UNDERCITY_TABLE_FIELDS2 + " FROM " + UNDERCITY_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_UNDERCITY_NAME = "SELECT " + UNDERCITY_TABLE_FIELDS2 + " FROM " + UNDERCITY_TABLE + " WHERE name LIKE ?";
	public static final String GET_UNDERCITY_BY_ID = "SELECT * FROM " + UNDERCITY_TABLE + " WHERE id = ?";

	//monster ai
	public static final String MONSTER_AI_TABLE = "kfsys_monster_ai";
	public static final String MONSTER_AI_TABLE_FIELDS = "id,name,configValue";
	public static final String MONSTER_AI_TABLE_FIELDS2 = "id,name";
	public static final String GET_ALL_MONSTER_AI = "SELECT " + MONSTER_AI_TABLE_FIELDS2 + " FROM " + MONSTER_AI_TABLE;
	public static final String SEARCHBY_MONSTER_AI_ID = "SELECT " + MONSTER_AI_TABLE_FIELDS2 + " FROM " + MONSTER_AI_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_MONSTER_AI_NAME = "SELECT " + MONSTER_AI_TABLE_FIELDS2 + " FROM " + MONSTER_AI_TABLE + " WHERE name LIKE ?";
	public static final String GET_MONSTER_AI_BY_ID = "SELECT * FROM " + MONSTER_AI_TABLE + " WHERE id = ?";

	//script
	//select id,name,description,windowName,parameters from kfsys_script order by id asc
	public static final String SCRIPT_TABLE = "kfsys_script";
	public static final String SCRIPT_TABLE_FIELDS = "id,name,description,windowName,parameters";
	public static final String SCRIPT_TABLE_FIELDS2 = "id,name,nickName,description,windowName";
	public static final String GET_ALL_SCRIPT = "SELECT " + SCRIPT_TABLE_FIELDS2 + " FROM " + SCRIPT_TABLE;
	public static final String SEARCHBY_SCRIPT_ID = "SELECT " + SCRIPT_TABLE_FIELDS2 + " FROM " + SCRIPT_TABLE + " WHERE id LIKE ?";
	public static final String SEARCHBY_SCRIPT_NAME = "SELECT " + SCRIPT_TABLE_FIELDS2 + " FROM " + SCRIPT_TABLE + " WHERE name LIKE ?";
	public static final String GET_SCRIPT_BY_ID = "SELECT * FROM " + SCRIPT_TABLE + " WHERE id = ?";

	//skill
	public static final String SKILL_TABLE = "kfsys_skill";
	public static final String SKILL_TABLE_FIELDS = "id,vocationId,name,deputyName,skillLibId,type,icon,level,playerLevel,frontSkillId,studyBookId,asset,gold,stoneNum,costTime,description,groupType,groupNum,onlyBattleUse,targetType,passiveType,passiveValue,passiveAdd,effectValue,coolingTime,constMp,singingTime,buffRate,buffId,actionIds,playTime";
	public static final String SKILL_TABLE_FIELDS2 = "id,name,skillLibId,level,playerLevel,frontSkillId,asset,effectValue";
	public static final String GET_ALL_SKILL = "SELECT " + SKILL_TABLE_FIELDS2 + " FROM " + SKILL_TABLE + " ORDER BY id ASC";
	public static final String SEARCHBY_SKILL_ID = "SELECT " + SKILL_TABLE_FIELDS2 + " FROM " + SKILL_TABLE + " WHERE id LIKE ? ORDER BY id ASC";
	public static final String SEARCHBY_SKILL_NAME = "SELECT " + SKILL_TABLE_FIELDS2 + " FROM " + SKILL_TABLE + " WHERE name LIKE ? ORDER BY id ASC";
	public static final String GET_BY_SKILL_VOCATIONID = "SELECT " + SKILL_TABLE_FIELDS2 + " FROM " + SKILL_TABLE + " WHERE vocationId = ? ORDER BY id ASC";
	public static final String GET_SKILL_BY_ID = "SELECT * FROM " + SKILL_TABLE + " WHERE id = ?";

	//buff
	public static final String BUFF_TABLE = "kfsys_buff";
	public static final String BUFF_TABLE_FIELDS = "id,libId,signId,typeGain,name,level,icon,description,rate,lifeTime,effectValue,intervalTime,valueA,valueB";
	public static final String BUFF_TABLE_FIELDS2 = "id,libId,name,description,icon";
	public static final String GET_ALL_BUFFL = "SELECT " + BUFF_TABLE_FIELDS2 + " FROM " + BUFF_TABLE + " ORDER BY id ASC";
	public static final String SEARCHBY_BUFF_ID = "SELECT " + BUFF_TABLE_FIELDS2 + " FROM " + BUFF_TABLE + " WHERE id LIKE ? ORDER BY id ASC";
	public static final String SEARCHBY_BUFF_NAME = "SELECT " + BUFF_TABLE_FIELDS2 + " FROM " + BUFF_TABLE + " WHERE name LIKE ? ORDER BY id ASC";
	public static final String GET_BUFF_BY_ID = "SELECT * FROM " + BUFF_TABLE + " WHERE id = ?";

}
