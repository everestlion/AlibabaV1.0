package normal.data;

import java.util.HashMap;

import normal.object.GoodsMode;

public class DataCenter {
	
	private static final HashMap<Integer, GoodsMode> goodsModes = new HashMap<Integer, GoodsMode>();
	
	public static GoodsMode getGoodsModeById(int id) {
		return goodsModes.get(id);
	}
	
	public static void addGoodsMode(GoodsMode goodsMode) {
		goodsModes.put(goodsMode.getGoodsId(), goodsMode);
	}

}
