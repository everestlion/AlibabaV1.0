package normal.data;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import normal.config.Ignore;
import normal.object.GoodsMode;
import normal.object.Monster;
import normal.object.MonsterDeploy;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;

import db.JDBC;

public class DataDao {
	
	public static List<Object> searchGoodsById(int goodsId) {
		List<Object> list = new ArrayList<Object>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.SEARCHBY_GOODS_ID);
			if (goodsId <= 0) {
				ps.setString(1, "%");
			} else {
				ps.setString(1, goodsId + "%");
			}
			
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createObject(GoodsMode.class, rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static GoodsMode getGoodsById(int goodsId) {
		GoodsMode goodsMode = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.GET_GOODS_BY_ID);
			ps.setInt(1, goodsId);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				goodsMode = (GoodsMode) createObject(GoodsMode.class, rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return goodsMode;
	}
	
	
	public static List<Object> searchGoodsByName(String name) {
		List<Object> list = new ArrayList<Object>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.SEARCHBY_GOODS_NAME);
			ps.setString(1, name);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createObject(GoodsMode.class, rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<Object> getAllGoods() {
		List<Object> list = new ArrayList<Object>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.GET_ALL_GOODS_);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createObject(GoodsMode.class, rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<Object> getGoodsByType(int type) {
		List<Object> list = new ArrayList<Object>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.SEARCHBY_GOODS_TYPE);
			ps.setInt(1, type);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createObject(GoodsMode.class, rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<Monster> searchMonsterById(int id) {
		List<Monster> list = new ArrayList<Monster>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.SEARCHBY_MONSTER_ID);
			if (id <= 0) {
				ps.setString(1, "%");
			} else {
				ps.setString(1, id + "%");
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createMonster(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<Monster> searchMonsterByName(String name) {
		List<Monster> list = new ArrayList<Monster>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.SEARCHBY_MONSTER_NAME);
			ps.setString(1, name);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createMonster(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<Monster> getAllMonster() {
		List<Monster> list = new ArrayList<Monster>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.GET_ALL_MONSTER);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createMonster(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<Object[]> getAllMonsterDeploys() {
		List<Object[]> list = new ArrayList<Object[]>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.GET_ALL_MONSTER_DEPLOY);
			rs = ps.executeQuery();
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();  
			//得到当前的列数
			int colCount = rsmd.getColumnCount();
			while (rs.next()) {
				Object[] values = new Object[colCount];
				for (int i = 1; i <= colCount; i++) {
					values[i - 1] = rs.getObject(i);
				}
				list.add(values);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<Object[]> searchMonsterDeployById(int id) {
		List<Object[]> list = new ArrayList<Object[]>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.SEARCHBY_MONSTER_DEPLOY_ID);
			if (id <= 0) {
				ps.setString(1, "%");
			} else {
				ps.setString(1, id + "%");
			}
			rs = ps.executeQuery();
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();  
			//得到当前的列数
			int colCount = rsmd.getColumnCount();
			while (rs.next()) {
				Object[] values = new Object[colCount];
				for (int i = 1; i <= colCount; i++) {
					values[i - 1] = rs.getObject(i);
				}
				list.add(values);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static List<MonsterDeploy> searchMonsterDeployByName(String name) {
		List<MonsterDeploy> list = new ArrayList<MonsterDeploy>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(SQL.SEARCHBY_MONSTER_DEPLOY_NAME);
			ps.setString(1, name);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(createMonsterDeploy(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	private static MonsterDeploy createMonsterDeploy(ResultSet rs) {
		MonsterDeploy monsterDeploy = new MonsterDeploy();
		try {
			monsterDeploy.setId(rs.getInt("id"));
			monsterDeploy.setName(rs.getString("name"));
			monsterDeploy.setSceneId(rs.getInt("sceneId"));
			monsterDeploy.setX(rs.getInt("x"));
			monsterDeploy.setY(rs.getInt("y"));
			monsterDeploy.setDefaultMonsterId(rs.getInt("defaultMonsterId"));
			monsterDeploy.setMonsterIds(rs.getString("monsterIds"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return monsterDeploy;
	}
	
	private static Monster createMonster(ResultSet rs) {
		Monster monster = new Monster();
		try {
			monster.setId(rs.getInt("id"));
			monster.setName(rs.getString("name"));
			monster.setIcon(rs.getString("icon"));
			monster.setLevel(rs.getInt("level"));
			monster.setHp(rs.getInt("hp"));
			monster.setType(rs.getInt("type"));
			monster.setBoss(rs.getInt("boss"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return monster;
	}

	private static Object createObject(Class<?> clazz, ResultSet rs) {
		Object one = null;
		try {
			one = clazz.newInstance();
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				if (f.isAnnotationPresent(Ignore.class)) {
					continue;
				}
				f.setAccessible(true);
				if (f.getType().isAssignableFrom(int.class) || f.getType().isAssignableFrom(Integer.class)) {
					f.set(one, rs.getInt(f.getName()));
				} else if (f.getType().isAssignableFrom(String.class)) {
					f.set(one, rs.getString(f.getName()));
				} else if (f.getType().isAssignableFrom(boolean.class) || f.getType().isAssignableFrom(Boolean.class)) {
					f.set(one, rs.getBoolean(f.getName()));
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return one;
	}
	
	public static List<Object[]> commGetList(String sql, Object... params) {
		List<Object[]> list = new ArrayList<Object[]>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(sql);
			for (int i = 1; i <= params.length; i++) {
				if (params[i - 1] instanceof String) {
					ps.setString(i, params[i - 1] + "");
				} else if (params[i - 1] instanceof Integer) {
					ps.setInt(i, (Integer)params[i - 1]);
				}
			}
			rs = ps.executeQuery();
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData(); 
			//得到当前的列数
			int colCount = rsmd.getColumnCount();
			while (rs.next()) {
				Object[] values = new Object[colCount];
				for (int i = 1; i <= colCount; i++) {
					values[i - 1] = rs.getObject(i);
				}
				list.add(values);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return list;
	}
	
	public static Object commGetOne(Class<?> clazz, String sql, Object... params) {
		Object obj = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = JDBC.prepareStatement(sql);
			for (int i = 1; i <= params.length; i++) {
				if (params[i - 1] instanceof String) {
					ps.setString(i, params[i - 1] + "");
				} else if (params[i - 1] instanceof Integer) {
					ps.setInt(i, (Integer)params[i - 1]);
				}
			}
			
			rs = ps.executeQuery();
			if (rs.next()) {
				obj = createObject(clazz, rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
					ps = null;
				}
				if (rs != null) {
					rs.close();
					rs = null;
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return obj;
	}
	
}
