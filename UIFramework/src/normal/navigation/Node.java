package normal.navigation;

import javax.swing.ImageIcon;

public class Node {
	
	private String text = null;
	private int id = 0;
	private int parentId = 0;
	private ImageIcon imageIcon = null;
	
	public Node(int id, int parentId, String text, ImageIcon imageIcon) {
		this.id = id;
		this.parentId = parentId;
		this.text = text;
		this.imageIcon = imageIcon;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public ImageIcon getImageIcon() {
		return imageIcon;
	}

	public void setImageIcon(ImageIcon imageIcon) {
		this.imageIcon = imageIcon;
	}
}
