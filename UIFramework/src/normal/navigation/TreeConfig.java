package normal.navigation;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.TreeMap;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import normal.ui.NavigationTree;
import util.ImageUtil;

public class TreeConfig {
	public static final TreeMap<Integer, DefaultMutableTreeNode> treeNodes = new TreeMap<Integer, DefaultMutableTreeNode>();
	public static final HashMap<Integer, Integer> goodsTypeMap = new HashMap<Integer, Integer>();
	public static final HashMap<Integer, Integer> taskTypeMap = new HashMap<Integer, Integer>();
	public static final HashMap<Integer, Integer> skillTypeMap = new HashMap<Integer, Integer>();
	
	public static final int ID_ROOT = 0;
	public static final int ID_GOODS = 1;
	public static final int ID_DRUG = 101;
	public static final int ID_BOOK = 102;
	public static final int ID_OTHER = 103;
	public static final int ID_EQUIP_FORGE = 104;
	public static final int ID_DISCIPLE_GOODS = 105;
	public static final int ID_DISCIPLE_BOOK = 106;
	public static final int ID_BAOJIAN_GOODS = 107;
	public static final int ID_VIP_SCORE = 108;
	public static final int ID_SYSTEMGIVE = 109;
	public static final int ID_TASK_GOODS = 110;
	public static final int ID_HELMET = 111;
	public static final int ID_UPERBODY = 112;
	public static final int ID_PENDENT_GOODS = 113;
	public static final int ID_SHOES = 114;
	public static final int ID_ARMS = 115;
	public static final int ID_WRIST = 116;
	public static final int ID_NEACKLACE = 117;
	public static final int ID_BELT = 118;
	public static final int ID_RING = 119;
	public static final int ID_RING_MARRY = 120;
	
	//任务管理
	public static final int ID_TASK = 2;
	public static final int ID_TASK_MAIN = 201;
	public static final int ID_TASK_BRANCH = 202;
	public static final int ID_TASK_PRESTIGE = 203;
	public static final int ID_TASK_CYCLE = 204;
	public static final int ID_TASK_DAILLY = 205;
	public static final int ID_TASK_DART = 206;
	public static final int ID_TASK_AIM = 207;
	public static final int ID_TASK_UNDERCITY = 208;
	public static final int ID_TASK_DRAMA = 209;
	public static final int ID_TASK_SECRET = 210;
	public static final int ID_TASK_CONVASATION = 211;
	public static final int ID_TASK_KILLMONSTER = 212;
	public static final int ID_TASK_COLLECTION = 213;
	public static final int ID_TASK_GATHER = 214;
	public static final int ID_TASK_TRIGER = 215;
	
	public static final int ID_MONSTER = 3;
	public static final int ID_MONSTER_DEPLOY = 4;
	
	//剧情管理
	public static final int ID_DRAMA = 5;
	//npc管理
	public static final int ID_NPC = 6;
	//场景管理
	public static final int ID_SCENE = 7;
	//挂件管理
	public static final int ID_PENDANT = 8;
	//副本管理
	public static final int ID_UNDERCITY = 9;
	//怪物AI管理
	public static final int ID_MONSTER_AI= 10;
	//脚本管理
	public static final int ID_SCRIPT = 11;
	//技能管理
	public static final int ID_SKILL = 12;
	public static final int ID_SKILL_WUZHE = 1201;
	public static final int ID_SKILL_QIZONG = 1202;
	public static final int ID_SKILL_YAOSHI = 1203;
	public static final int ID_SKILL_MONSTER = 1204;
	public static final int ID_SKILL_PET = 1205;
	public static final int ID_SKILL_BIRD = 1206;
	
	//BUFF
	public static final int ID_BUFF = 13;
	
	
	/**
	 * 上身
	 */
	/**
	 * <option value="101">角色药品</option>
	<option value="102">技能书</option>
<option value="103">杂货</option>
<option value="104">装备打造</option>
<option value="105">徒弟物品</option>
<option value="106">徒弟技能书</option>
<option value="107">宝鉴材料</option>
<option value="168">积分商城</option>
<option value="188">系统赠送</option>
<option value="199" selected>任务品</option>
<option value="201">头盔</option>
<option value="202">上身</option>
<option value="203">挂件</option>
<option value="204">鞋子</option>
<option value="205">武器</option>
<option value="206">护腕</option>
<option value="207">项链</option>
<option value="208">腰带</option>
<option value="209">戒指</option>
<option value="211">婚戒</option>
	 */
	static {
		treeNodes.put(ID_ROOT, new DefaultMutableTreeNode(new Node(ID_ROOT, -1, "列表", ImageUtil.TASK_NODE_IMAGE)));
		//道具
		treeNodes.put(ID_GOODS, new DefaultMutableTreeNode(new Node(ID_GOODS, ID_ROOT, "道具", null)));
		treeNodes.put(ID_DRUG, new DefaultMutableTreeNode(new Node(ID_DRUG, ID_GOODS, "角色药品", null)));
		treeNodes.put(ID_BOOK, new DefaultMutableTreeNode(new Node(ID_BOOK, ID_GOODS, "技能书", null)));
		treeNodes.put(ID_OTHER, new DefaultMutableTreeNode(new Node(ID_OTHER, ID_GOODS, "杂货", null)));
		treeNodes.put(ID_EQUIP_FORGE, new DefaultMutableTreeNode(new Node(ID_EQUIP_FORGE, ID_GOODS, "装备打造", null)));
		treeNodes.put(ID_DISCIPLE_GOODS, new DefaultMutableTreeNode(new Node(ID_DISCIPLE_GOODS, ID_GOODS, "徒弟物品", null)));
		treeNodes.put(ID_DISCIPLE_BOOK, new DefaultMutableTreeNode(new Node(ID_DISCIPLE_BOOK, ID_GOODS, "徒弟技能书", null)));
		treeNodes.put(ID_BAOJIAN_GOODS, new DefaultMutableTreeNode(new Node(ID_BAOJIAN_GOODS, ID_GOODS, "宝鉴材料", null)));
		treeNodes.put(ID_VIP_SCORE, new DefaultMutableTreeNode(new Node(ID_VIP_SCORE, ID_GOODS, "积分商城", null)));
		treeNodes.put(ID_SYSTEMGIVE, new DefaultMutableTreeNode(new Node(ID_SYSTEMGIVE, ID_GOODS, "系统赠送", null)));
		treeNodes.put(ID_TASK_GOODS, new DefaultMutableTreeNode(new Node(ID_TASK_GOODS, ID_GOODS, "任务品", null)));
		treeNodes.put(ID_HELMET, new DefaultMutableTreeNode(new Node(ID_HELMET, ID_GOODS, "头盔", null)));
		treeNodes.put(ID_UPERBODY, new DefaultMutableTreeNode(new Node(ID_UPERBODY, ID_GOODS, "上身", null)));
		treeNodes.put(ID_PENDENT_GOODS, new DefaultMutableTreeNode(new Node(ID_PENDENT_GOODS, ID_GOODS, "挂件", null)));
		treeNodes.put(ID_SHOES, new DefaultMutableTreeNode(new Node(ID_SHOES, ID_GOODS, "鞋子", null)));
		treeNodes.put(ID_ARMS, new DefaultMutableTreeNode(new Node(ID_ARMS, ID_GOODS, "武器", null)));
		treeNodes.put(ID_WRIST, new DefaultMutableTreeNode(new Node(ID_WRIST, ID_GOODS, "护腕", null)));
		treeNodes.put(ID_NEACKLACE, new DefaultMutableTreeNode(new Node(ID_NEACKLACE, ID_GOODS, "项链", null)));
		treeNodes.put(ID_BELT, new DefaultMutableTreeNode(new Node(ID_BELT, ID_GOODS, "腰带", null)));
		treeNodes.put(ID_RING, new DefaultMutableTreeNode(new Node(ID_RING, ID_GOODS, "戒指", null)));
		treeNodes.put(ID_RING_MARRY, new DefaultMutableTreeNode(new Node(ID_RING_MARRY, ID_GOODS, "婚戒", null)));
		
		//怪物管理
		treeNodes.put(ID_MONSTER, new DefaultMutableTreeNode(new Node(ID_MONSTER, ID_ROOT, "怪物", null)));
		treeNodes.put(ID_MONSTER_DEPLOY, new DefaultMutableTreeNode(new Node(ID_MONSTER_DEPLOY, ID_ROOT, "怪物部署", null)));
		
		//任务管理
		treeNodes.put(ID_TASK, new DefaultMutableTreeNode(new Node(ID_TASK, ID_ROOT, "任务", null)));
		treeNodes.put(ID_TASK_MAIN, new DefaultMutableTreeNode(new Node(ID_TASK_MAIN, ID_TASK, "主线", null)));
		treeNodes.put(ID_TASK_BRANCH, new DefaultMutableTreeNode(new Node(ID_TASK_BRANCH, ID_TASK, "支线", null)));
		treeNodes.put(ID_TASK_PRESTIGE, new DefaultMutableTreeNode(new Node(ID_TASK_PRESTIGE, ID_TASK, "声望", null)));
		treeNodes.put(ID_TASK_CYCLE, new DefaultMutableTreeNode(new Node(ID_TASK_CYCLE, ID_TASK, "跑环", null)));
		treeNodes.put(ID_TASK_DAILLY, new DefaultMutableTreeNode(new Node(ID_TASK_DAILLY, ID_TASK, "日常", null)));
		treeNodes.put(ID_TASK_DART, new DefaultMutableTreeNode(new Node(ID_TASK_DART, ID_TASK, "押镖", null)));
		treeNodes.put(ID_TASK_AIM, new DefaultMutableTreeNode(new Node(ID_TASK_AIM, ID_TASK, "目标", null)));
		treeNodes.put(ID_TASK_UNDERCITY, new DefaultMutableTreeNode(new Node(ID_TASK_UNDERCITY, ID_TASK, "副本", null)));
		treeNodes.put(ID_TASK_DRAMA, new DefaultMutableTreeNode(new Node(ID_TASK_DRAMA, ID_TASK, "剧情", null)));
		treeNodes.put(ID_TASK_SECRET, new DefaultMutableTreeNode(new Node(ID_TASK_SECRET, ID_TASK, "秘典", null)));
		
		treeNodes.put(ID_TASK_CONVASATION, new DefaultMutableTreeNode(new Node(ID_TASK_CONVASATION, ID_TASK, "对话", null)));
		treeNodes.put(ID_TASK_KILLMONSTER, new DefaultMutableTreeNode(new Node(ID_TASK_KILLMONSTER, ID_TASK, "打怪", null)));
		treeNodes.put(ID_TASK_COLLECTION, new DefaultMutableTreeNode(new Node(ID_TASK_COLLECTION, ID_TASK, "收集", null)));
		treeNodes.put(ID_TASK_GATHER, new DefaultMutableTreeNode(new Node(ID_TASK_GATHER, ID_TASK, "采集", null)));
		treeNodes.put(ID_TASK_TRIGER, new DefaultMutableTreeNode(new Node(ID_TASK_TRIGER, ID_TASK, "触发", null)));
		
		treeNodes.put(ID_DRAMA, new DefaultMutableTreeNode(new Node(ID_DRAMA, ID_ROOT, "剧情", null)));
		treeNodes.put(ID_NPC, new DefaultMutableTreeNode(new Node(ID_NPC, ID_ROOT, "Npc", null)));
		treeNodes.put(ID_SCENE, new DefaultMutableTreeNode(new Node(ID_SCENE, ID_ROOT, "场景", null)));
		treeNodes.put(ID_PENDANT, new DefaultMutableTreeNode(new Node(ID_PENDANT, ID_ROOT, "挂件", null)));
		treeNodes.put(ID_UNDERCITY, new DefaultMutableTreeNode(new Node(ID_UNDERCITY, ID_ROOT, "副本", null)));
		treeNodes.put(ID_MONSTER_AI, new DefaultMutableTreeNode(new Node(ID_MONSTER_AI, ID_ROOT, "怪物AI", null)));
		treeNodes.put(ID_SCRIPT, new DefaultMutableTreeNode(new Node(ID_SCRIPT, ID_ROOT, "脚本", null)));
		
		treeNodes.put(ID_SKILL, new DefaultMutableTreeNode(new Node(ID_SKILL, ID_ROOT, "技能", null)));
		treeNodes.put(ID_SKILL_WUZHE, new DefaultMutableTreeNode(new Node(ID_SKILL_WUZHE, ID_SKILL, "武者", null)));
		treeNodes.put(ID_SKILL_QIZONG, new DefaultMutableTreeNode(new Node(ID_SKILL_QIZONG, ID_SKILL, "气宗", null)));
		treeNodes.put(ID_SKILL_YAOSHI, new DefaultMutableTreeNode(new Node(ID_SKILL_YAOSHI, ID_SKILL, "药师", null)));
		treeNodes.put(ID_SKILL_MONSTER, new DefaultMutableTreeNode(new Node(ID_SKILL_MONSTER, ID_SKILL, "怪物", null)));
		treeNodes.put(ID_SKILL_PET, new DefaultMutableTreeNode(new Node(ID_SKILL_PET, ID_SKILL, "宠物", null)));
		treeNodes.put(ID_SKILL_BIRD, new DefaultMutableTreeNode(new Node(ID_SKILL_BIRD, ID_SKILL, "灵兽", null)));
		
		treeNodes.put(ID_BUFF, new DefaultMutableTreeNode(new Node(ID_BUFF, ID_ROOT, "BUFF", null)));
				
		goodsTypeMap.put(ID_DRUG, 101);
		goodsTypeMap.put(ID_BOOK, 102);
		goodsTypeMap.put(ID_OTHER, 103);
		goodsTypeMap.put(ID_EQUIP_FORGE, 104);
		goodsTypeMap.put(ID_DISCIPLE_GOODS, 105);
		goodsTypeMap.put(ID_DISCIPLE_BOOK, 106);
		goodsTypeMap.put(ID_BAOJIAN_GOODS, 107);
		goodsTypeMap.put(ID_VIP_SCORE, 168);
		goodsTypeMap.put(ID_SYSTEMGIVE, 188);
		goodsTypeMap.put(ID_TASK_GOODS, 199);
		goodsTypeMap.put(ID_HELMET, 201);
		goodsTypeMap.put(ID_UPERBODY, 202);
		goodsTypeMap.put(ID_PENDENT_GOODS, 203);
		goodsTypeMap.put(ID_SHOES, 204);
		goodsTypeMap.put(ID_ARMS, 205);
		goodsTypeMap.put(ID_WRIST, 206);
		goodsTypeMap.put(ID_NEACKLACE, 207);
		goodsTypeMap.put(ID_BELT, 208);
		goodsTypeMap.put(ID_RING, 209);
		goodsTypeMap.put(ID_RING_MARRY, 211);
		//type
		taskTypeMap.put(ID_TASK_MAIN, 1);
		taskTypeMap.put(ID_TASK_BRANCH, 2);
		taskTypeMap.put(ID_TASK_PRESTIGE, 4);
		taskTypeMap.put(ID_TASK_CYCLE, 8);
		taskTypeMap.put(ID_TASK_DAILLY, 16);
		taskTypeMap.put(ID_TASK_DART, 32);
		taskTypeMap.put(ID_TASK_AIM, 64);
		taskTypeMap.put(ID_TASK_UNDERCITY, 128);
		taskTypeMap.put(ID_TASK_DRAMA, 256);
		taskTypeMap.put(ID_TASK_SECRET, 512);
		//classId
		taskTypeMap.put(ID_TASK_CONVASATION, 1);
		taskTypeMap.put(ID_TASK_KILLMONSTER, 2);
		taskTypeMap.put(ID_TASK_COLLECTION, 3);
		taskTypeMap.put(ID_TASK_GATHER, 6);
		taskTypeMap.put(ID_TASK_TRIGER, 7);
		
		skillTypeMap.put(ID_SKILL_WUZHE, 1);
		skillTypeMap.put(ID_SKILL_QIZONG, 2);
		skillTypeMap.put(ID_SKILL_YAOSHI, 3);
		skillTypeMap.put(ID_SKILL_MONSTER, 32);
		skillTypeMap.put(ID_SKILL_PET, 64);
		skillTypeMap.put(ID_SKILL_BIRD, 128);

	}
	
	public static NavigationTree createTree() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode();
		for (DefaultMutableTreeNode e : treeNodes.values()) {
			Node node = (Node) e.getUserObject();
			if (node.getParentId() == -1) {
				root.add(e);
			} else {
				DefaultMutableTreeNode parentNode = treeNodes.get(node.getParentId());
				if (parentNode == null) {
					parentNode = treeNodes.get(0);
					if (parentNode == null) {
						parentNode = root;
					}
				}
				parentNode.add(e);
			}
		}
		NavigationTree tree = new NavigationTree(root);
		//ecTree(tree);//展开所有节点
		return tree;
	}
	
	public static void ecTree(JTree tree) {
	    TreeNode root = (TreeNode) tree.getModel().getRoot();
	    expandTree(tree, new TreePath(root));
	}
	 
	private static void expandTree(JTree tree, TreePath parent) {
	    TreeNode node = (TreeNode) parent.getLastPathComponent();
	    if (node.getChildCount() >= 0) {
	       for (Enumeration<?> e = node.children(); e.hasMoreElements();) {
	           TreeNode n = (TreeNode) e.nextElement();
	           TreePath path = parent.pathByAddingChild(n);
	           expandTree(tree, path);
	       }
	    }
	    tree.expandPath(parent);
	}
	

}
