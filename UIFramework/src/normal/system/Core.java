package normal.system;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Core {
	
	private static final ScheduledExecutorService scheduExec;

	static {
		scheduExec = new ScheduledThreadPoolExecutor(1);
	}
	
	public static void runBoss () {
		scheduExec.scheduleAtFixedRate(new Runnable() {
			
			@Override
			public void run() {
//				System.gc();
			}
		}, 30, 30, TimeUnit.SECONDS);
	}

}
