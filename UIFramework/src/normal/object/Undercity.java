package normal.object;

import normal.config.Ignore;
import util.StringProcesser;

/**
 * 系�????对象
 * 
 * @author fantadust
 * 
 */
public class Undercity {
	/**
	 * ???id
	 */
	private int id;
	/**
	 * ?????��
	 */
	private String name;
	/**
	 * ???类�?
	 */
	private int type;
	/**
	 * �?���??
	 */
	private int level;
	/**
	 * �?????大�?�?
	 */
	private int levelMax;
	/**
	 * �??次�?
	 */
	private int times;
	/**
	 * �?????人�?�??
	 */
	private int personType;
	/**
	 * ????��?
	 */
	private String icon;
	private int enterSceneId, exitSceneId;
	/**
	 * ????��?id??��
	 */
	private String sceneIds;
	/**
	 * ?????��
	 */
	private String info;

	/**
	 * ???�?��
	 */
	private int sortLevel;

	/**
	 * ???�??
	 */
	private int awardId;

	private int[] sceneIdsArr;
	/**
	 * ??��
	 */
	@Ignore
	public static final int typeSingle = 1;
	/**
	 * �?��
	 */
	@Ignore
	public static final int typeTeam = 2;
	/**
	 * ??��??
	 */
	@Ignore
	public static final int typePicCheckpoint = 3;
	/**
	 * �????
	 */
	@Ignore
	public static final int typeSoldierCheckpoint = 4;
	/**
	 * �??�??
	 */
	@Ignore
	public static final int typeFamAne = 5;
	/**
	 * �?????
	 */
	@Ignore
	public static final int typeMarry = 6;
	/**
	 * �?��???
	 */
	@Ignore
	public static final int typeUnion = 16;
	/**
	 * ?��????
	 */
	@Ignore
	public static final int typeDrama = 32;
	/**
	 * 活�????
	 */
	@Ignore
	public static final int typeSpalc = 64;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getSceneIds() {
		return sceneIds;
	}

	public void setSceneIds(String sceneIds) {
		this.sceneIds = sceneIds;
		sceneIdsArr = StringProcesser.toIntArray(sceneIds);
	}

	public int getEnterSceneId() {
		return enterSceneId;
	}

	public void setEnterSceneId(int enterSceneId) {
		this.enterSceneId = enterSceneId;
	}

	public int getExitSceneId() {
		return exitSceneId;
	}

	public void setExitSceneId(int exitSceneId) {
		this.exitSceneId = exitSceneId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int[] getSceneIdsArr() {
		return sceneIdsArr;
	}

	public int getSortLevel() {
		return sortLevel;
	}

	public void setSortLevel(int sortLevel) {
		this.sortLevel = sortLevel;
	}

	public int getAwardId() {
		return awardId;
	}

	public void setAwardId(int awardId) {
		this.awardId = awardId;
	}

	public int getLevelMax() {
		return levelMax;
	}

	public void setLevelMax(int levelMax) {
		this.levelMax = levelMax;
	}

	public int getPersonType() {
		return personType;
	}

	public void setPersonType(int personType) {
		this.personType = personType;
	}

	public boolean isOpenAiBattle(){
		return type == typeSingle || type == typeUnion || type == typeSoldierCheckpoint;
	}
}
