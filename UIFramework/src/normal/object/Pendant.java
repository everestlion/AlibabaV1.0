package normal.object;

import normal.config.Ignore;

/**
 * ?��????件�?�?
 * 
 * @author fantadust [9677889@qq.com]
 * @date 2011-8-15 �??6:44:48
 */
public class Pendant {
	/**
	 * �??
	 */
	private int id;
	/**
	 * ??��
	 */
	private String name;
	/**
	 * 类�? 0???  1 ?��?
	 */
	private int type;
	/**
	 * ????��?
	 */
	private int sceneId;
	private int x;
	private int y;
	private int randX;
	private int randY;
	private String icon;
	/**
	 * ???任�?�?��
	 */
	private int taskId;
	/**
	 * ????��?id
	 */
	private int gainId;
	/**
	 * ????��?
	 */
	private int gainNum;
	/**
	 * ?��??��?
	 */
	private int typeRefresh;
	/**
	 * ?��??��?
	 */
	private int refreshNum;
	/**
	 * ?��?
	 */
	private int intervalTime;
	/**
	 * ?��???��
	 */
	private String clickTips;
	/**
	 * ????��?�?????
	 */
	private int storyOnOpen;	
	/**
	 * �??????��?
	 */
	@Ignore
	private int lastTime;

	/**
	 * �??�??
	 */
	@Ignore
	public static final int refreshTypeForever = 0;
	/**
	 * �???��?????��?
	 */
	@Ignore
	public static final int refreshTypePersonal = 1;
	/**
	 * ?�享???.系�??��?
	 */
	@Ignore
	public static final int refreshTypeShareD = 2;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSceneId() {
		return sceneId;
	}

	public void setSceneId(int sceneId) {
		this.sceneId = sceneId;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public int getGainId() {
		return gainId;
	}

	public void setGainId(int gainId) {
		this.gainId = gainId;
	}

	public int getGainNum() {
		return gainNum;
	}

	public void setGainNum(int gainNum) {
		this.gainNum = gainNum;
	}

	public int getTypeRefresh() {
		return typeRefresh;
	}

	public void setTypeRefresh(int typeRefresh) {
		this.typeRefresh = typeRefresh;
	}

	public int getIntervalTime() {
		return intervalTime;
	}

	public void setIntervalTime(int intervalTime) {
		this.intervalTime = intervalTime;
	}

	public int getLastTime() {
		return lastTime;
	}

	public void setLastTime(int lastTime) {
		this.lastTime = lastTime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getRandX() {
		return randX;
	}

	public void setRandX(int randX) {
		this.randX = randX;
	}

	public int getRandY() {
		return randY;
	}

	public void setRandY(int randY) {
		this.randY = randY;
	}

	public int getRefreshNum() {
		return refreshNum;
	}

	public void setRefreshNum(int refreshNum) {
		this.refreshNum = refreshNum;
	}

	public String getClickTips() {
		return clickTips;
	}

	public void setClickTips(String clickTips) {
		this.clickTips = clickTips;
	}

	public int getStoryOnOpen() {
		return storyOnOpen;
	}

	public void setStoryOnOpen(int storyOnOpen) {
		this.storyOnOpen = storyOnOpen;
	}

}
