package normal.object;


public class MonsterDeploy {
	public int id;// 系统id
	public String name;// 别名
	public int sceneId;// 场景id
	public int x;// x
	public int y;// y
	public int defaultMonsterId;// 默认怪物
	public String monsterIds;// 怪物列表
	/**
	 * 遇见怪物触发剧情脚本
	 */
	public int storyOnSee;
	/**
	 * 杀死怪物触发剧情脚本
	 */
	public int storyOnDead;
	/**
	 * 剧情激活需要任务
	 */
	public int storyOnTask;
	/**
	 * 刷新脚本
	 */
	public int refreshApp;

//	public MonsterVO defaultMonster;// 默认怪物vo
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSceneId() {
		return sceneId;
	}
	public void setSceneId(int sceneId) {
		this.sceneId = sceneId;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getDefaultMonsterId() {
		return defaultMonsterId;
	}
	public void setDefaultMonsterId(int defaultMonsterId) {
		this.defaultMonsterId = defaultMonsterId;
	}
	public String getMonsterIds() {
		return monsterIds;
	}
	public int getStoryOnSee() {
		return storyOnSee;
	}
	public void setStoryOnSee(int storyOnSee) {
		this.storyOnSee = storyOnSee;
	}
	public int getStoryOnDead() {
		return storyOnDead;
	}
	public void setStoryOnDead(int storyOnDead) {
		this.storyOnDead = storyOnDead;
	}
	public int getStoryOnTask() {
		return storyOnTask;
	}
	public void setStoryOnTask(int storyOnTask) {
		this.storyOnTask = storyOnTask;
	}
	public int getRefreshApp() {
		return refreshApp;
	}
	public void setRefreshApp(int refreshApp) {
		this.refreshApp = refreshApp;
	}
	public void setMonsterIds(String monsterIds) {
		this.monsterIds = monsterIds;
	}

}
