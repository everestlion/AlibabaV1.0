package normal.object;



/**
 * 系统npc实体类
 * @author fantadust
 * @date 2011-4-29 下午04:57:44
 */
public class Npc {
	/**
	 * npc id
	 */
	private int id;
	/**
	 * npc名称
	 */
	private String name;
	/**
	 * 附属名称
	 */
	private String deputyName;
	/**
	 * npc图标
	 */
	private String icon;
	
	private int sceneId;

	/**
	 * npc位置
	 */
	private int x;
	private int y;
	
	private String scriptIds;//功能脚本id列表
	/**
	 * 是否隐藏
	 */
	private boolean isHidden;
	/**
	 * 是否可点击
	 */
	private boolean isClick;
	/**
	 * 额外属性
	 */
	private String extAttribute;
	
	/**
	 * 不可行走区域
	 */
	private String forbidenArea;
	/**
	 * 可行走区域
	 */
	private String activeArea;
	
	/**
	 * 偏移量
	 */
	private String basePoint;
	/**
	 * npc默认说话
	 */
	private String dialog;
	
	//private Map<Integer, ScriptVO> scripts = new HashMap<Integer, ScriptVO>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getSceneId() {
		return sceneId;
	}
	public void setSceneId(int sceneId) {
		this.sceneId = sceneId;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getScriptIds() {
		return scriptIds;
	}
	public void setScriptIds(String scriptIds) {
		this.scriptIds = scriptIds;
	}
	public boolean isHidden() {
		return isHidden;
	}
	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}
	public boolean isClick() {
		return isClick;
	}
	public void setClick(boolean isClick) {
		this.isClick = isClick;
	}
	public String getExtAttribute() {
		return extAttribute;
	}
	public void setExtAttribute(String extAttribute) {
		this.extAttribute = extAttribute;
	}
	public String getForbidenArea() {
		return forbidenArea;
	}
	public void setForbidenArea(String forbidenArea) {
		this.forbidenArea = forbidenArea;
	}
	public String getActiveArea() {
		return activeArea;
	}
	public void setActiveArea(String activeArea) {
		this.activeArea = activeArea;
	}
	public String getBasePoint() {
		return basePoint;
	}
	public void setBasePoint(String basePoint) {
		this.basePoint = basePoint;
	}

	public String getDialog() {
		return dialog;
	}
	public void setDialog(String dialog) {
		this.dialog = dialog;
	}
	
	public String getDeputyName() {
		return deputyName;
	}
	public void setDeputyName(String deputyName) {
		this.deputyName = deputyName;
	}
	/**
	 * 获取npc的坐标信息
	 * @return
	 */
	public String getPlacePlus(){
		
		return getSceneId()+","+getX()+","+getY();
	}
	
	

	
	
	
	
	
	
	
	
}
