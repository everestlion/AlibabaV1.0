package normal.object;

import normal.config.Ignore;


/**
 * 系统任务模型实体类
 * @author fantadust
 * @date 2011-4-29 下午04:58:51
 */
public class TaskModel {
	
	/**
	 * 任务id
	 */
	private int taskId;
	/**
	 * 任务名称
	 */
	private String name;
	private String description;//任务描述
	

	/**
	 * 任务类型
	 */
	private int type;
	private int classId;
	/**
	 * 任务等级
	 */
	private int level;
	/**
	 * 可接任务的最大等级
	 */
	private int levelMax;
	/**
	 * 颜色
	 */
	private int color;
	/**
	 * 接受任务npc
	 */
	private int sourceNpcId;
	/**
	 * 提交任务npc
	 */
	private int targetNpcId;
	
	private int frontTaskId;
	
	private String acceptConditions;//接任务条件
	private String submitConditions;//交任务条件

	/**
	 * 获得经验
	 */
	private int playerExp;
	private int playerAsset;
	/**
	 * 玩家银票
	 */
	private int playerGoldTicke;
	/**
	 * 聲望獎勵
	 */
	private int prestige;
	

	/**
	 * 帮会资金
	 */
	private int unionAsset;
	/**
	 * 帮派资源
	 */
	private int unionGest;
	/**
	 * 帮派贡献度
	 */
	private int unionDedicate;
	/**
	 * 全部奖励
	 */
	private String allRewards;

	/**
	 * 任选其一奖励
	 */
	private String oneRewards;
	/**
	 * 接任务文案
	 */
	private String acceptText;
	/**
	 * 交任务文案
	 */
	private String submitText;
	/**
	 * 未完成的文案
	 */
	private String unCompleteText;
	/**
	 * 交任务触发剧情id
	 */
	private int storyOnSubmit;

	/**
	 * 给物品
	 */
	private String giveItem;	

	/**
	 * 次数限制0一生一次,其他每天几次
	 */
	private int timesLimit;
	/**
	 * 快速完成支付的银两
	 */
	private int fastPay;
	/**
	 * 是否显示详细信息
	 */
	private int showInfo;
	
	/**
	 * ============================
	 * 
	 * 
	 * 
	 * 以下为附加
	 */
	

	
	/**
	 * 主线
	 */
	@Ignore
	public static final int typeMain = 1;
	/**
	 * 支线
	 */
	@Ignore
	public static final int typeExt = 2;
	/**
	 * 声望
	 */
	@Ignore
	public static final int typePrestige = 4;
	/**
	 * 商会跑环
	 */
	@Ignore
	public static final int typeUnionCycle = 8;	
	/**
	 * 日常任务
	 */
	@Ignore
	public static final int typeDaily = 16;
	/**
	 * 押镖
	 */
	@Ignore
	public static final int typeDart = 32;
	/**
	 * 目标任务
	 */	
	@Ignore
	public static final int typeIntent = 64;
	/**
	 * 副本任务
	 */
	@Ignore
	public static final int typeUndercity=128;
	/**
	 * 挑战门派任务
	 */
	@Ignore
	public static final int typeDream = 256;
	/**
	 * 秘典任务
	 */
	@Ignore
	public static final int typeSecret = 512;
	
	public int getTaskId() {
		return taskId;
	}
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getLevelMax() {
		return levelMax;
	}
	public void setLevelMax(int levelMax) {
		this.levelMax = levelMax;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public int getSourceNpcId() {
		return sourceNpcId;
	}
	public void setSourceNpcId(int sourceNpcId) {
		this.sourceNpcId = sourceNpcId;
	}
	public int getTargetNpcId() {
		return targetNpcId;
	}
	public void setTargetNpcId(int targetNpcId) {
		this.targetNpcId = targetNpcId;
	}
	
	public int getFrontTaskId() {
		return frontTaskId;
	}
	
	public void setFrontTaskId(int frontTaskId) {
		this.frontTaskId = frontTaskId;
	}
	public String getAcceptConditions() {
		return acceptConditions;
	}
	public void setAcceptConditions(String acceptConditions) {
		this.acceptConditions = acceptConditions;
	}
	public String getSubmitConditions() {
		return submitConditions;
	}
	public void setSubmitConditions(String submitConditions) {
		this.submitConditions = submitConditions;
	}	
	public int getPlayerExp() {
		return playerExp;
	}
	public void setPlayerExp(int playerExp) {
		this.playerExp = playerExp;
	}
	public int getPlayerAsset() {
		return playerAsset;
	}
	public int getPrestige() {
		return prestige;
	}
	public void setPrestige(int prestige) {
		this.prestige = prestige;
	}
	public void setPlayerAsset(int playerAsset) {
		this.playerAsset = playerAsset;
	}

	public int getUnionAsset() {
		return unionAsset;
	}
	public void setUnionAsset(int unionAsset) {
		this.unionAsset = unionAsset;
	}
	public int getUnionGest() {
		return unionGest;
	}
	public void setUnionGest(int unionGest) {
		this.unionGest = unionGest;
	}
	public String getAllRewards() {
		return allRewards;
	}
	public void setAllRewards(String allRewards) {
		this.allRewards = allRewards;
	}
	public String getOneRewards() {
		return oneRewards;
	}
	public void setOneRewards(String oneRewards) {
		this.oneRewards = oneRewards;
	}
	public String getAcceptText() {
		return acceptText;
	}
	public void setAcceptText(String acceptText) {
		this.acceptText = acceptText;
	}
	public String getSubmitText() {
		return submitText;
	}
	public void setSubmitText(String submitText) {
		this.submitText = submitText;
	}
	public String getUnCompleteText() {
		return unCompleteText;
	}
	public void setUnCompleteText(String unCompleteText) {
		this.unCompleteText = unCompleteText;
	}
	public int getStoryOnSubmit() {
		return storyOnSubmit;
	}
	public void setStoryOnSubmit(int storyOnSubmit) {
		this.storyOnSubmit = storyOnSubmit;
	}	
	public String getGiveItem() {
		return giveItem;
	}
	public void setGiveItem(String giveItem) {
		this.giveItem = giveItem;
	}
	public int getTimesLimit() {
		return timesLimit;
	}
	public void setTimesLimit(int timesLimit) {
		this.timesLimit = timesLimit;
	}
	public int getFastPay() {
		return fastPay;
	}
	public void setFastPay(int fastPay) {
		this.fastPay = fastPay;
	}
	public int getPlayerGoldTicke() {
		return playerGoldTicke;
	}
	public void setPlayerGoldTicke(int playerGoldTicke) {
		this.playerGoldTicke = playerGoldTicke;
	}
	public int getUnionDedicate() {
		return unionDedicate;
	}
	public void setUnionDedicate(int unionDedicate) {
		this.unionDedicate = unionDedicate;
	}
	public int getShowInfo() {
		return showInfo;
	}
	public void setShowInfo(int showInfo) {
		this.showInfo = showInfo;
	}
	
	//
	// ==============隔隔隔
	//
	




	/**
	 * 主线任务
	 * @return
	 */
	public boolean isTypeMain(){
		return type == typeMain;
	}
	/**
	 * 特殊任务,商会声望日常押镖
	 * @return
	 */
	public boolean isSpecialType(){
		return type == typeUnionCycle || type == typePrestige || type == typeDaily || type == typeDart;
	}
	
	public boolean isSpecialType3(){
		return isTypePrestige() || isTypeUnionCype() || isTypeDaily();
	}
	
	/**
	 * 是否押镖
	 * @return
	 */
	public boolean isTypeDart(){
		return type == typeDart;
	}
	/**
	 * 是否日常
	 * @return
	 */
	public boolean isTypeDaily(){
		return type == typeDaily;
	}
	/**
	 * 帮派跑环	
	 * @return
	 */
	public boolean isTypeUnionCype(){
		return type == typeUnionCycle;
	}
	/**
	 * 声望任务
	 * @return
	 */
	public boolean isTypePrestige(){
		return type == typePrestige;
	}
	/**
	 * 目标任务
	 * @return
	 */
	public boolean isTypeIntent(){
		return type == typeIntent;
	}
	/**
	 * 剧情任务
	 * @return
	 */
	public boolean isTypeDream(){
		return type == typeDream;
	}
	/**
	 * 秘典任务
	 * @return
	 */
	public boolean isTypeSecret(){
		return type == typeSecret;
	}

	
}
