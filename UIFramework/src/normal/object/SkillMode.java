package normal.object;

import normal.config.Ignore;

/**
 * 系�????模�?
 * 
 * @author fantadust
 * @date 2011-5-4 �??04:56:21
 */
public class SkillMode {
	/**
	 * �??
	 */
	private int id;
	/**
	 * ???id
	 */
	private int vocationId;
	/**
	 * ??��
	 */
	private String name;
	/**
	 * ???�?
	 */
	private String deputyName;
	/**
	 * ?????d
	 */
	private int skillLibId;
	/**
	 * ???类�?
	 */
	private int type;
	/**
	 * ????��?
	 */
	private String icon;
	/**
	 * ???�?��
	 */
	private int level;
	/**
	 * ?��?�?���??
	 */
	private int playerLevel;
	/**
	 * ??��???id
	 */
	private int frontSkillId;
	/**
	 * �?????使�????�?
	 */
	private int studyBookId;
	/**
	 * ??��????�两
	 */
	private int asset;
	/**
	 * �????????
	 */
	private int gold;
	/**
	 * ??��??????头个??
	 */
	private int stoneNum;
	/**
	 * ??��??????费�???
	 */
	private int costTime;
	/**
	 * ??��
	 */
	private String description;
	/**
	 * ???群�?
	 */
	private boolean groupType;
	/**
	 * ?��?�??
	 */
	private int groupNum;
	/**
	 * ???�?????�?��??
	 */
	private boolean onlyBattleUse;
	/**
	 * ???类�? 1=>'???',2=>'???'
	 */
	private int targetType;
	/**
	 * �????????类�?
	 */
	private int passiveType;
	/**
	 * �????????�????
	 */
	private int passiveValue;
	/**
	 * �????????追�?
	 */
	private String passiveAdd;
	/**
	 * 伤�?
	 */
	private int effectValue;

	/**
	 * ?��??��? �??
	 */
	private int coolingTime;
	/**
	 * �??�????
	 */
	private int constMp;
	/**
	 * ????��? �??
	 */
	private int singingTime;
	/**
	 * �??buff?????
	 */
	private int buffRate;	
	/**
	 * ??��???buff??d
	 */
	private int buffId;
	/**
	 * ????��?
	 */
	private String actionIds;
	/**
	 * ?��?????��?
	 */
	private int playTime;

	/**
	 * ???�??
	 */
	@Ignore
	public static final int typeVocational = 1;
	/**
	 * �??
	 */
	@Ignore
	public static final int typeStrength = 2;
	/**
	 * �??
	 */
	@Ignore
	public static final int typeCheats = 4;
	@Ignore
	public static final int typeMartial = 8;
	@Ignore
	public static final int typeMonster = 32;
	@Ignore
	public static final int typePet = 64;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVocationId() {
		return vocationId;
	}

	public void setVocationId(int vocationId) {
		this.vocationId = vocationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeputyName() {
		return deputyName;
	}

	public void setDeputyName(String deputyName) {
		this.deputyName = deputyName;
	}

	public int getSkillLibId() {
		return skillLibId;
	}

	public void setSkillLibId(int skillLibId) {
		this.skillLibId = skillLibId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(int playerLevel) {
		this.playerLevel = playerLevel;
	}

	public int getFrontSkillId() {
		return frontSkillId;
	}

	public void setFrontSkillId(int frontSkillId) {
		this.frontSkillId = frontSkillId;
	}

	public int getStudyBookId() {
		return studyBookId;
	}

	public void setStudyBookId(int studyBookId) {
		this.studyBookId = studyBookId;
	}

	public int getAsset() {
		return asset;
	}

	public void setAsset(int asset) {
		this.asset = asset;
	}

	public int getStoneNum() {
		return stoneNum;
	}

	public void setStoneNum(int stoneNum) {
		this.stoneNum = stoneNum;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public int getCostTime() {
		return costTime;
	}

	public void setCostTime(int costTime) {
		this.costTime = costTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isGroupType() {
		return groupType;
	}

	public void setGroupType(boolean groupType) {
		this.groupType = groupType;
	}

	public boolean isOnlyBattleUse() {
		return onlyBattleUse;
	}

	public void setOnlyBattleUse(boolean onlyBattleUse) {
		this.onlyBattleUse = onlyBattleUse;
	}

	public int getTargetType() {
		return targetType;
	}

	public void setTargetType(int targetType) {
		this.targetType = targetType;
	}

	public int getPassiveType() {
		return passiveType;
	}

	public void setPassiveType(int passiveType) {
		this.passiveType = passiveType;
	}

	public int getPassiveValue() {
		return passiveValue;
	}

	public void setPassiveValue(int passiveValue) {
		this.passiveValue = passiveValue;
	}

	public int getEffectValue() {
		return effectValue;
	}

	public void setEffectValue(int effectValue) {
		this.effectValue = effectValue;
	}

	public int getCoolingTime() {
		return coolingTime;
	}

	public void setCoolingTime(int coolingTime) {
		this.coolingTime = coolingTime;
	}

	public int getConstMp() {
		return constMp;
	}

	public void setConstMp(int constMp) {
		this.constMp = constMp;
	}

	public int getSingingTime() {
		return singingTime;
	}

	public void setSingingTime(int singingTime) {
		this.singingTime = singingTime;
	}

	public int getBuffId() {
		return buffId;
	}

	public void setBuffId(int buffId) {
		this.buffId = buffId;
	}

	public String getActionIds() {
		return actionIds;
	}

	public void setActionIds(String actionIds) {
		this.actionIds = actionIds;
	}

	public int getPlayTime() {
		return playTime;
	}

	public void setPlayTime(int playTime) {
		this.playTime = playTime;
	}


	public String getPassiveAdd() {
		return passiveAdd;
	}

	public void setPassiveAdd(String passiveAdd) {
		this.passiveAdd = passiveAdd;
	}

	public int getGroupNum() {
		return groupNum;
	}

	public void setGroupNum(int groupNum) {
		this.groupNum = groupNum;
	}

	public int getBuffRate() {
		return buffRate;
	}

	public void setBuffRate(int buffRate) {
		this.buffRate = buffRate;
	}



}
