package normal.object;

public class Buff {
	/**
	 * 编号
	 */
	private int id;
	/**
	 * 根id
	 */
	private int libId;
	/**
	 * 标志id
	 */
	private int signId;
	/**
	 * 类型
	 */
	private boolean typeGain;

	public boolean isTypeGain() {
		return typeGain;
	}

	public void setTypeGain(boolean typeGain) {
		this.typeGain = typeGain;
	}

	/**
	 * 名称
	 */
	private String name;
	/**
	 * 等级
	 */
	private int level;
	/**
	 * 默认图片
	 */
	private String icon;
	/**
	 * 描述
	 */
	private String description;

	/**
	 * 触发几率
	 */
	private int rate;
	/**
	 * 生命周期
	 */
	private int lifeTime;
	/**
	 * 效用
	 */
	private int effectValue;
	/**
	 * 间隔时间
	 */
	private int intervalTime;
	/**
	 * 值1
	 */
	private int valueA;
	/**
	 * 值2
	 */
	private int valueB;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLibId() {
		return libId;
	}

	public void setLibId(int libId) {
		this.libId = libId;
	}

	public int getSignId() {
		return signId;
	}

	public void setSignId(int signId) {
		this.signId = signId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public int getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(int lifeTime) {
		this.lifeTime = lifeTime;
	}

	public int getEffectValue() {
		return effectValue;
	}

	public void setEffectValue(int effectValue) {
		this.effectValue = effectValue;
	}

	public int getIntervalTime() {
		return intervalTime;
	}

	public void setIntervalTime(int intervalTime) {
		this.intervalTime = intervalTime;
	}

	public int getValueA() {
		return valueA;
	}

	public void setValueA(int valueA) {
		this.valueA = valueA;
	}

	public int getValueB() {
		return valueB;
	}

	public void setValueB(int valueB) {
		this.valueB = valueB;
	}
}
