package normal.object;

import normal.config.Ignore;

/**
 * 系统物品模型实体类
 * 
 */
public class GoodsMode {
	public int goodsId;
	public String name;
	/**
	 * 副名称
	 */
	public String deputyName;
	/**
	 * 物品类型
	 */
	public int goodTypeId;
	/**
	 * 职业id
	 */
	public int vocationId;
	/**
	 * 最大堆叠量
	 */
	public int maxStack;
	/**
	 * 小图
	 */
	public String icon;
	/**
	 * 角色等级
	 */
	public int playerLevel;

	public int classId;// 套装id
	/**
	 * 颜色(0:白色，1:绿色，2:蓝色，3:紫色，4:橙色)
	 */
	public int color;
	/**
	 * 金币
	 */
	public int gold;
	/**
	 * vip积分价格
	 */
	public int vipScore;
	/**
	 * 游戏铜币
	 */
	public int asset;
	/**
	 * npc回收价格
	 */
	public int npcRecover;
	/**
	 * 是否在商城里面开放
	 */
	public boolean openOnShop;
	/**
	 * 支付方式
	 */
	public int paytype;
	/**
	 * 是否开放购买
	 */
	public boolean buyEnable;

	/**
	 * 是否开放出售
	 */
	public boolean saleEnable;
	/**
	 * 是否可以使用
	 */
	public boolean useEnable;
	/**
	 * 是否可以批量使用
	 */
	public boolean useBatch;
	/**
	 * 是否可以赠送
	 */
	public boolean giftEnable;
	/**
	 * 是否绑定
	 */
	public int bindType;
	/**
	 * 出售界面排序id
	 */
	public int orderId;
	public int appId;// buff id
	public int valueA;// buff值
	public int valueB;// buff值
	/**
	 * 子道具
	 */
	public String subPops;
	/**
	 * 冷却时间
	 */
	public int coolingTime;
	/**
	 * 描述
	 */
	public String goodDesc;
	/**
	 * 效果
	 */
	public String goodEffect;
	public int power;
	public int physical;
	public int agile;
	public int wit;	
	/**
	 * 攻速
	 */
	public int speed;
	/**
	 * 追加
	 */
	public int append;
	/**
	 * 购买的 限制
	 */
	public int buyLimit;
	/**
	 * 添加的字段,从哪个怪物掉落
	 */
	@Ignore
	public int fallByMonsterId;
	public int[] gatherScene;
	/**
	 * 打开后可以获得的物品
	 */
//	public List<SubPops> subPopsList;
	
	public int getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeputyName() {
		return deputyName;
	}

	public void setDeputyName(String deputyName) {
		this.deputyName = deputyName;
	}

	public int getGoodTypeId() {
		return goodTypeId;
	}

	public void setGoodTypeId(int goodTypeId) {
		this.goodTypeId = goodTypeId;
	}

	public int getVocationId() {
		return vocationId;
	}

	public void setVocationId(int vocationId) {
		this.vocationId = vocationId;
	}

	public int getMaxStack() {
		return maxStack > 0 ? maxStack : 1;
	}

	public void setMaxStack(int maxStack) {
		this.maxStack = maxStack;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(int playerLevel) {
		this.playerLevel = playerLevel;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public int getVipScore() {
		return vipScore;
	}

	public void setVipScore(int vipScore) {
		this.vipScore = vipScore;
	}

	public int getAsset() {
		return asset;
	}

	public void setAsset(int asset) {
		this.asset = asset;
	}

	public int getNpcRecover() {
		return npcRecover;
	}

	public void setNpcRecover(int npcRecover) {
		this.npcRecover = npcRecover;
	}

	public boolean isOpenOnShop() {
		return openOnShop;
	}

	public void setOpenOnShop(boolean openOnShop) {
		this.openOnShop = openOnShop;
	}

	public int getPaytype() {
		return paytype;
	}

	public void setPaytype(int paytype) {
		this.paytype = paytype;
	}

	public boolean isBuyEnable() {
		return buyEnable;
	}

	public void setBuyEnable(boolean buyEnable) {
		this.buyEnable = buyEnable;
	}

	public boolean isSaleEnable() {
		return saleEnable;
	}

	public void setSaleEnable(boolean saleEnable) {
		this.saleEnable = saleEnable;
	}

	public int getBindType() {
		return bindType;
	}

	public void setBindType(int bindType) {
		this.bindType = bindType;
	}

	public boolean isUseEnable() {
		return useEnable;
	}

	public boolean isGiftEnable() {
		return giftEnable;
	}

	public void setUseEnable(boolean useEnable) {
		this.useEnable = useEnable;
	}

	public void setGiftEnable(boolean giftEnable) {
		this.giftEnable = giftEnable;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public int getValueA() {
		return valueA;
	}

	public void setValueA(int buffValueA) {
		this.valueA = buffValueA;
	}

	public int getValueB() {
		return valueB;
	}

	public void setValueB(int buffValueB) {
		this.valueB = buffValueB;
	}

	public int getCoolingTime() {
		return coolingTime;
	}

	public void setCoolingTime(int coolingTime) {
		this.coolingTime = coolingTime;
	}

	public String getGoodDesc() {
		return goodDesc;
	}

	public void setGoodDesc(String goodDesc) {
		this.goodDesc = goodDesc;
	}

	public String getGoodEffect() {
		return goodEffect;
	}

	public void setGoodEffect(String goodEffect) {
		this.goodEffect = goodEffect;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getFallByMonsterId() {
		return fallByMonsterId;
	}

	public void setFallByMonsterId(int fallByMonsterId) {
		this.fallByMonsterId = fallByMonsterId;
	}


	
	public int[] getGatherScene() {
		return gatherScene;
	}

	public void setGatherScene(int[] gatherScene) {
		this.gatherScene = gatherScene;
	}

	public int getAppend() {
		return append;
	}
	public void setAppend(int append) {
		this.append = append;
	}
	public int getPower() {
		return power;
	}

	public int getPhysical() {
		return physical;
	}

	public int getAgile() {
		return agile;
	}

	public int getWit() {
		return wit;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public void setPhysical(int physical) {
		this.physical = physical;
	}

	public void setAgile(int agile) {
		this.agile = agile;
	}

	public void setWit(int wit) {
		this.wit = wit;
	}

	public String getSubPops() {
		return subPops;
	}

	public void setSubPops(String subPops) {
		this.subPops = subPops;
	}


	public int getBuyLimit() {
		return buyLimit;
	}

	public void setBuyLimit(int buyLimit) {
		this.buyLimit = buyLimit;
	}

	
	public boolean isUseBatch() {
		return useBatch;
	}

	public void setUseBatch(boolean useBatch) {
		this.useBatch = useBatch;
	}


}
