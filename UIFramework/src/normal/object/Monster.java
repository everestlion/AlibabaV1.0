package normal.object;


public class Monster {
	public int id;// 编号
	public String name;// 名称
	public String icon;// 图片
	/**
	 * 类型 0 近战,1远程
	 */
	public int type;// 类型
	public int level;// 等级
	public int hp;// 血
	public int mp;// 蓝
	public int attack;// 攻击
	public int defense;
	public int hit;// 命中
	public int crit;// 闪避
	public int tough;
	public int dodge;
	public int attackSpeed;
	public int addition1;
	public int addition2;
	public int addition3;
	public int assetR;// 掉钱几率
	public int asset;// 掉钱数量
	public int exp;// 经验值
	/**
	 * 0普通 1精英 2 boss
	 */
	public int boss;// 是否boss
	public String itemFallData;// 掉落物品数据
	public int itemFallMax;// 最大掉落数
	public int talkR;// 说话几率
	public String talkData;// 说话内容
	/**
	 * 怪物移动速度
	 */
	public int moveSpeed;

	public Long uniqueId = 0L;

	public int[] inScend;

	/**
	 * 是否主动攻击
	 */
	public boolean initiative;
	/**
	 * ai
	 */
	public int ai;

//	public List<GoodsFall> itemFallDataList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getMp() {
		return mp;
	}

	public void setMp(int mp) {
		this.mp = mp;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getHit() {
		return hit;
	}

	public void setHit(int hit) {
		this.hit = hit;
	}

	public int getCrit() {
		return crit;
	}

	public void setCrit(int crit) {
		this.crit = crit;
	}

	public int getTough() {
		return tough;
	}

	public void setTough(int tough) {
		this.tough = tough;
	}

	public int getDodge() {
		return dodge;
	}

	public void setDodge(int dodge) {
		this.dodge = dodge;
	}

	public int getAttackSpeed() {
		return attackSpeed;
	}

	public void setAttackSpeed(int attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	public int getAddition1() {
		return addition1;
	}

	public void setAddition1(int addition1) {
		this.addition1 = addition1;
	}

	public int getAddition2() {
		return addition2;
	}

	public void setAddition2(int addition2) {
		this.addition2 = addition2;
	}

	public int getAddition3() {
		return addition3;
	}

	public void setAddition3(int addition3) {
		this.addition3 = addition3;
	}

	public int getAssetR() {
		return assetR;
	}

	public void setAssetR(int assetR) {
		this.assetR = assetR;
	}

	public int getAsset() {
		return asset;
	}

	public void setAsset(int asset) {
		this.asset = asset;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public int getBoss() {
		return boss;
	}

	public void setBoss(int boss) {
		this.boss = boss;
	}

	public int getItemFallMax() {
		return itemFallMax;
	}

	public void setItemFallMax(int itemFallMax) {
		this.itemFallMax = itemFallMax;
	}

	public String getItemFallData() {
		return itemFallData;
	}

//	public void setItemFallData(String itemFallData) {
//		this.itemFallData = itemFallData;
//		if (itemFallData != null && itemFallData.length() > 0) {
//			try {
//				itemFallDataList = JsonProcesser.objectJsonArrayDecode(itemFallData, GoodsFall.class);
//			} catch (Exception e) {
//				LogUtil.getMain().error("Monster config err | id: " + id + "|itemFallData|" + itemFallData);
//				// e.printStackTrace();
//			}
//
//		}
//
//	}
//
//	public List<GoodsFall> getItemFallDataList() {
//		return itemFallDataList;
//	}
//
//	public void setItemFallDataList(List<GoodsFall> itemFallDataList) {
//		this.itemFallDataList = itemFallDataList;
//	}

	public int getTalkR() {
		return talkR;
	}

	public void setTalkR(int talkR) {
		this.talkR = talkR;
	}

	public String getTalkData() {
		return talkData;
	}

	public void setTalkData(String talkData) {
		this.talkData = talkData;
	}

	public int[] getInScend() {
		return inScend;
	}

	public void setInScend(int[] inScend) {
		this.inScend = inScend;
	}

	public Long getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(Long uniqueId) {
		this.uniqueId = uniqueId;
	}

	public int getMoveSpeed() {
		return moveSpeed;
	}

	public void setMoveSpeed(int moveSpeed) {
		this.moveSpeed = moveSpeed;
	}

	public boolean isInitiative() {
		return initiative;
	}

	public void setInitiative(boolean initiative) {
		this.initiative = initiative;
	}

	public int getAi() {
		return ai;
	}

	public void setAi(int ai) {
		this.ai = ai;
	}


}
