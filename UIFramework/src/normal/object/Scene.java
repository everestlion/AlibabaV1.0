package normal.object;

public class Scene {
	/**
	 * 地图id
	 */
	protected int id;
	/**
	 * 地图名称
	 */
	protected String name;
	/**
	 * 地图类型
	 */
	protected int type;
	/**
	 * 所属地图
	 */
	protected int rootId;
	/**
	 * 地图文件
	 */
	protected String mapFile;
	/**
	 * 图标
	 */
	protected String icon;

	/**
	 * 进入时候执行脚本id
	 */
	protected int scriptOnEnter;
	/**
	 * 离开时候执行脚本id
	 */
	protected int scriptOnExit;

	protected String defaultBirth;// 默认出生点

	protected boolean upOpen;
	protected boolean downOpen;
	protected boolean leftOpen;
	protected boolean rightOpen;
	protected String upTeleport;
	protected String downTeleport;
	protected String leftTeleport;
	protected String rightTeleport;
	/**
	 * 关卡打开的条件
	 */
	protected int openKey, openParam1, openParam2;
	/**
	 * 传送门打开的场景id
	 */
	protected int upScene, downScene, leftScene, rightScene;
	/**
	 * 进入提示
	 */
	protected String enterTips;
	/**
	 * 副本时间
	 */
	protected int liftTime;
	/**
	 * 场景音乐
	 */
	protected String music;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getMapFile() {
		return mapFile;
	}

	public void setMapFile(String mapFile) {
		this.mapFile = mapFile;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getScriptOnEnter() {
		return scriptOnEnter;
	}

	public void setScriptOnEnter(int scriptOnEnter) {
		this.scriptOnEnter = scriptOnEnter;
	}

	public int getScriptOnExit() {
		return scriptOnExit;
	}

	public void setScriptOnExit(int scriptOnExit) {
		this.scriptOnExit = scriptOnExit;
	}

	public String getDefaultBirth() {
		return defaultBirth;
	}

	public void setDefaultBirth(String defaultBirth) {
		this.defaultBirth = defaultBirth;
	}

	public boolean isUpOpen() {
		return upOpen;
	}

	public void setUpOpen(boolean upOpen) {
		this.upOpen = upOpen;
	}

	public boolean isDownOpen() {
		return downOpen;
	}

	public void setDownOpen(boolean downOpen) {
		this.downOpen = downOpen;
	}

	public boolean isLeftOpen() {
		return leftOpen;
	}

	public void setLeftOpen(boolean leftOpen) {
		this.leftOpen = leftOpen;
	}

	public boolean isRightOpen() {
		return rightOpen;
	}

	public void setRightOpen(boolean rightOpen) {
		this.rightOpen = rightOpen;
	}

	public String getUpTeleport() {
		return upTeleport;
	}

	public void setUpTeleport(String upTeleport) {
		this.upTeleport = upTeleport;
	}

	public String getDownTeleport() {
		return downTeleport;
	}

	public void setDownTeleport(String downTeleport) {
		this.downTeleport = downTeleport;
	}

	public String getLeftTeleport() {
		return leftTeleport;
	}

	public void setLeftTeleport(String leftTeleport) {
		this.leftTeleport = leftTeleport;
	}

	public String getRightTeleport() {
		return rightTeleport;
	}

	public void setRightTeleport(String rightTeleport) {
		this.rightTeleport = rightTeleport;
	}

	public int getRootId() {
		return rootId;
	}

	public void setRootId(int rootId) {
		this.rootId = rootId;
	}

	public int getOpenKey() {
		return openKey;
	}

	public void setOpenKey(int openKey) {
		this.openKey = openKey;
	}

	public int getOpenParam1() {
		return openParam1;
	}

	public void setOpenParam1(int openParam1) {
		this.openParam1 = openParam1;
	}

	public int getOpenParam2() {
		return openParam2;
	}

	public void setOpenParam2(int openParam2) {
		this.openParam2 = openParam2;
	}

	public String getEnterTips() {
		return enterTips;
	}

	public void setEnterTips(String enterTips) {
		this.enterTips = enterTips;
	}

	public int getLiftTime() {
		return liftTime;
	}

	public void setLiftTime(int liftTime) {
		this.liftTime = liftTime;
	}

	public int getUpScene() {
		return upScene;
	}

	public void setUpScene(int upScene) {
		this.upScene = upScene;
	}

	public int getDownScene() {
		return downScene;
	}

	public void setDownScene(int downScene) {
		this.downScene = downScene;
	}

	public int getLeftScene() {
		return leftScene;
	}

	public void setLeftScene(int leftScene) {
		this.leftScene = leftScene;
	}

	public int getRightScene() {
		return rightScene;
	}

	public void setRightScene(int rightScene) {
		this.rightScene = rightScene;
	}

	public String getMusic() {
		return music;
	}

	public void setMusic(String music) {
		this.music = music;
	}
	
	
}
