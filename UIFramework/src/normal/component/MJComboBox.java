package normal.component;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class MJComboBox extends JComboBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 203617487405514413L;
	
	
	public int[] values;
	
	public MJComboBox (String[] itemNames, int[] values) {
		super(itemNames);
		this.values = values;
	}
	public MJComboBox () {
	}
	
	public void setItems(String[] items) {
		setModel(new DefaultComboBoxModel(items));
	}
	
	public void setValues(int[] values) {
		this.values = values;
	}

	public int getValue(int selectedId) {
		return values[selectedId];
	}
	
	public int getValueIndex(int val) {
		if (values == null || values.length == 0) {
			return 0;
		}
		for (int i = 0; i < values.length; i++) {
			if (values[i] == val) {
				return i;
			}
		}
		return 0;
	}
}
