package normal.ui.table;

import java.util.HashMap;
import java.util.List;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

public class ResultTable extends JTable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1277916843957843723L;
	
	public static HashMap<Integer, ResultTableModel> modelMap = new HashMap<Integer, ResultTableModel>();

	public ResultTable() {
		super();
	}

	public void refresh(int type, List<Object[]> resources) {
		ResultTableModel curModel = (ResultTableModel)this.getModel();
		if (curModel.getType() != type) {
			curModel = modelMap.get(type);
			if (curModel == null) {
				curModel = new ResultTableModel(type);
				modelMap.put(type, curModel);
			}
			this.setModel(curModel);
		}
		curModel.setResources(resources);
		//通知监听器, 在范围内加入数据
		curModel.fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
		setTableFace();
	}

	public void setTableFace() {
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setRowHeight(25);
		
		//设置列宽 设置单元格渲染
		ResultTableModel table = (ResultTableModel)this.getModel();
		TableColumn column = null;
		for (Object[] e : table.getColumns()) {
			column = this.getColumn((String)e[0]);
			column.setMinWidth((Integer)e[1]);
			column.setCellRenderer(new ResultTableCellRenderer());
		}
		
		//去掉表格的线
		setShowVerticalLines(false);
	}
	
	public boolean isCellEditable(int row, int column) {
		return false;
	}

}
