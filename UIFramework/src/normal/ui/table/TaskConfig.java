package normal.ui.table;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class TaskConfig {
	
	public static final Map<Integer, String> npcMap = new HashMap<Integer, String>();
	public static final Map<Integer, String> typeMap = new HashMap<Integer, String>();
	public static final Map<Integer, String> classMap = new HashMap<Integer, String>();
	
	static {
		parseXML("/normal/config/taskNpc.xml");
		typeMap.put(1, "主线");
		typeMap.put(2, "支线");
		typeMap.put(4, "声望");
		typeMap.put(8, "跑环");
		typeMap.put(16, "日常");
		typeMap.put(32, "押镖");
		typeMap.put(64, "目标");
		typeMap.put(128, "副本");
		typeMap.put(256, "剧情");
		typeMap.put(512, "秘典");
		
		classMap.put(1, "对话");
		classMap.put(2, "打怪");
		classMap.put(3, "收集");
		classMap.put(6, "采集");
		classMap.put(7, "触发");
	}
	
	@SuppressWarnings("unchecked")
	public static void parseXML(String filePath) {
		SAXReader reader = new SAXReader();
		Document document = null;
		try {
			reader.setEncoding("utf8");
			document = reader.read(TaskConfig.class.getResource(filePath).getFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (document == null) {
			System.out.println("parse taskNpc.xml err");
			return;
		}
		
		Element rootElement = document.getRootElement();
		if (rootElement.hasContent() && rootElement.hasMixedContent()) {
			Iterator<Element> iterator = rootElement.elementIterator();
			while (iterator.hasNext()) {
				Element element = iterator.next();
				if (element.getNodeType() == Node.ELEMENT_NODE) {
					npcMap.put(Integer.parseInt(element.attributeValue("value")), element.getTextTrim());
				}
			}
		}
	}

}
