package normal.ui.table;

import normal.navigation.TreeConfig;

public class TableModelConfig {
	
	public static final Object[][] MONSTER_TABLE_HEADER = {{"怪物id", 50}, {"名称", 200}, {"等级", 50}, {"图片", 200}, {"血量", 50}, {"类型", 50}, {"是否boss", 50}};
	
	public static final Object[][] MONSTER_Deploy_TABLE_HEADER = {{"怪物id", 50}, {"名称", 200}, {"场景", 200}, {"x", 50}, {"y", 50}, {"默认怪物", 50}, {"怪物列表", 200}};
	
	public static final Object[][] GOODS_MODE_TABLE_HEADER = {{"道具id", 50}, {"名称", 200}, {"图片", 200}, {"可购买", 50}, 
		{"商城显示", 50}, {"是否可交易", 50}, {"金币", 50}, {"积分", 50}, {"银两", 50}, {"最大堆叠", 50}};

	public static final Object[][] TASK_TABLE_HEADER = {{"任务id", 50}, {"任务名称", 200}, {"前置id", 50}, {"等级", 50}, {"最大等级", 50}, {"任务类型", 100}, {"任务属性", 100}, {"接任务", 200}, {"交任务", 200}};
	
	public static final Object[][] DRAMA_TABLE_HEADER = {{"剧情id", 50}, {"剧情名称", 200}, {"类型", 50}, {"电影编号", 50}};
	
	public static final Object[][] NPC_TABLE_HEADER = {{"ID", 50}, {"名称", 100}, {"所属场景", 100}, {"图片", 50}, {"x", 50}, {"y", 50}, {"scriptIds", 100}, {"是否可点", 50}};
	
	public static final Object[][] SCENE_TABLE_HEADER = {{"ID", 50}, {"名称", 100}, {"父属城市", 100}, {"图片", 50}, {"类型", 50}, {"副本打开类型", 50}};
	
	public static final Object[][] PENDANT_TABLE_HEADER = {{"编号", 50}, {"名称", 100}, {"场景id", 100}, {"x", 50}, {"y", 50}, {"图片", 50}, {"需要任务", 50}, {"采集物品", 50}, {"数量", 50}, {"刷新方式", 50}, {"间隔", 50}};
	
	public static final Object[][] UNDERCITY_TABLE_HEADER = {{"ID", 50}, {"名称", 100}, {"等级", 100}, {"最大等级", 50}, {"类型", 50}, {"图片", 50}, {"人数要求", 50}};
	
	public static final Object[][] MONSTER_AI_TABLE_HEADER = {{"ID", 50}, {"名称", 100}};
	
	public static final Object[][] SCRIPT_TABLE_HEADER = {{"ID", 50}, {"菜单名称", 100}, {"别名", 100}, {"描述", 100}, {"windowName", 100}};

	public static final Object[][] SKILL_TABLE_HEADER = {{"ID", 50}, {"名称", 100}, {"技能根id", 50}, {"技能等级", 50}, {"角色等级", 50}, {"前置技能", 50}, {"需要银两", 50}, {"属性值", 50}};

	public static final Object[][] BUFF_TABLE_HEADER = {{"ID", 50}, {"根id", 50}, {"名称", 100}, {"描述", 200}, {"图片", 100}};
	
	public static Object[][] getTableHeadeByType(int type) {
		switch (type) {
		case TreeConfig.ID_GOODS:
			return GOODS_MODE_TABLE_HEADER;
		case TreeConfig.ID_TASK:
			return TASK_TABLE_HEADER;
		case TreeConfig.ID_MONSTER:
			return MONSTER_TABLE_HEADER;
		case TreeConfig.ID_MONSTER_DEPLOY:
			return MONSTER_Deploy_TABLE_HEADER;
		case TreeConfig.ID_DRAMA:
			return DRAMA_TABLE_HEADER;
		case TreeConfig.ID_NPC:
			return NPC_TABLE_HEADER;
		case TreeConfig.ID_SCENE:
			return SCENE_TABLE_HEADER;
		case TreeConfig.ID_PENDANT:
			return PENDANT_TABLE_HEADER;
		case TreeConfig.ID_UNDERCITY:
			return UNDERCITY_TABLE_HEADER;
		case TreeConfig.ID_MONSTER_AI:
			return MONSTER_AI_TABLE_HEADER;
		case TreeConfig.ID_SCRIPT:
			return SCRIPT_TABLE_HEADER;
		case TreeConfig.ID_SKILL:
			return SKILL_TABLE_HEADER;
		case TreeConfig.ID_BUFF:
			return BUFF_TABLE_HEADER;
		default:
			break;
		}
		return null;
	}
}
