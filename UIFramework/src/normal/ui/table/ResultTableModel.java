package normal.ui.table;

import java.util.List;

import javax.swing.table.DefaultTableModel;

public class ResultTableModel extends DefaultTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3106735447060600451L;

	private List<Object[]> resources;
	
	private Object[][] columns;
	
	private int type;
	
	public ResultTableModel(int type) {
		super();
		this.type = type;
		this.columns = TableModelConfig.getTableHeadeByType(type);
	}

	public int getRowCount() {
		if (this.resources != null) {
			return this.resources.size();
		} else {
			return 0;
		}
	}

	public String getColumnName(int col) {
		return (String)columns[col][0];
	}

	public int getColumnCount() {
		return columns.length;
	}

	public void setResources(List<Object[]> resources) {
		this.resources = resources;
	}

	public List<Object[]> getResources() {
		return this.resources;
	}
	
	public Object[][] getColumns() {
		return columns;
	}

	public void setColumns(Object[][] columns) {
		this.columns = columns;
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object[] r = this.resources.get(row);
		if (r == null) {
			return super.getValueAt(row, column);
		}
		return r[column];
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public Object[] getOneResource(int row){
		return resources.get(row);
	}

}
