package normal.ui.table;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ResultTableCellRenderer extends DefaultTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3108621425857806638L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		//判断是否需要显示图片
		if (value instanceof Icon) {
			this.setIcon((Icon)value);
		} else {
			String columnName = table.getModel().getColumnName(column);
			String valString = null;
			if ("任务类型".equals(columnName)) {
				valString = TaskConfig.typeMap.get(Integer.parseInt(value.toString()));
			} else if ("任务属性".equals(columnName)) {
				valString = TaskConfig.classMap.get(Integer.parseInt(value.toString()));
			} else if ("接任务".equals(columnName) || "交任务".equals(columnName)) {
				valString = TaskConfig.npcMap.get(Integer.parseInt(value.toString()));
			} else {
				valString = value.toString();
			}
			this.setText(valString);
			this.setToolTipText(valString);
		}
		//判断是否选中
		if (isSelected) {
			super.setBackground(table.getSelectionBackground());
		} else {
			setBackground(table.getBackground());
		}
		//设置居中
		this.setHorizontalAlignment(JLabel.CENTER);
		return this;
	}

	
}
