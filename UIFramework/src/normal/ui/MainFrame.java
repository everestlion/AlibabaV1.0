package normal.ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import normal.data.DataDao;
import normal.data.SQL;
import normal.navigation.Node;
import normal.navigation.TreeConfig;
import normal.object.Buff;
import normal.object.Drama;
import normal.object.GoodsMode;
import normal.object.Monster;
import normal.object.MonsterAI;
import normal.object.MonsterDeploy;
import normal.object.Npc;
import normal.object.Pendant;
import normal.object.Scene;
import normal.object.Script;
import normal.object.SkillMode;
import normal.object.TaskModel;
import normal.object.Undercity;
import normal.ui.detail.BuffPanel;
import normal.ui.detail.ConfigPanel;
import normal.ui.detail.DetailPanel;
import normal.ui.detail.DramaPanel;
import normal.ui.detail.GoodsModePanel;
import normal.ui.detail.MonsterAIPanel;
import normal.ui.detail.MonsterDeployPanel;
import normal.ui.detail.MonsterPanel;
import normal.ui.detail.NpcPanel;
import normal.ui.detail.PendantPanel;
import normal.ui.detail.ScenePanel;
import normal.ui.detail.ScriptPanel;
import normal.ui.detail.SkillPanel;
import normal.ui.detail.TaskPanel;
import normal.ui.detail.UndercityPanel;
import normal.ui.table.ResultTable;
import normal.ui.table.ResultTableModel;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import util.ImageUtil;
import db.JDBC;
public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 625747362370605893L;
	
	//导航树
	private NavigationTree navTree;
	//下载列表
	private ResultTable resultTable;
	//信息列表
	private JList infoJList;
	//工具栏
//	private JToolBar toolBar = new JToolBar();
	private JPanel searchPanel = new JPanel();
	private JButton searchButton = new JButton("搜索");
	private JTextField searchField = new JTextField(15);
	private JRadioButton searchType1 = new JRadioButton("按ID");
	private JRadioButton searchType2 = new JRadioButton("按名称");
	private JSplitPane rightPane;
	
	//悬浮窗口
	@SuppressWarnings("unused")
	private SuspendWindow suspendWindow;
	
	private JTabbedPane tab = new JTabbedPane();
	
	private JPopupMenu popupMenu = new JPopupMenu();
	
	private JMenuItem copyItem = new JMenuItem("复制内容", ImageUtil.SUSPEND_OPEN_IAMGE);
	private int copyItemColumn = 0;
	
	private DetailPanel detailPanel;
	private JScrollPane detailScrollPane;
	
	private JPanel configPanel;
	
	public MainFrame() {
		createSearchPanel();
		this.navTree = TreeConfig.createTree();
		
		createResultTable();
		createPopmenu();
		//创建悬浮窗口
//		this.suspendWindow = new SuspendWindow(this);
		//得到屏幕大小
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		//导航滚动
		JScrollPane navPane = new JScrollPane(this.navTree);
		JScrollPane resultPane = new JScrollPane(this.resultTable);
		int downloadPaneHeight = (int)(screen.height/1.5);
		int downloadPaneWidth = (int)(screen.width/0.8);
		resultPane.setPreferredSize(new Dimension(downloadPaneWidth, downloadPaneHeight));
		JScrollPane infoPane = new JScrollPane(this.infoJList);
		//主界面右边的分隔Pane
		rightPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, resultPane, infoPane);
		rightPane.setDividerLocation(500);
		rightPane.setDividerSize(3);
		tab.addTab("列表", rightPane);
		detailPanel = new GoodsModePanel();
		detailScrollPane = new JScrollPane(detailPanel);
		tab.addTab("详细信息", detailScrollPane);
		configPanel = new ConfigPanel(downloadPaneWidth, downloadPaneHeight);
		tab.addTab("配置", new JScrollPane(configPanel));
		//主界面左边的分隔Pane
		JSplitPane leftPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, this.searchPanel, navPane);
		leftPane.setDividerLocation(100);
		leftPane.setDividerSize(3);
		//主界面的分隔Pane
		JSplitPane mainPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPane, tab);
		mainPane.setDividerSize(3);
		mainPane.setDividerLocation((int)(screen.width/5.5));
//		searchButton.setUI(new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.red));
		// 初始化监听器
		initListener();
		this.add(mainPane);
		this.setPreferredSize(new Dimension(screen.width, screen.height - 30));
		this.setVisible(true);
		this.setTitle("工具");
		this.pack();
	}
	
	private void createResultTable() {
		ResultTableModel tableModel = new ResultTableModel(2);
		this.resultTable = new ResultTable();
		this.resultTable.setModel(tableModel);
		this.resultTable.setTableFace();
	}
	
	private void createSearchPanel() {
		Box searchTypeBox = Box.createHorizontalBox();
		ButtonGroup group = new ButtonGroup ();
		group.add(searchType1);
		group.add(searchType2);
		searchType2.setSelected(true);
		searchTypeBox.add(searchType1);
		searchTypeBox.add(Box.createHorizontalStrut(10));
		searchTypeBox.add(searchType2);
		searchPanel.add(searchTypeBox);
		searchPanel.add(Box.createHorizontalStrut(10));
		searchPanel.add(searchField);
		searchPanel.add(Box.createHorizontalStrut(10));
		searchPanel.add(searchButton);
		searchPanel.add(Box.createHorizontalStrut(10));
	}
	
	private void createPopmenu() {
		popupMenu.add(copyItem);
	}
	
	private void initListener() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				JDBC.close();
				System.exit(0);
			}
		});
		
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tab.getSelectedIndex() != 1) {
//					tab.setSelectedIndex(1);
				}
				search();
			}
		});
		
		searchField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				search();
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				search();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				search();
			}
		});
		
		resultTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() > 1) {
					showDetail();
					if (tab.getSelectedIndex() != 1) {
						tab.setSelectedIndex(1);
					}
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					int nowAtRow = resultTable.rowAtPoint(e.getPoint());
					if (resultTable.getSelectedRow() == nowAtRow) {
						popupMenu.show(resultTable, e.getX(), e.getY());
						copyItemColumn = resultTable.columnAtPoint(e.getPoint());
					} else {
						resultTable.setRowSelectionInterval(nowAtRow, nowAtRow);
					}
				}
			}
		});
		
		navTree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				navTreeSelection(e);
			}

		});
		
		searchType1.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!searchType1.isSelected() && searchType1.isShowing()) {
					return;
				}
				search();
			}
		});
		
		searchType2.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (!searchType2.isSelected() && searchType2.isShowing()) {
					return;
				}
				search();
			}
		});
		
		copyItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectRow = resultTable.getSelectedRow();
				String val = resultTable.getValueAt(selectRow, copyItemColumn).toString();
				setClipboardText(val);
			}
			
		});
		
	}
	
	private void showDetail() {
		ResultTableModel tableModel = (ResultTableModel) resultTable.getModel();
		int id = (Integer)tableModel.getOneResource(resultTable.getSelectedRow())[0];
		int rootType = getSelectedRootType();
		Object object = null;
		switch (rootType) {
		case TreeConfig.ID_GOODS:
			object = DataDao.commGetOne(GoodsMode.class, SQL.GET_GOODS_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(GoodsModePanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new GoodsModePanel();
			} 
			break;
		case TreeConfig.ID_MONSTER:
			object = DataDao.commGetOne(Monster.class, SQL.GET_MONSTER_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(MonsterPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new MonsterPanel();
			} 
			break;
		case TreeConfig.ID_MONSTER_DEPLOY:
			object = DataDao.commGetOne(MonsterDeploy.class, SQL.GET_MONSTER_DEPLOY_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(MonsterDeployPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new MonsterDeployPanel();
			} 
			break;
		case TreeConfig.ID_TASK:
			object = DataDao.commGetOne(TaskModel.class, SQL.GET_TASK_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(TaskPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new TaskPanel();
			} 
			break;
		case TreeConfig.ID_DRAMA:
			object = DataDao.commGetOne(Drama.class, SQL.GET_DRAMA_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(DramaPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new DramaPanel();
			} 
			break;
		case TreeConfig.ID_NPC:
			object = DataDao.commGetOne(Npc.class, SQL.GET_NPC_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(NpcPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new NpcPanel();
			} 
			break;
		case TreeConfig.ID_SCENE:
			object = DataDao.commGetOne(Scene.class, SQL.GET_SCENE_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(ScenePanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new ScenePanel();
			} 
			break;
		case TreeConfig.ID_PENDANT:
			object = DataDao.commGetOne(Pendant.class, SQL.GET_PENDANT_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(PendantPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new PendantPanel();
			} 
			break;
		case TreeConfig.ID_UNDERCITY:
			object = DataDao.commGetOne(Undercity.class, SQL.GET_UNDERCITY_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(UndercityPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new UndercityPanel();
			} 
			break;
		case TreeConfig.ID_MONSTER_AI:
			object = DataDao.commGetOne(MonsterAI.class, SQL.GET_MONSTER_AI_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(MonsterAIPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new MonsterAIPanel();
			} 
			break;
		case TreeConfig.ID_SCRIPT:
			object = DataDao.commGetOne(Script.class, SQL.GET_SCRIPT_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(ScriptPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new ScriptPanel();
			} 
			break;
		case TreeConfig.ID_SKILL:
			object = DataDao.commGetOne(SkillMode.class, SQL.GET_SKILL_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(SkillPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new SkillPanel();
			} 
			break;
		case TreeConfig.ID_BUFF:
			object = DataDao.commGetOne(Buff.class, SQL.GET_BUFF_BY_ID, id);
			detailPanel = DetailPanel.getPanelModel(BuffPanel.layoutFile);
			if (detailPanel == null) {
				detailPanel = new BuffPanel();
			} 
			break;

		default:
			break;
		}
		detailPanel.updateData(object);
		detailScrollPane.setViewportView(detailPanel);
	}
	
	private void setClipboardText(String val) {
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();//获取系统剪贴板
		Transferable tText = new StringSelection(val);
		clip.setContents(tText, null);
	}
	
	private void search() {
		String searchStr = searchField.getText().trim();;
		if (searchType1.isSelected()) {
			int id = 0;
			try {
				if (searchStr == null || searchStr.length() <= 0) {
					id = 0;
				} else {
					id = Integer.parseInt(searchStr);
				}
			} catch (Exception e2) {
				e2.printStackTrace();
				return;
			}
			searchById(id);
		} else if (searchType2.isSelected()) {
			searchByName(searchStr);
		}
		
	}
	
	public int getSelectedRootType() {
		if (navTree.getLastSelectedPathComponent() == null) {
			return -1;
		}
		Node node = (Node) ((DefaultMutableTreeNode) navTree.getLastSelectedPathComponent()).getUserObject();
		int rootId = 0;
		if (node == null || node.getId() == TreeConfig.ID_ROOT) {
			rootId = TreeConfig.ID_GOODS;
		} else {
			rootId = node.getParentId() == TreeConfig.ID_ROOT ? node.getId() : node.getParentId();
		}
		return rootId;
	}
	
	private void searchById(int id) {
		int rootType = getSelectedRootType();
		if (rootType == -1) {
			return;
		}
		String p = id <= 0 ? "%" : id + "%";
		List<Object[]> list = null;
		switch (rootType) {
		case TreeConfig.ID_GOODS:
			list = DataDao.commGetList(SQL.SEARCHBY_GOODS_ID, p);
			break;
		case TreeConfig.ID_TASK:
			list = DataDao.commGetList(SQL.SEARCHBY_TASK_ID, p);
			break;
		case TreeConfig.ID_MONSTER:
			list = DataDao.commGetList(SQL.SEARCHBY_MONSTER_ID, p);
			break;
		case TreeConfig.ID_MONSTER_DEPLOY:
			list = DataDao.commGetList(SQL.SEARCHBY_MONSTER_DEPLOY_ID, p);
			break;
		case TreeConfig.ID_DRAMA:
			list = DataDao.commGetList(SQL.SEARCHBY_DRAMA_ID, p);
			break;
		case TreeConfig.ID_NPC:
			list = DataDao.commGetList(SQL.SEARCHBY_NPC_ID, p);
			break;
		case TreeConfig.ID_SCENE:
			list = DataDao.commGetList(SQL.SEARCHBY_SCENE_ID, p);
			break;
		case TreeConfig.ID_PENDANT:
			list = DataDao.commGetList(SQL.SEARCHBY_PENDANT_ID, p);
			break;
		case TreeConfig.ID_UNDERCITY:
			list = DataDao.commGetList(SQL.SEARCHBY_UNDERCITY_ID, p);
			break;
		case TreeConfig.ID_MONSTER_AI:
			list = DataDao.commGetList(SQL.SEARCHBY_MONSTER_AI_ID, p);
			break;
		case TreeConfig.ID_SCRIPT:
			list = DataDao.commGetList(SQL.SEARCHBY_SCRIPT_ID, p);
			break;
		case TreeConfig.ID_SKILL:
			list = DataDao.commGetList(SQL.SEARCHBY_SKILL_ID, p);
			break;
		case TreeConfig.ID_BUFF:
			list = DataDao.commGetList(SQL.SEARCHBY_BUFF_ID, p);
			break;

		default:
			break;
		}
		updateRessultTable(rootType, list);
	}
	
	private void searchByName(String str) {
		int rootType = getSelectedRootType();
		if (rootType == -1) {
			return;
		}
		String p = str == null || str.length() <= 0 ? "%" : "%" + str + "%";
		List<Object[]> list = null;
		switch (rootType) {
		case TreeConfig.ID_GOODS:
			list = DataDao.commGetList(SQL.SEARCHBY_GOODS_NAME, p);
			break;
		case TreeConfig.ID_TASK:
			list = DataDao.commGetList(SQL.SEARCHBY_TASK_NAME, p);
			break;
		case TreeConfig.ID_MONSTER:
			list = DataDao.commGetList(SQL.SEARCHBY_MONSTER_NAME, p);
			break;
		case TreeConfig.ID_MONSTER_DEPLOY:
			list = DataDao.commGetList(SQL.SEARCHBY_MONSTER_DEPLOY_NAME, p);
			break;
		case TreeConfig.ID_DRAMA:
			list = DataDao.commGetList(SQL.SEARCHBY_DRAMA_NAME, p);
			break;
		case TreeConfig.ID_NPC:
			list = DataDao.commGetList(SQL.SEARCHBY_NPC_NAME, p);
			break;
		case TreeConfig.ID_SCENE:
			list = DataDao.commGetList(SQL.SEARCHBY_SCENE_NAME, p);
			break;
		case TreeConfig.ID_PENDANT:
			list = DataDao.commGetList(SQL.SEARCHBY_PENDANT_NAME, p);
			break;
		case TreeConfig.ID_UNDERCITY:
			list = DataDao.commGetList(SQL.SEARCHBY_UNDERCITY_NAME, p);
			break;
		case TreeConfig.ID_MONSTER_AI:
			list = DataDao.commGetList(SQL.SEARCHBY_MONSTER_AI_NAME, p);
			break;
		case TreeConfig.ID_SCRIPT:
			list = DataDao.commGetList(SQL.SEARCHBY_SCRIPT_NAME, p);
			break;
		case TreeConfig.ID_SKILL:
			list = DataDao.commGetList(SQL.SEARCHBY_SKILL_NAME, p);
			break;
		case TreeConfig.ID_BUFF:
			list = DataDao.commGetList(SQL.SEARCHBY_BUFF_NAME, p);
			break;
			
		default:
			break;
		}
		updateRessultTable(rootType, list);
	}
	
	public void updateRessultTable(int type, List<Object[]> list) {
		resultTable.refresh(type, list);
	}
	
	private void navTreeSelection(TreeSelectionEvent e) {
		if (e.getNewLeadSelectionPath() == null) {//防止点小图标收起/打开时报空指针
			return ;
		}
		if (tab.getSelectedIndex() != 0) {
			tab.setSelectedIndex(0);
		}
		Node node = (Node) ((DefaultMutableTreeNode) navTree.getLastSelectedPathComponent()).getUserObject();
		int selectNodeId = node.getId();
		int rootType = getSelectedRootType();
		List<Object[]> list = null;
		switch (rootType) {
		case TreeConfig.ID_GOODS://道具
			if (selectNodeId == 0 || selectNodeId == TreeConfig.ID_GOODS) {
				list = DataDao.commGetList(SQL.GET_ALL_GOODS);
			} else {
				list = DataDao.commGetList(SQL.SEARCHBY_GOODS_TYPE, TreeConfig.goodsTypeMap.get(selectNodeId));
			}
			break;
		case TreeConfig.ID_TASK://任务
			if (selectNodeId == TreeConfig.ID_TASK) {
				list = DataDao.commGetList(SQL.GET_ALL_TASK);
			} else if (selectNodeId >= TreeConfig.ID_TASK_MAIN && selectNodeId <= TreeConfig.ID_TASK_SECRET){
				list = DataDao.commGetList(SQL.GET_BY_TASK_TYPE, TreeConfig.taskTypeMap.get(selectNodeId));
			} else {
				list = DataDao.commGetList(SQL.GET_BY_TASK_CLASS, TreeConfig.taskTypeMap.get(selectNodeId));
			}
			break;
		case TreeConfig.ID_MONSTER:
			list = DataDao.commGetList(SQL.GET_ALL_MONSTER);
			break;
		case TreeConfig.ID_MONSTER_DEPLOY:
			list = DataDao.commGetList(SQL.GET_ALL_MONSTER_DEPLOY);
			break;
		case TreeConfig.ID_DRAMA:
			list = DataDao.commGetList(SQL.GET_ALL_DRAMA);
			break;
		case TreeConfig.ID_NPC:
			list = DataDao.commGetList(SQL.GET_ALL_NPC);
			break;
		case TreeConfig.ID_SCENE:
			list = DataDao.commGetList(SQL.GET_ALL_SCENE);
			break;
		case TreeConfig.ID_PENDANT:
			list = DataDao.commGetList(SQL.GET_ALL_PENDANT);
			break;
		case TreeConfig.ID_UNDERCITY:
			list = DataDao.commGetList(SQL.GET_ALL_UNDERCITY);
			break;
		case TreeConfig.ID_MONSTER_AI:
			list = DataDao.commGetList(SQL.GET_ALL_MONSTER_AI);
			break;
		case TreeConfig.ID_SCRIPT:
			list = DataDao.commGetList(SQL.GET_ALL_SCRIPT);
			break;
		case TreeConfig.ID_SKILL:
			if (selectNodeId == TreeConfig.ID_SKILL) {
				list = DataDao.commGetList(SQL.GET_ALL_SKILL);
			}  else {
				list = DataDao.commGetList(SQL.GET_BY_SKILL_VOCATIONID, TreeConfig.skillTypeMap.get(selectNodeId));
			}
			break;
		case TreeConfig.ID_BUFF:
			list = DataDao.commGetList(SQL.GET_ALL_BUFFL);
			break;
		default:
			break;
		}
		if (list != null) {
			updateRessultTable(rootType, list);
		}
	}
	

		
	
	public static void main(String[] args) {
		try {
//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
//			UIManager.setLookAndFeel("org.jvnet.substance.skin.SubstanceBusinessBlackSteelLookAndFeel");
//			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName());
			
			
			//BeautyEye
			UIManager.put("RootPane.setupButtonVisible", false);
			//开启/关闭窗口在不活动时的半透明效果 设置此开关量为false即表示关闭之，BeautyEye LNF中默认是true
			BeautyEyeLNFHelper.translucencyAtFrameInactive = false;
			 //设置本属性将改变窗口边框样式定义
	        BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.translucencyAppleLike;
			org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
		} catch (Exception e) {
			e.printStackTrace();
		}
		new MainFrame();
//		Core.runBoss();
	}

}
