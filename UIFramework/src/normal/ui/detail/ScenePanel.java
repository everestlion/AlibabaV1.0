package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.Scene;
import normal.ui.UIBuilder;

public class ScenePanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private Scene data;
	
	public static String layoutFile = "/layout/sceneLayout.xml";
	
	public ScenePanel(Scene data) {
		super(layoutFile);
		this.data = data;
		setValue();
	}
	
	public ScenePanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (Scene)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public Scene getData() {
		return data;
	}

	public void setData(Scene data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
