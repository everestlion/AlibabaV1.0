package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.TaskModel;
import normal.ui.UIBuilder;

public class TaskPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private TaskModel data;
	
	public static String layoutFile = "/layout/taskLayout.xml";
	
	public TaskPanel(TaskModel taskmodel) {
		super(layoutFile);
		this.data = taskmodel;
		setValue();
	}
	
	public TaskPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (TaskModel)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public TaskModel getData() {
		return data;
	}

	public void setData(TaskModel data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
