package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.Monster;
import normal.ui.UIBuilder;

public class MonsterPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private Monster data;
	
	public static String layoutFile = "/layout/monsterLayout.xml";
	
	public MonsterPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (Monster)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public Monster getData() {
		return data;
	}

	public void setData(Monster data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
