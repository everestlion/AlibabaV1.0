package normal.ui.detail;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import jdk.nashorn.internal.runtime.regexp.joni.SearchAlgorithm;

import com.sun.istack.internal.FinalArrayList;
import com.sun.org.apache.bcel.internal.generic.NEW;

import normal.data.DataDao;
import normal.data.SQL;
import normal.navigation.TreeConfig;
import normal.object.Npc;
import normal.ui.UIBuilder;

public class ConfigPanel extends JPanel{

	/** 
	 * 
	 */
	private static final long serialVersionUID = 5846521325760476085L;

	public static String layoutFile = "/layout/configLayout.xml";
	private JSplitPane vSplitPane;
	private JSplitPane hSplitPane;
	
	private JPanel rightPanel;
	private JPanel leftPanel;
	
	private JPanel leftUpPanel;
	private JPanel leftDownPanel;
	
	private static final String[] models = {"请选择模板", "模板一", "模板二", "模板三", "模板四"};
	private JComboBox modelChooser;
	
	private JTextArea modelTextArea;
	
	private JButton addButton = new JButton("加");
	
	private List<Box> itemBoxList = new ArrayList<Box>();
	
	private Box leftDownBox = Box.createVerticalBox();
	
	private List<Object[]> list = new ArrayList<Object[]>();
	
	public ConfigPanel(int width, int hight) {
//		this.setPreferredSize(new Dimension(width, hight));
		modelChooser = new JComboBox(models);
		modelChooser.setEditable(true);
		rightPanel = new JPanel();
		leftPanel = new JPanel();
		rightPanel.setPreferredSize(new Dimension(width - 750, hight + 85));
		leftUpPanel = new JPanel();
		Box box = Box.createVerticalBox();
		box.add(modelChooser);
		modelTextArea = new JTextArea(8, 30);
		box.add(modelTextArea);
		leftUpPanel.add(box);
		leftDownPanel = new JPanel();
		leftDownPanel.add(leftDownBox);
		vSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, leftUpPanel, leftDownPanel);
		vSplitPane.setDividerLocation(200);
		hSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, vSplitPane, rightPanel);
		hSplitPane.setDividerLocation(300);
		
		add(hSplitPane);
		initListener();
		initLeftDow();
	}
	
	
	
	private void initListener() {
		modelChooser.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				modelTextArea.setText(getTemplate(modelChooser.getSelectedIndex()));
			}
		});
		
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				addMoreItem();
			}
		});
		
	}



	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public static String getTemplate(int index) {
		String tempString = null;
		switch (index) {
		case 0:
			tempString = "说明";
			break;
		case 1: // 模版一
			tempString = "<config>\r\n"
					+ "<id>@id</id>\r\n"
					+ "<rootId></rootId>\r\n"
					+ "<typeId>@typeId</typeId>\r\n"
					+ "<use></use>\r\n"
					+ "<title></title>\r\n"
					+ "<subPops>[{\"goodsId\":@goodsId,\"num\":@num,\"bind\":1}]</subPops>\r\n"
					+ "</config>\r\n";
			break;
		case 2: // 模版二
			tempString = "{{@goodsId, @num}}";
			break;
		case 3: // 模版二
			tempString = "{{@goodsId, @num}}";
			break;
		case 4:
			tempString = "{@goodsId}";
			break;

		default:
			break;
		}
		return tempString;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	public void setComboBox(final JTextField nameField, final JTextField idField) {
		final DefaultComboBoxModel model = new DefaultComboBoxModel();
		final JComboBox cbInput = new JComboBox(model) {
			public Dimension getPreferredSize() {
				return new Dimension(super.getPreferredSize().width, 0);
			}
		};
		setAdjusting(cbInput, false);
		getAllList();
		for (Object[] item : list) {
			if (item[1] != null) {
				model.addElement(item[1]);
			}
		}
		cbInput.setSelectedItem(null);
		cbInput.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!isAdjusting(cbInput)) {
					if (cbInput.getSelectedItem() != null) {
						String name = cbInput.getSelectedItem().toString();
						nameField.setText(name);
						for (Object[] item : list) {
							if (item[0] != null && item[1].toString().toLowerCase().equals(name)) {
								idField.setText(item[0].toString());
							}
						}
					}
				}
			}
		});
		
		nameField.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				cbInput.setPopupVisible(false);
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				cbInput.setPopupVisible(true);
				
			}
		});

		nameField.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				setAdjusting(cbInput, true);
				if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					if (cbInput.isPopupVisible()) {
						e.setKeyCode(KeyEvent.VK_ENTER);
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN) {
					e.setSource(cbInput);
					cbInput.dispatchEvent(e);
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						nameField.setText(cbInput.getSelectedItem().toString());
						cbInput.setPopupVisible(false);
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					cbInput.setPopupVisible(false);
				}
				setAdjusting(cbInput, false);
			}
		});
		nameField.getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {
				updateList();
			}

			public void removeUpdate(DocumentEvent e) {
				updateList();
			}

			public void changedUpdate(DocumentEvent e) {
				updateList();
			}

			private void updateList() {
				cbInput.setPopupVisible(false);
				setAdjusting(cbInput, true);
				model.removeAllElements();
				String input = nameField.getText();
				for (Object[] item : list) {
					if (item[1] != null && (item[1].toString().toLowerCase().startsWith(input) || input.isEmpty())) {
						model.addElement(item[1]);
					}
				}
				cbInput.validate();
				cbInput.setPopupVisible(model.getSize() > 0);
				setAdjusting(cbInput, false);
			}
		});
		nameField.setLayout(new BorderLayout());
		nameField.add(cbInput, BorderLayout.SOUTH);
	}
	
	private static boolean isAdjusting(JComboBox cbInput) {
		if (cbInput.getClientProperty("is_adjusting") instanceof Boolean) {
			return (Boolean) cbInput.getClientProperty("is_adjusting");
		}
		return false;
	}

	private static void setAdjusting(JComboBox cbInput, boolean adjusting) {
		cbInput.putClientProperty("is_adjusting", adjusting);
	}
	
	public void addMoreItem () {
		leftDownBox.add(Box.createVerticalStrut(10));
		Box box = Box.createHorizontalBox();
		JTextField txtInput = new JTextField(20);
		
		box.add(txtInput);
		box.add(Box.createHorizontalStrut(5));
		JTextField idTextField = new JTextField(10);
		box.add(idTextField);
		box.add(Box.createHorizontalStrut(5));
		box.add(new JTextField(5));
		box.add(Box.createHorizontalStrut(5));
		setComboBox(txtInput, idTextField);
		box.add(addButton);
		if (itemBoxList.size() > 0) {
			itemBoxList.get(itemBoxList.size() - 1).remove(addButton);
		}
		itemBoxList.add(box);
		leftDownBox.add(box);
		System.out.println(leftDownBox.getComponentCount());
		leftDownPanel.validate();
	}
	
	
	
	private void getAllList() {
		if (list == null || list.isEmpty()) {
			list = DataDao.commGetList(SQL.SEARCHBY_GOODS_NAME, "%");
		}
	}
	
	public void initLeftDow() {
		leftDownBox.add(Box.createVerticalStrut(10));
		Box box = Box.createHorizontalBox();
		box.add(new JLabel("名称"));
		box.add(Box.createHorizontalStrut(5));
		box.add(new JLabel("ID"));
		box.add(Box.createHorizontalStrut(5));
		box.add(new JLabel("数量"));
		box.add(Box.createHorizontalStrut(5));
		leftDownBox.add(box);
		addMoreItem();
	}
}
