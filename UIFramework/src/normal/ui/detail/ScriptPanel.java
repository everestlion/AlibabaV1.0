package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.Script;
import normal.ui.UIBuilder;

public class ScriptPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private Script data;
	
	public static String layoutFile = "/layout/scriptLayout.xml";
	
	public ScriptPanel(Script data) {
		super(layoutFile);
		this.data = data;
		setValue();
	}
	
	public ScriptPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (Script)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public Script getData() {
		return data;
	}

	public void setData(Script data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
