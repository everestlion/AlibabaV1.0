package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.GoodsMode;

public class GoodsModePanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private GoodsMode data;
	
	public static String layoutFile = "/layout/goodsModeLayout.xml";
	
	public GoodsModePanel() {
		super(layoutFile);
	}
	
	
	public void updateData(Object data) {
		this.data = (GoodsMode)data;
		setValue();
	}

	public GoodsMode getData() {
		return data;
	}

	public void setData(GoodsMode data) {
		this.data = data;
	}

	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}

	@Override
	public Object getVal(Field field) {
		try {
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean haveData() {
		return data != null;
	}

}
