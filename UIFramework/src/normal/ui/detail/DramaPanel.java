package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.Drama;
import normal.ui.UIBuilder;

public class DramaPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private Drama data;
	
	public static String layoutFile = "/layout/dramaLayout.xml";
	
	public DramaPanel(Drama data) {
		super(layoutFile);
		this.data = data;
		setValue();
	}
	
	public DramaPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (Drama)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public Drama getData() {
		return data;
	}

	public void setData(Drama data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
