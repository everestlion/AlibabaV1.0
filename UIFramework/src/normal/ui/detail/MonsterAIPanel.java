package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.MonsterAI;
import normal.ui.UIBuilder;

public class MonsterAIPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private MonsterAI data;
	
	public static String layoutFile = "/layout/monsterAILayout.xml";
	
	public MonsterAIPanel(MonsterAI data) {
		super(layoutFile);
		this.data = data;
		setValue();
	}
	
	public MonsterAIPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (MonsterAI)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public MonsterAI getData() {
		return data;
	}

	public void setData(MonsterAI data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
