package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.Undercity;
import normal.ui.UIBuilder;

public class UndercityPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private Undercity data;
	
	public static String layoutFile = "/layout/undercityLayout.xml";
	
	public UndercityPanel(Undercity data) {
		super(layoutFile);
		this.data = data;
		setValue();
	}
	
	public UndercityPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (Undercity)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public Undercity getData() {
		return data;
	}

	public void setData(Undercity data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
