package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.SkillMode;
import normal.ui.UIBuilder;

public class SkillPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private SkillMode data;
	
	public static String layoutFile = "/layout/skillLayout.xml";
	
	public SkillPanel(SkillMode data) {
		super(layoutFile);
		this.data = data;
		setValue();
	}
	
	public SkillPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (SkillMode)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public SkillMode getData() {
		return data;
	}

	public void setData(SkillMode data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
