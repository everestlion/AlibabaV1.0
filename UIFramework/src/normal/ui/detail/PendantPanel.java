package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.Pendant;
import normal.ui.UIBuilder;

public class PendantPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private Pendant data;
	
	public static String layoutFile = "/layout/pendantLayout.xml";
	
	public PendantPanel(Pendant data) {
		super(layoutFile);
		this.data = data;
		setValue();
	}
	
	public PendantPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (Pendant)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public Pendant getData() {
		return data;
	}

	public void setData(Pendant data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			field.setAccessible(true);
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
