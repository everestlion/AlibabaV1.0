package normal.ui.detail;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;

import normal.component.MJComboBox;
import normal.ui.UIBuilder;

public abstract class DetailPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9178185836120592200L;
	
	public Map<String, JComponent> componentMap = new HashMap<String, JComponent>();
	
	public static Map<String, DetailPanel> panelModelMap = new HashMap<String, DetailPanel>(); 
	
	public Box leftBox = Box.createVerticalBox();
	public JButton editButton = new JButton("编辑");
	public JButton confinmButton = new JButton("确定");
	public JButton cancleButton = new JButton("取消");
	
	TitledBorder titledBorder = new TitledBorder("详细信息");
	
	public Box editBox = Box.createHorizontalBox();
	public DetailPanel (String layout) {
		createUI(layout);
		editBox.add(editButton);
		Box mainbBox = (Box) componentMap.get("mainBox");
		if (mainbBox != null) {
			leftBox = mainbBox;
		} else {
			this.add(leftBox);
		}
		leftBox.setBorder(titledBorder);
		leftBox.add(editBox);
		initListener();
//		this.setLayout(new BorderLayout());
		if (!panelModelMap.containsKey(layout)) {
			panelModelMap.put(layout, this);
		}
	}
	
	public void initListener() {
		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (JComponent c : componentMap.values()) {
					if (c instanceof JTextComponent) {
						((JTextComponent)c).setEditable(true);
					} else if (c instanceof JComboBox) {
						((JComboBox)c).setEditable(true);
					}
					c.setForeground(Color.BLACK);
				}
				editBox.removeAll();
				editBox.add(confinmButton);
				editBox.add(cancleButton);
				leftBox.validate();
			}
		});
		
		confinmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (JComponent c : componentMap.values()) {
					if (c instanceof JTextComponent) {
						((JTextComponent)c).setEditable(false);
					} else if (c instanceof JComboBox) {
						((JComboBox)c).setEditable(false);
					}
					c.setForeground(Color.GRAY);
				}
				editBox.removeAll();
				editBox.add(editButton);
				leftBox.validate();
			}
		});
		cancleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				for (JComponent c : componentMap.values()) {
					if (c instanceof JTextComponent) {
						((JTextComponent)c).setEditable(false);
					} else if (c instanceof JComboBox) {
						((JComboBox)c).setEditable(false);
					}
					c.setForeground(Color.GRAY);
				}
				editBox.removeAll();
				editBox.add(editButton);
				leftBox.validate();
			}
		});
	}
	
	public void setValue() {
		if (!this.haveData()) {
			System.out.println("err no data");
			return;
		}
		Component component = null;
		Class<?> clazz = null;
		Object val = null;
		for (Field f : this.getFields()) {
			if (componentMap.containsKey(f.getName())) {
				component = componentMap.get(f.getName());
				clazz = f.getType();
				try {
					f.setAccessible(true);
					val = this.getVal(f);
					if (component instanceof JTextField) {
						((JTextField)component).setText(val == null ? "" : String.valueOf(val));
					} else if (component instanceof JTextArea) {
						((JTextArea)component).setText(val == null ? "" : String.valueOf(val));
					} else if (component instanceof MJComboBox) {
						MJComboBox comboBox = (MJComboBox) component;
						int value = 0;
						if (clazz.isAssignableFrom(Boolean.class) || clazz.isAssignableFrom(boolean.class)) {
							value = (Boolean)val ? 1 : 0;
						} else if (clazz.isAssignableFrom(Integer.class) || clazz.isAssignableFrom(int.class)) {
							value = (Integer)val;
						}
						comboBox.setSelectedIndex(comboBox.getValueIndex(value));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXml(layoutFile, this);
		}
	}
	
	public void updatePanel() {
	}
	
	public void addOne(JComponent jLabel, JComponent content) {
//		leftBox.add(Box.createVerticalStrut(10));
		JPanel box = new JPanel();
//		Box box = Box.createHorizontalBox();
		box.add(jLabel);
		box.add(Box.createHorizontalStrut(10));
		box.add(content);
		box.add(Box.createHorizontalGlue());
		leftBox.add(box);
		componentMap.put(content.getName(), content);
	}
	
	public static DetailPanel getPanelModel(String modelName) {
		return panelModelMap.get(modelName);
	}
	
	public abstract void updateData(Object data);
	
	public abstract Field[] getFields();
	
	public abstract Object getVal(Field field);
	public abstract boolean haveData();
	
	public void print() {
		for (Component component : leftBox.getComponents()) {
			if (component instanceof Box) {
				for (Component e : ((Box)component).getComponents()) {
					System.out.println(e.getName());
				}
			} else {
				System.out.println(component.getName());
			}
		}
	}

	public void addToMap(String name, JComponent component) {
		if (componentMap.containsKey(name)) {
			System.out.println("err duplicated component name..." + name);
		} else {
			componentMap.put(name, component);
		}
	}

}
