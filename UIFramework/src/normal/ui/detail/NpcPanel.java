package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.Npc;
import normal.ui.UIBuilder;

public class NpcPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private Npc data;
	
	public static String layoutFile = "/layout/npcLayout.xml";
	
	public NpcPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (Npc)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public Npc getData() {
		return data;
	}

	public void setData(Npc data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
