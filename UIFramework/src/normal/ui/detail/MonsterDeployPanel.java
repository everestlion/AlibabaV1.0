package normal.ui.detail;

import java.lang.reflect.Field;

import normal.object.MonsterDeploy;
import normal.ui.UIBuilder;

public class MonsterDeployPanel extends DetailPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400882114874631105L;
	
	private MonsterDeploy data;
	
	public static String layoutFile = "/layout/monsterDeployLayout.xml";
	
	public MonsterDeployPanel() {
		super(layoutFile);
	}
	
	public void updateData(Object data) {
		this.data = (MonsterDeploy)data;
		setValue();
	}
	
	public void createUI(String layoutFile) {
		if (layoutFile != null) {
			UIBuilder.processXmlx(layoutFile, this);
		}
	}

	public MonsterDeploy getData() {
		return data;
	}

	public void setData(MonsterDeploy data) {
		this.data = data;
	}


	@Override
	public Field[] getFields() {
		return data.getClass().getDeclaredFields();
	}


	@Override
	public Object getVal(Field field) {
		try {
			return field.get(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public boolean haveData() {
		return data != null;
	}


}
