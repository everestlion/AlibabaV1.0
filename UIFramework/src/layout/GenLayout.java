package layout;

import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class GenLayout {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		parseXML("/layout/HTML_monster.xml");
//		parseXML("/layout/HTML_task.xml");
//		parseXML("/layout/HTML_monsterDeploy.xml");
//		parseXML("/layout/HTML_drama.xml");
//		parseXML("/layout/HTML_npc.xml");
//		parseXML("/layout/HTML_scene.xml");
//		parseXML("/layout/HTML_pendant.xml");
//		parseXML("/layout/HTML_undercity.xml");
//		parseXML("/layout/HTML_monster_ai.xml");
//		parseXML("/layout/HTML_script.xml");
//		parseXML("/layout/HTML_skill.xml");
		parseXML("/layout/HTML_buff.xml");
		
	}
	
	@SuppressWarnings("unchecked")
	public static void parseXML(String filePath) {
		System.out.println(filePath);
		SAXReader reader = new SAXReader();
		Document document = null;
		try {
			reader.setEncoding("utf8");
			document = reader.read(GenLayout.class.getResource(filePath).getFile());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (document == null) {
			System.out.println("parse " + filePath + " err");
			return;
		}
		StringBuilder sb = new StringBuilder();
		Element rootElement = document.getRootElement();
		if (rootElement.hasContent() && rootElement.hasMixedContent()) {
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			sb.append("<Box orientation=\"Vertical\" vGap=\"5\" name=\"mainBox\">");
			Iterator<Element> iterator = rootElement.elementIterator();
			while (iterator.hasNext()) {
				
				Element element = iterator.next();
				Iterator<Element> subs = element.elementIterator();
				sb.append("<Box orientation=\"Horizontal\" hGap=\"10\">");
				while(subs.hasNext()) {
					Element sub = subs.next();
					if (sub.getNodeType() == Node.ELEMENT_NODE) {
						if (sub.getName().equals("dt")) {
							sb.append("<JLabel><text>");
							sb.append(sub.getTextTrim().replace("：", ""));
							sb.append("</text></JLabel>");
						} else if (sub.getName().equals("dd")) {
							Iterator<Element> childs = sub.elementIterator();
							while(childs.hasNext()) {
								Element child = childs.next();
								if (child.getNodeType() == Node.ELEMENT_NODE) {
									if (child.getName().equals("input")) {
										if (child.attributeValue("type").equals("text")) {
											sb.append("<JTextField name=\"");
											sb.append(child.attributeValue("id"));
											sb.append("\" size=\"50\" editable=\"false\"></JTextField>");
										} else if (child.attributeValue("type").equals("radio")) {
											sb.append("<JComboBox name=\"");
											sb.append(child.attributeValue("name"));
											sb.append("\" editable=\"false\">");
											sb.append("<items>否,是</items>");
											sb.append("<itemsValues>0,1</itemsValues>");
											sb.append("</JComboBox>");
										}
									}else if (child.getName().equals("textarea")) {
										sb.append("<JTextArea name=\"");
										sb.append(child.attributeValue("id"));
										sb.append("\" rows=\"4\" columns=\"10\" editable=\"false\"></JTextArea>");
									}else if (child.getName().equals("select")) {
										Iterator<Element> options = child.elementIterator();
										StringBuilder itemSB = new StringBuilder();
										StringBuilder itemValuesSB = new StringBuilder();
										while(options.hasNext()) {
											Element option = options.next();
											if (option.getNodeType() == Node.ELEMENT_NODE) {
												if (option.getName().equals("option")) {
													if (itemSB.length() > 0) {
														itemSB.append(",");
													}
													if (itemValuesSB.length() > 0) {
														itemValuesSB.append(",");
													}
													itemSB.append(option.getTextTrim());
													String value = option.attributeValue("value");
													itemValuesSB.append(value == null || value.length() <= 0 ? -1 : value);
												}
											}
										}
										sb.append("<JComboBox name=\"");
										sb.append(child.attributeValue("id"));
										sb.append("\" editable=\"false\">");
										sb.append("<items>");
										sb.append(itemSB.toString());
										sb.append("</items>");
										sb.append("<itemsValues>");
										sb.append(itemValuesSB.toString());
										sb.append("</itemsValues>");
										sb.append("</JComboBox>");
									} else if (child.getName().equals("label")) {
										sb.append("<JLabel><text>");
										sb.append(child.getTextTrim());
										sb.append("</text></JLabel>");
									}
								}
							}
						}
					}
				}
				sb.append("</Box>");
			}
			sb.append("</Box>");
		}
		System.out.println(sb.toString());
		System.out.println("======================================================");
	}

}
