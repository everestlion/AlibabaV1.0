package simple;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MsgFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2277863999978335853L;
	
	private JLabel msgLabel = new JLabel();
	
	private Box mainBox = Box.createHorizontalBox();
	
	public MsgFrame() {
		this.setTitle("提示");
		this.setSize(200, 100);
		mainBox.add(Box.createHorizontalStrut(10));
		mainBox.add(msgLabel);
		mainBox.add(Box.createHorizontalStrut(10));
		this.add(mainBox);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screen.width/4, screen.height/4);
//		this.pack();
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				setVisible(false);
			}
		});
	}
	
	public void setMsg(String msg) {
		msgLabel.setText(msg);
	}
	

}
