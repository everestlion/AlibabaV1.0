package simple;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.mysql.jdbc.PreparedStatement;

import db.JDBC;

class MainFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7846917601936632570L;
	
	public static final int GAME_WIDTH = 640;
	public static final int GAME_HEIGHT = 480;
	
	private Box nameBox = Box.createHorizontalBox();
	
	private JLabel nameLabel = new JLabel("模版：");
	
	private JButton BUTTON_OK = new JButton("OK");
	
	private Box configBox = Box.createHorizontalBox();
	
	private Box configTextBox = Box.createVerticalBox();
	private JLabel configLabel = new JLabel("配置文本");
	private JTextArea configArea = new JTextArea(10, 20);
	
	private Box exampleBox = Box.createVerticalBox();
	private JLabel exampleLabel = new JLabel("模版文本");
	private JTextArea exampleArea = new JTextArea(10, 20);
	
	private Box resultBox = Box.createVerticalBox();
	private JLabel resultLabel = new JLabel("生成文本");
	private JTextArea resultArea = new JTextArea(30, 40);
	Object[] types = {"说明", "模版一", "模版二", "模版三", "自定义"};
	private JComboBox choose = new JComboBox(types);
	
	private JScrollPane scroll=new JScrollPane(resultArea);
	
	private Box mainBox = Box.createVerticalBox();
	
	private int chooseIndex = 0;
	
	private MsgFrame msgFrame;
	
	public MainFrame () {
		
		msgFrame = new MsgFrame();
		this.setTitle("Tools Tool");
		setSize(GAME_WIDTH,GAME_HEIGHT);
		setLocation(400,20);
		this.setBackground(Color.gray);
		this.setResizable(false);
		configArea.setLineWrap(true);
		nameBox.add(Box.createHorizontalStrut(10));
		nameBox.add(nameLabel);
		nameBox.add(Box.createHorizontalStrut(10));
		nameBox.add(choose);
		nameBox.add(Box.createHorizontalStrut(10));
		nameBox.add(BUTTON_OK);
		
		configArea.setLineWrap(true);
		exampleArea.setLineWrap(true);
		resultArea.setLineWrap(true);
		
		configTextBox.add(Box.createVerticalStrut(10));
		configTextBox.add(configLabel);
		configTextBox.add(Box.createVerticalStrut(10));
		configTextBox.add(configArea);
		configTextBox.add(Box.createVerticalStrut(10));
		
		exampleBox.add(Box.createVerticalStrut(10));
		exampleBox.add(exampleLabel);
		exampleBox.add(Box.createVerticalStrut(10));
		exampleBox.add(exampleArea);
		exampleBox.add(Box.createVerticalStrut(10));
		
		configBox.add(Box.createHorizontalStrut(10));
		configBox.add(exampleBox);
		configBox.add(Box.createHorizontalStrut(20));
		configBox.add(configTextBox);
		configBox.add(Box.createHorizontalStrut(10));
		
		resultBox.add(Box.createVerticalStrut(10));
		resultBox.add(resultLabel);
		resultBox.add(Box.createVerticalStrut(10));
		resultBox.add(scroll);
		resultBox.add(Box.createVerticalStrut(10));
		
		mainBox.add(Box.createVerticalStrut(10));
		mainBox.add(nameBox);
		mainBox.add(Box.createVerticalStrut(10));
		mainBox.add(configBox);
		mainBox.add(Box.createVerticalStrut(10));
		mainBox.add(resultBox);
		mainBox.add(Box.createVerticalStrut(10));
		
		
		//分别设置水平和垂直滚动条自动出现 
		//scroll.setHorizontalScrollBarPolicy( 
		//JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
		//scroll.setVerticalScrollBarPolicy( 
		//JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED); 
		 
		//分别设置水平和垂直滚动条总是出现 
		//scroll.setHorizontalScrollBarPolicy( 
		//JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
		scroll.setVerticalScrollBarPolicy( 
		JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); 
		
		this.add(mainBox);
		this.pack();
//		this.addKeyListener(new MyKeyListener(this));
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JDBC.close();
				System.exit(0);
			}
		});
		
		BUTTON_OK.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				process();
			}
		});
		
		choose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseIndex = choose.getSelectedIndex();
				switch (chooseIndex) {
				case 0:
					exampleArea.setText("说明");
					break;
				case 1: // 模版一
					exampleArea.setText("<config>\r\n"
							+ "<id>@id</id>\r\n"
							+ "<rootId></rootId>\r\n"
							+ "<typeId>@typeId</typeId>\r\n"
							+ "<use></use>\r\n"
							+ "<title></title>\r\n"
							+ "<subPops>[{\"goodsId\":@goodsId,\"num\":@num,\"bind\":1}]</subPops>\r\n"
							+ "</config>\r\n");
					break;
				case 2: // 模版二
					exampleArea.setText("{{@goodsId, @num}}");
					break;
				case 3: // 模版二
					exampleArea.setText("{{@goodsId, @num}}");
					break;
				case 4:
					exampleArea.setText("{@goodsId}");
					break;

				default:
					break;
				}
			}
		});
	
	}
	
	// 生成操作
	public void process () {
		String exampleText = getExampleText();
		if (exampleText == null || exampleText.equals("")) {
			return;
		}
		String configText = getConfigText();
		if (configText == null || configText.equals("")) {
			return;
		}
		String[] every = configText.split("\\|\\|");
		
		if (every.length <= 0 || !exampleText.contains("@")) {
			System.out.println("exampleText no @");
			return;
		}
		StringBuffer resultSB = new StringBuffer();
		String sql = "select goodsId from kfsys_goods_mode where name like ?";
		try {
			PreparedStatement ps = (PreparedStatement) JDBC.getConn().prepareStatement(sql);
			StringBuffer sb = new StringBuffer();
			StringBuffer warnSb = new StringBuffer();
			String pattern = getPattern(exampleText);
			int rootId = 0;
			if (chooseIndex == 1) {
				String rootIdStr = exampleText.substring(exampleText.indexOf("<rootId>") + 8, exampleText.indexOf("</rootId>"));
				try {
					rootId = Integer.parseInt(rootIdStr) * 100;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			StringBuffer finalResult = new StringBuffer();
			int typeId = 0;
			for (String e : every) {
				String[] items = e.split("、");
				String oneConfig = new String(exampleText);
				if (chooseIndex == 1) {
					if (rootId > 0) {
						typeId ++;
						rootId ++;
						oneConfig = oneConfig.replace("@id", rootId + "");
						oneConfig = oneConfig.replace("@typeId", typeId + "");
					} else {
						oneConfig = oneConfig.replace("@id", "");
						oneConfig = oneConfig.replace("@typeId", "");
						
					}
				}
				for (String ee : items) {
					String num = "";
					if (ee.contains("x")) {
						String[] arr = ee.split("x");
						num = arr[1];
						ps.setString(1, arr[0]);
					} else {
						ps.setString(1, ee);
					}
					ResultSet rs = ps.executeQuery();
					if (rs == null || !rs.next()) {
						System.out.println(ee + " err none");
						warnSb.append(ee).append(",");
						continue;
					}
					String one = new String(pattern);
					if (one.contains("@goodsId")) {
						one = one.replaceAll("@goodsId", rs.getInt(1)+"");
					}
					if (one.contains("@num")) {
						one = one.replaceAll("@num", num);
					}
					if (one.contains("@2")) {
						one = one.replaceAll("@2", 1 + "");
					}
					if (sb.length() > 0) {
						if (chooseIndex == 1) {
							sb.append(", ");
						} else if (chooseIndex == 2) {
							sb.append("}, {");
						}
					} 
					sb.append(one);
					if (choose.getSelectedIndex() == 0) {
						resultSB.append("\r\n");
					} else {
						resultSB.append(",");
					}
				}
				finalResult.append(oneConfig.replace(pattern, sb.toString()));
			}
//			resultSB.replace(exampleText.indexOf(">["), exampleText.indexOf("]<"), sb.toString());
			this.resultArea.setText(finalResult.toString());
			msg(warnSb.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getPattern(String exampleText) {
		String pattern = null;
		switch (choose.getSelectedIndex()) {
		case 1:
			pattern = exampleText.substring(exampleText.indexOf(">[") + 2, exampleText.indexOf("]<"));
			break;
		case 2:
			pattern = exampleText.substring(exampleText.indexOf("{{") + 2, exampleText.indexOf("}}"));
			break;
		case 3:
			pattern = exampleText.substring(exampleText.indexOf("{") + 1, exampleText.indexOf("}"));
			break;

		default:
			break;
		}
		return pattern;
	}
	
	public void msg (String msg) {
		msgFrame.setMsg(createMsg(msg));
		msgFrame.setVisible(true);
	}
	
	public String createMsg(String msg) {
		String cMsg = "";
		if (msg == null || msg.length() <= 0) {
			cMsg = "生成成功！";
		} else {
			cMsg = msg + "配置有误！！";
		}
		return cMsg;
	}
	
	public String getConfigText() {
		return configArea.getText();
	}
	
	public String getExampleText() {
		return exampleArea.getText();
	}
	
	public static void main(String[] args) {
		MainFrame mainFrame = new MainFrame();
		mainFrame.setVisible(true);
	}

}
