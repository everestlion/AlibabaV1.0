package simple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.ResultSet;

import com.mysql.jdbc.PreparedStatement;

import db.JDBC;

public class CommTools {
	
	private static final String filePath = "E:\\test\\config.txt";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int type = 4;
		switch (type) {
		case 1://处理列格式数据
			System.out.println(processColumnData());
			break;
		case 2://矩阵数据
			System.out.println(processMatrixDataString());
			break;
		case 3://道具名称取id
			System.out.println(processGoodsConfig(true));
			break;
		case 4://道具id，数量
			System.out.println(processColumnData2());
			break;

		default:
			break;
		}
		
	}
	
	/**
	 * 处理列格式数据
	 * 
	 * @return
	 */
	public static String processColumnData() {
		StringBuffer sb = new StringBuffer();
		File file = new File(filePath);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String str = null;
			while ((str = br.readLine()) != null) {
				sb.append(str.trim()).append(", ");
			}
			sb.deleteCharAt(sb.lastIndexOf(","));
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return sb.toString().trim();
	}
	/**
	 * 处理列格式数据
	 * 
	 * @return
	 */
	public static String processColumnData2() {
		StringBuffer sb = new StringBuffer();
		File file = new File(filePath);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String str = null;
			sb.append("{");
			while ((str = br.readLine()) != null) {
				sb.append("{");
				String[] arr = str.split("，");
				sb.append(arr[0].trim()).append(", ").append(arr[1].trim());
				sb.append("}, ");
			}
			sb.append("}");
			sb.deleteCharAt(sb.lastIndexOf(","));
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return sb.toString().trim();
	}
	
	/**
	 * 道具名称取id
	 * 
	 * @return
	 */
	public static String processGoodsConfig(boolean withNum) {
		StringBuffer sb = new StringBuffer();
		File file = new File(filePath);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String str = null;
			String sql = "select goodsId from kfsys_goods_mode where name like ?";
			PreparedStatement ps = (PreparedStatement) JDBC.getConn().prepareStatement(sql);
			sb.append("{");
			while ((str = br.readLine()) != null) {
				if ("-".equals(str.trim())) {
					sb.deleteCharAt(sb.length() - 1);
					sb.append("}, {");
					continue;
				}
				String[] arr = str.split("x");
				ps.setString(1, arr[0]);
				ResultSet rs = ps.executeQuery();
				if (rs == null || !rs.next()) {
					System.out.println(str + " err none");
					continue;
				}
				sb.append("{").append(rs.getInt(1)).append(", ").append(arr[1].trim()).append("},");
//				System.out.println(rs.getInt(1));
				if (rs.next()) {
					System.out.println(str + " err more");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			JDBC.close();
		}
		return sb.toString();
	}
	
	/**
	 *  1440，1080，1080，360
		2880，2160，2160，720
		5760，	4320，	4320，	1440
		8640，	6480，	6480，	2160
		14400，	10800，	10800，	3600
		20160，	15120，	15120，	5040
		25920，	19440，19440，	6480
		34560，	25920，	25920，	8640
		43200，32400，	32400，	10800
		57600，	43200，	43200，	14400
	 */
	public static String processMatrixDataString () {
		StringBuffer sb = new StringBuffer();
		File file = new File(filePath);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String str = null;
			sb.append("{");
			while ((str = br.readLine()) != null) {
				str = str.trim();
				String[] arr = str.split("，");
				sb.append("{");
				for (String e : arr) {
					sb.append(e.trim()).append(", ");
				}
				sb.deleteCharAt(sb.lastIndexOf(","));
				sb.append("}, ");
			}
			sb.append("}");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return sb.toString();
	}

}
