package test;

/**
 * 介绍如何实现在 中添加进度条
 * @author GuoXiaoHe
 *
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JtableM implements Runnable {
	private JTable jt; //
	private JFrame jf; //
	private JScrollPane js; // 滚动面板；
	private DefaultTableModel dtm; // 默认表格数据模型；
	private TableColumnModel tcm;// 表格列模型；
	int count_0 = 10, count_1 = 50, count_2 = 80;

	public void show() {
		jf = new JFrame("embeded jtable");
		jf.setSize(500, 200);
		jt = new JTable();
		dtm = new MyTableModel();
		jt.setModel(dtm);
		tcm = jt.getColumnModel();
		// jt.setPreferredSize(newDimension(400,150));
		js = new JScrollPane(jt);
		jt.setSize(200, 100);
		js.setSize(400, 200);
		js.setViewportView(jt);
		jf.getContentPane().add(js, BorderLayout.CENTER);
		// 用于监听退出事件;
		jf.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		js.setVisible(true);
		this.table_init();
		// jf.pack();
		jf.setVisible(true);
	}

	private void table_init() {
		this.tcm.getColumn(1).setCellRenderer(new MyTableCellRenderProgressBar());
		// 设置第二列的渲染方式为一个；用来显示进度;
		this.dtm.addRow(new Object[] { "凤凰传奇", 50, "-250MB" });// 向表中添加数据;
		this.dtm.addRow(new Object[] { "F.I.R", 10, "-250MB" });
		this.dtm.addRow(new Object[] { "苏打绿", 80, "-250MB" });
	}

	public void run() {
		while (true) {
			try {
				this.dtm.setValueAt(count_0, 0, 1);// 更改进度条的值;
				this.dtm.setValueAt(count_1, 1, 1);
				this.dtm.setValueAt(count_2, 2, 1);
				count_0 += 1;
				count_1 += 3;
				count_2 += 2;
				if (count_0 > 100)
					count_0 = 0;
				if (count_1 > 100)
					count_1 = 0;
				if (count_2 > 100)
					count_2 = 0;
				Thread.sleep(100);
			} catch (Exception e) {
			}
		}
	}

	public static void main(String args[]) {
		JtableM jm = new JtableM();
		jm.show();
		Thread t = new Thread(jm);
		t.start();
	}
}

class MyTableModel extends DefaultTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2731142515696839969L;
	// 使用提供的默认数据模型，但是稍微的更改一下，主要是为了设定某些列可以不可以更改之类的
	boolean isEdit[] = { false, false, false };// 设置所有列都不可以更改;

	public MyTableModel() {
		super(null, new String[] { "文件名", "进度", "大小" });
	}

	public boolean isCellEditable(int row, int col) {
		return isEdit[col];
	}
}

class MyTableCellRenderProgressBar extends JProgressBar implements TableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1842062381261009886L;

	/**
	 * 用来显示的控件，这里直接继承了JprgressBar的实现，同时必须实现TableCellRenderer接口 用来更改单元格数据;
	 */

	public MyTableCellRenderProgressBar() {
		super(0, 100);
		this.setForeground(new Color(45, 147, 192));
		this.setStringPainted(true);
		this.setBorderPainted(false);
	}

	// 必须实现的接口;
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		this.setValue(Integer.parseInt(value.toString()));
		return this;
	}
}
