package test;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class TestFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8584754697916448398L;
	
	public TestFrame () {
		Fliter3GPanel mainPanel = new Fliter3GPanel();
//		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
//		MyComponent myComponent = new MyComponent(new Label("label"), new JTextField(10));
//		MyComponent myComponent2 = new MyComponent(new Label("label2"), new JTextField(10));
//		mainPanel.add(myComponent);
//		mainPanel.add(myComponent2);
		JScrollPane scrollPane = new JScrollPane(mainPanel);
		this.add(mainPanel);
		this.setPreferredSize(new Dimension(200, 200));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new TestFrame();
	}
}
