package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.ContextMenuProvider;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;

/**
 * This sample demnostrates technique of disabling context menu.
 *
 * @author Alexei Orischenko
 */
public class DisableContextMenuSample {
    public static void main(String[] args) throws Exception {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer");
        frame.setSize(700, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setVisible(true);

        ContextMenuProvider provider = new ContextMenuProvider() {
            public JPopupMenu getPopupMenu(Element contextElement) {
                // don't show any menu
                return null;
            }
        };

        browser.setContextMenuProvider(provider);
        browser.navigate("http://www.google.com");
    }
}