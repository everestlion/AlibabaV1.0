package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.DefaultWebBrowserEventsHandler;

import javax.swing.*;
import java.awt.*;

/**
 * This sample adds navigation post data listener.
 * <p/>
 * <p>postdata.asp test page:
 * <%@ Language="JScript"%>
 * <p/>
 * Post Data Length:
 * <%
 * var len = Request.TotalBytes;
 * Response.Write(len);
 * %>
 *
 * @author Alexei Orischenko
 */
public class NavigationPostDataSample {
    public static void main(String[] args) throws Exception {
        final Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Sample");
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setEventHandler(new DefaultWebBrowserEventsHandler() {
            public boolean beforeNavigate(WebBrowser webBrowser, String url, String targetFrameName, final String postData, String headers) {
                Runnable runnable = new Runnable() {
                    public void run() {
                        JOptionPane.showMessageDialog(browser, "postData: " + postData, "title", JOptionPane.INFORMATION_MESSAGE);
                    }
                };
                SwingUtilities.invokeLater(runnable);
                return false;
            }
        });

        JOptionPane.showMessageDialog(browser, "Navigation with post data", "JExplorer", JOptionPane.INFORMATION_MESSAGE);
        browser.navigate("http://localhost/jexplorer/postdata.asp", "name=Alex&product=Pizza");
    }
}