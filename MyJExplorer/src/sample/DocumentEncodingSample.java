package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.automation.types.BStr;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.Browsers;
import com.jniwrapper.win32.mshtml.IHTMLDocument2;
import com.jniwrapper.win32.mshtml.impl.IHTMLDocument2Impl;
import com.jniwrapper.win32.shdocvw.IWebBrowser2;

import javax.swing.*;

/**
 * This sample demonstrates technique of setting document encoding (charset).
 *
 * @author Alexei Orischenko
 */
public class DocumentEncodingSample {
    private static final String CHARSET_RUSSIAN = "windows-1251";

    public static void main(String[] args) throws Exception {
        final Browser browser = new Browser();

        JFrame frame = new JFrame("Document Encoding");
        frame.setContentPane(browser);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.navigate("http://www.google.com");
        browser.waitReady();

        browser.getOleMessageLoop().doInvokeAndWait(new Runnable() {
            public void run() {
                IWebBrowser2 browser2 = Browsers.getBrowserPeer(browser);
                IHTMLDocument2 document2 = new IHTMLDocument2Impl(browser2.getDocument());
                String charset = document2.getCharset().getValue();

                System.out.println("Current charset is " + charset +
                        "\r\nChange document charset to " + CHARSET_RUSSIAN);

                charset = CHARSET_RUSSIAN;
                document2.setCharset(new BStr(charset));
            }
        });
    }
}