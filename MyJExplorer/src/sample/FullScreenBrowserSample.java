package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.event.DefaultWebBrowserEventsHandler;
import com.jniwrapper.win32.ie.event.NewWindowEventHandler;
import com.jniwrapper.win32.ie.event.StatusEventAdapter;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * This sample creates browser added to a Swing container and demonstrates how to use onFullScreen event.
 *
 * @author Ikryanov Vladimir
 */
public class FullScreenBrowserSample {
    private static final String HTML_CONTENT =
            "<html>\n" +
                    "   <head>\n" +
                    "   <title>PopupTest - test fullscreen popup window</TITLE>\n" +
                    "   <script>\n" +
                    "   <!--\n" +
                    "       function fullwin(){window.open('http://www.google.com', 'Google', 'fullscreen,scrollbars')}\n" +
                    "   //-->\n" +
                    "   </script>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "   <A HREF='javascript:fullwin()'>Open Full Screen Window</A>\n" +
                    "</body>\n" +
                    "</html>";

    public static void main(String[] args) {
        final JFrame frame = new JFrame("JExplorer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final Browser browser = new Browser();
        frame.setContentPane(browser);
        frame.setSize(800, 600);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        browser.setNewWindowHandler(new NewWindowEventHandler() {
            public NewWindowAction newWindow() {
                final JFrame newWindow = new JFrame("JExplorer - Child Browser Window");
                final Browser newBrowser = new Browser(browser);

                newBrowser.setEventHandler(new DefaultWebBrowserEventsHandler() {
                    public boolean windowClosing(boolean isChildWindow) {
                        newWindow.setVisible(false);
                        newBrowser.setEnabled(false);
                        return false; // allow to close browser
                    }
                });

                newWindow.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        newWindow.removeWindowListener(this);
                        newWindow.remove(newBrowser);
                        newBrowser.close();
                    }
                });

                newBrowser.addStatusListener(new StatusEventAdapter() {
                    public void onFullScreen(boolean fullScreen) {
                        // maximization the browser window
                        newWindow.setExtendedState(newWindow.getExtendedState() | JFrame.MAXIMIZED_BOTH);
                    }
                });

                newWindow.getContentPane().add(newBrowser);

                NewWindowEventHandler.NewWindowAction result = new NewWindowEventHandler.NewWindowAction(NewWindowEventHandler.NewWindowAction.ACTION_REPLACE_BROWSER);
                result.setBrowser(newBrowser);
                newWindow.setVisible(true);

                return result;
            }
        });

        browser.setContent(HTML_CONTENT);
        browser.waitReady();
    }
}