package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.UInt;
import com.jniwrapper.UInt16;
import com.jniwrapper.UInt32;
import com.jniwrapper.win32.automation.IDispatch;
import com.jniwrapper.win32.automation.IDispatchEx;
import com.jniwrapper.win32.automation.impl.IDispatchExImpl;
import com.jniwrapper.win32.automation.types.*;
import com.jniwrapper.win32.com.ComException;
import com.jniwrapper.win32.com.types.IID;
import com.jniwrapper.win32.com.types.LocaleID;
import com.jniwrapper.win32.ie.HeadlessBrowser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.NavigationEventAdapter;
import com.jniwrapper.win32.mshtml.IHTMLDocument2;
import com.jniwrapper.win32.mshtml.IHTMLWindow2;
import com.jniwrapper.win32.mshtml.impl.IHTMLDocument2Impl;
import com.jniwrapper.win32.shdocvw.IWebBrowser2;

/**
 * The sample shows how to get the name and values of all JavaScript
 * variables for the currently loaded web page. In the result of
 * execution the names and values of all JavaScript variables will
 * be printed in the console in the following form:
 * <pre>
 *  memberName = ...
 *  memberValue = ...
 * </pre>
 *
 * @author Vladimir Ikryanov
 */
public class GetAllJavaScriptVariablesSample {
    private static final UInt16 DISPATCH_PROPERTYGET = new UInt16(0x2);
    private static final UInt32 ENUM_ALL = new UInt32(0x00000002);

    public static void main(String[] args) {
        HeadlessBrowser browser = new HeadlessBrowser();
        browser.navigate("http://www.google.com");
        browser.addNavigationListener(new NavigationEventAdapter() {
            public void entireDocumentCompleted(WebBrowser webBrowser, String url) {
                IWebBrowser2 webBrowser2 = (IWebBrowser2) webBrowser.getBrowserPeer();
                IDispatch document = webBrowser2.getDocument();
                document.setAutoDelete(false);
                IHTMLDocument2 document2 = new IHTMLDocument2Impl(document);
                document.release();
                IHTMLWindow2 window = document2.getParentWindow();
                IDispatchEx dispatch = new IDispatchExImpl(window);
                try {
                    // Enumerates the members of the JavaScript window object
                    DispID dispId = dispatch.getNextrDispID(ENUM_ALL, DispID.DISPID_UNKNOWN);
                    while (dispId != null) {
                        // Retrieves the name of a member
                        BStr memberName = dispatch.getMemberName(dispId);
                        System.out.println("memberName = " + memberName);

                        // Retrieves the value of a member
                        UInt error = new UInt();
                        Variant memberValue = new Variant();
                        ExcepInfo excepInfo = new ExcepInfo();
                        try {
                            dispatch.invoke(dispId, IID.IID_NULL,
                                    LocaleID.LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET,
                                    new DispParams(), memberValue, excepInfo, error);
                            System.out.println("memberValue = " + memberValue.getValue());
                        } catch (ComException e) {
                            System.out.println("Cannot get member value. The reason is: " +
                                    excepInfo.getBstrDescription());
                        }

                        // Retrieves the next member of the JavaScript window object
                        dispId = dispatch.getNextrDispID(ENUM_ALL, dispId);
                    }
                } catch (ComException e) {
                    System.out.println("Done.");
                }
            }
        });
    }
}