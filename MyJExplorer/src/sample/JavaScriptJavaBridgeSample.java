package sample;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.BrowserFunction;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The sample demonstrates how to invoke Java function
 * from JavaScript side using JavaScriptJava bridge.
 */
public class JavaScriptJavaBridgeSample {
    public static void main(String[] args) {
        final Browser browser = new Browser();

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                browser.close();
            }
        });
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.registerFunction("MyFunction", new BrowserFunction() {
            public Object invoke(Object... args) {
                for (Object arg : args) {
                    System.out.println("arg = " + arg);
                }
                return "MyFunction Result";
            }
        });

        browser.executeScript("alert(MyFunction('string', 100, 200.1));");
    }
}
