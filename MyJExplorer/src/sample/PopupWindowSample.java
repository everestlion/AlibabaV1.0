package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.BrowserOptions;
import com.jniwrapper.win32.ie.event.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The sample show how to configure popup window according to
 * the parameters that were passed into window.open JavaScript function.
 *
 * @author Ikryanov Vladimir
 */
public class PopupWindowSample {
    public static Browser previousBrowser;
    public static int count;

    public static void main(String[] args) {
        JFrame frame = new JFrame("JExplorer: PopupWindowSample");
        JPanel panel = new JPanel();
        JButton btn1 = new JButton("Launch JExplorer");
        //JButton btn2 = new JButton("Launch JExplorer2");
        
        /*MenuExp me = new MenuExp();
        me.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        me.setVisible(true);*/
        JMenuBar menuBar = new JMenuBar();
        
        // Add the menubar to the frame
        frame.setJMenuBar(menuBar);
        
        // Define and add two drop down menu to the menubar
        JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        
        // Create and add simple menu item to one of the drop down menu
        JMenuItem newAction = new JMenuItem("New");
        JMenuItem openAction = new JMenuItem("Open");
        JMenuItem exitAction = new JMenuItem("Exit");
        JMenuItem cutAction = new JMenuItem("Cut");
        JMenuItem copyAction = new JMenuItem("Copy");
        JMenuItem pasteAction = new JMenuItem("Paste");
        
        // Create and add CheckButton as a menu item to one of the drop down
        // menu
        JCheckBoxMenuItem checkAction = new JCheckBoxMenuItem("Check Action");
        // Create and add Radio Buttons as simple menu items to one of the drop
        // down menu
        JRadioButtonMenuItem radioAction1 = new JRadioButtonMenuItem(
                "Radio Button1");
        JRadioButtonMenuItem radioAction2 = new JRadioButtonMenuItem(
                "Radio Button2");
        // Create a ButtonGroup and add both radio Button to it. Only one radio
        // button in a ButtonGroup can be selected at a time.
        ButtonGroup bg = new ButtonGroup();
        bg.add(radioAction1);
        bg.add(radioAction2);
        fileMenu.add(newAction);
        fileMenu.add(openAction);
        fileMenu.add(checkAction);
        fileMenu.addSeparator();
        fileMenu.add(exitAction);
        editMenu.add(cutAction);
        editMenu.add(copyAction);
        editMenu.add(pasteAction);
        editMenu.addSeparator();
        editMenu.add(radioAction1);
        editMenu.add(radioAction2);
        // Add a listener to the New menu item. actionPerformed() method will
        // invoked, if user triggred this menu item
        newAction.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("You have clicked on the new action");
            }
        });
        
        btn1.addActionListener(new ActionListener() {
            Browser browser = null;// = new Browser();

            public void actionPerformed(ActionEvent e) {
                final JFrame frame = new JFrame("Window1");
                if (browser == null) {
                    browser = new Browser(new BrowserOptions(true));
                    browser.setNewWindowHandler(new DefaultNewWindowHandler(browser));
                    browser.setEventHandler(new DefaultWebBrowserEventsHandler() {
                    	public boolean windowClosing(boolean isChildWindow) {
                    		SwingUtilities.invokeLater(new Runnable() {
                    			public void run() {
                    				frame.dispose();
                    				browser.close();
                    			}
                    		});
                    		return true; // allows closing browser
                    	}
                    });
                    //browser.navigate("C:\\Documents and Settings\\pavanka\\Desktop\\JE_XT_116\\SoeDummy.html");
                    browser.navigate("C:\\Users\\oleg.zagrivyi\\Desktop\\GIS_View.html");

                    frame.setSize(700, 500);
                    frame.setLocationRelativeTo(null);
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.getContentPane().add(browser, BorderLayout.CENTER);
                    frame.setVisible(true);
                    //frame.toFront();
                    //frame.requestFocus();
                    frame.setAlwaysOnTop(true);
                } else {
                    browser.navigate("C:\\Users\\oleg.zagrivyi\\Desktop\\GIS_View.html");
                	//browser.navigate("C:\\Documents and Settings\\pavanka\\Desktop\\JE_XT_116\\SoeDummy.html");
                    //frame.toFront();
                    //frame.requestFocus();
                    frame.setAlwaysOnTop(true);
                }
            }
        });
        panel.add(btn1);
        //panel.add(btn2);
        frame.add(panel);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    /**
     * Represents default implementation of
     * {@link com.jniwrapper.win32.ie.event.NewWindowEventHandler}
     * for {@link com.jniwrapper.win32.ie.Browser} component.
     */
    public static class DefaultNewWindowHandler implements NewWindowEventHandler {

        private Browser parent;
        private WindowAdapter windowAdapter;

        DefaultNewWindowHandler(Browser browser) {
            parent = browser;
        }

        public NewWindowEventHandler.NewWindowAction newWindow() {
            final JFrame newWindow = new JFrame();
            newWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            final Browser newBrowser = new Browser(parent);
            final JPanel contentPane = new JPanel(new BorderLayout());

            newBrowser.setNewWindowHandler(this);
            newBrowser.addStatusListener(new StatusEventAdapter() {
                public void titleChanged(final String title) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            newWindow.setTitle(title);
                        }
                    });
                }
            });

            newBrowser.addBrowserWindowListener(new BrowserWindowAdapter() {
                public void onWindowResize(final BrowserWindowEvent event) {
                    Runnable runnable = new Runnable() {
                        public void run() {
                            Rectangle bounds = event.getWindowBounds();
                            contentPane.setPreferredSize(bounds.getSize());
                            newWindow.setLocation(bounds.getLocation());
                            newWindow.pack();
                        }
                    };
                    if (newWindow.isVisible()) {
                        SwingUtilities.invokeLater(runnable);
                    } else {
                        runnable.run();
                    }
                }

                public void onWindowResizable(final BrowserWindowEvent event) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            newWindow.setResizable(event.isWindowResizable());
                        }
                    });
                }

                public void onFullScreen(final BrowserWindowEvent event) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            boolean fullScreen = event.isFullScreen();
                            int state = fullScreen ? Frame.MAXIMIZED_BOTH : Frame.NORMAL;
                            newWindow.setExtendedState(state);
                        }
                    });
                }

                public void onVisible(BrowserWindowEvent event) {
                    newWindow.setVisible(event.isVisible());
                }
            });

            windowAdapter = new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    //newWindow.dispose();
                    if (newBrowser != null && !newBrowser.isClosed()) 
                    {
                        newBrowser.close();
                    }
                }
            };
            newWindow.addWindowListener(windowAdapter);

            newBrowser.setEventHandler(new DefaultWebBrowserEventsHandler() {
                public boolean windowClosing(boolean isChildWindow) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            newWindow.removeWindowListener(windowAdapter);
                            newWindow.dispose();
                            if (newBrowser != null && !newBrowser.isClosed()) {
                                newBrowser.close();
                            }
                        }
                    });
                    return true; // allows closing browser
                }
            });

            contentPane.add(newBrowser, BorderLayout.CENTER);
            newWindow.setSize(parent.getSize());
            newWindow.setContentPane(contentPane);

            NewWindowEventHandler.NewWindowAction result = new NewWindowEventHandler.NewWindowAction(
                    NewWindowEventHandler.NewWindowAction.ACTION_REPLACE_BROWSER);
            result.setBrowser(newBrowser);

            return result;
        }
    }

}