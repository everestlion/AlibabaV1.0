package sample;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.DisposeEvent;
import com.jniwrapper.win32.ie.event.DisposeListener;

import javax.swing.*;
import java.awt.*;

/**
 * The sample demonstrates how to register DisposeListener to receive
 * events when the Browser instance is disposed.
 */
public class DisposeBrowserSample {
    public static void main(String[] args) {
        WebBrowser browser = new Browser();

        JFrame frame = new JFrame("JExplorer - Create Browser Sample");
        frame.setSize(700, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().add((Component) browser, BorderLayout.CENTER);
        frame.setVisible(true);

        browser.addDisposeListener(new DisposeListener() {
            public void browserDisposed(DisposeEvent event) {
                System.out.println("Browser is disposed.");
            }
        });
        browser.navigate("http://www.google.com");
    }
}
