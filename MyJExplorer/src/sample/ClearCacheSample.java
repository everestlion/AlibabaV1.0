package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browsers;
import com.jniwrapper.win32.wininet.InternetCacheEntry;

/**
 * The sample demonstrates how to to delete all internet
 * temporary files, clear URL history and remove all cookies.
 *
 * @author Ikryanov Vladimir
 */
public class ClearCacheSample {
    public static void main(String[] args) {
        // removes all internet temporary files
        Browsers.clearCache(InternetCacheEntry.NORMAL_CACHE_ENTRY);
        // clears url history
        Browsers.clearCache(InternetCacheEntry.URLHISTORY_CACHE_ENTRY);
        // removes all stored cookies
        Browsers.clearCache(InternetCacheEntry.COOKIE_CACHE_ENTRY);
    }
}