package sample;
import com.jniwrapper.win32.ie.IEAutomation;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.NavigationEventAdapter;

import java.io.IOException;

/**
 * The sample demonstrates how to create IEAutomation instance that
 * represents a standard Internet Explorer application, register
 * listeners to it and work with this component.
 */
public class IEAutomationSample {
    public static void main(String[] args) throws IOException {
        IEAutomation automation = new IEAutomation();
        automation.setVisible(true);
        automation.addNavigationListener(new NavigationEventAdapter() {
            public void entireDocumentCompleted(WebBrowser webBrowser, String url) {
                System.out.println("url = " + url);
            }
        });
        automation.navigate("http://www.google.com");

        // do some other stuff with IEAutomation
        System.out.println("Press ENTER to close IEAutomation instance");
        System.in.read();

        automation.close();
    }
}
