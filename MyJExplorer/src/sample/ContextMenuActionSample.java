package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.ContextMenuProvider;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;

/**
 * This sample setups right click event listener.
 *
 * @author Alexei Orischenko
 */
public class ContextMenuActionSample {
    public static void main(String[] args) throws Exception {
        final Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Sample");
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setContextMenuProvider(new ContextMenuProvider() {
            public JPopupMenu getPopupMenu(Element contextElement) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        JOptionPane.showMessageDialog(browser, "Right click", "JExplorer", JOptionPane.INFORMATION_MESSAGE);
                    }
                });

                // don't show any menu
                return null;
            }
        });

        browser.navigate("http://google.com");
        browser.waitReady();
    }
}