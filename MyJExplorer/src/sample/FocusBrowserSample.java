package sample;
import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FocusBrowserSample {
    public static void main(String[] args) {
        final Browser browser = new Browser();
        final JTextField textField = new JTextField();

        JButton setFocusToBrowser = new JButton("Set focus to Browser");
        setFocusToBrowser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                browser.requestFocusInWindow();
            }
        });
        JButton setFocusToTextField = new JButton("Set focus to TextField");
        setFocusToTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textField.requestFocus();
            }
        });

        JPanel actionPane = new JPanel();
        actionPane.add(setFocusToBrowser);
        actionPane.add(setFocusToTextField);

        JFrame frame = new JFrame();
        frame.getContentPane().add(textField, BorderLayout.NORTH);
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.getContentPane().add(actionPane, BorderLayout.SOUTH);
        frame.setSize(500, 400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.navigate("http://www.google.com");
    }
}
