package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * This sample demonstrates technique of adding browser into the dialog.
 *
 * @author Alexei Orischenko
 */
public class BrowserInDialogSample {
    public static void main(String[] args) throws Exception {
        Browser browser = new Browser();

        JDialog dialog = new JDialog();
        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        dialog.setTitle("Dialog Sample");
        dialog.setSize(800, 600);

        dialog.getContentPane().add(browser);
        dialog.setVisible(true);

        browser.waitReady();
        browser.navigate("google.com");
        browser.waitReady();
    }
}