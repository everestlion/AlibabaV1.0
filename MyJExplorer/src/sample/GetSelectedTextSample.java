package sample;
/*
 * Copyright (c) 2000-2012 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.automation.IDispatch;
import com.jniwrapper.win32.automation.OleMessageLoop;
import com.jniwrapper.win32.automation.types.BStr;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.mshtml.IHTMLDocument2;
import com.jniwrapper.win32.mshtml.IHTMLSelectionObject;
import com.jniwrapper.win32.mshtml.IHTMLTxtRange;
import com.jniwrapper.win32.mshtml.impl.IHTMLDocument2Impl;
import com.jniwrapper.win32.mshtml.impl.IHTMLTxtRangeImpl;
import com.jniwrapper.win32.shdocvw.IWebBrowser2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This example demonstrates how to get selected text on loaded web page.
 */
public class GetSelectedTextSample {
    public static void main(String[] args) {
        final Browser browser = new Browser();

        JButton printTextButton = new JButton("Print selected text");
        printTextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                OleMessageLoop oleMessageLoop = browser.getOleMessageLoop();
                oleMessageLoop.doInvokeLater(new Runnable() {
                    public void run() {
                        IWebBrowser2 webBrowser2 = (IWebBrowser2) browser.getBrowserPeer();
                        IDispatch document = webBrowser2.getDocument();
                        IHTMLDocument2 document2 = new IHTMLDocument2Impl(document);
                        IHTMLSelectionObject selection = document2.getSelection();
                        IDispatch range = selection.createRange();
                        IHTMLTxtRange textRange = new IHTMLTxtRangeImpl(range);

                        BStr text = textRange.getText();
                        System.out.println("text = " + text.getValue());

                        document2.setAutoDelete(false);
                        document2.release();

                        textRange.setAutoDelete(false);
                        textRange.release();
                    }
                });
            }
        });

        JFrame frame = new JFrame();
        frame.add(printTextButton, BorderLayout.SOUTH);
        frame.add(browser, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.navigate("http://www.teamdev.com");
    }
}
