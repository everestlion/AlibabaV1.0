package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.HeadlessBrowser;
import com.jniwrapper.win32.ie.WebBrowser;

/**
 * This sample creates browser without GUI. This browser doesn't require a Swing container as parent.
 *
 * @author Alexei Orischenko
 */
public class CreateHeadlessBrowserSample {
    public static void main(String[] args) {
        WebBrowser browser = new HeadlessBrowser();

        browser.navigate("http://google.com");

        // Wait until page is loaded completely        
        browser.waitReady();

        String content = browser.getContent();
        System.out.println("Source of page: ");
        System.out.println(content);

        System.exit(0);
    }
}