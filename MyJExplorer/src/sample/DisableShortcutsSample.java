package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.KeyFilter;

import javax.swing.*;

/**
 * The sample demonstrates how to disable the specified shortcuts
 * in {@code WebBrowser} component.
 */
public class DisableShortcutsSample {
    public static void main(String[] args) {
        Browser browser = new Browser();

        JFrame frame = new JFrame();
        frame.setContentPane(browser);
        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setKeyFilter(new KeyFilter() {
            public boolean isFilter(KeyEvent evt) {
                return (evt.getKeyCode() == 'F' && evt.isControlPressed()) ||
                        (evt.getKeyCode() == 'P' && evt.isControlPressed());
            }
        });
        browser.navigate("http://www.google.com");
    }
}
