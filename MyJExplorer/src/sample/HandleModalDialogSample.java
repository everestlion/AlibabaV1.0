package sample;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.dom.HTMLDialog;
import com.jniwrapper.win32.ie.event.HtmlDialogEvent;
import com.jniwrapper.win32.ie.event.HtmlDialogListener;
import org.w3c.dom.html.HTMLDocument;

import javax.swing.*;
import java.awt.*;

/**
 * The sample shows how to handle showing modal dialog windows
 * that were opened through {@code window.showModalDialog} or
 * {@code window.showModelessDialog} JavaScript function.
 * <p/>
 * In this sample we receive an event when the dialog is shown,
 * print the dialog's document title in console and close the
 * opened modal dialog window.
 */
public class HandleModalDialogSample {
    public static void main(String[] args) {
        Browser browser = new Browser();
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(600, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setHtmlDialogListener(new HtmlDialogListener() {
            public void show(HtmlDialogEvent event) {
                HTMLDialog dialog = event.getDialog();
                // Receiving document of the opened modal dialog window
                HTMLDocument document = dialog.getDocument();
                System.out.println("document title = " + document.getTitle());
                // Closes modal dialog window
                dialog.close();
            }
        });

        browser.executeScript("window.showModalDialog('http://www.google.com', 'name', '');");
    }
}