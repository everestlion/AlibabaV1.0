package sample;
import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * This sample demonstrates how to register MouseListener to
 * receive mouse events for a Browser component.
 */
public class MouseListener {
    public static void main(String[] args) {
        Browser browser = new Browser();

        JFrame frame = new JFrame();
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                System.out.println("e = " + e);
            }

            public void mousePressed(MouseEvent e) {
                System.out.println("e = " + e);
            }

            public void mouseReleased(MouseEvent e) {
                System.out.println("e = " + e);
            }
        });

        browser.navigate("http://www.google.com");
    }
}
