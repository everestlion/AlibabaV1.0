package sample;
import com.jniwrapper.win32.ie.Browser;
import org.w3c.dom.NodeList;
import org.w3c.dom.html.HTMLAnchorElement;
import org.w3c.dom.html.HTMLDocument;

import javax.swing.*;
import java.awt.*;

/**
 * @author Vladimir Ikryanov
 */
public class GetDocumentSample {
    public static void main(String[] args) {
        Browser browser = new Browser();

        JFrame frame = new JFrame();
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setContent("<html><body><a id='test' href='http://www.google.com'>link</a></body></html>");
        browser.waitReady();

        HTMLDocument document = browser.getDocument();
        NodeList list = document.getChildNodes();
        int length = list.getLength();
        System.out.println("length = " + length);

        browser.close();
        frame.dispose();
    }
}
