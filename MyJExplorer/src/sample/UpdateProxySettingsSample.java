package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.proxy.ProxyConfiguration;

import javax.swing.*;

/**
 * The  sample  demonstrates how to  use ProxyManager to change
 * global proxy settings and set proxy information for the
 * current process without affecting global machine settings.
 *
 * @author Ikryanov Vladimir
 */
public class UpdateProxySettingsSample {
    public static void main(String[] args) {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer: UpdateProxySettingsSample");
        frame.setContentPane(browser);
        frame.setSize(750, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        ProxyConfiguration configuration = browser.getProxy();
        configuration.setConnectionType(ProxyConfiguration.ConnectionType.PROXY);
        configuration.setProxy("http://no.proxy.no:8090", ProxyConfiguration.ServerType.HTTP);
        browser.setProxy(configuration);

        browser.navigate("http://www.teamdev.com");
    }
}