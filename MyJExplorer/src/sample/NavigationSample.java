package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.*;

/**
 * This sample demonstrates the technique of using browser history and refreshing document.
 *
 * @author Alexei Orischenko
 */
public class NavigationSample {
    public static void main(String[] args) throws Exception {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Sample");
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.navigate("www.teamdev.com");
        browser.waitReady();

        JOptionPane.showMessageDialog(browser, "Browser will load Google page");

        browser.navigate("www.google.com");
        browser.waitReady();

        JOptionPane.showMessageDialog(browser, "Browser will go back");

        browser.goBack();
        browser.waitReady();

        JOptionPane.showMessageDialog(browser, "Browser will go forward");

        browser.goForward();
        browser.waitReady();

        JOptionPane.showMessageDialog(browser, "Browser will refresh page");

        browser.refresh();
    }
}