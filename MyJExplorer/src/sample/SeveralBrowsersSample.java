package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * This sample creates window with several Browsers.
 *
 * @author Serge Piletsky
 */
public class SeveralBrowsersSample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("JExplorer - Several Browsers Sample");
        frame.setSize(850, 650);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        final Browser browser1 = new Browser();
        final Browser browser2 = new Browser();
        final Browser browser3 = new Browser();
        final Browser browser4 = new Browser();

        Container contentPane = frame.getContentPane();

        contentPane.setLayout(new GridLayout(2, 2));

        contentPane.add(browser1);
        contentPane.add(browser2);
        contentPane.add(browser3);
        contentPane.add(browser4);


        frame.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                browser1.navigate("http://www.google.com");
                browser2.navigate("http://www.yahoo.com");
                browser3.navigate("http://www.altavista.com");
                browser4.navigate("http://search.msn.com");
            }
        });
        frame.setVisible(true);
    }
}