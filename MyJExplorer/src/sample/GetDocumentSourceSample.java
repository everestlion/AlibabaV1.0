package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.HeadlessBrowser;

/**
 * The sample shows how to get the original HTML of a currently
 * loaded document.  Like the "View source"  command in context
 * menu of Internet Explorer application.
 *
 * @author Ikryanov Vladimir
 */
public class GetDocumentSourceSample {
    public static void main(String[] args) {
        HeadlessBrowser browser = new HeadlessBrowser();
        browser.navigate("http://login.alibaba.com");
        browser.waitReady();

        String html = browser.getContent(true);
        System.out.println("html = " + html);
    }
}