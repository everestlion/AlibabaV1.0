package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.cookie.CookieManager;
import com.jniwrapper.win32.ie.dom.Cookie;

import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * The sample demonstrates how to use CookieManager to add, change or delete cookies.
 *
 * @author Ikryanov Vladimir
 */
public class CookieManagerSample {
    public static void main(String[] args) throws Exception {
        // creates new cookie entry
        Cookie cookie = new Cookie("name1", "value1");
        cookie.setExpireDate(new Date(System.currentTimeMillis() + 3600000));

        URL domain = new URL("http://www.teamdev.com");

        // creates CookieManager instance
        CookieManager cookieManager = CookieManager.getInstance();

        // maps new cookie entry to the specified domain address
        cookieManager.setCookie(domain, cookie);

        // deletes cookie by domain address
        cookieManager.deleteCookie(domain, cookie);

        Set<Cookie> cookies = cookieManager.getPersistentCookies();
        Iterator iterator = cookies.iterator();
        for (Cookie cookieEntry : cookies) {
            System.out.println("cookieEntry.getName() = " + cookieEntry.getName());
            System.out.println("cookieEntry.getValue() = " + cookieEntry.getValue());
            System.out.println("cookieEntry.getExpireDate() = " + cookieEntry.getExpireDate());
        }
        // allows deleting all persistent cookies
        // cookieManager.deleteAllPersistentCookies();
    }
}