package sample;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.event.NavigationEventAdapter;

import javax.swing.*;
import java.awt.*;

/**
 * The sample demonstrates how to register onRefresh event
 * listener to get notifications when a user clicks Refresh button,
 * Refresh context menu item or press F5.
 */
public class RefreshEventSample {
    public static void main(String[] args) {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Refresh Event");
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.addNavigationListener(new NavigationEventAdapter() {
            @Override
            public void onRefresh() {
                System.out.println("RefreshEventSample.onRefresh");
            }
        });
        browser.navigate("http://www.baidu.com");
    }
}
