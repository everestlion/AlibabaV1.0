package sample;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.dom.Cookie;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;

/**
 * The sample demonstrates how to get/set browser cookies.
 */
public class CookieSample {
    public static void main(String[] args) throws MalformedURLException {
        URL domain = new URL("http://www.google.com");

        Browser browser = new Browser();
        browser.navigate(domain.toString());
        browser.waitReady();

        Cookie cookie = new Cookie("name1", "value1");
        cookie.setExpireDate(new Date(System.currentTimeMillis() + 3600000));
        Set<Cookie> cookies = new HashSet<Cookie>();
        cookies.add(cookie);
        browser.setCookies(domain, cookies);

        Set<Cookie> receivedCookies = browser.getCookies(domain);
        for (Cookie cookieEntry : receivedCookies) {
            System.out.println("name = " + cookieEntry.getName());
            System.out.println("value = " + cookieEntry.getValue());
            System.out.println("expire date = " + cookieEntry.getExpireDate());
        }
    }
}
