package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.event.NewWindowEventHandler;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * This sample creates browser added to a Swing container and demonstrates
 * how to handle NewWindow browser's event.
 *
 * @author Serge Piletsky
 */
public class CreateChildBrowserSample {
    private static final String HTML_CONTENT =
            "<html><body>" +
                    "<script> function openAnotherWindow() { window.open(" +
                    "\"http://www.google.com\", " +
                    "\"Test\", \"width=500,height=300\"); } </script>" +
                    "<a id='theLink' href=\"javascript:openAnotherWindow()\" >" +
                    "Open Google in another window.</a> " +
                    "</body></html>";

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(WindowsLookAndFeel.class.getName());

        final JFrame frame = new JFrame("JExplorer - Child Browser Sample");
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);

        final Browser browser = new Browser();
        browser.setContent(HTML_CONTENT);

        browser.setNewWindowHandler(new NewWindowEventHandler() {
            public NewWindowAction newWindow() {
                int result = JOptionPane.showConfirmDialog(frame, "Open a child browser in new window?");
                if (result == JOptionPane.YES_OPTION) {
                    final Browser childBrowser = new Browser(browser);

                    JFrame childBrowserFrame = new JFrame("ChildBrowserFrame");
                    childBrowserFrame.setSize(640, 480);
                    childBrowserFrame.setLocationRelativeTo(null);
                    childBrowserFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    childBrowserFrame.getContentPane().add(childBrowser);
                    childBrowserFrame.setVisible(true);
                    childBrowserFrame.addWindowListener(new WindowAdapter() {
                        public void windowClosed(WindowEvent e) {
                            childBrowser.close();
                        }
                    });

                    NewWindowAction newWindowAction = new NewWindowAction(NewWindowAction.ACTION_REPLACE_BROWSER);
                    newWindowAction.setBrowser(childBrowser);
                    return newWindowAction;
                } else {
                    return new NewWindowAction(NewWindowAction.ACTION_CANCEL);
                }
            }
        });

        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setVisible(true);

        JOptionPane.showMessageDialog(frame, "Click link that opens child window.", "JExplorer", JOptionPane.INFORMATION_MESSAGE);
    }
}