package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.DefaultWebBrowserEventsHandler;
import com.jniwrapper.win32.ie.event.NewWindowEventListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @author Vladimir Ikryanov
 */
public class ClosePopupSample {
    public static void main(String[] args) {
        final java.util.List<WebBrowser> popups = new ArrayList<WebBrowser>();
        final WebBrowser browser = new Browser();

        JButton openPopupButton = new JButton("Open Popup");
        openPopupButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                browser.navigate("about:blank", "_blank", null);
            }
        });

        JButton closePopupsButton = new JButton("Close All Popups");
        closePopupsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (WebBrowser webBrowser : popups) {
                    Window window = SwingUtilities.getWindowAncestor((Browser) webBrowser);
                    window.dispose();
                }
            }
        });

        JPanel toolPanel = new JPanel(new FlowLayout());
        toolPanel.add(openPopupButton);
        toolPanel.add(closePopupsButton);

        JFrame frame = new JFrame("JExplorer: ClosePopupSample");
        Container contentPane = frame.getContentPane();
        contentPane.add(toolPanel, BorderLayout.NORTH);
        contentPane.add((Component) browser, BorderLayout.CENTER);
        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        browser.addNewWindowListener(new NewWindowEventListener() {
            public void windowOpened(final WebBrowser webBrowser) {
                webBrowser.setEventHandler(new DefaultWebBrowserEventsHandler() {
                    public boolean windowClosing(boolean isChildWindow) {
                        popups.remove(webBrowser);
                        return super.windowClosing(isChildWindow);
                    }
                });
                popups.add(webBrowser);
            }
        });
    }
}