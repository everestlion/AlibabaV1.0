package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.command.SaveAsCommand;

import javax.swing.*;
import java.awt.*;

/**
 * This sample invokes Save dialog.
 *
 * @author Alexei Orischenko
 */
public class SaveDialogSample {
    public static void main(String[] args) throws Exception {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Sample");
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.navigate("http://google.com");
        browser.waitReady();

        JOptionPane.showMessageDialog(browser, "Invoke Save Dialog", "JExplorer", JOptionPane.INFORMATION_MESSAGE);

        browser.execute(new SaveAsCommand());
    }
}