package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.event.HttpSecurityHandler;
import com.jniwrapper.win32.ie.event.HttpSecurityAction;
import com.jniwrapper.win32.ie.event.SecurityProblem;

import javax.swing.*;

/**
 * The sample shows how to handle the Invalid Certificate and Invalid
 * Certificate Name security errors. All other security problems will
 * be handled by default.
 */
public class SecurityProblemSample {
    public static void main(String[] args) {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Sample");
        frame.setContentPane(browser);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setHttpSecurityHandler(new HttpSecurityHandler() {
            public HttpSecurityAction onSecurityProblem(SecurityProblem problem) {
                return HttpSecurityAction.DEFAULT;
            }
        });

        browser.navigate("https://cingular-pc-uat.dcstest-us.net/qpass_onboarder/welcome.do");
    }
}
