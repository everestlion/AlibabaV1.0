package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.event.DefaultWebBrowserEventsHandler;

import javax.swing.*;

/**
 * This sample demonstrates technique of disabling download dialogs
 * that appear on link click.
 *
 * @author Alexei Orischenko
 */
public class DisableDownloadSample {
    public static void main(String[] args) throws Exception {
        Browser browser = new Browser();

        JFrame frame = new JFrame("Disable Download");
        frame.setContentPane(browser);
        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setEventHandler(new DefaultWebBrowserEventsHandler() {
            public boolean beforeFileDownload() {
                // disable downloads
                return true;
            }
        });

        browser.navigate("http://mirror.mkhelif.fr/apache/ant/");

        JOptionPane.showMessageDialog(browser, "Disable download dialogs on link click",
                "Disable Download", JOptionPane.INFORMATION_MESSAGE);
    }
}