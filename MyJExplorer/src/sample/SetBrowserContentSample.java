package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.*;

/**
 * This sample demonstrates the technique of loading HTML documents from String object.
 *
 * @author Alexei Orischenko
 */
public class SetBrowserContentSample {
    private static final String HTML_CONTENT = "<html><body><h1>Simple document</h1></body></html>";

    public static void main(String[] args) throws Exception {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Sample");
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        // wait when the browser will be ready to work
        browser.waitReady();

        browser.setContent(HTML_CONTENT);
        browser.waitReady();

        String browserContent = browser.getContent();
        JOptionPane.showMessageDialog(browser, "Page source:\n" + browserContent, "JExplorer", JOptionPane.INFORMATION_MESSAGE);
    }
}