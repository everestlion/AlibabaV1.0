package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * The sample shows how to save the loaded HTML document into an image
 * and save it to a file in PNG format. In this sample the full web
 * page will be saved as image including invisible parts.
 *
 * @author Vladimir Ikryanov
 */
public class HtmlToImageSample {
    public static void main(String[] args) throws IOException {
        Browser browser = new Browser();

        JFrame frame = new JFrame();
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(100, 100);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.navigate("http://www.google.com");
        browser.waitReady();

        Image image = browser.getScreenShot(true);
        ImageIO.write((BufferedImage) image, "PNG", new File("C:\\webpage.png"));
    }
}