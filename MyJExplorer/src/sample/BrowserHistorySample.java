package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.BrowserHistory;
import com.jniwrapper.win32.ie.HeadlessBrowser;

import java.util.Iterator;
import java.util.List;

/**
 * This sample demonstrates technique of working with
 * browser history: list of visited pages.
 *
 * @author Alexei Orischenko
 */
public class BrowserHistorySample {
    public static void main(String[] args) {
        System.out.println("Create browser");

        HeadlessBrowser browser = new HeadlessBrowser();
        System.out.println("Load Google page");
        browser.navigate("http://www.google.com");
        browser.waitReady();

        System.out.println("Load TeamDev page");
        browser.navigate("http://www.teamdev.com");
        browser.waitReady();

        BrowserHistory history = new BrowserHistory();
        System.out.println("");
        System.out.println("Browser history:");
        printHistory(history);

        history.clearHistory();

        System.out.println("");
        System.out.println("Browser history after clearing:");
        printHistory(history);

        System.exit(0);
    }

    private static void printHistory(BrowserHistory history) {
        List<BrowserHistory.Entry> historyEntries = history.getHistory();
        for (BrowserHistory.Entry hEntry : historyEntries) {
            System.out.println(hEntry.getUrl());
        }
    }
}