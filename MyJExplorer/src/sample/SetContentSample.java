package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.NativeResourceCollector;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.NavigationEventAdapter;

import javax.swing.*;
import java.awt.*;

/**
 * This sample shows how to set specified HTML content into WebBrowser
 * document and print the actual document HTML after document is
 * loaded completely.
 */
public class SetContentSample {
    public static void main(String[] args) throws InterruptedException {
        Browser browser = new Browser();


        JFrame frame = new JFrame("JExplorer");
        Container contentPane = frame.getContentPane();
        contentPane.add(browser, BorderLayout.CENTER);
        frame.setSize(400, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.addNavigationListener(new NavigationEventAdapter() {
            public void entireDocumentCompleted(WebBrowser webBrowser, String url) {
                System.out.println("content = " + webBrowser.getContent());
            }
        });
        String html = "<html><title>Empty page</title>" +
                "<body><h1>Simple Text</h1><p>Paragraph</p></body></html>";
//        for (int i = 0; i < 100; i++) {
//            System.out.println("i = " + i);
            browser.setContent(html);
//            browser.waitReady();
//            browser.setContent(html);
//            browser.setContent(html);
//        }
    }
}