package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.DefaultWebBrowserEventsHandler;
import com.jniwrapper.win32.ie.event.StatusCode;

import javax.swing.*;
import java.awt.*;

/**
 * This sample demonstrates technique of listenening navigate error events.
 *
 * @author Alexei Orischenko
 */
public class NavigateErrorSample {
    public static void main(String[] args) throws Exception {
        Browser browser = new Browser();

        JFrame frame = new JFrame("JExplorer Sample");
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setEventHandler(new DefaultWebBrowserEventsHandler() {
            public boolean navigationErrorOccured(WebBrowser webBrowser, String url, String frame, StatusCode statusCode) {
                System.out.println("url = " + url);
                System.out.println("frame = " + frame);
                System.out.println("statusCode = " + statusCode);
                return super.navigationErrorOccured(webBrowser, url, frame, statusCode);
            }
        });

        JOptionPane.showMessageDialog(browser, "Load unexisting resource...", "JExplorer", JOptionPane.INFORMATION_MESSAGE);
        browser.navigate("http://someexistingsitejniwrpatest.com");
    }
}