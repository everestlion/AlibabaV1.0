package sample;
import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * This sample demonstrates how to register KeyListener to
 * receive key events for a Browser component.
 */
public class KeyListener {
    public static void main(String[] args) {
        Browser browser = new Browser();

        JFrame frame = new JFrame();
        frame.getContentPane().add(browser, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                System.out.println("e = " + e);
            }

            public void keyReleased(KeyEvent e) {
                System.out.println("e = " + e);
            }

            public void keyTyped(KeyEvent e) {
                System.out.println("e = " + e);
            }
        });

        browser.navigate("http://www.google.com");
    }
}