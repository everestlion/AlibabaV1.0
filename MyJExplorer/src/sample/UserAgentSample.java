package sample;
/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;

import javax.swing.*;
import java.awt.*;

/**
 * The sample shows how to specify a Browser User-Agent.
 */
public class UserAgentSample {
    public static void main(String[] args) {
        final WebBrowser browser = new Browser();

        JFrame frame = new JFrame("JExplorer - Create Browser Sample");
        frame.setSize(700, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().add((Component) browser, BorderLayout.CENTER);
        frame.setVisible(true);

        ((Browser) browser).setUserAgent("User-Agent: Mozilla/5.0 (Linux; U; Android 0.5; en-us) AppleWebKit/522+ (KHTML, like Gecko) Safari/419.3");
    }
}
