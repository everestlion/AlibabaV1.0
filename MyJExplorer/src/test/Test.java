package test;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.text.html.parser.Element;

import org.w3c.dom.html.HTMLDocument;
import org.w3c.dom.html.HTMLElement;

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.dom.DOMUtils;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Browser browser = new Browser();

		JFrame frame = new JFrame();
		frame.setSize(800, 600);
		frame.add(browser, BorderLayout.CENTER);
		frame.setVisible(true);
		
		browser.navigate("http://login.alibaba.com ");
		browser.waitReady();
		

		HTMLDocument document = browser.getDocument();
		System.out.println(document.getTextContent());
	}

}
