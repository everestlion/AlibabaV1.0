/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class DOMAttributeTableModel extends AbstractTableModel implements NodeDetailsTableModel {
    private static final String HEADER_ATTRIBUTE_NAME = "Name";
    private static final String HEADER_ATTRIBUTE_VALUE = "Value";

    static class Attribute implements Comparable {
        private String _name;
        private String _value;

        public Attribute(Attr attr) {
            _name = attr.getName();
            if (attr.getSpecified()) {
                _value = attr.getValue();
            } else {
                _value = "";
            }
        }

        public String getName() {
            return _name;
        }

        public String getValue() {
            return _value;
        }

        public int compareTo(Object o) {
            return getName().compareTo(((Attribute) o).getName());
        }
    }

    private Node _browserNode;
    private List<Attribute> _attributes;
    private boolean _onlySpecified;

    public DOMAttributeTableModel() {
    }

    public synchronized void setBrowserNode(Node browserNode) {
        _browserNode = browserNode;
        _attributes = null;
    }

    private void readAttributes() {
        if (_attributes != null)
            return;

        int len;
        NamedNodeMap attributes = null;
        if (_browserNode == null) {
            len = 0;
        } else {
            attributes = _browserNode.getAttributes();
            if (attributes == null) {
                len = 0;
            } else {
                len = attributes.getLength();
            }
        }

        _attributes = new ArrayList<Attribute>(len);
        for (int i = 0; i < len; i++) {
            final Attr attr = (Attr) attributes.item(i);

            if (isOnlySpecified()) {
                if (attr.getSpecified()) {
                    _attributes.add(new Attribute(attr));
                }
            } else {
                _attributes.add(new Attribute(attr));
            }
        }
        sortAttributes();
    }

    public synchronized Object getValueAt(int rowIndex, int columnIndex) {
        Attribute attr = _attributes.get(rowIndex);
        if (columnIndex == 0) {
            return attr.getName();
        } else if (columnIndex == 1) {
            return attr.getValue();
        }
        return null;
    }

    public int getColumnCount() {
        return 2;
    }

    public synchronized int getRowCount() {
        readAttributes();
        return _attributes.size();
    }

    public String getColumnName(int column) {
        if (column == 0)
            return HEADER_ATTRIBUTE_NAME;
        if (column == 1)
            return HEADER_ATTRIBUTE_VALUE;
        return null;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public boolean isOnlySpecified() {
        return _onlySpecified;
    }

    public synchronized void setOnlySpecified(boolean onlySpecified) {
        _attributes = null;
        _onlySpecified = onlySpecified;
        readAttributes();
    }

    private void sortAttributes() {
        if (_attributes != null) {
            Collections.sort(_attributes);
        }
    }
}