/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo.components;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ikryanov Vladimir
 */
public class IconLoader {
    private static IconLoader instance;

    private Map<String, ImageIcon> domains;
    private ImageIcon docIcon;
    private ImageIcon defaultIcon;

    private IconLoader() {
        defaultIcon = new ImageIcon(URLField.class.getResource("res/doc.png"));
        docIcon = new ImageIcon(URLField.class.getResource("res/doc.gif"));
        domains = Collections.synchronizedMap(new HashMap<String, ImageIcon>());
    }

    public static IconLoader getInstance() {
        return instance == null ? instance = new IconLoader() : instance;
    }

    public void loadIcon(final String url, final IconLoadProcess callback) {
        if (!hasIcon(url)) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        BufferedImage image = new FavoriteIcon(url).toImage();
                        ImageIcon imageIcon = new ImageIcon(image);
                        domains.put(getHost(url), imageIcon);
                        callback.complete(imageIcon);
                    }
                    catch (Exception e) {
                        domains.put(getHost(url), defaultIcon);
                    }
                }
            }).start();
        } else {
            callback.complete(domains.get(getHost(url)));
        }
    }

    public ImageIcon getDefaultIcon() {
        return defaultIcon;
    }

    public ImageIcon getDocIcon() {
        return docIcon;
    }

    private boolean hasIcon(String url) {
        return domains.get(getHost(url)) != null;
    }

    private String getHost(String url) {
        try {
            return new URL(url).getHost();
        }
        catch (Exception e) {
            return "";
        }
    }

    public interface IconLoadProcess {
        public void complete(Icon icon);
    }
}