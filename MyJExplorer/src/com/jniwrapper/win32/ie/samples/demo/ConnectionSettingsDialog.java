/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import com.jniwrapper.win32.ie.Browsers;
import com.jniwrapper.win32.ie.proxy.ProxyConfiguration;
import com.jniwrapper.win32.ie.proxy.ProxyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ConnectionSettingsDialog extends JDialog {
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionSettingsDialog.class);

    private static final int PROXY_CONFIG_TYPE = ProxyConfiguration.Type.PROCESS;

    private static boolean _userThisProxyServer = false;

    private JPanel _contentPane;
    private JButton _buttonOK;
    private JButton _buttonCancel;
    private JRadioButton _directConnectionRadioButton;
    private JRadioButton _webProxyAutoDiscoveryRadioButton;
    private JRadioButton _manualProxyConfigurationRadioButton;
    private JRadioButton _automaticProxyConfigurationURLRadioButton;
    private JTextField _httpProxyTextField;
    private JTextField _httpPortTextField;
    private JTextField _sslProxyTextField;
    private JTextField _sslPortTextField;
    private JTextField _ftpProxyTextField;
    private JTextField _ftpPortTextField;
    private JCheckBox _useThisProxyServerCheckBox;
    private JTextField _gopherProxyTextField;
    private JTextField _gopherPortTextField;
    private JTextField _socksProxyTextField;
    private JTextField _socksPortTextField;
    private JTextField _automaticProxyConfigurationURLTextField;
    private JTextField _noProxyTextField;
    private JPanel _manualProxyConfigurationPanel;
    private final Collection<JTextField> _sharedFields = new HashSet<JTextField>();

    public ConnectionSettingsDialog(Frame owner) {
        super(owner, "Connection Settings");
        setupUI();
        setContentPane(_contentPane);
        setModal(true);
        getRootPane().setDefaultButton(_buttonOK);

        _buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        _buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        _contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        _manualProxyConfigurationRadioButton.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                updateManualProxyConfiguration();
            }
        });

        _automaticProxyConfigurationURLRadioButton.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                updateAutomaticProxyConfiguration();
            }
        });

        _useThisProxyServerCheckBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                updateShareProxy();
            }
        });

        _httpProxyTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                updateSharedFields();
            }

            public void removeUpdate(DocumentEvent e) {
                updateSharedFields();
            }

            public void changedUpdate(DocumentEvent e) {
                updateSharedFields();
            }
        });

        _httpPortTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                updateSharedFields();
            }

            public void removeUpdate(DocumentEvent e) {
                updateSharedFields();
            }

            public void changedUpdate(DocumentEvent e) {
                updateSharedFields();
            }
        });

        ProxyManager proxyManager = ProxyManager.getInstance();
        ProxyConfiguration proxySettings = Browsers.getProxy();
        int connectionType = proxySettings.getConnectionType();

        _directConnectionRadioButton.setSelected(
                connectionType == ProxyConfiguration.ConnectionType.DIRECT);
        _webProxyAutoDiscoveryRadioButton.setSelected(
                connectionType == ProxyConfiguration.ConnectionType.AUTO_DETECT);
        _manualProxyConfigurationRadioButton.setSelected(
                connectionType == ProxyConfiguration.ConnectionType.PROXY);
        _automaticProxyConfigurationURLRadioButton.setSelected(
                connectionType == ProxyConfiguration.ConnectionType.AUTO_PROXY_URL);

        _useThisProxyServerCheckBox.setSelected(_userThisProxyServer);

        initProxyServer(_httpProxyTextField, _httpPortTextField,
                proxySettings.getProxy(ProxyConfiguration.ServerType.HTTP));
        initProxyServer(_sslProxyTextField, _sslPortTextField,
                proxySettings.getProxy(ProxyConfiguration.ServerType.SECURE));
        initProxyServer(_ftpProxyTextField, _ftpPortTextField,
                proxySettings.getProxy(ProxyConfiguration.ServerType.FTP));
        initProxyServer(_gopherProxyTextField, _gopherPortTextField,
                proxySettings.getProxy(ProxyConfiguration.ServerType.GOPHER));
        initProxyServer(_socksProxyTextField, _socksPortTextField,
                proxySettings.getProxy(ProxyConfiguration.ServerType.SOCKS));

        String proxyExceptions = "";
        Set<String> exceptions = proxySettings.getProxyExceptions();
        Iterator iterator = exceptions.iterator();
        for(String exception : exceptions) {
            proxyExceptions += exception + ";";
        }
        _noProxyTextField.setText(proxyExceptions);

        String autoconfigUrl = proxySettings.getAutoconfigProxyAddress();
        if (autoconfigUrl != null) {
            _automaticProxyConfigurationURLTextField.setText(autoconfigUrl);
        } else {
            _automaticProxyConfigurationURLTextField.setText("");
        }

        updateManualProxyConfiguration();
        updateAutomaticProxyConfiguration();
    }

    private void initProxyServer(JTextField host, JTextField port, String server) {
        if (server != null) {
            int index = server.indexOf(":");
            host.setText((index == -1) ? server : server.substring(0, index));
            port.setText((index == -1) ? "" : server.substring(index + 1));
        }
    }

    private void updateAutomaticProxyConfiguration() {
        _automaticProxyConfigurationURLTextField.setEnabled(_automaticProxyConfigurationURLRadioButton.isSelected());
    }

    private void updateManualProxyConfiguration() {
        final boolean manualConfigEnabled = _manualProxyConfigurationRadioButton.isSelected();
        _manualProxyConfigurationPanel.setEnabled(manualConfigEnabled);
        enableContainer(_manualProxyConfigurationPanel, manualConfigEnabled);
        updateShareProxy();
    }

    private void enableContainer(Container container, boolean enabled) {
        Component[] components = container.getComponents();
        if (components != null && components.length > 0) {
            int count = components.length;
            for (int i = 0; i < count; i++) {
                final Component component = components[i];
                if (!_sharedFields.contains(component)) {
                    component.setEnabled(enabled);
                    if (component instanceof Container) {
                        enableContainer((Container) component, enabled);
                    }
                }
            }
        }
    }

    private void updateShareProxy() {
        final boolean enabled = _manualProxyConfigurationRadioButton.isSelected() && !_useThisProxyServerCheckBox.isSelected();
        for (JTextField textField : _sharedFields) {
            textField.setEnabled(enabled);
        }
        updateSharedFields();
    }

    private void updateSharedFields() {
        final String httpProxy = _httpProxyTextField.getText();
        final String httpPort = _httpPortTextField.getText();
        if (_useThisProxyServerCheckBox.isSelected()) {
            _ftpProxyTextField.setText(httpProxy);
            _sslProxyTextField.setText(httpProxy);
            _gopherProxyTextField.setText(httpProxy);
            _socksProxyTextField.setText(httpProxy);
            _ftpPortTextField.setText(httpPort);
            _sslPortTextField.setText(httpPort);
            _gopherPortTextField.setText(httpPort);
            _socksPortTextField.setText(httpPort);
        }
    }

    private void onOK() {
        ProxyManager proxyManager = ProxyManager.getInstance();
        ProxyConfiguration proxySettings = Browsers.getProxy();

        int proxyType = ProxyConfiguration.ConnectionType.DIRECT;
        if (_webProxyAutoDiscoveryRadioButton.isSelected()) {
            proxyType = ProxyConfiguration.ConnectionType.AUTO_DETECT;
        } else if (_manualProxyConfigurationRadioButton.isSelected()) {
            proxyType = ProxyConfiguration.ConnectionType.PROXY;
        } else if (_automaticProxyConfigurationURLRadioButton.isSelected()) {
            proxyType = ProxyConfiguration.ConnectionType.AUTO_PROXY_URL;
        }
        proxySettings.setConnectionType(proxyType);

        setProxy(proxySettings, ProxyConfiguration.ServerType.HTTP,
                _httpProxyTextField.getText(), _httpPortTextField.getText());
        setProxy(proxySettings, ProxyConfiguration.ServerType.SECURE,
                _sslProxyTextField.getText(), _sslPortTextField.getText());
        setProxy(proxySettings, ProxyConfiguration.ServerType.FTP,
                _ftpProxyTextField.getText(), _ftpPortTextField.getText());
        setProxy(proxySettings, ProxyConfiguration.ServerType.GOPHER,
                _gopherProxyTextField.getText(), _gopherPortTextField.getText());
        setProxy(proxySettings, ProxyConfiguration.ServerType.SOCKS,
                _socksProxyTextField.getText(), _socksPortTextField.getText());

        Browsers.setProxy(proxySettings);

        _userThisProxyServer = _useThisProxyServerCheckBox.isSelected();

        dispose();
    }

    private void setProxy(ProxyConfiguration proxyConfiguration, int serverType, String host, String port) {
        if (host != null && host.length() > 0) {
            String serverAddress = host;
            serverAddress += port != null && port.length() > 0 ? ":" + port : "";
            proxyConfiguration.setProxy(serverAddress, serverType);
        }
    }

    private void onCancel() {
        dispose();
    }

    private void setupUI() {
        _contentPane = new JPanel();
        _contentPane.setLayout(new GridBagLayout());
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.weightx = 1.0;
            gbc.insets = new Insets(5, 5, 5, 5);
            gbc.fill = GridBagConstraints.BOTH;
            _contentPane.add(panel1, gbc);
        }
        final JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1, 2, 8, 8));
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(4, 4, 4, 4);
            gbc.fill = GridBagConstraints.NONE;
            gbc.anchor = GridBagConstraints.EAST;
            panel1.add(buttonPanel, gbc);
        }
//        buttonPanel.add(new JPanel());
        _buttonOK = new JButton();
        _buttonOK.setText("OK");
        buttonPanel.add(_buttonOK);
        _buttonCancel = new JButton();
        _buttonCancel.setText("Cancel");
        buttonPanel.add(_buttonCancel);
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridBagLayout());
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(10, 10, 2, 10);
            gbc.fill = GridBagConstraints.BOTH;
            _contentPane.add(panel3, gbc);
        }
        _directConnectionRadioButton = new JRadioButton();
        _directConnectionRadioButton.setSelected(true);
        _directConnectionRadioButton.setText("Direct connection to the Internet");
        _directConnectionRadioButton.setMnemonic('D');
        _directConnectionRadioButton.setDisplayedMnemonicIndex(0);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            panel3.add(_directConnectionRadioButton, gbc);
        }
        _webProxyAutoDiscoveryRadioButton = new JRadioButton();
        _webProxyAutoDiscoveryRadioButton.setText("Auto-detect proxy settings for this network");
        _webProxyAutoDiscoveryRadioButton.setMnemonic('A');
        _webProxyAutoDiscoveryRadioButton.setDisplayedMnemonicIndex(0);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.weightx = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            panel3.add(_webProxyAutoDiscoveryRadioButton, gbc);
        }
        _manualProxyConfigurationRadioButton = new JRadioButton();
        _manualProxyConfigurationRadioButton.setText("Manual proxy configuration:");
        _manualProxyConfigurationRadioButton.setMnemonic('M');
        _manualProxyConfigurationRadioButton.setDisplayedMnemonicIndex(0);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 2;
            gbc.weightx = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            panel3.add(_manualProxyConfigurationRadioButton, gbc);
        }
        _automaticProxyConfigurationURLRadioButton = new JRadioButton();
        _automaticProxyConfigurationURLRadioButton.setText("Automatic proxy configuration URL");
        _automaticProxyConfigurationURLRadioButton.setMnemonic('U');
        _automaticProxyConfigurationURLRadioButton.setDisplayedMnemonicIndex(30);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 4;
            gbc.weightx = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            panel3.add(_automaticProxyConfigurationURLRadioButton, gbc);
        }
        _manualProxyConfigurationPanel = new JPanel();
        _manualProxyConfigurationPanel.setLayout(new GridBagLayout());
        _manualProxyConfigurationPanel.setEnabled(true);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 20, 2, 2);
            gbc.anchor = GridBagConstraints.EAST;
            gbc.fill = GridBagConstraints.VERTICAL;
            panel3.add(_manualProxyConfigurationPanel, gbc);
        }
        _httpProxyTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 0;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_httpProxyTextField, gbc);
        }
        _httpPortTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 3;
            gbc.gridy = 0;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.ipadx = 30;
            _manualProxyConfigurationPanel.add(_httpPortTextField, gbc);
        }
        _sslProxyTextField = new JTextField();
        _sslProxyTextField.setText("");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 2;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_sslProxyTextField, gbc);
        }
        _sslPortTextField = new JTextField();
        _sslPortTextField.setText("");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 3;
            gbc.gridy = 2;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_sslPortTextField, gbc);
        }
        _ftpProxyTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 3;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_ftpProxyTextField, gbc);
        }
        _ftpPortTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 3;
            gbc.gridy = 3;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_ftpPortTextField, gbc);
        }
        _useThisProxyServerCheckBox = new JCheckBox();
        _useThisProxyServerCheckBox.setText("Use this proxy server for all protocols");
        _useThisProxyServerCheckBox.setMnemonic('T');
        _useThisProxyServerCheckBox.setDisplayedMnemonicIndex(4);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 1;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            _manualProxyConfigurationPanel.add(_useThisProxyServerCheckBox, gbc);
        }
        _gopherProxyTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 4;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_gopherProxyTextField, gbc);
        }
        _gopherPortTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 3;
            gbc.gridy = 4;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_gopherPortTextField, gbc);
        }
        _socksProxyTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 5;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_socksProxyTextField, gbc);
        }
        _socksPortTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 3;
            gbc.gridy = 5;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_socksPortTextField, gbc);
        }
        final JLabel label1 = new JLabel();
        label1.setText("HTTP Proxy:");
        label1.setDisplayedMnemonic('H');
        label1.setDisplayedMnemonicIndex(0);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.EAST;
            _manualProxyConfigurationPanel.add(label1, gbc);
        }
        final JLabel label2 = new JLabel();
        label2.setText("SSL Proxy:");
        label2.setDisplayedMnemonic('S');
        label2.setDisplayedMnemonicIndex(0);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 2;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.EAST;
            _manualProxyConfigurationPanel.add(label2, gbc);
        }
        final JLabel label3 = new JLabel();
        label3.setText("FTP Proxy:");
        label3.setDisplayedMnemonic('F');
        label3.setDisplayedMnemonicIndex(0);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.EAST;
            _manualProxyConfigurationPanel.add(label3, gbc);
        }
        final JLabel label4 = new JLabel();
        label4.setText("Gopher Proxy:");
        label4.setDisplayedMnemonic('G');
        label4.setDisplayedMnemonicIndex(0);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 4;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.EAST;
            _manualProxyConfigurationPanel.add(label4, gbc);
        }
        final JLabel label5 = new JLabel();
        label5.setText("SOCKS Proxy:");
        label5.setDisplayedMnemonic('O');
        label5.setDisplayedMnemonicIndex(1);
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 5;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.EAST;
            _manualProxyConfigurationPanel.add(label5, gbc);
        }
        final JLabel label6 = new JLabel();
        label6.setText("No proxy for:");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 7;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.EAST;
            _manualProxyConfigurationPanel.add(label6, gbc);
        }
        _noProxyTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 7;
            gbc.gridwidth = 3;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            _manualProxyConfigurationPanel.add(_noProxyTextField, gbc);
        }
        final JLabel label7 = new JLabel();
        label7.setText("Port:");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 2;
            gbc.gridy = 0;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            _manualProxyConfigurationPanel.add(label7, gbc);
        }
        final JLabel label8 = new JLabel();
        label8.setText("Port:");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 2;
            gbc.gridy = 2;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            _manualProxyConfigurationPanel.add(label8, gbc);
        }
        final JLabel label9 = new JLabel();
        label9.setText("Port:");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 2;
            gbc.gridy = 3;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            _manualProxyConfigurationPanel.add(label9, gbc);
        }
        final JLabel label10 = new JLabel();
        label10.setText("Port:");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 2;
            gbc.gridy = 4;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            _manualProxyConfigurationPanel.add(label10, gbc);
        }
        final JLabel label11 = new JLabel();
        label11.setText("Port:");
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 2;
            gbc.gridy = 5;
            gbc.insets = new Insets(2, 2, 2, 2);
            gbc.anchor = GridBagConstraints.WEST;
            _manualProxyConfigurationPanel.add(label11, gbc);
        }
        _automaticProxyConfigurationURLTextField = new JTextField();
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 5;
            gbc.weightx = 1.0;
            gbc.insets = new Insets(2, 25, 2, 5);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            panel3.add(_automaticProxyConfigurationURLTextField, gbc);
        }
        label1.setLabelFor(_httpProxyTextField);
        label2.setLabelFor(_sslProxyTextField);
        label3.setLabelFor(_ftpProxyTextField);
        label4.setLabelFor(_gopherProxyTextField);
        label5.setLabelFor(_socksProxyTextField);
        label6.setLabelFor(_noProxyTextField);
        label8.setLabelFor(_sslPortTextField);
        label9.setLabelFor(_ftpPortTextField);
        label10.setLabelFor(_gopherPortTextField);
        label11.setLabelFor(_socksPortTextField);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(_directConnectionRadioButton);
        buttonGroup.add(_webProxyAutoDiscoveryRadioButton);
        buttonGroup.add(_manualProxyConfigurationRadioButton);
        buttonGroup.add(_automaticProxyConfigurationURLRadioButton);

        _sharedFields.add(_ftpProxyTextField);
        _sharedFields.add(_ftpPortTextField);
        _sharedFields.add(_sslProxyTextField);
        _sharedFields.add(_sslPortTextField);
        _sharedFields.add(_gopherProxyTextField);
        _sharedFields.add(_gopherPortTextField);
        _sharedFields.add(_socksProxyTextField);
        _sharedFields.add(_socksPortTextField);
    }

    public static void main(String[] args) {
        ConnectionSettingsDialog dialog = new ConnectionSettingsDialog(null);
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }
}
