/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.NavigationEventAdapter;
import com.jniwrapper.win32.ie.event.StatusCode;
import com.jniwrapper.win32.ie.event.StatusEventListener;
import com.jniwrapper.win32.ie.event.WebBrowserEventsHandler;
import com.jniwrapper.win32.shdocvw.IWebBrowser2;

import java.awt.*;

/**
 * @author Serge Piletsky
 */
class BrowserEventsListener extends NavigationEventAdapter implements WebBrowserEventsHandler, StatusEventListener {
    protected TabbedBrowsersPane _tabbedBrowsersPane;
    protected WebBrowser _webBrowser;

    public boolean windowClosing(boolean isChildWindow) {
        _webBrowser.getOleMessageLoop().doInvokeLater(new Runnable() {
            public void run() {
                if (_tabbedBrowsersPane.getTabbedPane().getTabCount() > 1) {
                    _tabbedBrowsersPane.removeBrowser(_webBrowser);
                }
            }
        });

        return true;
    }

    public void titleChanged(String title) {
        _tabbedBrowsersPane.updateBrowserTitleText(_webBrowser, title);
    }

    public void backButtonEnabled(boolean enabled) {
        _tabbedBrowsersPane.backButtonEnabled(_webBrowser, enabled);
    }

    public void forwardButtonEnabled(boolean enabled) {
        _tabbedBrowsersPane.forwardButtonEnabled(_webBrowser, enabled);
    }

    public boolean beforeNavigate(WebBrowser webBrowser, String url, String targetFrameName, String postData, String headers) {
        return false;
    }

    public Dimension clientAreaSizeRequested(Dimension clientAreaSize) {
        return clientAreaSize;
    }

    public boolean beforeFileDownload() {
        return true;
    }

    public boolean navigationErrorOccured(WebBrowser webBrowser, String url, String frame, StatusCode statusCode) {
        return false;
    }

    public void progressChanged(int progress, int progressMax) {
        _tabbedBrowsersPane.progressChanged(progress, progressMax);
    }

    public void dataEncryptionLevelChanged(DataEncryptionLevel level) {
    }

    public void privacyStateChanged(boolean impacted) {
    }

    public void onFullScreen(boolean fullScreen) {
    }

    public void statusTextChanged(String text) {
    }

    public void entireDocumentCompleted(final WebBrowser webBrowser, String url) {
        WebBrowser activeBrowser = _tabbedBrowsersPane.getActiveBrowser();
        IWebBrowser2 activeBrowserPeer = (IWebBrowser2) activeBrowser.getBrowserPeer();
        IWebBrowser2 browserPeer = (IWebBrowser2) webBrowser.getBrowserPeer();
        if (browserPeer.equals(activeBrowserPeer)) {
            Browser browser = (Browser) activeBrowser;

//            browser.firePropertyChange(WebBrowser.Properties.READY_STATE,
//                    ReadyState.READYSTATE_LOADING, ReadyState.READYSTATE_COMPLETE);
        }
    }
}