/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo.components;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The bean class that allows storing screenshot data of a specified web page.
 *
 * @author Ikryanov Vladimir
 */
public class CaptureData {
    private Image screenShot;
    private Image thumbnail;
    private Dimension currentSize;

    public CaptureData(Image screenShot) {
        this.screenShot = screenShot;
        this.currentSize = new Dimension();
    }

    public Image getScreenShot() {
        return screenShot;
    }

    public Image getThumbnail(Dimension size) {
        if (thumbnail == null || !currentSize.equals(size)) {
            int width = screenShot.getWidth(null);
            int height = screenShot.getHeight(null);
            Dimension scaledSize = getScaledDimension(new Dimension(width, height), size);
            thumbnail = screenShot.getScaledInstance(scaledSize.width, scaledSize.height, BufferedImage.SCALE_SMOOTH);
            currentSize.setSize(size);
        }
        return thumbnail;
    }

    private static Dimension getScaledDimension(Dimension sourceSize, Dimension maxSize) {
        int width;
        int height;

        double sourceAspectRatio = (double) sourceSize.width / (double) sourceSize.height;
        double destAspectRatio = (double) maxSize.width / (double) maxSize.height;

        if (sourceAspectRatio > destAspectRatio) {
            width = maxSize.width;
            height = (int) (width / sourceAspectRatio);
        } else {
            height = maxSize.height;
            width = (int) (height * sourceAspectRatio);
        }

        if (width == 0 && maxSize.width > 0) width = 1;
        if (height == 0 && maxSize.height > 0) height = 1;

        return new Dimension(width, height);
    }
}