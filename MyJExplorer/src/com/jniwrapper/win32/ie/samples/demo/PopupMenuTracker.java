/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class PopupMenuTracker extends MouseAdapter {
    private JPopupMenu _popupMenu;

    public PopupMenuTracker(JComponent components[], JPopupMenu popupMenu) {
        _popupMenu = popupMenu;
        for (JComponent component : components) {
            component.addMouseListener(this);
        }
    }

    public PopupMenuTracker(JComponent component, JPopupMenu popupMenu) {
        this(new JComponent[]{component}, popupMenu);
    }

    public void mouseReleased(MouseEvent e) {
        trackPopup(e);
    }

    public void mouseClicked(MouseEvent e) {
        trackPopup(e);
    }

    public void mousePressed(MouseEvent e) {
        trackPopup(e);
    }

    private void trackPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            _popupMenu.show((Component) e.getSource(), e.getX(), e.getY());
        }
    }
}