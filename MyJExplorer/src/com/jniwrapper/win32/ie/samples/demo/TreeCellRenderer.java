/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import com.jniwrapper.win32.ie.samples.demo.components.IconLoader;
import com.jniwrapper.win32.shell.SHFileInfo;
import com.jniwrapper.win32.shell.ShellIcon;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

class TreeCellRenderer extends DefaultTreeCellRenderer {
    private static ImageIcon FOLDER_ICON = new ImageIcon(new ShellIcon("").toImage());
    private static ImageIcon FOLDER_OPEN_ICON = new ImageIcon(SHFileInfo.getFileInfo("", SHFileInfo.SHGFI_SMALLICON | SHFileInfo.SHGFI_ICON | SHFileInfo.SHGFI_OPENICON).getIcon().toImage());

    public TreeCellRenderer() {
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (!leaf) {
            if (expanded) {
                setIcon(FOLDER_OPEN_ICON);
            } else {
                setIcon(FOLDER_ICON);
            }
        } else {
            setIcon(IconLoader.getInstance().getDocIcon());
        }
        return this;
    }
}