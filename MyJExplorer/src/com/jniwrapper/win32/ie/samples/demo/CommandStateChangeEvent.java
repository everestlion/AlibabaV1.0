/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import com.jniwrapper.win32.ie.WebBrowser;

import java.util.EventObject;

class CommandStateChangeEvent extends EventObject {
    private boolean _isBackButton;
    private boolean _enabled;

    public CommandStateChangeEvent() {
        super(new Object());
    }

    public CommandStateChangeEvent(WebBrowser browser, boolean isBackButton, boolean enabled) {
        super(browser);
        _isBackButton = isBackButton;
        _enabled = enabled;
    }

    public WebBrowser getBrowser() {
        return (WebBrowser) getSource();
    }

    public boolean isEnabled() {
        return _enabled;
    }

    public boolean isBackButton() {
        return _isBackButton;
    }

    public String toString() {
        return "CommandStateChangeEvent: back button=" + _isBackButton + "; enabled=" + _enabled;
    }
}