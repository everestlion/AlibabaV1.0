/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo.components;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 * @author Serge Piletsky
 */
public class ActionButton extends JButton {
    public ActionButton(String hint, Action action) {
        super(action);
        setUI(new BasicButtonUI());
        setBorder(null);
        setBorderPainted(false);
        setRolloverEnabled(true);
        setToolTipText(hint);
        setText(null);
        setFocusable(false);
        setDefaultCapable(false);
    }
}