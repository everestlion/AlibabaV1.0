/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import org.w3c.dom.Node;

import javax.swing.tree.DefaultTreeModel;

class DOMNodeTreeModel extends DefaultTreeModel {
    public DOMNodeTreeModel(Node docNode) {
        super(new DOMNodeTreeNode(docNode));
    }
}