/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo.components;

import com.jniwrapper.win32.gdi.Icon;

import java.awt.*;
import java.io.IOException;
import java.net.URL;

/**
 * @author Serge Piletsky
 */
public class FavoriteIcon extends Icon {
    private static final String FAVORITE_ICON_FILE = "/favicon.ico";

    public FavoriteIcon(String url) throws IOException {
        URL inURL = new URL(url);
        URL favoriteIconURL = new URL(inURL.getProtocol(), inURL.getHost(), FAVORITE_ICON_FILE);
        load(favoriteIconURL.openStream(), new Dimension(16, 16));
    }
}