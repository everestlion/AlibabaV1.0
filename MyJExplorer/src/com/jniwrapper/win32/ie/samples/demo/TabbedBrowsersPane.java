/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import com.jniwrapper.win32.ie.*;
import com.jniwrapper.win32.ie.samples.demo.components.IconLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.HashMap;
import java.util.Map;

/**
 * TabbedBrowsersPane class provides multi-tab browser support.
 *
 * @author Serge Piletsky
 */
class TabbedBrowsersPane extends TitledPane {
    private static final Logger LOG = LoggerFactory.getLogger(TabbedBrowsersPane.class);

    public static final String PROPERTY_PROGRESS = "progress";
    public static final String PROPERTY_INDEX = "index";
    public static final String PROPERTY_COMMAND_STATE = "commandState";
    public static final int DEFAULT_INDEX = -1;
    public static final int THUMBNAIL_OFFSET = 25;
    public static final Dimension THUMBNAIL_SIZE = new Dimension(200, 200);

    private Map<WebBrowser, WebBrowser> _component2BrowserMap = new HashMap<WebBrowser, WebBrowser>();
    private Map<WebBrowser, Boolean[]> _browserState2BrowserMap = new HashMap<WebBrowser, Boolean[]>();
    private JTabbedPane _tabbedPane = new JTabbedPane();
    private JPopupMenu _actions;
    private JMenuItem _openLinkInNewBrowser;
    private int _currentIndex = DEFAULT_INDEX;

    public TabbedBrowsersPane(JPopupMenu actions, final Action closeAction) {
        super();
        setBorder(null);
        _tabbedPane.setBorder(null);
        _tabbedPane.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON2) {
                    closeAction.actionPerformed(null);
                }
            }

            public void mouseExited(MouseEvent e) {
                firePropertyChange(DEFAULT_INDEX);
            }
        });
        _tabbedPane.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                Point point = e.getPoint();
                int count = _tabbedPane.getTabCount();
                for (int index = 0; index < count; index++) {
                    Rectangle rectangle = _tabbedPane.getBoundsAt(index);
                    if (rectangle.contains(point)) {
                        firePropertyChange(index);
                        return;
                    }
                }
                firePropertyChange(DEFAULT_INDEX);
            }
        });
        _tabbedPane.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                setTitle(_tabbedPane.getTitleAt(_tabbedPane.getSelectedIndex()));
                updateCommandsState();
            }
        });

        _tabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
        setComponent(_tabbedPane);

        assignCloseAction(closeAction);
        assignActions(actions);
        _actions = actions;
        _openLinkInNewBrowser = new JMenuItem(new OpenLinkInNewTab());
    }

    private void firePropertyChange(int index) {
        firePropertyChange(index, false);
    }

    public void firePropertyChange(int index, boolean force) {
        if (!force && _currentIndex != index) {
            _tabbedPane.firePropertyChange(PROPERTY_INDEX, _currentIndex, index);
            _currentIndex = index;
        }
        if (force) {
            _tabbedPane.firePropertyChange(PROPERTY_INDEX, DEFAULT_INDEX, index);
            _currentIndex = index;
        }
    }

    private class OpenLinkInNewTab extends AbstractAction {
        public OpenLinkInNewTab() {
            super("Open Link in New Tab");
        }

        public void actionPerformed(ActionEvent e) {
            WebBrowser webBrowser = getActiveBrowser();
            if (!(webBrowser instanceof Browser)) {
                return;
            }

            Element element = ((Browser) webBrowser).getContextElement();
            if (element == null) {
                return;
            }

            if ("A".equalsIgnoreCase(element.getNodeName())) {
                WebBrowser browser = createBrowser();
                String url = element.getAttribute("href");
                browser.navigate(url);
            }
        }
    }

    private void updateCommandsState() {
        final WebBrowser activeBrowser = getActiveBrowser();

        if (activeBrowser == null) {
            return;
        }

        Boolean[] commandsState = _browserState2BrowserMap.get(activeBrowser);
        if (commandsState == null) {
            commandsState = new Boolean[2];
            commandsState[0] = Boolean.FALSE;
            commandsState[1] = Boolean.FALSE;
        }

        fireCommandStateChanged(new CommandStateChangeEvent(activeBrowser, true, commandsState[0]));
        fireCommandStateChanged(new CommandStateChangeEvent(activeBrowser, false, commandsState[1]));
    }

    public void addBrowser(final Browser webBrowser) {
        webBrowser.setPreferredSize(new Dimension(100, 100));
        setupWebBrowser2EventsHandler(webBrowser);
        final Runnable runnable = new Runnable() {
            public void run() {
                ContextMenuProvider contextMenuProvider = new ContextMenuProvider() {
                    public JPopupMenu getPopupMenu(Element contextNode) {
                        final boolean isLink = "A".equals(contextNode.getTagName());
                        if (isLink) {
                            _actions.add(_openLinkInNewBrowser);
                        } else {
                            _actions.remove(_openLinkInNewBrowser);
                        }
                        return _actions;
                    }
                };
                webBrowser.setContextMenuProvider(contextMenuProvider);

                _tabbedPane.setSelectedComponent(webBrowser);

                updateBrowserTitleIcon(webBrowser, IconLoader.getInstance().getDefaultIcon());
                updateBrowserTitleText(webBrowser, "about:blank");
            }
        };

        _tabbedPane.addTab(null, webBrowser);

        SwingUtilities.invokeLater(runnable);

        _component2BrowserMap.put(webBrowser, webBrowser);
    }

    public WebBrowser createBrowser() {
        Browser webBrowser = new Browser();
        addBrowser(webBrowser);
        return webBrowser;
    }

    int getBrowserIndex(WebBrowser webBrowser) {
        int tabCount = _tabbedPane.getTabCount();
        for (int i = 0; i < tabCount; i++) {
            WebBrowser browser = (WebBrowser) _tabbedPane.getComponentAt(i);
            if (browser.equals(webBrowser)) {
                return i;
            }
        }
        if (webBrowser instanceof Component) {
            return _tabbedPane.indexOfComponent((Component) webBrowser);
        }
        return _tabbedPane.indexOfComponent((Component) getActiveBrowser());
    }

    /**
     * Updates the title of the browser.
     *
     * @param webBrowser a browser to update the title for.
     * @param title      a new title.
     */
    public void updateBrowserTitleText(WebBrowser webBrowser, String title) {
        int index = getBrowserIndex(webBrowser);

        if (index >= 0) {
//            Graphics2D graphics = (Graphics2D) _tabbedPane.getGraphics().create();
            // creates new title that smaller then required length
            String processedTitle = title;
//            graphics.dispose();

            _tabbedPane.setTitleAt(index, processedTitle);
            _tabbedPane.setToolTipTextAt(index, title);

            WebBrowser activeBrowser = getActiveBrowser();
            if (activeBrowser.equals(webBrowser)) {
                setTitle(title);
            }
        }
    }

    /**
     * Casts the specified title to the specified lenght in pixels.
     *
     * @param graphics for the tabbed page object.
     * @param title    that will be casted to the specified lingth in pixels.
     * @param length   in pixels.
     * @return updated title.
     */
    private String createProcessedTitle(Graphics2D graphics, String title, int length) {
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int stringWidth = fontMetrics.stringWidth(title);
        if (stringWidth > length) {
            int endIndex = title.length() - 3;
            do {
                title = title.substring(0, --endIndex);
                stringWidth = fontMetrics.stringWidth(title);
            }
            while (stringWidth > length);

            title += "...";
        } else {
            do {
                title += "|";
                System.out.println("title = " + title);
                stringWidth = fontMetrics.stringWidth(title);
                System.out.println("stringWidth = " + stringWidth);
            }
            while (stringWidth >= 0 && stringWidth < length);
        }

        return title;
    }

    public void updateBrowserTitleIcon(WebBrowser webBrowser, Icon icon) {
        int index = getBrowserIndex(webBrowser);
        if (index != -1) {
//            _tabbedPane.setIconAt(index, icon);
        }
    }

    /**
     * Removes a browser from the pane.
     *
     * @param webBrowser webBrowser to remove.
     */
    public void removeBrowser(final WebBrowser webBrowser) {
        final int index = setActiveBrowser(webBrowser);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _tabbedPane.remove(index);
                if (index == 0) {
                    _tabbedPane.setSelectedIndex(index);
                }
                webBrowser.close();
                _component2BrowserMap.remove(webBrowser);
                updateCommandsState();
            }
        });
    }

    void removeBrowser() {
        final WebBrowser browser = getActiveBrowser();

        if (_tabbedPane.getTabCount() > 1) {
            setActiveBrowser(browser);
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (_tabbedPane.getTabCount() > 1) {
                    removeBrowser(browser);
                }
            }
        });
//        else if (_tabbedPane.getTabCount() == 1)
//        {
//            WebBrowser browserForClose = getActiveBrowser();
//            createBrowser();
//            removeBrowser(browserForClose);
//        }
    }

    private int setActiveBrowser(WebBrowser webBrowser) {
        int newIndex;
        int index = getBrowserIndex(webBrowser);
        if (index == (_tabbedPane.getTabCount() - 1)) {
            newIndex = index - 1;
        } else {
            newIndex = index + 1;
        }

        _tabbedPane.setSelectedIndex(newIndex);

        return index;
    }

    /**
     * Returns an active browser.
     *
     * @return active browser.
     */
    public WebBrowser getActiveBrowser() {
        final Component selectedComponent = _tabbedPane.getSelectedComponent();
        return _component2BrowserMap.get(selectedComponent);
    }

    private void setupWebBrowser2EventsHandler(final WebBrowser browser) {
        final BrowserEventsListener eventHandler = new BrowserEventsListener();

        browser.addNavigationListener(eventHandler);
        browser.addStatusListener(eventHandler);
        browser.setEventHandler(eventHandler);

        eventHandler._tabbedBrowsersPane = this;
        eventHandler._webBrowser = browser;

        BrowserNewWindowEventHandler handler = new BrowserNewWindowEventHandler(browser);
        handler._tabbedBrowsersPane = this;

        browser.setNewWindowHandler(handler);
    }

    public JTabbedPane getTabbedPane() {
        return _tabbedPane;
    }

    private void updateButtonState(WebBrowser browser, boolean isBackButton, boolean enable) {
        Boolean[] commandsState = _browserState2BrowserMap.get(browser);
        if (commandsState == null) {
            commandsState = new Boolean[2];
            commandsState[0] = Boolean.FALSE;
            commandsState[1] = Boolean.FALSE;
            _browserState2BrowserMap.put(browser, commandsState);
        }

        int index = isBackButton ? 0 : 1;
        commandsState[index] = enable;

        fireCommandStateChanged(new CommandStateChangeEvent(browser, isBackButton, enable));
    }

    public void backButtonEnabled(WebBrowser browser, boolean enabled) {
        updateButtonState(browser, true, enabled);
    }

    public void forwardButtonEnabled(WebBrowser browser, boolean enabled) {
        updateButtonState(browser, false, enabled);
    }

    public void progressChanged(int progress, int progressMax) {
        firePropertyChange(PROPERTY_PROGRESS, new Integer(progress), new Integer(progressMax));
    }

    protected void fireCommandStateChanged(CommandStateChangeEvent event) {
        firePropertyChange(PROPERTY_COMMAND_STATE, null, event);
    }
}