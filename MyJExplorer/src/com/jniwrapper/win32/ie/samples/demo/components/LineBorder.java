/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo.components;

import javax.swing.border.AbstractBorder;
import java.awt.*;

public class LineBorder extends AbstractBorder {
    public static final int HORISONTAL = 0;
    public static final int VERTICAL = 1;

    private int _type = HORISONTAL;

    public LineBorder() {
    }

    public LineBorder(int type) {
        _type = type;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        g.translate(x, y);

        if (_type == HORISONTAL) {
            int w = width;
            int h = height - 1;

            g.setColor(Color.lightGray);
            g.drawLine(0, h - 1, w - 1, h - 1);

            g.setColor(Color.white);
            g.drawLine(1, h, w, h);
        } else if (_type == VERTICAL) {
            int h = height;

            g.setColor(Color.lightGray);
            g.drawLine(0, 0, 0, h - 1);

            g.setColor(Color.white);
            g.drawLine(1, 1, 1, h);
        }

        g.translate(-x, -y);
    }

    public Insets getBorderInsets(Component c) {
        return new Insets(0, 0, 0, 0);
    }

    public boolean isBorderOpaque() {
        return true;
    }
}