/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo.components;

import com.jniwrapper.util.FlagSet;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.MessageFormat;

/**
 * This component represents functionality for choosing URLs.
 *
 * @author Serge Piletsky
 */
public class URLField extends JComboBox {
    private static final String BLANK_PAGE = "about:blank";

    private IconLoader _iconLoader = IconLoader.getInstance();
    private URLComboBoxEditor _URLComboBoxEditor = new URLComboBoxEditor();

    public URLField() {
        setRenderer(new URLListCellRenderer());
        setEditor(_URLComboBoxEditor);
        setEditable(true);
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _iconLoader.loadIcon((String) getSelectedItem(), new IconLoader.IconLoadProcess() {
                    public void complete(Icon icon) {
                        _URLComboBoxEditor._urlIcon.setIcon(icon);
                        _URLComboBoxEditor._editor.requestFocus();
                    }
                });
            }
        });
    }

    public void setURLText(final String url) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _URLComboBoxEditor._editor.setText(url);
                _iconLoader.loadIcon(url, new IconLoader.IconLoadProcess() {
                    public void complete(Icon icon) {
                        _URLComboBoxEditor._urlIcon.setIcon(icon);
                        _URLComboBoxEditor.loadFavoriteIcon();
                    }
                });
            }
        });
    }

    public void setURL(String url) {
        if (url == null || url.length() == 0 || url.equals(BLANK_PAGE)) {
            _URLComboBoxEditor._urlIcon.setIcon(_iconLoader.getDefaultIcon());
            _URLComboBoxEditor._editor.setText("");
            return;
        }
        setSelectedItem(url);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                _URLComboBoxEditor.loadFavoriteIcon();
            }
        });
    }

    private static String getHost(String url) {
        try {
            return new URL(url).getHost();
        }
        catch (Exception e) {
            return "";
        }
    }

    public void requestFocus() {
        _URLComboBoxEditor._editor.requestFocus();
    }

    public Object getSelectedItem() {
        final Object result = super.getSelectedItem();
        return result == null ? "" : result.toString();
    }

    private class URLComboBoxEditor extends JPanel implements ComboBoxEditor {
        private static final String HTTP_PROTOCOL = "http://";
        private static final String DEFAULT_URL_PATTERN = HTTP_PROTOCOL + "www.{0}.com";
        private JTextField _editor = new JTextField();
        private JLabel _urlIcon;
        private Object _oldValue;

        public URLComboBoxEditor() {
            setBorder(null);
            setLayout(new BorderLayout());
            setBackground(_editor.getBackground());

            _urlIcon = new JLabel(_iconLoader.getDefaultIcon());
            _urlIcon.setPreferredSize(new Dimension(20, 20));

            _editor.setBorder(null);
            _editor.addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent e) {
                    final String text = _editor.getText();
                    FlagSet modifiers = new FlagSet(e.getModifiersEx());
                    if (e.getKeyChar() == '\n') {
                        if (modifiers.contains(KeyEvent.CTRL_DOWN_MASK)) {
                            final String newURL = MessageFormat.format(DEFAULT_URL_PATTERN, text);
                            _editor.setText(newURL);
                            _editor.postActionEvent();
                        } else if (!text.toLowerCase().startsWith(HTTP_PROTOCOL)) {
                            _editor.setText(text);
                            _editor.postActionEvent();
                        }
                    }
                }
            });


            add(_urlIcon, BorderLayout.WEST);
            add(_editor, BorderLayout.CENTER);
        }

        private void loadFavoriteIcon() {
            String urlText = _editor.getText();

            DefaultComboBoxModel model = (DefaultComboBoxModel) URLField.this.getModel();
            if (model.getIndexOf(urlText) == -1) {
                insertItemAt(urlText, 0);
                _iconLoader.loadIcon(urlText, new IconLoader.IconLoadProcess() {
                    public void complete(Icon icon) {
                        _urlIcon.setIcon(icon);
                    }
                });
            }
        }

        public Component getEditorComponent() {
            return this;
        }

        public void setItem(Object anObject) {
            if (anObject != null) {
                _oldValue = anObject;
            }
        }

        public Object getItem() {
            Object newValue = _editor.getText();
            if (_oldValue != null && !(_oldValue instanceof String)) {
                if (newValue.equals(_oldValue.toString())) {
                    return _oldValue;
                } else {
                    try {
                        Method method = _oldValue.getClass().getMethod("valueOf", String.class);
                        newValue = method.invoke(_oldValue, _editor.getText());
                    }
                    catch (Exception e) {
                        return _oldValue;
                    }
                }
            }
            return newValue;
        }

        public void selectAll() {
            _editor.selectAll();
            _editor.requestFocus();
        }

        public void addActionListener(ActionListener l) {
            _editor.addActionListener(l);
        }

        public void removeActionListener(ActionListener l) {
            _editor.removeActionListener(l);
        }
    }

    class URLListCellRenderer extends JPanel implements ListCellRenderer {
        private JLabel _urlIcon;

        public URLListCellRenderer() {
            setBorder(null);
            _urlIcon = new JLabel();
            _urlIcon.setBorder(new EmptyBorder(1, 4, 1, 0));
            setLayout(new BorderLayout());
            add(_urlIcon, BorderLayout.WEST);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String text = value.toString();
            _urlIcon.setText(text);
            if (index != -1) {
                IconLoader.getInstance().loadIcon(text, new IconLoader.IconLoadProcess() {
                    public void complete(Icon icon) {
                        _urlIcon.setIcon(icon);
                    }
                });
                if (isSelected) {
                    _urlIcon.setForeground(list.getSelectionForeground());
                    setBackground(list.getSelectionBackground());
                } else {
                    _urlIcon.setForeground(list.getForeground());
                    setBackground(list.getBackground());
                }
            }
            return this;
        }
    }
}