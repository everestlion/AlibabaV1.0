/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import com.jniwrapper.win32.ie.WebBrowser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Vladimir Kondrashchenko
 */
public class SettingsMenu extends JMenu {
    private JCheckBoxMenuItem _allowImages = new JCheckBoxMenuItem("Allow Images");
    private JCheckBoxMenuItem _allowVideos = new JCheckBoxMenuItem("Allow Videos");
    private JCheckBoxMenuItem _allowSounds = new JCheckBoxMenuItem("Allow Sounds");
    private JCheckBoxMenuItem _allowScripts = new JCheckBoxMenuItem("Allow Scripts");
    private JCheckBoxMenuItem _allowJavaApplets = new JCheckBoxMenuItem("Allow Java Applets");
    private JCheckBoxMenuItem _allowDownloadActiveX = new JCheckBoxMenuItem("Allow Download ActiveX");
    private JCheckBoxMenuItem _allowRunActiveX = new JCheckBoxMenuItem("Allow Run ActiveX");
    private JCheckBoxMenuItem _allowNewWindow = new JCheckBoxMenuItem("Allow New Window");
    private JCheckBoxMenuItem _allowContextMenu = new JCheckBoxMenuItem("Allow Context Menu");

//    private JMenuItem _connectionSettings = new JMenuItem("Connection Settings...");

    private WebBrowser.Properties _properties;

    public SettingsMenu(Frame owner) {
        super("Settings");

//        Frame _owner = owner;

        add(_allowImages);
        add(_allowVideos);
        add(_allowSounds);
        addSeparator();
        add(_allowScripts);
        add(_allowRunActiveX);
        add(_allowDownloadActiveX);
        add(_allowJavaApplets);
        addSeparator();
        add(_allowNewWindow);
        add(_allowContextMenu);
//        addSeparator();
//        add(_connectionSettings);

        initActionListeners();
    }

    private void initActionListeners() {
        _allowImages.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowImages(_allowImages.isSelected());
            }
        });

        _allowVideos.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowVideos(_allowVideos.isSelected());
            }
        });

        _allowSounds.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowSounds(_allowSounds.isSelected());
            }
        });

        _allowScripts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowScripts(_allowScripts.isSelected());
            }
        });

        _allowRunActiveX.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowRunActiveX(_allowRunActiveX.isSelected());
            }
        });

        _allowDownloadActiveX.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowDownloadActiveX(_allowDownloadActiveX.isSelected());
            }
        });

        _allowJavaApplets.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowJavaApplets(_allowJavaApplets.isSelected());
            }
        });

        _allowNewWindow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowNewWindow(_allowNewWindow.isSelected());
            }
        });

        _allowContextMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                _properties.setAllowContextMenu(_allowContextMenu.isSelected());
            }
        });

//        _connectionSettings.addActionListener(new ActionListener()
//        {
//            public void actionPerformed(ActionEvent e)
//            {
//                ConnectionSettingsDialog dialog = new ConnectionSettingsDialog(_owner);
//                dialog.pack();
//                dialog.setLocationRelativeTo(_owner);
//                dialog.setVisible(true);
//            }
//        });
    }

    public void setProperties(WebBrowser.Properties properties) {
        _properties = properties;

        _allowContextMenu.setSelected(_properties.isAllowContextMenu());
        _allowDownloadActiveX.setSelected(_properties.isAllowDownloadActiveX());
        _allowImages.setSelected(_properties.isAllowImages());
        _allowJavaApplets.setSelected(_properties.isAllowJavaApplets());
        _allowNewWindow.setSelected(_properties.isAllowNewWindow());
        _allowRunActiveX.setSelected(_properties.isAllowRunActiveX());
        _allowScripts.setSelected(_properties.isAllowScripts());
        _allowSounds.setSelected(_properties.isAllowSounds());
        _allowVideos.setSelected(_properties.isAllowVideos());
    }
}