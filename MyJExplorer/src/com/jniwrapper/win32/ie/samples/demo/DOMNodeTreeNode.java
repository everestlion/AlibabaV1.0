/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.List;

class DOMNodeTreeNode extends DefaultMutableTreeNode {
    private static final String INNER_HTML = "innerHTML";
    private static final String ELEMENT_TITLE = "TITLE";
    private static final String ELEMENT_STYLE = "STYLE";
    private static final String ELEMENT_SCRIPT = "SCRIPT";

    private Node _browserNode;
    private List<DOMNodeTreeNode> _children;
    private String _text;
    private static final String NAME_TEXT = "#text";

    private DOMNodeTreeNode(String text) {
        _text = text;
    }

    public DOMNodeTreeNode(Node browserNode) {
        _browserNode = browserNode;
    }

    public Node getBrowserNode() {
        return _browserNode;
    }

    public String getText() {
        return _text;
    }

    private TreeNode[] getChildNodes() {
        if (_browserNode == null) {
            return new TreeNode[0];
        }
        if (_children == null) {
            if (_text == null) {
                NodeList childNodes = _browserNode.getChildNodes();
                int length = childNodes.getLength();

                _children = new ArrayList<DOMNodeTreeNode>(length);
                for (int i = 0; i < length; i++) {
                    _children.add(new DOMNodeTreeNode(childNodes.item(i)));
                }
            } else {
                _children = new ArrayList<DOMNodeTreeNode>(0);
            }

            if (_browserNode != null) {
                String nodeName = _browserNode.getNodeName();

                if (nodeName.equalsIgnoreCase(ELEMENT_TITLE) ||
                        nodeName.equalsIgnoreCase(ELEMENT_STYLE) ||
                        nodeName.equalsIgnoreCase(ELEMENT_SCRIPT)) {
                    Element element = (Element) _browserNode;
                    String attributeValue = element.getAttribute(INNER_HTML);
                    DOMNodeTreeNode node = new DOMNodeTreeNode(attributeValue);
                    _children.add(node);
                }
            }
        }

        TreeNode result[] = new TreeNode[_children.size()];
        _children.toArray(result);

        return result;
    }

    public TreeNode getChildAt(int index) {
        return getChildNodes()[index];
    }

    public int getChildCount() {
        return getChildNodes().length;
    }

    public boolean isLeaf() {
        return getChildCount() == 0;
    }

    public String toString() {
        if (_browserNode == null) {
            if (_text != null) {
                return NAME_TEXT;
            } else {
                return "";
            }
        } else {
            return _browserNode.getNodeName();
        }
    }
}