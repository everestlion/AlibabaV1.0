/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo.components;

import javax.swing.*;
import java.awt.*;

public class LineBevel extends JPanel {
    public LineBevel(int type) {
        init(type);
    }

    private void init(int type) {
        setBorder(new LineBorder(type));

        if (type == LineBorder.HORISONTAL)
            setPreferredSize(new Dimension(1, 2));
        else
            setPreferredSize(new Dimension(2, 1));
    }

    public LineBevel() {
        init(LineBorder.HORISONTAL);
    }
}