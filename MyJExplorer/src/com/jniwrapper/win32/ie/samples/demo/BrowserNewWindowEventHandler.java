/*
 * Copyright (c) 2000-2009 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */
package com.jniwrapper.win32.ie.samples.demo;

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.NewWindowEventHandler;

/**
 * @author Alexei Orischenko
 */
class BrowserNewWindowEventHandler implements NewWindowEventHandler {
    protected TabbedBrowsersPane _tabbedBrowsersPane;
    protected WebBrowser _browser;

    public BrowserNewWindowEventHandler(WebBrowser browser) {
        _browser = browser;
    }

    public NewWindowEventHandler.NewWindowAction newWindow() {
        Browser browser = new Browser((Browser) _browser);

        _tabbedBrowsersPane.addBrowser(browser);

        NewWindowEventHandler.NewWindowAction result = new NewWindowEventHandler.NewWindowAction(NewWindowEventHandler.NewWindowAction.ACTION_REPLACE_BROWSER);
        result.setBrowser(browser);

        return result;
    }
}