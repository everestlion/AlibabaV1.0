package utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class Utils {

	
	
	public static byte[] getUrlFileData(String fileUrl)
	{
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		try {
			URL url = new URL(fileUrl);
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			httpConn.connect();
			InputStream cin = httpConn.getInputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = cin.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}
			cin.close();
			byte[] fileData = outStream.toByteArray();
			outStream.close();
			return fileData;
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (outStream != null) {
					outStream.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String getHtmlContent(URL url, String encode) {
		StringBuffer contentBuffer = new StringBuffer();
	
		int responseCode = -1;
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setRequestProperty("User-Agent",
					"Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");// IE代理进行下载
			con.setConnectTimeout(60000);
			con.setReadTimeout(60000);
			// 获得网页返回信息码
			responseCode = con.getResponseCode();
			if (responseCode == -1) {
				System.out.println(url.toString()
						+ " : connection is failure...");
				con.disconnect();
				return null;
			}
			if (responseCode >= 400) // 请求失败
			{
				System.out.println("请求失败:get response code: " + responseCode);
				con.disconnect();
				return null;
			}
	
			InputStream inStr = con.getInputStream();
			InputStreamReader istreamReader = new InputStreamReader(inStr,
					encode);
			BufferedReader buffStr = new BufferedReader(istreamReader);
	
			String str = null;
			while ((str = buffStr.readLine()) != null)
				contentBuffer.append(str);
			inStr.close();
		} catch (IOException e) {
			e.printStackTrace();
			contentBuffer = null;
			System.out.println("error: " + url.toString());
		} finally {
			con.disconnect();
		}
		return contentBuffer.toString();
	}

	public static String getHtmlContent(String url, String encode) {
		if (!url.toLowerCase().startsWith("http://")) {
			url = "http://" + url;
		}
		try {
			URL rUrl = new URL(url);
			return getHtmlContent(rUrl, encode);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void writeToXml(Document document, String xmlPath) {
		File xmlFile = new File(xmlPath);
		if (xmlFile.exists()) {
			xmlFile.delete();
		}
		
		XMLWriter output;
		OutputFormat format = OutputFormat.createPrettyPrint();
		try {
			output = new XMLWriter(new FileWriter(xmlPath), format);
			output.write(document);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static <T> void setXmlValue(Element element, Object pm, Class<T> clazz) {
		try {
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				if (Modifier.isStatic(f.getModifiers())) {
					continue;
				}
				String name = f.getName();
				System.out.println(name);
				Node node = element.element(name);
				if (node != null) {
					f.setAccessible(true);
					Object obj = f.get(pm);
					if (obj != null) {
						if (f.getType() == Integer.class || f.getType() == int.class) {
							node.setText(String.valueOf(obj));
						} else {
							node.setText((String) obj);
						}
					}
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public static <T> void setValue(Node e, Object pm, Class<T> clazz) {
		try {
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				String name = f.getName();
				System.out.println(name);
				Node node = ((Element)e).element(name);
				if (node != null) {
					String value = node.getText();
					if (value != null && value.length() > 0) {
						f.setAccessible(true);
						if (f.getType() == Integer.class || f.getType() == int.class) {
							f.setInt(pm, Integer.parseInt(value));
						} else {
							f.set(pm, value);
						}
					} 
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
	
}
