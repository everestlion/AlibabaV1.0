package utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelUtils {

	public static void writeToExcel(HashMap<String, String[]> data, String fileName) {
		FileOutputStream fos = null;
		HSSFWorkbook wb = new HSSFWorkbook();
		try {
			HSSFSheet sheet = wb.createSheet("data");
			int rowCounter = 1, colCounter = 0;
			for (String[] e : data.values()) {
				HSSFRow row = sheet.createRow(rowCounter);
				for (String val : e) {
					HSSFCell cell = row.createCell(colCounter);
					cell.setCellValue(val);
					colCounter++;
				}
				rowCounter++;
				colCounter = 0;
			}
			
			fos = new FileOutputStream(fileName);
			wb.write(fos);
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
