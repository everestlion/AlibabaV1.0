package utils;

import java.util.concurrent.TimeUnit;

public class TimeUtils {

	public static void sleep(long time) {
		try {
			TimeUnit.MILLISECONDS.sleep(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
