package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegexProcesser {

	
	public static String format(String str) {

		Pattern p = Pattern.compile("\\{npc:(\\d+)\\}");

		Matcher m = p.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			int index = Integer.parseInt(m.group(1));
			
			m.appendReplacement(sb, index+"");

		}
		return sb.toString();
	}
	
}
