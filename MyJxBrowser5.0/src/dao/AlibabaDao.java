package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import utils.TimeProcesser;
import vo.Product;

public class AlibabaDao {
	
	public static final String TABLE = "products";

	public static void insertProduct(Product p, String searchKeyword) {
		
		String sql = "INSERT INTO " + TABLE + " VALUES(0,'" + p.getProductId() + "','" + p.getTitle() + "','" + 
					p.getHref() + "','" + p.getCompany() + "'," + p.getOrder()  + "," + p.getCatlogTyep()  + ",'" + searchKeyword + "',CURRENT_DATE());";
		
		System.out.println(sql);
		
		try {
			DBUtil.insert(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static Map<String, Map<String, Product>> selectProductsDB() {
		Map<String, Map<String, Product>> map = new HashMap<String, Map<String,Product>>();
		String date = TimeProcesser.getDateByUnixTime(TimeProcesser.getUnixTime() - 86400);
		String sql = "SELECT * FROM " + TABLE + " WHERE date='" + date +"';";
		System.out.println(sql);
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = DBUtil.getConnection().createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next() != false) {
				Product product = new Product();
				product.setProductId(rs.getString(2));
				product.setTitle(rs.getString(3));
				product.setHref(rs.getString(4));
				product.setCompany(rs.getString(5));
				product.setOrder(rs.getInt(6));
				product.setCatlogTyep(rs.getInt(7));
				product.setSearchKeywords(rs.getString(8));
				System.out.println(product.getTitle());
				if (product.getSearchKeywords() == null) {
					System.out.println(product.getProductId());
				} else {
					if (map.containsKey(product.getSearchKeywords())) {
						map.get(product.getSearchKeywords()).put(product.getProductId(), product);
					} else {
						Map<String, Product> pMap = new HashMap<String, Product>();
						pMap.put(product.getProductId(), product);
						map.put(product.getSearchKeywords(), pMap);
					}
				}
			}
			System.out.println(map.size());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
//				if (conn != null) {
////					conn.close();
//				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return map;
	}
	
	public static void main(String[] args) {
		selectProductsDB();
	}
	
}
