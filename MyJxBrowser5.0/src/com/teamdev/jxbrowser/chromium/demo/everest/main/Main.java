package com.teamdev.jxbrowser.chromium.demo.everest.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import message.AutoLoginMessage;
import message.CollectCustomerDetailMessage;
import message.DoSelectProductModelMessage;
import message.EditProductPageMessage;
import message.HotKeywordMessage;
import message.MessageManager;
import message.OnAlertMessage;
import message.PostProductFromImportPageMessage;
import message.PreSelectPhotoMessage;
import message.RepetProductGroupListMessage;
import message.RepetProductGroupMessage;
import message.SelectPhotoMessage;
import message.SelectProductModelMessage;

import org.apache.commons.lang3.StringUtils;

import utils.TimeUtils;
import vo.HotWords;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserContext;
import com.teamdev.jxbrowser.chromium.CloseStatus;
import com.teamdev.jxbrowser.chromium.DialogParams;
import com.teamdev.jxbrowser.chromium.FileChooserMode;
import com.teamdev.jxbrowser.chromium.FileChooserParams;
import com.teamdev.jxbrowser.chromium.KeyFilter;
import com.teamdev.jxbrowser.chromium.PopupContainer;
import com.teamdev.jxbrowser.chromium.PopupParams;
import com.teamdev.jxbrowser.chromium.RequestCompletedParams;
import com.teamdev.jxbrowser.chromium.demo.resources.Resources;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import com.teamdev.jxbrowser.chromium.swing.DefaultDialogHandler;
import com.teamdev.jxbrowser.chromium.swing.DefaultNetworkDelegate;
import com.teamdev.jxbrowser.chromium.swing.DefaultPopupHandler;

import config.UrlConfig;

public class Main {
	
	private static Map<String, HotWords> hotwordsMap = new HashMap<String, HotWords>();
	
	private static BrowserView browserView;
	
	private static BrowserView browserView2;
	
	private static JFrame hideFrame;
	
	private static BrowserView browserView3;
	private static JFrame selectProductFrame;
	
	private static BrowserView browserView4;
	private static JFrame collectKeywordsFream;
	
	private static BrowserView browserView5;
	private static JFrame repeatProductFrame;
	
	private static BrowserView browserView6;
	private static JFrame selectProductImageFrame;
	
	private static BrowserView browserView7;
	private static JFrame messagesFrame;
	
	private static BrowserView tempView;
	private static boolean isLogin;
	private static JButton autoLoginButton = new JButton("自动登陆");
	
	public static void main(String[] args) {
//		System.setProperty("teamdev.license.info", "false");
//		Browser browser = BrowserFactory.create();
		BrowserContext browserContext = BrowserContext.defaultContext();
		browserContext.getNetworkService().setNetworkDelegate(new DefaultNetworkDelegate() {
			
			@Override
			public void onCompleted(RequestCompletedParams arg0) {
				
				if (arg0.getURL() != null) {
					if (arg0.getURL().startsWith(UrlConfig.hotKeywordSearchUrl2)) {
						if (HotKeywordMessage.isStart()) {
							MessageManager.sendMessage(new HotKeywordMessage());
						}
					} else if (arg0.getURL().startsWith(UrlConfig.repetProductGrouListpUrl)) {
						MessageManager.sendMessage(new RepetProductGroupListMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.repetProductGroupUrl)) {
						MessageManager.sendMessage(new RepetProductGroupMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.editProductUrl)) {
						MessageManager.sendMessage(new EditProductPageMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.postProductFromImportUrl)) {
						MessageManager.sendMessage(new PostProductFromImportPageMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.selectPhotoUrl)) {
						MessageManager.sendMessage(new SelectPhotoMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.selectPhotoCatlogUrl)) {
//						SelectPhotoMessage.notifyFinished();
						MessageManager.sendMessage(new PreSelectPhotoMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.productManageUrl2)) {
						MessageManager.sendMessage(new SelectProductModelMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.logoutUrl)) {
						setLogin(false);
					}else if (arg0.getURL().startsWith(UrlConfig.photoBankUrl)) {
						
					} else if (arg0.getURL().startsWith(UrlConfig.messagesUrl) || arg0.getURL().startsWith(UrlConfig.messageAjaxUrl)) {
						if (CollectCustomerDetailMessage.start) {
							MessageManager.sendMessage(new CollectCustomerDetailMessage());
						}
					}
					
				}
				System.out.println("onCompleted===" + arg0.getURL());
			}
		});
		
		Browser browser = new Browser(browserContext);
		browserView = new BrowserView(browser);
		setListeners(browserView);
		JFrame frame = new JFrame("Main");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(browserView, BorderLayout.CENTER);
        frame.setIconImage(new ImageIcon("resource/wand.png").getImage());
        //全屏
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        //方法一
//        Rectangle bounds = new Rectangle( screenSize );
//        frame.setBounds(bounds);
        //方法二
        int width = (int)screenSize.getWidth();
        int height = (int)screenSize.getHeight();
//        frame.setSize(width, height);
        Insets scrInsets=Toolkit.getDefaultToolkit().getScreenInsets(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
        frame.setBounds(scrInsets.left,scrInsets.top,width-scrInsets.left-scrInsets.right,height-scrInsets.top-scrInsets.bottom); 
//        frame.setSize(700, 500);
        
        
        tempView = new BrowserView(new Browser(BrowserContext.defaultContext()));
        
        JPanel southPanel = new JPanel();
	    southPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    
	    autoLoginButton.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		MessageManager.sendMessage(new AutoLoginMessage());
	    	}
	    });
	    southPanel.add(autoLoginButton);
	    
	    JButton selectProductButton = creatSelectProductButton();
	    southPanel.add(selectProductButton);
	    
	    JButton collectKeywordsButton = createCollectKeywordsButton();
	    southPanel.add(collectKeywordsButton);
	    
	    JButton selectImageButton = createSelectImageButton();
	    southPanel.add(selectImageButton);
	    
	    JButton dealRepeatProductButton = createDealRepeatProductButton();
	    southPanel.add(dealRepeatProductButton);
	    
	    JButton messagesButton = createMessagesButton();
	    southPanel.add(messagesButton);
	    
	    frame.add(southPanel, BorderLayout.SOUTH);
        
        
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        Browser browser2 = new Browser(browserContext);
        browserView2 = new BrowserView(browser2);
        setListeners(browserView2);
        hideFrame = new JFrame("Hide");
        hideFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        hideFrame.add(browserView2, BorderLayout.CENTER);
        hideFrame.setSize(700, 500);
        hideFrame.setLocationRelativeTo(null);
        hideFrame.setVisible(false);
       
        browser.loadURL(UrlConfig.homepageUrl);
        
       
//        try {
//        	while (browser.isLoading()) {
//        		TimeUnit.MILLISECONDS.wait(50);
//        	}
//        } catch (InterruptedException e) {
//        	e.printStackTrace();
//        }
	}
	
	private static void setListeners(BrowserView browserView) {
		Browser browser = browserView.getBrowser();
		browser.addLoadListener(new LoadAdapter() {
			public void onFinishLoadingFrame(FinishLoadingEvent e) {
				super.onFinishLoadingFrame(e);
				
				System.out.println(e.getValidatedURL() + " finish loading......." + e.isMainFrame());
				
				
			}
        	
		});
        
        browser.setPopupHandler(new DefaultPopupHandler() {

			@Override
			public PopupContainer handlePopup(PopupParams arg0) {
				System.out.println("popup........");
				return super.handlePopup(arg0);
			}
        	
        });
        
        browserView.setKeyFilter(new KeyFilter<KeyEvent>() {
			
			@Override
			public boolean filter(KeyEvent event) {
				if (event.getKeyCode() == KeyEvent.VK_F5) {
					browser.reload();
				}
				return false;
			}
		});
        
        browser.setDialogHandler(new DefaultDialogHandler(browserView) {

			@Override
			public void onAlert(DialogParams arg0) {
				String message = arg0.getMessage();
				if (StringUtils.isEmpty(message)) {
					return;
				}
				String[] arr = message.split(",");
				if (StringUtils.isNumeric(arr[0])) {
					MessageManager.sendMessage(new OnAlertMessage(arr));
				} else {
					super.onAlert(arg0);
				}
				
				
			}
			
			@Override
            public CloseStatus onFileChooser(FileChooserParams params) {
                if (params.getMode() == FileChooserMode.Open) {
                    JFileChooser fileChooser = new JFileChooser();
                    int returnValue = fileChooser.showOpenDialog(
                            browserView);
                    if (returnValue == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = fileChooser.getSelectedFile();
                        params.setSelectedFiles(selectedFile.getAbsolutePath());
                        return CloseStatus.OK;
                    }
                }
                return CloseStatus.CANCEL;
            }
        
        	
        });
		
	}
	
	private static JButton createMessagesButton() {
		JButton button = new JButton("询盘");
	    button.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browserView7 == null) {
	    		  Browser browser = new Browser(BrowserContext.defaultContext());
	    		  browserView7 = new BrowserView(browser);
	    		  setListeners(browserView7);
	    	  }
	    	  if (messagesFrame == null) {
	    		  messagesFrame = new JFrame("Deal Repeat Product");
	    		  messagesFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  messagesFrame.add(browserView7, BorderLayout.CENTER);
	    		  messagesFrame.setSize(800, 800);
	    		  messagesFrame.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("整理客户资料");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						if (!CollectCustomerDetailMessage.start) {
							CollectCustomerDetailMessage.start = true;
							MessageManager.sendMessage(new CollectCustomerDetailMessage());
						}
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  messagesFrame.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	    	  browserView7.getBrowser().loadURL(UrlConfig.messagesUrl);
	    	  messagesFrame.setVisible(true);
	      
	      }

	    });
		return button;
	
	}
	
	private static JButton createSelectImageButton() {
		JButton button = new JButton("选择图片");
	    button.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browserView6 == null) {
	    		  Browser browser = new Browser(BrowserContext.defaultContext());
	    		  browserView6 = new BrowserView(browser);
	    		  setListeners(browserView6);
	    	  }
	    	  if (selectProductImageFrame == null) {
	    		  selectProductImageFrame = new JFrame("Deal Repeat Product");
	    		  selectProductImageFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  selectProductImageFrame.add(browserView6, BorderLayout.CENTER);
	    		  selectProductImageFrame.setSize(800, 800);
	    		  selectProductImageFrame.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("下载选中图片");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						PreSelectPhotoMessage.doSelect();
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  selectProductImageFrame.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	    	  browserView6.getBrowser().loadURL(UrlConfig.photoBankUrl);
	    	  selectProductImageFrame.setVisible(true);
	      
	      }

	    });
		return button;
	}
	
	
	private static JButton createDealRepeatProductButton() {
		JButton button = new JButton("处理重复产品");
	    button.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browserView5 == null) {
	    		  Browser browser = new Browser(BrowserContext.defaultContext());
	    		  browserView5 = new BrowserView(browser);
	    		  setListeners(browserView5);
	    	  }
	    	  if (repeatProductFrame == null) {
	    		  repeatProductFrame = new JFrame("Deal Repeat Product");
	    		  repeatProductFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  repeatProductFrame.add(browserView5, BorderLayout.CENTER);
	    		  repeatProductFrame.setSize(800, 800);
	    		  repeatProductFrame.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("Deal This Page");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {//TODO ACTION
//						HotKeywordMessage.setStart(true);
//						MessageManager.sendMessage(new HotKeywordMessage());
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  repeatProductFrame.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	    	  browserView5.getBrowser().loadURL(UrlConfig.repetProductGrouListpUrl);
	  		  repeatProductFrame.setVisible(true);
	      
	      }

	    });
		return button;
	}

	private static JButton createCollectKeywordsButton() {
		JButton collectKeywordsButton = new JButton("关键词采集");
	    collectKeywordsButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browserView4 == null) {
	    		  Browser browser = new Browser(BrowserContext.defaultContext());
	    		  browserView4 = new BrowserView(browser);
	    		  setListeners(browserView4);
	    	  }
	    	  if (collectKeywordsFream == null) {
	    		  collectKeywordsFream = new JFrame("Collect keywords");
	    		  collectKeywordsFream.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  collectKeywordsFream.add(browserView4, BorderLayout.CENTER);
	    		  collectKeywordsFream.setSize(800, 800);
	    		  collectKeywordsFream.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("Start");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						HotKeywordMessage.setStart(true);
						MessageManager.sendMessage(new HotKeywordMessage());
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  collectKeywordsFream.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	    	  browserView4.getBrowser().loadURL(UrlConfig.hotKeywordSearchUrl);
	  		collectKeywordsFream.setVisible(true);
	      
	      }
	    });
		return collectKeywordsButton;
	}

	private static JButton creatSelectProductButton() {
		JButton selectProductButton = new JButton("选择产品模版");
	    selectProductButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browserView3 == null) {
	    		  Browser browser = new Browser(BrowserContext.defaultContext());
	    		  browserView3 = new BrowserView(browser);
	    		  setListeners(browserView3);
	    	  }
	    	  if (selectProductFrame == null) {
	    		  selectProductFrame = new JFrame("Select Product");
	    		  selectProductFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  selectProductFrame.add(browserView3, BorderLayout.CENTER);
	    		  selectProductFrame.setSize(800, 800);
	    		  selectProductFrame.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("下载选中模版");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						MessageManager.sendMessage(new DoSelectProductModelMessage(null));
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  selectProductFrame.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	  		  browserView3.getBrowser().loadURL(UrlConfig.productManageUrl);
	  		  selectProductFrame.setVisible(true);
	      }
	    }); 
	    return selectProductButton;
	}
	
	public static BrowserView getBrowserView() {
		return browserView;
	}
	
	public static BrowserView getBrowservView2() {
		return browserView2;
	}
	
	public static Map<String, HotWords> getHotwordsMap () {
		return hotwordsMap;
	}
	
	public static boolean isLogin() {
		return isLogin;
	}

	public static void setLogin(boolean isLogin) {
		autoLoginButton.setEnabled(!isLogin);
		Main.isLogin = isLogin;
	}

	public static void hideWindowLoad(String groupUrl) {
		browserView2.getBrowser().loadURL(groupUrl);
	}
	
	public static void showHideWindow() {
		hideFrame.setVisible(true);
	}

	public static BrowserView getBrowserView3() {
		return browserView3;
	}
	public static BrowserView getBrowserView4() {
		return browserView4;
	}
	public static BrowserView getBrowserView5() {
		return browserView5;
	}
	public static BrowserView getBrowserView6() {
		return browserView6;
	}
	public static BrowserView getBrowserView7() {
		return browserView7;
	}
	public static BrowserView getTempView() {
		return tempView;
	}
	
	public static String getTempHtml(String url) {
		Browser browser = tempView.getBrowser();
		browser.loadURL(url);
		TimeUtils.sleep(1000);
		return browser.getHTML();
	}


	public static JFrame getSelectProductFram() {
		return selectProductFrame;
	}

	public static void setSelectProductFram(JFrame selectProductFram) {
		Main.selectProductFrame = selectProductFram;
	}
}
