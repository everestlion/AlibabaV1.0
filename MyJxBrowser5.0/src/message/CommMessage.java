package message;

import utils.TimeUtils;

public abstract class CommMessage extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Object[] args;
	
	public CommMessage() {
	}

	public static void waitFinished(long time) {
		TimeUtils.sleep(time);
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	public abstract Object run();
	
}
