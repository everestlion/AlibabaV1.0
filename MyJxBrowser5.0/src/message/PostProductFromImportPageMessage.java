package message;

import java.util.concurrent.TimeUnit;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;

public class PostProductFromImportPageMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;

	public 	PostProductFromImportPageMessage() {
		
	}

	@Override
	public Object run() {


//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		try {
			TimeUnit.MILLISECONDS.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Browser browser = Main.getBrowservView2().getBrowser();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
		
		DOMDocument document =browser.getDocument();
        DOMElement categoryIdsPathStr = document.findElement(By.id("categoryIdsPathStr"));
        DOMElement productName = document.findElement(By.id("productName"));
        DOMElement productKeyword = document.findElement(By.id("productKeyword"));
        DOMElement keywords2 = document.findElement(By.id("keywords2"));
        DOMElement keywords3 = document.findElement(By.id("keywords3"));
        
        DOMElement getImageFromPhotobank = document.findElement(By.id("getImageFromPhotobank"));
        getImageFromPhotobank.click();
        
        try {
			TimeUnit.MILLISECONDS.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
        DOMElement btnAddTo = document.findElement(By.id("btnAddTo"));
        if (btnAddTo != null) {
        	System.out.println("find you.....");
        }
        
//        DOMElement xImageFiles = document.findElement(By.id("xImageFiles");
//        xImageFiles.setAttribute("value", "[{&quot;fileId&quot;:1976413066,&quot;fileSavePath&quot;:&quot;http://kfdown.s.aliimg.com/kf/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ/201141992/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ.jpg&quot;,&quot;imgURL&quot;:&quot;http://kfdown.s.aliimg.com/kf/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ/201141992/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ.jpg&quot;,&quot;fileName&quot;:&quot;color i6&quot;,&quot;fileDestOrder&quot;:1,&quot;fileSrcOrder&quot;:0,&quot;fileFlag&quot;:&quot;add&quot;,&quot;isError&quot;:false}]");
//        DOMElement xImageType = document.findElement(By.id("xImageType");//一张：STATIC 多张：DYNAMIC
//
//    	DOMElement ul = document.findElement(By.id("file-list");
//    	ul.setInnerHTML("<div class=&quot;ui-uploader ui-uploader-multi-img util-clearfix&quot; data-widget-cid=&quot;widget-10&quot;>"+
//"<p class=&quot;ui-form-help ui-feedback ui-feedback-addon ui-feedback-error ui-feedback-body&quot; style=&quot;display: none; margin-bottom: 5px;&quot; data-role=&quot;uploader-error-msg&quot;></p>"+
//
//"<div class=&quot;ui-uploader-content&quot; style=&quot;position: relative;&quot;>"+
//	
//	"<ul class=&quot;ui-uploader-list ui-uploader-img-list util-clearfix ui-sortable list-sort&quot; data-role=&quot;uploader-list&quot; style=&quot;width:540px; height:110px; background: url(http://img.alibaba.com/simg/sprites/env/productposting/uploader-bg.png) repeat-x;&quot; data-widget-cid=&quot;widget-19&quot;><li class=&quot;ui-uploader-img-item ui-uploader-complete&quot; data-role=&quot;uploader-item&quot; style=&quot;width:80px;&quot; data-widget-cid=&quot;widget-20&quot;>"+
//"<div class=&quot;ui-uploader-img-wrap&quot; style=&quot;position: relative;margin: 0;width: 80px;height: 80px;line-height: 78px;border: 1px solid #ddd;background: #f9f9f9;&quot;>"+
//	"<img data-role=&quot;uploader-preview&quot; data-switch-status=&quot;complete&quot; width=&quot;77&quot; src=&quot;http://kfdown.s.aliimg.com/kf/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ/201141992/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ.jpg&quot; height=&quot;78&quot;>"+
//	"<div class=&quot;ui-progressbar-wrap&quot; data-role=&quot;uploader-itemprogressbar&quot; data-switch-status=&quot;progress&quot; style=&quot;width: 86px; display: none;&quot;></div>"+
//"</div>"+
//
//
//"<div class=&quot;ui-uploader-file-name&quot; style=&quot;width:64px;&quot; data-role=&quot;uploader-filename&quot; title=&quot;color i6&quot;>"+
//	"color i6"+
//"</div>"+
//
//
//"<div class=&quot;ui-uploader-action&quot;>"+
//	"<span data-switch-status=&quot;fileSelect start&quot; style=&quot;display: none;&quot;>待上传</span>"+
//	"<a href=&quot;javascript:void(0);&quot; data-switch-status=&quot;complete&quot; onclick=&quot;return false;&quot; class=&quot;status-action-remove&quot; data-uploader-uid=&quot;_web_file_uid_1422974685960_1&quot; data-role=&quot;uploader-remove&quot;>移除</a>"+
//	"<a href=&quot;javascript:void(0);&quot; data-switch-status=&quot;fileSelect start progress&quot; onclick=&quot;return false;&quot; class=&quot;status-action-cancel&quot; data-uploader-uid=&quot;_web_file_uid_1422974685960_1&quot; data-role=&quot;uploader-cancel&quot; style=&quot;display: none;&quot;>取消</a>"+
//"</div>"+
//"</li></ul>"+
//	"<div style=&quot;position:absolute;top:0;left:0; width:40px; height:40px; background: url(http://img.alibaba.com/simg/sprites/env/productposting/photo-cover.png) 0 0 no-repeat;&quot;></div>"+
//	"<div class=&quot;ui-uploader-info util-clearfix&quot; style=&quot;display: none; clear: both; margin-top: 3px;&quot; data-role=&quot;uploader-totalprogressbar&quot; data-widget-cid=&quot;widget-11&quot;><span class=&quot;ui-uploader-hint&quot;>已上传: <span data-role=&quot;uploader-uploaded&quot;>0</span>/ 待上传:<span data-role=&quot;uploader-pending&quot;>0</span>, 剩余:<span data-role=&quot;uploader-available&quot;>6</span></span>"+
//
//
//"<div class=&quot;ui-progressbar&quot; data-role=&quot;uploader-totalprogressbar&quot; data-widget-cid=&quot;widget-18&quot; style=&quot;width: 100%;&quot;>"+
//	"<div class=&quot;ui-progressbar-status&quot; data-role=&quot;progressbar-status&quot; style=&quot;width: 0%;&quot;></div>"+
//	"<span class=&quot;ui-progressbar-label&quot; data-role=&quot;progressbar-label&quot; style=&quot;display: none;&quot;></span>"+
//"</div>"+
//
//"<div class=&quot;ui-uploader-action&quot;>"+
//"<a href=&quot;javascript:void(0);&quot; class=&quot;status-action-cancel&quot; data-role=&quot;uploader-cancelall&quot; onclick=&quot;return false;&quot;>全部取消</a>"+
//"</div></div>"+
//"</div>"+
//"</div>");
    
//        xImageType.setAttribute("value", "STATIC");
        
        
        DOMElement submitButton = document.findElement(By.id("submitFormBtnA"));
//        submitButton.click();
        
	
		return null;
	}

}
