package message;

import java.util.List;
import java.util.Set;

import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEvent;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventListener;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventType;

public class SelectPhotoMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	public 	SelectPhotoMessage() {
		
	}
	
	
	@Override
	public Object run() {
		TimeUtils.sleep(1000);

//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		
		Browser browser = Main.getBrowservView2().getBrowser();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
	
		
		Set<Long> frameIds = browser.getFramesIds();
		for (long id : frameIds) {}
		
		//8
		DOMDocument document = browser.getDocument();
		if (document != null) {
			DOMElement photoBankGroupList = document.findElement(By.id("photoBankGroupList"));
			DOMElement btnAddTo = document.findElement(By.id("btnAddTo"));
			if (btnAddTo != null && photoBankGroupList != null) {
				final List<DOMElement> treeviewLabels = photoBankGroupList.findElements(By.className("AE-treeview-label"));
				for (DOMElement eElement : treeviewLabels) {
					String path = eElement.getAttribute("path");
					if (path != null && path.length() > 0) {//已处理过
						continue;
					}
					addEventHandler(eElement, null);
				}
				
				String[] path = {"For iphone 5", "For iphone", "33", "QQ截图20150130210217"};
				for (int i = 0; i < path.length - 1; i++) {
					int length = treeviewLabels.size();
					for (int k = 0; k < length; k++) {
						DOMNode ee = treeviewLabels.get(k);
						if (path[i].equalsIgnoreCase(((DOMElement)ee).getAttribute("title"))) {
							((DOMElement)ee).click();
							waitFinished(500);
							treeviewLabels.clear();
							treeviewLabels.addAll(ee.getParent().getParent().findElements(By.className("AE-treeview-label")));
							length = treeviewLabels.size();
						}
					}
				}
				
				waitFinished(2000);
				
				DOMElement photoBankPhotoList = document.findElement(By.id("photoBankPhotoList"));
				List<DOMElement> photoTitleList = photoBankPhotoList != null ? photoBankPhotoList.findElements(By.className("photo-title")) : null;
				if (photoTitleList != null && photoTitleList.size() > 0) {
					for (DOMElement titleNode : photoTitleList) {
						String title = titleNode.getAttribute("title");
						if (title != null && title.equals(path[path.length - 1])) {
							DOMNode picNode = titleNode.getParent().getParent();
							List<DOMElement> btnApply = picNode.findElements(By.className("btn-list-apply"));
							List<DOMElement> photoThumbnail = picNode.findElements(By.className("photo-thumbnail"));
//							((DOMElement)picNode.findElements(By.className("photo-title").get(0)).setAttribute("title", "HTB1cidfHXXXXXX.XFXXq6xXFXXX3");
//							((DOMElement)picNode.findElements(By.className("photo-title").get(0)).setNodeValue("HTB1cidfHXXXXXX.XFXXq6xXFXXX3");
							if (btnApply.size() > 0 && photoThumbnail.size() > 0) {
								DOMElement applyElement = btnApply.get(0);
								DOMElement photoThumbnailElement =  photoThumbnail.get(0);
//								photoThumbnailElement.setAttribute("src", "http://kfdown.s.aliimg.com/kf/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ/201141992/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ.jpg");
								photoThumbnailElement.setAttribute("src", "http://kfdown.s.aliimg.com/kf/HTB1ld5lHXXXXXXQXVXXq6xXFXXXH/201141992/HTB1ld5lHXXXXXXQXVXXq6xXFXXXH.jpg");
								applyElement.click();
							}
						}
						
					}
				}
				
			}
			
		}
	
	
	
	
		return null;
	}

	private void addEventHandler(final DOMElement eElement, DOMElement parent) {
		final String nodeName = eElement.getAttribute("title");
		String path = parent != null ? parent.getAttribute("path").concat("/").concat(nodeName) : nodeName;
		eElement.setAttribute("path", path);
		eElement.addEventListener(DOMEventType.OnClick, new DOMEventListener() {

			public void handleEvent(DOMEvent arg0) {
				System.out.println(eElement.getAttribute("path"));
				List<DOMElement> treeviewLabels = eElement.getParent().getParent().findElements(By.className("AE-treeview-label"));
				for (DOMElement ee : treeviewLabels) {
					String path = ee.getAttribute("path");
					if (path != null && path.length() > 0) {//已处理过
						continue;
					}
					addEventHandler(ee, eElement);
				}
			}
			
		}, false);
	
		
	}

}
