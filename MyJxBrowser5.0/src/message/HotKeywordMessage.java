package message;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import vo.HotWords;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;

import dao.HotWordsDao;

public class HotKeywordMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	private static boolean isStart;

	public 	HotKeywordMessage() {
		
	}

	public static boolean isStart() {
		return isStart;
	}

	public static void setStart(boolean isStart) {
		HotKeywordMessage.isStart = isStart;
	}

	@Override
	public Object run() {


//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		try {
			TimeUnit.MILLISECONDS.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Browser browser = Main.getBrowserView4().getBrowser();
		if (browser == null) {
			System.err.println("browser null");
			return null;
		}
		DOMDocument document =browser.getDocument();
        
        List<DOMElement> elements = document.findElements(By.className("J-keyword-line"));//J-keywords-content
        DOMElement keywordsContent = document.findElement(By.id("J-keywords-content"));
        if (keywordsContent != null) {
        	System.out.println("keywordsContent find......");
        }
        if (elements != null && elements.size() > 0) {
        	Map<String, HotWords> hotwordsMap = Main.getHotwordsMap();
        	for (DOMNode node : elements) {
        		List<DOMElement> children = node.findElements(By.tagName("td"));
        		if (children != null && children.size() >= 4) {
        			try {
        				String name = ((DOMElement)children.get(0)).getInnerHTML();
        				if (!hotwordsMap.containsKey(name)) {
        					String suppliers = ((DOMElement)children.get(1)).getInnerHTML();
        					String showcases = ((DOMElement)children.get(2)).getInnerHTML();
        					String searchTimes = ((DOMElement)children.get(3)).getInnerHTML();
        					HotWords hotWord = new HotWords();
        					hotWord.setName(name);
        					hotWord.setSearchTimes(Integer.parseInt(searchTimes));
        					hotWord.setSupplierNum(Integer.parseInt(suppliers));
        					hotWord.setShowcaseNum(Integer.parseInt(showcases));
        					hotwordsMap.put(name, hotWord);
        					System.out.println(hotwordsMap.size());
        				}
					} catch (Exception e) {
						e.printStackTrace();
					}
        		}
        	}
        	
        	List<DOMElement> nextPageNode = document.findElements(By.className("ui-pagination-next"));
			if (nextPageNode != null && nextPageNode.size() > 0 && "ui-pagination-next".equals(((DOMElement)nextPageNode.get(0)).getAttribute("class"))) {
				nextPageNode.get(0).click();
				System.out.println("click...");
				List<DOMElement> j_checkcodeElement = document.findElements(By.className("ui-pagination-active"));//短信验证
		        if (j_checkcodeElement != null && j_checkcodeElement.size() > 0) {
		        	System.out.println("active page....." + j_checkcodeElement.get(0).getInnerHTML());
		        } 
			} else {
				setStart(false);
//				browser.executeJavaScript("alert('共收集" + hotwordsMap.size() + "个关键词')");
				JOptionPane.showMessageDialog(null, "共收集" + hotwordsMap.size() + "个关键词", "提示", JOptionPane.INFORMATION_MESSAGE, null);
				for (HotWords e : hotwordsMap.values()) {
					HotWordsDao.insertHotWord(e);
				}
			}
        }
	
	
		return null;
	}

}
