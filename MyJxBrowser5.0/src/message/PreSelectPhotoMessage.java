package message;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEvent;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventListener;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventType;

public class PreSelectPhotoMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	public 	PreSelectPhotoMessage() {
		
	}
	
	
	@Override
	public Object run() {
		selectedPhoto.clear();
		TimeUtils.sleep(1000);

		Browser browser = Main.getBrowserView6().getBrowser();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
		DOMDocument document = browser.getDocument();
		if (document != null) {
			DOMElement photoBankGroupList = document.findElement(By.id("photoBankGroupList"));
			if (photoBankGroupList != null) {
				final List<DOMElement> treeviewLabels = photoBankGroupList.findElements(By.className("AE-treeview-label"));
				for (DOMElement eElement : treeviewLabels) {
					String path = eElement.getAttribute("path");
					if (path != null && path.length() > 0) {//已处理过
						continue;
					}
					addEventHandler(eElement, null);
				}
				
			}
			
		}
	
		return null;
	}

	private void addEventHandler(final DOMElement eElement, DOMElement parent) {
		final String nodeName = eElement.getAttribute("title");
		String path = parent != null ? parent.getAttribute("path").concat("/").concat(nodeName) : nodeName;
		eElement.setAttribute("path", path);
		eElement.addEventListener(DOMEventType.OnClick, new DOMEventListener() {

			public void handleEvent(DOMEvent arg0) {
				curPath = eElement.getAttribute("path");
				System.out.println(eElement.getAttribute("path"));
				List<DOMElement> treeviewLabels = eElement.getParent().getParent().findElements(By.className("AE-treeview-label"));
				for (DOMElement ee : treeviewLabels) {
					String path = ee.getAttribute("path");
					if (path != null && path.length() > 0) {//已处理过
						continue;
					}
					addEventHandler(ee, eElement);
				}
			}
			
		}, false);
	
		
	}
	
	private static HashMap<String, String> selectedPhoto = new HashMap<String, String>();
	private static HashMap<String, String> selectedPhotoAll = new HashMap<String, String>();
	
	private static String curPath;
	
	public void addEventHandler2(final DOMElement eElement) {
		boolean add = eElement.hasAttribute("add");
		if (!add) {
			eElement.addEventListener(DOMEventType.OnClick, new DOMEventListener() {
				
				public void handleEvent(DOMEvent arg0) {
					DOMElement parent = (DOMElement) eElement.getParent();
					List<DOMElement> titleList = parent.findElements(By.className("photo-name labeled"));
					String title = titleList.get(0).findElements(By.tagName("span")).get(0).getAttribute("title");
					String key = curPath + "/" + title;
					if(selectedPhoto.containsKey(key)) {
						selectedPhoto.remove(key);
						System.out.println("remove:" + key);
					} else {
						List<DOMElement> imageContainerList = parent.findElements(By.className("image-container"));
						List<DOMElement> imageList = imageContainerList.get(0).findElements(By.tagName("img"));
						String imgSrc = imageList.get(0).getAttribute("src");
						selectedPhoto.put(key, imgSrc);
						System.out.println("put: " + key + " : " + imgSrc);
					}
				}
				
			}, false);
			eElement.setAttribute("add", "true");
		}
	
		
	}


	public static void doSelect() {
		Browser browser = Main.getBrowserView6().getBrowser();
		if (browser == null) {
			return;
		}
		DOMDocument document = browser.getDocument();
		List<DOMElement> selectedList = document.findElements(By.className("photo-operate clear photo-display"));
		if (selectedList != null && selectedList.size() > 0) {
			for (DOMElement e : selectedList) {
				DOMElement parent = (DOMElement) e.getParent();
				
				List<DOMElement> imageContainerList = parent.findElements(By.className("image-container"));
				List<DOMElement> imageList = imageContainerList.get(0).findElements(By.tagName("img"));
				String title = imageList.get(0).getAttribute("title");
				String imgSrc = imageList.get(0).getAttribute("src");
				String key = curPath + "/" + title;
				selectedPhoto.put(key, imgSrc);
				System.out.println("put: " + key + " : " + imgSrc);
			}
		}
	}
	
	public static void clear() {
		selectedPhoto.clear();
		selectedPhotoAll.clear();
	}

}
