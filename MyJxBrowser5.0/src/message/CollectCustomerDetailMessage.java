package message;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.HashMap;
import java.util.List;

import utils.ExcelUtils;
import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;

import config.UrlConfig;

public class CollectCustomerDetailMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	public static boolean start = false;
	
	private static HashMap<String, String[]> map = new HashMap<String, String[]>();
	private static HashMap<String, String[]> errmap = new HashMap<String, String[]>();
	
	public 	CollectCustomerDetailMessage() {
		
	}
	
	@Override
	public Object run() {
		TimeUtils.sleep(1000);

//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		
		Browser browser = Main.getBrowserView7().getBrowser();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
	
		DOMDocument document = browser.getDocument();
		if (document != null) {
			//message list
			List<DOMElement> wraper = document.findElements(By.className("aui2-row-wraper"));
			List<DOMElement> messagesList = wraper.get(0).findElements(By.className("aui2-grid-wraper  aui2-grid-gray"));
			StringBuffer stringBuffer = new StringBuffer();
			if (!messagesList.isEmpty()) {
				for (DOMElement e : messagesList) {
					Browser tempBrowser = new Browser();
					String href = UrlConfig.messagesUrlDefault + e.getAttribute("data-href");
					try {
						tempBrowser.loadURL(href);
						TimeUtils.sleep(2000);
//						System.out.println(tempBrowser.getHTML());
//						Transferable tText = new StringSelection(tempBrowser.getHTML());
//						Toolkit.getDefaultToolkit().getSystemClipboard().setContents(tText, null);
						DOMDocument tempdDocument = tempBrowser.getDocument();
						List<DOMElement> customersNameDomElements = tempdDocument.findElements(By.className("aui-customer-name-des"));
						String customerName = customersNameDomElements.get(0).getInnerHTML().trim();
						String[] customerData = new String[10];
						customerData[0] = customerName.replaceAll("\\s", " ");
						List<DOMElement> detailDomElements = tempdDocument.findElements(By.className("aui-detail-company-name"));
						String companyName = detailDomElements.get(0).findElements(By.tagName("a")).get(0).getInnerHTML().trim();
						customerData[1] = companyName;
						List<DOMElement> contactDetailDomElements = tempdDocument.findElements(By.className("aui-detail-company-contact-item"));
						int i = 1;
						for (DOMElement node : contactDetailDomElements) {
							i++;
							List<DOMElement> spanLeftElements = node.findElements(By.className("aui-detail-left-des"));
							String valueLeft = spanLeftElements.get(0).getInnerHTML().trim();
							List<DOMElement> spanRightElements = node.findElements(By.className("aui-detail-right-des"));
							List<DOMElement> spanRightAElements = spanRightElements.get(0).findElements(By.tagName("a"));
							String valueRight = null;
							if (spanRightAElements.isEmpty()) {
								valueRight = spanRightElements.get(0).getInnerHTML().trim();
							} else {
								List<DOMElement> countryNameElements = spanRightAElements.get(0).findElements(By.className("aui-detail-country-name"));
								valueRight = countryNameElements.get(0).getInnerHTML().trim();
							}
							customerData[i] = valueRight;
							System.out.println(valueLeft + ":" + valueRight);
						}
						map.put(customerName, customerData);
						stringBuffer.append(customerName).append(",");
					} catch (Exception e1) {
						System.err.println("err " + href);
						errmap.put(href, null);
						e1.printStackTrace();
					} finally {
//						start = false;
					}
				}
			}
			String outFileName = "E:/temp/customers-" + System.currentTimeMillis() + ".xls";
			ExcelUtils.writeToExcel(map, outFileName);
			System.out.println(stringBuffer.toString());
			//next page
			List<DOMElement> nextPageDisableList = document.findElements(By.className("ui-button ui-button-normal ui-button-medium ui-button-last disabled"));
			if (nextPageDisableList.isEmpty()) {
				List<DOMElement> nextPageList = document.findElements(By.className("ui-button ui-button-normal ui-button-medium ui-button-last"));
				if (!nextPageList.isEmpty()) {
					nextPageList.get(1).click();
				}
			} else {
				
			}
		}
		
		return null;
	}


}
