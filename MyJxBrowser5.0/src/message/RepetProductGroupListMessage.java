package message;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;
import com.teamdev.jxbrowser.chromium.dom.internal.DOMFactory;

import config.Command;
import config.UrlConfig;

public class RepetProductGroupListMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;

	public 	RepetProductGroupListMessage() {
		
	}

	@Override
	public Object run() {


//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		try {
			TimeUnit.MILLISECONDS.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Browser browser = Main.getBrowserView5().getBrowser();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
		
		DOMDocument document =browser.getDocument();
        
        List<DOMElement> elements = document.findElements(By.className("delete-group"));//J-keywords-content
        if (elements != null && elements.size() > 0) {
        	for (DOMElement node : elements) {
        		String clusterid = node.getAttribute("clusterid");
        		DOMElement operationGroupElement = (DOMElement) node.getParent().getParent();
        		DOMElement newElement = document.createElement("li");
        		newElement.setInnerHTML("<a href=\"\" onclick=\"alert('" + Command.dealRepeatProductCommand + ","+ clusterid +"')\">自动处理</a>");
        		operationGroupElement.appendChild(newElement);
        	}
//        	Test.openNewWindow(urlList.get(0));
//        	Main.hideWindowLoad(UrlConfig.postProductFromImportUrl);
//        	Main.showHideWindow();
        	
        }
	
	
		return null;
	}

}
