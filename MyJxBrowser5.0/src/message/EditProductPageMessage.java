package message;

import java.util.LinkedList;
import java.util.List;

import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;

public class EditProductPageMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;

	public 	EditProductPageMessage() {
		
	}

	@Override
	public Object run() {


//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		TimeUtils.sleep(1000);
		
		Browser browser = Main.getBrowservView2().getBrowser();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
		
		DOMDocument document =browser.getDocument();
        DOMElement productList = document.findElement(By.id("productList"));
        if (productList != null) {
        	List<DOMElement> repeatTip = document.findElements(By.className("repeat-tip"));
        	for (DOMElement e : repeatTip) {
        		List<String> urlList = new LinkedList<String>();
        		List<DOMElement> tip = e.findElements(By.tagName("span"));
        		if (tip == null || tip.size() <= 0) {
        			DOMNode itemNode = e.getParent().getParent();
        			if (itemNode != null) {
        				List<DOMElement> editUrlNode = itemNode.findElements(By.className("J-edit-product"));
        				if (editUrlNode != null && editUrlNode.size() > 0) {
        					String editUrl = editUrlNode.get(0).getAttribute("href");
        					if (editUrl != null) {
        						urlList.add(editUrl);
        						
        					}
        				}
        			}
        		}
        		for (String url : urlList) {
//					Test.openNewWindow(url);
				}
        	}

        }
	
	
		return null;
	}

}
