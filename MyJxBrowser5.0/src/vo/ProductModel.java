package vo;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import utils.Utils;


public class ProductModel {
	
	private String id;
	
	private String title;
	
	private String imagePath;
	
	private String keywords;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	private static String xmlPath = "resource/xml/model.xml";
	
	private static HashMap<String, ProductModel> map = new HashMap<String, ProductModel>();
	
	static {
		loadXml();
	}
	
	@SuppressWarnings("unchecked")
	public static void loadXml() {
		Document document = getDocument();
		if (document == null) {
			System.err.println("load model.xml err");
			return;
		}
		
		List<Node> models = document.selectNodes("models/product");
		
		for (Node e : models) {
			ProductModel pm = new ProductModel();
			Utils.setValue(e, pm, ProductModel.class);
			
			map.put(pm.getId(), pm);
		}
		
		System.out.println(map.size());
	}
	
	
	
	public static Document getDocument() {
		File xmlFile = new File(xmlPath);
		Document document = null;
		if (!xmlFile.exists()) {
			document = DocumentHelper.createDocument();
			document.setXMLEncoding("UTF-8");
			document.setRootElement(DocumentHelper.createElement("models"));
		} else {
			SAXReader reader = new SAXReader();
			try {
				document = reader.read(xmlFile);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		return document;
	}
	
	public static void addModel(ProductModel pm) {
		writeToXml(pm);
	}
	
	public static void modelValueChange(ProductModel pm) {
		writeToXml(pm);
	}
	
	public static void delModel(String id) {
		map.remove(id);
		Document document = getDocument();
		if (document == null) {
			System.err.println("load model.xml err");
			return;
		}
		Element rootElement = document.getRootElement();
		Node oldNode = document.selectSingleNode("/models/product[id=" + id + "]");
//		Node oldNode = document.elementByID(id);
		if (oldNode == null) {
			return;
		}
		rootElement.remove(oldNode);
		
		Utils.writeToXml(document, xmlPath);
	}
	
	@SuppressWarnings("deprecation")
	public static void writeToXml(ProductModel pm) {
		Document document = getDocument();
		if (document == null) {
			System.err.println("load model.xml err");
			return;
		}
		Element rootElement = document.getRootElement();
		Node oldNode = document.selectSingleNode("/models/product[id=" + pm.getId() + "]");
		if (oldNode != null) {
			Utils.setXmlValue((Element)oldNode, pm, ProductModel.class);
		} else {
			Element neweElement = DocumentHelper.createElement("product");
			neweElement.setAttributeValue("ID", pm.getId());
			neweElement.addElement("id");
			neweElement.addElement("title");
			neweElement.addElement("keywords");
			neweElement.addElement("imagePath");
			Utils.setXmlValue(neweElement, pm, ProductModel.class);
			rootElement.add(neweElement);
		}
		
		Utils.writeToXml(document, xmlPath);
		
	}
	

}
