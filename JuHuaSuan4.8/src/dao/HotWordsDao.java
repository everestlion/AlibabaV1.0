package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import vo.HotWords;

public class HotWordsDao {
	
	public static final String TABLE = "hotwords";
	
	public static Map<String, HotWords> selectHotWordsDB() {
		Map<String, HotWords> map = new HashMap<String, HotWords>();
		String sql = "SELECT * FROM " + TABLE;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = DBUtil.getConnection().createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next() != false) {
				HotWords hotword = new HotWords();
				hotword.setName(rs.getString(2));
				hotword.setSupplierNum(rs.getInt(3));
				hotword.setShowcaseNum(rs.getInt(4));
				hotword.setSearchTimes(rs.getInt(5));
				
				System.out.println(hotword.getName());
				if (hotword.getName() == null) {
					System.out.println("err");
				} else {
					String key = hotword.getName().toLowerCase().trim();
					if (!map.containsKey(key)) {
						map.put(key, hotword);
					}
				}
			}
			System.out.println(map.size());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
//				if (conn != null) {
////					conn.close();
//				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return map;
	}
	
	public static void insertHotWord(HotWords hotWords) {
		String sql = "INSERT INTO " + TABLE + " VALUES(0,'" + hotWords.getName() + "'," + hotWords.getSupplierNum() + "," + 
				hotWords.getShowcaseNum() + "," + hotWords.getSearchTimes()  + ");";
	
		System.out.println(sql);
		
		try {
//			DBUtil.insert(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public static void main(String[] args) {
		selectHotWordsDB();
	}
}
