package test;

import java.io.File;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import utils.TimeUtils;
import vo.ProductModel;
import vo.Project;

public class Test {
	
	public static ReentrantLock lock = new ReentrantLock();

	public static void main(String[] args) {
//		testLock();
		testXml();
	}
	
	public Test() {
		
	}
	
	public static void testXml() {
//		ProductModel pm = new ProductModel();
//		pm.setImagePath("xxx");
//		pm.setTitle("xxx");
//		pm.setKeywords("xxx");
//		ProductModel.addModel(pm);
//		ProductModel.delModel("2");
		
		
		Project project = Project.getProject();
		System.out.println(project.getTitle());
		project.setTitle("test");
		Project.valueChange(project);
	}
	
	public static void testLock() {
		Test test = new Test();
		synchronized (test) {
			notifyThread(test);
			try {
				test.wait();
				System.out.println("lllllllll");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		lock.lock();
		
		lock.unlock();
	}
	
	private static void notifyThread(final Test test) {
		Thread t = new Thread(new Runnable() {
			
			public void run() {
//				test.notify();
				synchronized (test) {
					TimeUtils.sleep(2000);
					test.notify();
					
				}
			}
		});
		t.start();
		
	}

	public static void lock() {
		Thread t = new Thread(new Runnable() {
			
			public void run() {
				lock.lock();
			}
		});
		t.start();
	}

}
