package com.teamdev.jxbrowser.chromium.demo.everest.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.lang3.StringUtils;

import message.AutoLoginMessage;
import message.DoSelectProductModelMessage;
import message.EditProductPageMessage;
import message.HotKeywordMessage;
import message.LoadDataMessage;
import message.MessageManager;
import message.OnAlertMessage;
import message.PostProductFromImportPageMessage;
import message.RepetProductGroupListMessage;
import message.RepetProductGroupMessage;
import message.SelectPhotoMessage;
import message.SelectProductModelMessage;
import vo.HotWords;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserContext;
import com.teamdev.jxbrowser.chromium.BrowserFactory;
import com.teamdev.jxbrowser.chromium.DefaultDialogHandler;
import com.teamdev.jxbrowser.chromium.DefaultNetworkDelegate;
import com.teamdev.jxbrowser.chromium.DefaultPopupHandler;
import com.teamdev.jxbrowser.chromium.DialogParams;
import com.teamdev.jxbrowser.chromium.PopupContainer;
import com.teamdev.jxbrowser.chromium.PopupParams;
import com.teamdev.jxbrowser.chromium.RequestCompletedParams;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;

import config.Command;
import config.UrlConfig;

public class Main {
	
	private static Map<String, HotWords> hotwordsMap = new HashMap<String, HotWords>();
	
	private static Browser browser;
	private static Browser browser2;
	private static JFrame hideFrame;
	
	private static Browser browser3;
	
	private static JFrame selectProductFrame;
	
	private static JFrame collectKeywordsFream;
	private static JFrame repeatProductFrame;
	
	private static boolean isLogin;
	private static JButton autoLoginButton = new JButton("自动登陆");
	
	public static Browser getBrowser() {
		return browser;
	}
	
	public static Browser getBrowser2() {
		return browser2;
	}
	
	public static Map<String, HotWords> getHotwordsMap () {
		return hotwordsMap;
	}
	
	public static boolean isLogin() {
		return isLogin;
	}

	public static void setLogin(boolean isLogin) {
		autoLoginButton.setEnabled(!isLogin);
		Main.isLogin = isLogin;
	}

	public static void main(String[] args) {
//		System.setProperty("teamdev.license.info", "false");
//		Browser browser = BrowserFactory.create();
		BrowserContext browserContext = BrowserContext.defaultContext();
		browserContext.setNetworkDelegate(new DefaultNetworkDelegate() {
			
			@Override
			public void onCompleted(RequestCompletedParams arg0) {
				
				if (arg0.getURL() != null) {
					if (arg0.getURL().startsWith(UrlConfig.hotKeywordSearchUrl2)) {
						if (HotKeywordMessage.isStart()) {
							MessageManager.sendMessage(new HotKeywordMessage());
						}
					} else if (arg0.getURL().startsWith(UrlConfig.repetProductGrouListpUrl)) {
						MessageManager.sendMessage(new RepetProductGroupListMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.repetProductGroupUrl)) {
						MessageManager.sendMessage(new RepetProductGroupMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.editProductUrl)) {
						MessageManager.sendMessage(new EditProductPageMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.postProductFromImportUrl)) {
						MessageManager.sendMessage(new PostProductFromImportPageMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.selectPhotoUrl)) {
						MessageManager.sendMessage(new SelectPhotoMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.selectPhotoCatlogUrl)) {
//						SelectPhotoMessage.notifyFinished();
					} else if (arg0.getURL().startsWith(UrlConfig.productManageUrl2)) {
						MessageManager.sendMessage(new SelectProductModelMessage());
					} else if (arg0.getURL().startsWith(UrlConfig.logoutUrl)) {
						setLogin(false);
					}
				}
				System.out.println("onCompleted===" + arg0.getURL());
			}
		});
		
		browser = createBrowser(browserContext);
		
		JFrame frame = new JFrame("Main");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
        //全屏
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        //方法一
//        Rectangle bounds = new Rectangle( screenSize );
//        frame.setBounds(bounds);
        //方法二
        int width = (int)screenSize.getWidth();
        int height = (int)screenSize.getHeight();
//        frame.setSize(width, height);
        Insets scrInsets=Toolkit.getDefaultToolkit().getScreenInsets(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
        frame.setBounds(scrInsets.left,scrInsets.top,width-scrInsets.left-scrInsets.right,height-scrInsets.top-scrInsets.bottom); 
//        frame.setSize(700, 500);
        
        
        JPanel southPanel = new JPanel();
	    southPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    
	    autoLoginButton.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		MessageManager.sendMessage(new AutoLoginMessage());
	    	}
	    });
	    southPanel.add(autoLoginButton);
	    
	    JButton selectProductButton = creatSelectProductButton();
	    southPanel.add(selectProductButton);
	    
	    JButton collectKeywordsButton = createCollectKeywordsButton();
	    southPanel.add(collectKeywordsButton);
	    
	    JButton dealRepeatProductButton = createDealRepeatProductButton();
	    southPanel.add(dealRepeatProductButton);
	    
	    frame.add(southPanel, BorderLayout.SOUTH);
        
        
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        browser2 = createBrowser(browserContext);
        hideFrame = new JFrame("Hide");
        hideFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        hideFrame.add(browser2.getView().getComponent(), BorderLayout.CENTER);
        hideFrame.setSize(700, 500);
        hideFrame.setLocationRelativeTo(null);
        hideFrame.setVisible(false);
        browser2.loadHTML("resources/data.html");
       
        browser.loadURL(UrlConfig.homepageUrl);
        
       
//        try {
//        	while (browser.isLoading()) {
//        		TimeUnit.MILLISECONDS.wait(50);
//        	}
//        } catch (InterruptedException e) {
//        	e.printStackTrace();
//        }
	}
	
	
	private static JButton createDealRepeatProductButton() {
		JButton button = new JButton("加载");
	    button.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browser3 == null) {
	    		  browser3 = createBrowser(BrowserContext.defaultContext());
	    	  }
	    	  if (repeatProductFrame == null) {
	    		  repeatProductFrame = new JFrame("数据");
	    		  repeatProductFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  repeatProductFrame.add(browser3.getView().getComponent(), BorderLayout.CENTER);
	    		  repeatProductFrame.setSize(800, 800);
	    		  repeatProductFrame.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("重新加载");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						String path = null;
						if (LoadDataMessage.dataFile == null) {
							path = ".";
						} else {
							path = LoadDataMessage.dataFile.getParent();
						}
						JFileChooser fileChooser = new JFileChooser(path);
						fileChooser.setFileFilter(new FileFilter() {
							
							@Override
							public String getDescription() {
								return "xls,xlsx";
							}
							
							@Override
							public boolean accept(File f) {
								return f.isDirectory() || f.getName().endsWith("xls") || f.getName().endsWith("xlsx");
							}
						});
						int option = fileChooser.showOpenDialog(null);
						if (option == JFileChooser.APPROVE_OPTION) {
							File selectFile = fileChooser.getSelectedFile();
							if (selectFile.exists()) {
								MessageManager.sendMessage(new LoadDataMessage(selectFile));
							} else {
								MessageManager.sendMessage(new OnAlertMessage(new String[]{String.valueOf(Command.comm), "文件不存在"}));
							}
						}
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  repeatProductFrame.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	    	  File file = new File("resource/data.html");
	  		  browser3.loadURL(file.getAbsolutePath());
	  		  repeatProductFrame.setVisible(true);
	      
	      }
	    });
		return button;
	}

	private static JButton createCollectKeywordsButton() {
		JButton collectKeywordsButton = new JButton("关键词采集");
	    collectKeywordsButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browser3 == null) {
	    		  browser3 = createBrowser(BrowserContext.defaultContext());
	    	  }
	    	  if (collectKeywordsFream == null) {
	    		  collectKeywordsFream = new JFrame("Collect keywords");
	    		  collectKeywordsFream.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  collectKeywordsFream.add(browser3.getView().getComponent(), BorderLayout.CENTER);
	    		  collectKeywordsFream.setSize(800, 800);
	    		  collectKeywordsFream.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("Start");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						HotKeywordMessage.setStart(true);
						MessageManager.sendMessage(new HotKeywordMessage());
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  collectKeywordsFream.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	  		  browser3.loadURL(UrlConfig.hotKeywordSearchUrl);
	  		collectKeywordsFream.setVisible(true);
	      
	      }
	    });
		return collectKeywordsButton;
	}

	private static JButton creatSelectProductButton() {
		JButton selectProductButton = new JButton("选择产品模版");
	    selectProductButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  if (browser3 == null) {
	    		  browser3 = createBrowser(BrowserContext.defaultContext());
	    	  }
	    	  if (selectProductFrame == null) {
	    		  selectProductFrame = new JFrame("Select Product");
	    		  selectProductFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
	    		  selectProductFrame.add(browser3.getView().getComponent(), BorderLayout.CENTER);
	    		  selectProductFrame.setSize(800, 800);
	    		  selectProductFrame.setLocationRelativeTo(null);
	    		  JPanel operationPanel = new JPanel();
	    		  operationPanel.setBorder(BorderFactory.createTitledBorder("Operation"));
	    		  JButton downSelectProduct = new JButton("下载选中模版");
	    		  downSelectProduct.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						MessageManager.sendMessage(new DoSelectProductModelMessage(null));
					}
				});
	    		  operationPanel.add(downSelectProduct);
	    		  selectProductFrame.add(operationPanel, BorderLayout.SOUTH);
	    	  }
	  		  browser3.loadURL(UrlConfig.productManageUrl);
	  		  selectProductFrame.setVisible(true);
	      }
	    }); 
	    return selectProductButton;
	}

	private static Browser createBrowser(BrowserContext browserContext) {
		final Browser browser = BrowserFactory.create(browserContext);

        browser.addLoadListener(new LoadAdapter() {
			public void onFinishLoadingFrame(FinishLoadingEvent e) {
				super.onFinishLoadingFrame(e);
				
				System.out.println(e.getValidatedURL() + " finish loading......." + e.isMainFrame());
				
				
			}
        	
		});
        
        browser.setPopupHandler(new DefaultPopupHandler() {

			@Override
			public PopupContainer handlePopup(PopupParams arg0) {
				System.out.println("popup........");
				return super.handlePopup(arg0);
			}
        	
        });
        
        browser.setDialogHandler(new DefaultDialogHandler() {

			@Override
			public void onAlert(DialogParams arg0) {
				String message = arg0.getMessage();
				if (StringUtils.isEmpty(message)) {
					return;
				}
				String[] arr = message.split(",");
				if (StringUtils.isNumeric(arr[0])) {
					MessageManager.sendMessage(new OnAlertMessage(arr));
				} else {
					super.onAlert(arg0);
				}
				
				
			}
        	
        });
        
        
        
        return browser;
		
	}


	public static void hideWindowLoad(String groupUrl) {
		browser2.loadURL(groupUrl);
	}
	
	public static void showHideWindow() {
		hideFrame.setVisible(true);
	}

	public static Browser getBrowser3() {
		return browser3;
	}

	public static void setBrowser3(Browser browser3) {
		Main.browser3 = browser3;
	}

	public static JFrame getSelectProductFram() {
		return selectProductFrame;
	}

	public static void setSelectProductFram(JFrame selectProductFram) {
		Main.selectProductFrame = selectProductFram;
	}
	
	public static void setRepatProductFrameTitle(String title) {
		if (repeatProductFrame != null) {
			repeatProductFrame.setTitle(title);
		}
	}
}
