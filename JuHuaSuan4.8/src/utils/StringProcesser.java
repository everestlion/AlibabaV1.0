package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class StringProcesser {

	/**
	 * 默认分隔�?
	 */
	public final static String SPLIT = ",";

	public static int[] toIntArray(String param, String split) throws NumberFormatException {
		if (param == null || param.length() <= 0)
			return null;
		String[] ss = toStringArray(param, split);
		int[] l = new int[ss.length];
		for (int i = 0; i < ss.length; i++) {
			l[i] = parseInt(ss[i]);
		}
		return l;
	}

	public static String removeHtml(String html) {
		return html.replaceAll("\\&[a-zA-Z]{1,10};", "").replaceAll("<[^>]*>", "").replaceAll("[(>)<]", "");
	}

	private static int parseInt(String str) {
		if (str == null || str.equals("") || str.equals("null")) {
			return 0;
		} else {
			return Integer.parseInt(str);
		}
	}

	public static int[] toIntArray(String param) throws NumberFormatException {
		return toIntArray(param, SPLIT);
	}

	public static List<Integer> toIntList(String param) throws NumberFormatException {
		if (param.length() <= 0)
			return null;
		String[] ss = param.split(SPLIT);
		List<Integer> list = new LinkedList<Integer>();
		for (int i = 0; i < ss.length; i++) {
			list.add(parseInt(ss[i]));
		}

		return list;
	}

	/**
	 * 将字符串按指定的分隔符分割为long数组
	 * 
	 * @param param
	 *            待分割字符串
	 * @param split
	 *            分隔�?
	 * @return
	 * @throws NumberFormatException
	 */
	public static long[] toLongArray(String param, String split) throws NumberFormatException {
		if (param.length() <= 0)
			return null;
		String[] ss = toStringArray(param, split);
		long[] l = new long[ss.length];
		for (int i = 0; i < ss.length; i++) {
			l[i] = Long.parseLong(ss[i]);
		}
		return l;
	}

	/**
	 * 使用默认分隔符分割为long数组
	 * 
	 * @param param
	 *            待分割字符串
	 * @return
	 */
	public static long[] toLongArray(String param) throws NumberFormatException {
		return toLongArray(param, SPLIT);
	}

	/**
	 * 将long数组拼成字符串，使用指定分隔符分割每个long元素
	 * 
	 * @param l
	 *            long数组
	 * @param split
	 *            拼接的分割符
	 * @return
	 */
	public static String toString(long[] l, String split) {
		String s = "";
		for (int i = 0; i < l.length; i++) {
			s += l[i];
			if (i < l.length - 1)
				s += split;
		}
		return s;
	}

	/**
	 * 将long数组拼成字符串，使用默认分隔符分割每个long元素
	 * 
	 * @param l
	 *            long数组
	 * @return
	 */
	public static String toString(long[] l) {
		return toString(l, SPLIT);
	}

	private static String[] toStringArray(String param, String split) {
		return param.split(split);
	}

	public static String toString(String[] str, String split) {

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < str.length; i++) {
			sb.append(str[i]);
			if (i < str.length - 1) {
				sb.append(split);
			}

		}

		return sb.toString();
	}

	public static String toString(int[] ids) {
		StringBuilder sb = new StringBuilder();
		if (ids == null) {
			return sb.toString();
		}
		for (int i = 0; i < ids.length; i++) {
			sb.append(ids[i]);
			if (i < ids.length - 1) {
				sb.append(SPLIT);
			}
		}

		return sb.toString();
	}

	public static String toString(String[] str) {
		return toString(str, SPLIT);
	}

	public static Set<Long> toLongSet(String param, String split) {
		Set<Long> set = new LinkedHashSet<Long>();
		if (param != null && !param.equals("")) {
			for (String s : param.split(split)) {
				try {
					long l = Long.valueOf(s);
					set.add(l);
				} catch (Exception e) {
					continue;
				}
			}
		}
		return set;
	}

	public static String toString(Set<Long> set, String split) {
		String s = "";
		if (set != null) {
			int i = 1;
			for (Long l : set) {
				if (l != null) {
					if (i == 1) {
						s = String.valueOf(l);
					} else {
						s = s.concat(split + l);
					}
					i++;
				}
			}
		}
		return s;
	}

	/**/// /
	// / 转全角的函数(SBC case)
	// /
	// /任意字符�?
	// /全角字符�?
	// /
	// /全角空格�?2288，半角空格为32
	// /其他字符半角(33-126)与全�?65281-65374)的对应关系是：均相差65248
	// /
	public static String ToSBC(String input) {
		// 半角转全角：
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 32) {
				c[i] = (char) 12288;
				continue;
			}
			if (c[i] < 127) {
				c[i] = (char) (c[i] + 65248);
			}
		}
		return new String(c);
	}

	/**
	 * 转半角的函数(DBC case)
	 * 全角空格�?2288，半角空格为32 
	 * 其他字符半角(33-126)与全�?65281-65374)的对应关系是：均相差65248
	 * @param input
	 * @return
	 */
	public static String ToDBC(String input) {
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 12288) {
				c[i] = (char) 32;
				continue;
			}
			if (c[i] > 65280 && c[i] < 65375) {
				c[i] = (char) (c[i] - 65248);
			}
		}
		return new String(c);
	}

	public static String removeFuHao(String source) {
		return source.replaceAll("\\p{P}", "").replaceAll(" ", "");
	}
	
	public static String readFile(String filePath){

		StringBuffer sb = new StringBuffer();
        try {

                String encoding="GBK";

                File file=new File(filePath);

                if(file.isFile() && file.exists()){ //判断文件是否存在

                    InputStreamReader read = new InputStreamReader(

                    new FileInputStream(file),encoding);//考虑到编码格式

                    BufferedReader bufferedReader = new BufferedReader(read);

                    String lineTxt = null;

                    while((lineTxt = bufferedReader.readLine()) != null){
                        sb.append(lineTxt);

                    }

                    read.close();

        }else{

            System.out.println("找不到指定的文件");

        }

        } catch (Exception e) {

            System.out.println("读取文件内容出错");

            e.printStackTrace();

        }
        return sb.toString();
     

    }

}
