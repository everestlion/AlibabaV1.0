package message;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import utils.Utils;
import vo.ProductModel;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;

import config.Config;
import config.UrlConfig;

public class DoSelectProductModelMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	private String id;

	public 	DoSelectProductModelMessage(String id) {
		this.id = id;
	}

	@Override
	public Object run() {
		Browser browser = Main.getBrowser3();
		
		DOMDocument document = browser.getDocument();
		if (id == null) {
			List<DOMNode> listcheckbox = document.getElementsByClassName("list-checkbox");
			StringBuffer sb = new StringBuffer();
			for (DOMNode e : listcheckbox) {
				DOMElement inputItem = (DOMElement) e;
				if (inputItem.getAttribute("selected") != null && inputItem.getAttribute("selected").length() > 0) {
					id = inputItem.getAttribute("productid");
					sb.append(id).append(" ");
					DOMElement productitemdiv = (DOMElement) e.getParent().getParent();
					process(productitemdiv);
				}
			}
		} else {
			DOMElement node = document.getElementById(id);
			DOMElement productitemdiv = (DOMElement) node.getParent().getParent();
			process(productitemdiv);
		}
		
		return null;
	}
	
	private void process(DOMNode productitemdiv) {
		List<DOMNode> imgdiv = productitemdiv.getElementsByClassName("list-img fl");
		DOMElement imgUrlNode = (DOMElement) imgdiv.get(0).getElementsByTagName("a").get(0).getElementsByClassName("ui-image-viewer").get(0);
		String imgUrl = imgUrlNode.getAttribute("absimageurl");
		byte[] imgBytes = Utils.getUrlFileData(imgUrl);
		ProductModel pm = new ProductModel();
		pm.setId(id);
		
		try {
			String path = "resource/img/" + imgUrl.substring(imgUrl.lastIndexOf("/") + 1, imgUrl.length());
			FileOutputStream fos = new FileOutputStream(new File(path));
			fos.write(imgBytes, 0, imgBytes.length);
			fos.close();
			System.out.println(path + " downloaded...");
			pm.setImagePath(path);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		List<DOMNode> itemtitle = productitemdiv.getElementsByClassName("list-title item-title fl");
		DOMElement titleElement = (DOMElement) itemtitle.get(0).getElementsByTagName("div").get(0).getElementsByTagName("a").get(0);
		String title = titleElement.getAttribute("title");
		pm.setTitle(title);
		
		StringBuffer sb = new StringBuffer();
		String tmp = title.trim().replaceAll("\\.", " ");
		String[] words = tmp.split(" ", 7);
		int count = 0;
		for (String e : words) {
			sb.append(e);
			count ++;
			if (count > 5) {
				break;
			}else {
				sb.append("-");
			}
		}
		sb.append("_").append(id).append(".html?s=p");
		
		String linkStr = UrlConfig.detailUrl + sb.toString();
		try {
			Document doc = Jsoup.connect(linkStr).get();
			Elements element = doc.getElementsByAttributeValue("name", "keywords");
			String keywords = element.attr("content");
			keywords = keywords != null ? keywords.substring(keywords.indexOf(" High Quality ") + 14, keywords.length()) : null;
			keywords = keywords != null ? keywords.replaceAll(Config.companyName, "") : null;
			keywords = keywords.substring(0, keywords.lastIndexOf(","));
			pm.setKeywords(keywords);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		ProductModel.addModel(pm);
		JOptionPane.showMessageDialog(null, "下载完成", "提示", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
}
