package message;

import javax.swing.JOptionPane;

import config.Command;



public class OnAlertMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	
	private String[] params;
	public 	OnAlertMessage(String[] params) {
		this.params = params;
	}

	@Override
	public Object run() {
		int command = Integer.parseInt(params[0]);
		switch (command) {
		case Command.downloadComfirmCommand:
//				JOptionPane.showMessageDialog(null, "已加入下载", "提示", JOptionPane.INFORMATION_MESSAGE, null);
			int option = JOptionPane.showConfirmDialog(null, "是否要下载？", "提示", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (option == 0) {
				MessageManager.sendMessage(new DoSelectProductModelMessage(params[1]));
			}
			break;
		case Command.delaRepeatProductCommand:
			int option2 = JOptionPane.showConfirmDialog(null, "是否要处理？", "提示", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			break;
		case Command.submit:
			int option3 = JOptionPane.showConfirmDialog(null, "是否要处理？", "提示", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			break;
		case Command.comm:
			JOptionPane.showMessageDialog(null, params[1]);
			break;
			
		default:
			break;
		}
		
		
	
		return null;
	}
	
}
