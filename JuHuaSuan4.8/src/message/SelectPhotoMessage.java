package message;

import java.util.List;
import java.util.Set;

import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEvent;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventListener;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventType;
import com.teamdev.jxbrowser.chromium.internal.WebView;

public class SelectPhotoMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	public 	SelectPhotoMessage() {
		
	}
	
	
	@Override
	public Object run() {
		TimeUtils.sleep(1000);

//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		
		Browser browser = Main.getBrowser2();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
	
		
		Set<Long> frameIds = ((WebView)browser).getFramesIds();
		for (long id : frameIds) {//8
			DOMDocument document = browser.getDocument(id);
			if (document != null) {
				DOMElement photoBankGroupList = document.getElementById("photoBankGroupList");
				DOMElement btnAddTo = document.getElementById("btnAddTo");
				if (btnAddTo != null && photoBankGroupList != null) {
					final List<DOMNode> treeviewLabels = photoBankGroupList.getElementsByClassName("AE-treeview-label");
					for (DOMNode e : treeviewLabels) {
						DOMElement eElement = (DOMElement)e;//TODO
						String path = eElement.getAttribute("path");
						if (path != null && path.length() > 0) {//已处理过
							continue;
						}
						addEventHandler(eElement, null);
					}
					
					String[] path = {"For iphone 5", "For iphone", "33", "QQ截图20150130210217"};
					for (int i = 0; i < path.length - 1; i++) {
						int length = treeviewLabels.size();
						for (int k = 0; k < length; k++) {
							DOMNode ee = treeviewLabels.get(k);
							if (path[i].equalsIgnoreCase(((DOMElement)ee).getAttribute("title"))) {
								((DOMElement)ee).click();
								waitFinished(500);
								treeviewLabels.clear();
								treeviewLabels.addAll(ee.getParent().getParent().getElementsByClassName("AE-treeview-label"));
								length = treeviewLabels.size();
							}
						}
					}
					
					waitFinished(2000);
					
					DOMElement photoBankPhotoList = document.getElementById("photoBankPhotoList");
					List<DOMNode> photoTitleList = photoBankPhotoList != null ? photoBankPhotoList.getElementsByClassName("photo-title") : null;
					if (photoTitleList != null && photoTitleList.size() > 0) {
						for (DOMNode titleNode : photoTitleList) {
							String title = ((DOMElement)titleNode).getAttribute("title");
							if (title != null && title.equals(path[path.length - 1])) {
								DOMNode picNode = titleNode.getParent().getParent();
								List<DOMNode> btnApply = picNode.getElementsByClassName("btn-list-apply");
								List<DOMNode> photoThumbnail = picNode.getElementsByClassName("photo-thumbnail");
//								((DOMElement)picNode.getElementsByClassName("photo-title").get(0)).setAttribute("title", "HTB1cidfHXXXXXX.XFXXq6xXFXXX3");
//								((DOMElement)picNode.getElementsByClassName("photo-title").get(0)).setNodeValue("HTB1cidfHXXXXXX.XFXXq6xXFXXX3");
								if (btnApply.size() > 0 && photoThumbnail.size() > 0) {
									DOMElement applyElement = (DOMElement) btnApply.get(0);
									DOMElement photoThumbnailElement = (DOMElement) photoThumbnail.get(0);
//									photoThumbnailElement.setAttribute("src", "http://kfdown.s.aliimg.com/kf/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ/201141992/HTB1oG1tHXXXXXa1XpXXq6xXFXXXQ.jpg");
									photoThumbnailElement.setAttribute("src", "http://kfdown.s.aliimg.com/kf/HTB1ld5lHXXXXXXQXVXXq6xXFXXXH/201141992/HTB1ld5lHXXXXXXQXVXXq6xXFXXXH.jpg");
									applyElement.click();
								}
							}
							
						}
					}
					
					break;
				}
				
			}
		
		}
	
	
		return null;
	}

	private void addEventHandler(final DOMElement eElement, DOMElement parent) {
		final String nodeName = eElement.getAttribute("title");
		String path = parent != null ? parent.getAttribute("path").concat("/").concat(nodeName) : nodeName;
		eElement.setAttribute("path", path);
		eElement.addEventListener(DOMEventType.OnClick, new DOMEventListener() {

			public void handleEvent(DOMEvent arg0) {
				System.out.println(eElement.getAttribute("path"));
				List<DOMNode> treeviewLabels = eElement.getParent().getParent().getElementsByClassName("AE-treeview-label");
				for (DOMNode ee : treeviewLabels) {
					DOMElement eeElement = (DOMElement)ee;
					String path = eeElement.getAttribute("path");
					if (path != null && path.length() > 0) {//已处理过
						continue;
					}
					addEventHandler(eeElement, eElement);
				}
			}
			
		}, false);
	
		
	}

}
