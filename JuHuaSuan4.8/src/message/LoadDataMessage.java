package message;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import utils.ExcelUtils;
import utils.StringProcesser;
import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;

public class LoadDataMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	public static File dataFile;
	
	public 	LoadDataMessage(File file) {
		if (file != null && (file.getName().endsWith("xls") || file.getName().endsWith("xlsx"))) {
			dataFile = file;
		} else {
			dataFile = new File("resource/test.xlsx");
		}
	}
	
	
	@Override
	public Object run() {

//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		
		Browser browser = Main.getBrowser3();
		if (browser == null) {
			return null;
		}
		//页面验证，防止页面已经跳转
		DOMDocument document = browser.getDocument();
		DOMElement element = document.getElementById("dataTable");
		DOMElement countElement = document.getElementById("count");
		countElement.setInnerHTML("0");
		element.setInnerHTML("loading...");
		try {
			String[][] data = ExcelUtils.getData(dataFile, 0, (short) 0);
			countElement.setInnerHTML(data.length + "");
//			String modelString = StringProcesser.readFile("resource/data.html");
			Main.setRepatProductFrameTitle(dataFile.getName());
			StringBuffer sb = new StringBuffer();
//			sb.append("<tr><td align=\"cente\">款号</td><td>价格</td><td>id</td><td>库存</td><td>卖点1</td><td>卖点2</td><td>卖点3</td><td> 最低价备注 </td><td>操作</td></tr>");
			int num = 0;
			for (String[] e : data) {
				sb.append("<tr>");
				String id = "";
				for (int i = 0; i < e.length - 1; i++) {
					sb.append("<td>").append(e[i]).append("</td>");
					if (i == 0) {
						id = e[i];
					}
				}
				if (num > 0) {
					sb.append("<td>").append("<input type='button' alt='"+ id +"' value='提交' id='"+ id +"' onclick=\"alert('3,"+ id +"')\"/>").append("</td>");
				}
				num++;
				sb.append("</tr>");
			}
			element.setInnerHTML(sb.toString());
//			modelString = modelString.replaceAll("{1}", sb.toString());
//			browser.loadHTML(modelString);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
