package message;

import java.util.Set;

import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;

import config.UrlConfig;


public class AutoLoginMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	

	public 	AutoLoginMessage() {
		
	}

	@Override
	public Object run() {
		try {
			
			Browser browser = Main.getBrowser();
			
			if (!browser.getURL().startsWith(UrlConfig.loginUrl)) {
				browser.loadURL(UrlConfig.loginUrl);
				while (browser.isLoading()) {
					TimeUtils.sleep(500);
				}
			}
			Set<Long> frameIds = browser.getFramesIds();
			for (long id : frameIds) {
				DOMDocument document = browser.getDocument(id);
				DOMElement summitBtnElement = document.getElementById("fm-login-submit");
				if (summitBtnElement != null) {
					DOMElement loginIdElement = document.getElementById("fm-login-id");
					loginIdElement.setAttribute("value", "sales001@xclelec.com");
					DOMElement passwordElement = document.getElementById("fm-login-password");
					passwordElement.setAttribute("value", "LEO123456");
					summitBtnElement.click();
					Main.setLogin(true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
