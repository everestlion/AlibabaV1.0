package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEvent;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventListener;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventType;
import com.teamdev.jxbrowser.chromium.events.StatusEvent;
import com.teamdev.jxbrowser.chromium.events.StatusListener;

import javax.swing.*;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.concurrent.TimeUnit;

/**
 * This sample demonstrates how to use request handler.
 */
public class NetworkDelegateSample {
	
	private static JFrame hideFrame;
    public static void main(String[] args) {
        BrowserContext browserContext = BrowserContext.defaultContext();
        browserContext.setNetworkDelegate(new DefaultNetworkDelegate() {
            @Override
            public void onBeforeURLRequest(BeforeURLRequestParams params) {
                // If navigate to teamdev.com, then change URL to google.com.
                if (params.getURL().equals("http://www.baidu.com/")) {
//                    params.setURL("www.baidu.com");
                }
            }

            @Override
            public void onBeforeSendHeaders(BeforeSendHeadersParams params) {
                // If navigate to google.com, then print User-Agent header value.
                if (params.getURL().equals("http://www.baidu.com/")) {
                    HttpHeaders headers = params.getHeaders();
                    System.out.println("User-Agent: " + headers.getHeader("User-Agent"));
                }
            }
        });

        Browser browser = BrowserFactory.create(browserContext);
        
        final Browser browser2 = BrowserFactory.create(browserContext);

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        JFrame frame2 = new JFrame();
        frame.addWindowListener(new WindowListener() {
			
			public void windowOpened(WindowEvent e) {
				System.out.println("new window...");
				
			}
			
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
        frame2.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frame2.add(browser2.getView().getComponent(), BorderLayout.CENTER);
        frame2.setSize(700, 500);
        frame2.setLocationRelativeTo(null);
        frame2.setVisible(false);
        hideFrame = frame2;
        browser.loadURL("http://www.baidu.com/");
        try {
			TimeUnit.MILLISECONDS.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        browser.getDocument().getElementById("su").addEventListener(DOMEventType.OnClick, new DOMEventListener() {
			
			public void handleEvent(DOMEvent arg0) {
				System.out.println("click");
//				browser2.loadURL("http://www.sina.com/");
//				hideFrame.setVisible(true);
				browser2.getView().getComponent().setVisible(true);
			}
		}, false);

        
    }
}
