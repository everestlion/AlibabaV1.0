package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * The sample demonstrates how to register your DialogHandler and
 * override the functionality that displays file chooser when
 * user uploads file using INPUT TYPE="file" HTML element on a web page.
 */
public class FileUploadSample {
    public static void main(String[] args) {
        Browser browser = BrowserFactory.create();

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.setDialogHandler(new SilentDialogHandler() {
            @Override
            public CloseStatus onFileChooser(FileChooserParams params) {
                boolean isOpenMode = params.getMode() == FileChooserMode.Open;
                boolean isOpenMultipleMode = params.getMode() == FileChooserMode.OpenMultiple;
                if (isOpenMode || isOpenMultipleMode) {
                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setMultiSelectionEnabled(isOpenMultipleMode);
                    int returnValue = fileChooser.showOpenDialog(
                            params.getBrowser().getView().getComponent());
                    if (returnValue == JFileChooser.APPROVE_OPTION) {
                        if (isOpenMultipleMode) {
                            params.setSelectedFiles(fileChooser.getSelectedFiles());
                        } else {
                            params.setSelectedFiles(fileChooser.getSelectedFile());
                        }
                        return CloseStatus.OK;
                    }
                }
                return CloseStatus.CANCEL;
            }
        });
        browser.loadURL("http://www.cs.tut.fi/~jkorpela/forms/file.html");
    }
}
