package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserFactory;
import com.teamdev.jxbrowser.chromium.DefaultPopupHandler;
import com.teamdev.jxbrowser.chromium.PopupContainer;
import com.teamdev.jxbrowser.chromium.PopupParams;
import com.teamdev.jxbrowser.chromium.events.*;
import com.teamdev.jxbrowser.chromium.internal.WebView;
import com.teamdev.jxbrowser.chromium.internal.ipc.events.ChannelListener;
import com.teamdev.jxbrowser.chromium.internal.ipc.message.Message;

import javax.swing.*;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The sample demonstrates how to listen to different load events such as
 * 'start loading frame', 'finish loading frame', 'fail loading frame',
 * 'document loaded in frame' and 'document loaded in main frame'.
 */
public class LoadListenerSample {
    public static void main(String[] args) {
        final Browser browser = BrowserFactory.create();

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(browser.getView().getComponent(), BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        browser.addLoadListener(new LoadAdapter() {
            @Override
            public void onStartLoadingFrame(StartLoadingEvent event) {
                if (event.isMainFrame()) {
                    System.out.println("Main frame has started loading");
                }
            }

            @Override
            public void onProvisionalLoadingFrame(ProvisionalLoadingEvent event) {
                if (event.isMainFrame()) {
                    System.out.println("Provisional load was committed for a frame");
                }
            }

            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent event) {
                if (event.isMainFrame()) {
                    System.out.println("Main frame has finished loading");
                }
                System.out.println("Main frame has finished loading");
            }

            @Override
            public void onFailLoadingFrame(FailLoadingEvent event) {
                int errorCode = event.getErrorCode();
                if (event.isMainFrame()) {
                    System.out.println("Main frame has failed loading: " + errorCode);
                }
            }

            @Override
            public void onDocumentLoadedInFrame(FrameLoadEvent event) {
                System.out.println("Frame document is loaded.");
            }

            @Override
            public void onDocumentLoadedInMainFrame(LoadEvent event) {
                System.out.println("Main frame document is loaded.");
            }
        });
        
        browser.setPopupHandler(new DefaultPopupHandler() {

			@Override
			public PopupContainer handlePopup(PopupParams paramPopupParams) {
				
				return new PopupContainer() {
                    public void insertBrowser(final Browser browser, Rectangle initialBounds) {
                        JComponent component = browser.getView().getComponent();
                        component.setPreferredSize(initialBounds.getSize());
//                        browser.getView().getComponent().get
                        final JFrame frame = new JFrame("Popup");
                        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        frame.add(component, BorderLayout.CENTER);
                        frame.pack();
                        frame.setLocation(initialBounds.x, initialBounds.y);
                        frame.setVisible(true);
                        frame.addWindowListener(new WindowAdapter() {
                            @Override
                            public void windowClosing(WindowEvent e) {
                                browser.dispose();
                            }
                        });
                        System.out.println(((WebView)browser).getChannel().getBrowserId());
                        ((WebView)browser).getChannel().addChannelListener(new ChannelListener() {
							
							public void onMessageReceived(Message paramMessage) {
								
							}
						});
                        browser.addDisposeListener(new DisposeListener() {
                            public void onDisposed(DisposeEvent event) {
                                frame.setVisible(false);
                            }
                        });
                    }
                };
			}
        	
        });

        browser.loadURL("http://www.baidu.com");
    }
}
