package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.*;

/**
 * The sample demonstrates how to configure Browser instance to use custom proxy settings.
 * By default Browser instance uses system proxy settings.
 */
public class ProxySample {
    public static void main(String[] args) {
        // Browser will use system proxy settings.
        // Browser browser = BrowserFactory.create();

        // Browser will automatically detect proxy settings.
        // Browser browser = BrowserFactory.create(new AutoDetectProxyConfig());

        // Browser will not use a proxy server.
        // Browser browser = BrowserFactory.create(new DirectProxyConfig());

        // Browser will use proxy settings received from proxy auto-config (PAC) file.
        // Browser browser = BrowserFactory.create(new URLProxyConfig("<pac-file-url>"));

        // Browser will use custom user's proxy settings.
        HostPortPair httpServer = new HostPortPair("http-proxy-server", 80);
        HostPortPair httpsServer = new HostPortPair("https-proxy-server", 80);
        HostPortPair ftpServer = new HostPortPair("ftp-proxy-server", 80);
        String exceptions = "<local>";  // bypass proxy server for local web pages

        Browser browser = BrowserFactory.create(new CustomProxyConfig(httpServer,
                httpsServer, ftpServer, exceptions));

        // In order to handle proxy authorization you can use the following way
        browser.getContext().setNetworkDelegate(new DefaultNetworkDelegate() {
            @Override
            public boolean onAuthRequired(AuthRequiredParams params) {
                if (params.isProxy()) {
                    params.setUsername("proxy-username");
                    params.setPassword("proxy-password");
                    return false;
                }
                return true;
            }
        });
    }
}
