package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserFactory;
import com.teamdev.jxbrowser.chromium.XPathResult;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;

import javax.swing.*;
import java.awt.*;

/**
 * This sample demonstrates how to evaluate the XPath expression and work with the result.
 */
public class XPathSample {
    public static void main(String[] args) {
        final Browser browser = BrowserFactory.create();

        JFrame frame = new JFrame();
        frame.getContentPane().add(browser.getView().getComponent(), BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.addLoadListener(new LoadAdapter() {
            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent event) {
                if (event.isMainFrame()) {
                    DOMDocument document = browser.getDocument();
                    XPathResult result = document.evaluate("count(//div)");
                    // If the expression is not a valid XPath expression or the document
                    // element is not available, we'll get an error.
                    if (result.isError()) {
                        System.out.println("Error: " + result.getErrorMessage());
                        return;
                    }

                    // Make sure that result is a number.
                    if (result.isNumber()) {
                        System.out.println("Result: " + result.getNumber());
                    }
                }
            }
        });
        browser.loadURL("http://www.baidu.com/");
    }
}
