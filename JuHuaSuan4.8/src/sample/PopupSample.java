package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.events.DisposeEvent;
import com.teamdev.jxbrowser.chromium.events.DisposeListener;

import config.UrlConfig;

import javax.swing.*;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The sample demonstrates how to register popup handler and
 * implement functionality that displays popup windows.
 */
public class PopupSample {
    public static void main(String[] args) {
        final Browser mainBrowser = BrowserFactory.create();

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(mainBrowser.getView().getComponent(), BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        mainBrowser.setPopupHandler(new PopupHandler() {
            public PopupContainer handlePopup(PopupParams params) {
                return new PopupContainer() {
                    public void insertBrowser(final Browser browser, Rectangle initialBounds) {
                        JComponent component = browser.getView().getComponent();
                        component.setPreferredSize(mainBrowser.getView().getComponent().getSize());
//                        browser.getView().getComponent().get
                        final JFrame frame = new JFrame("Popup");
                        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        frame.add(component, BorderLayout.CENTER);
                        frame.pack();
                        frame.setLocation(new Point(mainBrowser.getView().getComponent().getX() + 20, mainBrowser.getView().getComponent().getY() + 20));
                        frame.setVisible(true);
                        frame.addWindowListener(new WindowAdapter() {
                            @Override
                            public void windowClosing(WindowEvent e) {
                                browser.dispose();
                            }
                        });

                        browser.addDisposeListener(new DisposeListener() {
                            public void onDisposed(DisposeEvent event) {
                                frame.setVisible(false);
                            }
                        });
                    }
                };
            }
        });
        
//        browser.setPopupHandler(new DefaultPopupHandler() {
//
//			@Override
//			public PopupContainer handlePopup(PopupParams paramPopupParams) {
//				if (paramPopupParams.getURL().indexOf(UrlConfig.hotKeywordSearchUrl) >= 0) {
//					System.out.println(paramPopupParams.getTargetName());
//				}
//				System.out.println(paramPopupParams.getURL());
//				return super.handlePopup(paramPopupParams);
//			}
//        	
//        });

        mainBrowser.loadURL("http://www.baidu.com");
    }
}
