package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserFactory;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;

/**
 * This sample demonstrates how to load custom HTML string into
 * Browser component and display it.
 */
public class GetHTMLSample {
    public static void main(String[] args) {
        Browser browser = BrowserFactory.create();
        browser.addLoadListener(new LoadAdapter() {
            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent event) {
                if (event.isMainFrame()) {
                    Browser browser = event.getBrowser();
                    System.out.println("HTML = " + browser.getHTML());
                }
            }
        });
        browser.loadURL("http://www.teamdev.com");
    }
}
