package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserFactory;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * This sample demonstrates how to use querySelector DOM API.
 */
public class DOMQuerySelectorSample {
    public static void main(String[] args) {
        Browser browser = BrowserFactory.create();

        JFrame frame = new JFrame();
        frame.getContentPane().add(browser.getView().getComponent(), BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        browser.addLoadListener(new LoadAdapter() {
            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent event) {
                DOMDocument document = event.getBrowser().getDocument();
                DOMElement documentElement = document.getDocumentElement();
                // Get the div with id = "root".
                DOMNode divRoot = documentElement.querySelector("#root");
                // Get all paragraphs.
                List<DOMNode> paragraphs = divRoot.querySelectorAll("p");
                for (DOMNode paragraph : paragraphs) {
                    System.out.println("paragraph.getNodeValue() = " +
                            paragraph.getNodeValue());
                }
            }
        });
        browser.loadHTML(
                "<html><body><div id='root'>" +
                "<p>paragraph1</p>" +
                "<p>paragraph2</p>" +
                "<p>paragraph3</p>" +
                "</div></body></html>");
    }
}
