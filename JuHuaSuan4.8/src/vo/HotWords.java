package vo;

public class HotWords {
	
	private String name;
	
	private int searchTimes;
	
	private int supplierNum;
	
	private int showcaseNum;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSearchTimes() {
		return searchTimes;
	}

	public void setSearchTimes(int searchTimes) {
		this.searchTimes = searchTimes;
	}

	public int getSupplierNum() {
		return supplierNum;
	}

	public void setSupplierNum(int supplierNum) {
		this.supplierNum = supplierNum;
	}

	public int getShowcaseNum() {
		return showcaseNum;
	}

	public void setShowcaseNum(int showcaseNum) {
		this.showcaseNum = showcaseNum;
	}
	
	public String toString() {
		return supplierNum + "/" + showcaseNum + "/" + searchTimes;
	}
	

}
