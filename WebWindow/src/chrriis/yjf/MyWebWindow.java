package chrriis.yjf;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserCommandEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserListener;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserNavigationEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserWindowOpeningEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserWindowWillOpenEvent;

public class MyWebWindow {
	
	public static JComponent createContent(String url) {
	    JDesktopPane desktopPane = new JDesktopPane();
	    // Web Browser 2 internal frame, with a button on top
	    JInternalFrame webBrowser2InternalFrame = new JInternalFrame("���������");
	    webBrowser2InternalFrame.setBounds(30, 30, 600, 500);
	    webBrowser2InternalFrame.setResizable(true);
	    webBrowser2InternalFrame.setVisible(true);
	    GridBagLayout gridBag = new GridBagLayout();
	    GridBagConstraints cons = new GridBagConstraints();
	    cons.fill = GridBagConstraints.BOTH;
	    cons.gridx = 0;
	    cons.gridy = 0;
	    JPanel webBrowser2ContentPane = new JPanel(gridBag) {
	      @Override
	      public boolean isOptimizedDrawingEnabled() {
	        return false;
	      }
	    };
	    	// When a frame is iconified, components are destroyed. To avoid this, we use the option to destroy on finalization.
		    final JWebBrowser webBrowser2 = new JWebBrowser(JWebBrowser.proxyComponentHierarchy(), JWebBrowser.destroyOnFinalization());
		    webBrowser2.navigate(url);
		    cons.weightx = 1;
		    cons.weighty = 1;
		    gridBag.setConstraints(webBrowser2, cons);
		    webBrowser2ContentPane.add(webBrowser2);
		    JButton webBrowser2Button = new JButton("A Swing button");
		    cons.fill = GridBagConstraints.NONE;
		    cons.weightx = 0;
		    cons.weighty = 0;
		    gridBag.setConstraints(webBrowser2Button, cons);
		    webBrowser2ContentPane.add(webBrowser2Button);
		    webBrowser2ContentPane.setComponentZOrder(webBrowser2Button, 0);
		    webBrowser2InternalFrame.add(webBrowser2ContentPane, BorderLayout.CENTER);
		    webBrowser2InternalFrame.setIconifiable(true);
		    desktopPane.add(webBrowser2InternalFrame);
		    JPanel contentPane = new JPanel(new BorderLayout()) {
		      @Override
		      public void removeNotify() {
		        super.removeNotify();
		        // webBrowser2 is destroyed on finalization.
		        // Rather than wait for garbage collection, release when the component is removed from its parent.
		        webBrowser2.disposeNativePeer();
		      }
		    };
		    contentPane.add(desktopPane, BorderLayout.CENTER);
		    return contentPane;
	  }
	
	public static void main(String[] args) {
		NativeInterface.open();
	    UIUtils.setPreferredLookAndFeel();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  String url = "http://www.baidu.com";
	    	  JFrame frame = new JFrame("DJ Native Swing Test");
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        frame.getContentPane().add(createContent(url), BorderLayout.CENTER);
		        frame.setSize(800, 600);
		        frame.setLocationByPlatform(true);
		        frame.setVisible(true);
	      }
	      
	    });
	    NativeInterface.runEventPump();
	}

}
