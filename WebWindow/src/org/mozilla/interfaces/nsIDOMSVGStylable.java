/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGStylable.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGStylable extends nsISupports {

  String NS_IDOMSVGSTYLABLE_IID =
    "{ea8a6cb1-9176-45db-989d-d0e89f563d7e}";

  nsIDOMSVGAnimatedString getClassName();

  nsIDOMCSSStyleDeclaration getStyle();

  nsIDOMCSSValue getPresentationAttribute(String name);

}