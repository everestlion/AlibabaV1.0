/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/io/nsIFastLoadService.idl
 */

package org.mozilla.interfaces;

public interface nsIFastLoadFileIO extends nsISupports {

  String NS_IFASTLOADFILEIO_IID =
    "{715577db-d9c5-464a-a32e-0a40c29b22d4}";

  nsIInputStream getInputStream();

  nsIOutputStream getOutputStream();

}