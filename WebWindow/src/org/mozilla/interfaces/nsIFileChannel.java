/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/protocol/file/public/nsIFileChannel.idl
 */

package org.mozilla.interfaces;

/**
 * nsIFileChannel
 */
public interface nsIFileChannel extends nsISupports {

  String NS_IFILECHANNEL_IID =
    "{06169120-136d-45a5-b535-498f1f755ab7}";

  nsIFile getFile();

}