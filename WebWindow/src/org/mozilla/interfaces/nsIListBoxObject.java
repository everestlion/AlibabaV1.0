/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/xul/base/public/nsIListBoxObject.idl
 */

package org.mozilla.interfaces;

public interface nsIListBoxObject extends nsISupports {

  String NS_ILISTBOXOBJECT_IID =
    "{fde7c970-0b4e-49f4-b1eb-974ae6c96336}";

  nsIListBoxObject getListboxBody();

  int getRowCount();

  int getNumberOfVisibleRows();

  int getIndexOfFirstVisibleRow();

  void ensureIndexIsVisible(int rowIndex);

  void scrollToIndex(int rowIndex);

  void scrollByLines(int numLines);

  nsIDOMElement getItemAtIndex(int index);

  int getIndexOfItem(nsIDOMElement item);

}