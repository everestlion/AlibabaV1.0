/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULControlElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULControlElement extends nsIDOMXULElement {

  String NS_IDOMXULCONTROLELEMENT_IID =
    "{007b8358-1dd2-11b2-8924-d209efc3f124}";

  boolean getDisabled();

  void setDisabled(boolean aDisabled);

  int getTabIndex();

  void setTabIndex(int aTabIndex);

}