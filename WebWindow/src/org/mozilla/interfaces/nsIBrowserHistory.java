/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/places/public/nsIBrowserHistory.idl
 */

package org.mozilla.interfaces;

public interface nsIBrowserHistory extends nsIGlobalHistory2 {

  String NS_IBROWSERHISTORY_IID =
    "{96602bf3-de2a-42ed-812f-a83b130e6299}";

  /**
     * addPageWithDetails
     *
     * Adds a page to history with specific time stamp information. This is used in
     * the History migrator. 
     */
  void addPageWithDetails(nsIURI aURI, String aTitle, long aLastVisited);

  /**
     * lastPageVisited
     *
     * The last page that was visited in a top-level window.
     */
  String getLastPageVisited();

  /**
     * count
     *
     * Indicate if there are entries in global history
     * For performance reasons this does not return the real number of entries
     */
  long getCount();

  /**
     * removePage
     *
     * Remove a page from history
     */
  void removePage(nsIURI aURI);

  /**
     * removePages
     *
     * Remove a bunch of pages from history
     * Notice that this does not call observers for performance reasons,
     * instead setting aDoBatchNotify true will send Begin/EndUpdateBatch
     */
  void removePages(nsIURI[] aURIs, long aLength, boolean aDoBatchNotify);

  /**
     * removePagesFromHost
     *
     * Remove all pages from the given host.
     * If aEntireDomain is true, will assume aHost is a domain,
     * and remove all pages from the entire domain.
     * Notice that this does not call observers for single deleted uris,
     * instead it will send Begin/EndUpdateBatch
     */
  void removePagesFromHost(String aHost, boolean aEntireDomain);

  /**
     * removePagesByTimeframe
     *
     * Remove all pages for a given timeframe.
     * Limits are included: aBeginTime <= timeframe <= aEndTime
     * Notice that this does not call observers for single deleted uris,
     * instead it will send Begin/EndUpdateBatch
     */
  void removePagesByTimeframe(long aBeginTime, long aEndTime);

  /**
     * removeAllPages
     *
     * Remove all pages from global history
     */
  void removeAllPages();

  /**
     * hidePage
     *
     * Hide the specified URL from being enumerated (and thus
     * displayed in the UI)
     * If the page hasn't been visited yet, then it will be added
     * as if it was visited, and then marked as hidden
     */
  void hidePage(nsIURI aURI);

  /**
     * markPageAsTyped
     *
     * Designate the url as having been explicitly typed in by
     * the user, so it's okay to be an autocomplete result.
     */
  void markPageAsTyped(nsIURI aURI);

}