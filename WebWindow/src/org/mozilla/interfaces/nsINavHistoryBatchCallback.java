/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/places/public/nsINavHistoryService.idl
 */

package org.mozilla.interfaces;

public interface nsINavHistoryBatchCallback extends nsISupports {

  String NS_INAVHISTORYBATCHCALLBACK_IID =
    "{5143f2bb-be0a-4faf-9acb-b0ed3f82952c}";

  /**
 * @see runInBatchMode of nsINavHistoryService/nsINavBookmarksService
 */
  void runBatched(nsISupports aUserData);

}