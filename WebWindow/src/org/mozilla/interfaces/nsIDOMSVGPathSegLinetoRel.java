/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegLinetoRel extends nsISupports {

  String NS_IDOMSVGPATHSEGLINETOREL_IID =
    "{7933a81a-72c5-4489-ba64-5635f4c23063}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

}