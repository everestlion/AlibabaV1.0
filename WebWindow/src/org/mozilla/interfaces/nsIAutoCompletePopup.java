/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/autocomplete/public/nsIAutoCompletePopup.idl
 */

package org.mozilla.interfaces;

public interface nsIAutoCompletePopup extends nsISupports {

  String NS_IAUTOCOMPLETEPOPUP_IID =
    "{1b9d7d8a-6dd0-11dc-8314-0800200c9a66}";

  nsIAutoCompleteInput getInput();

  String getOverrideValue();

  int getSelectedIndex();

  void setSelectedIndex(int aSelectedIndex);

  boolean getPopupOpen();

  void openAutocompletePopup(nsIAutoCompleteInput input, nsIDOMElement element);

  void closePopup();

  void invalidate();

  void selectBy(boolean reverse, boolean page);

}