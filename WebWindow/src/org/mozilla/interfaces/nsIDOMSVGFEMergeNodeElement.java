/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEMergeNodeElement extends nsIDOMSVGElement {

  String NS_IDOMSVGFEMERGENODEELEMENT_IID =
    "{540c3447-4b07-4bd3-84df-30f66b68df14}";

  nsIDOMSVGAnimatedString getIn1();

}