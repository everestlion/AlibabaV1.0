/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleSelectable.idl
 */

package org.mozilla.interfaces;

/**
 * An interface for the accessibility module and in-process accessibility clients
 * for dealing with getting and changing the selection of accessible nodes.
 *
 * @status UNDER_REVIEW
 */
public interface nsIAccessibleSelectable extends nsISupports {

  String NS_IACCESSIBLESELECTABLE_IID =
    "{34d268d6-1dd2-11b2-9d63-83a5e0ada290}";

  long eSelection_Add = 0L;

  long eSelection_Remove = 1L;

  long eSelection_GetState = 2L;

  /**
     * Return an nsIArray of selected nsIAccessible children 
     */
  nsIArray getSelectedChildren();

  /**
     * Returns the number of accessible children currently selected.
     */
  int getSelectionCount();

  /**
     * Adds the specified accessible child of the object to the
     * object's selection.
     * If the specified object is already selected, then it does nothing.
     * @throws NS_ERROR_FAILURE if the specified object is not selectable.
     */
  void addChildToSelection(int index);

  /**
     * Removes the specified child of the object from the object's selection.
     * If the specified object was not selected, then it does nothing.
     * @throws NS_ERROR_FAILURE if the specified object is not selectable.
     */
  void removeChildFromSelection(int index);

  /**
     * Clears the selection in the object so that no children in the object
     * are selected.
     */
  void clearSelection();

  /**
     * Returns a reference to the accessible object representing the specified
     * selected child of the object.
     * @param index Zero-based selected accessible child index 
     * @return The nth selected accessible child
     */
  nsIAccessible refSelection(int index);

  /**
      * Determines if the current child of this object is selected
      * @param The zero-based accessible child index
      * @return Returns true if the child is selected, false if not.
      */
  boolean isChildSelected(int index);

  /**
     * Select all children
     * @return If the object does not accept multiple selection, return false.
     *         Otherwise, returns true.
     */
  boolean selectAllSelection();

}