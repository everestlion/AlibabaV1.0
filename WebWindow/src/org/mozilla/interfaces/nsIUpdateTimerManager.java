/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/mozapps/update/public/nsIUpdateService.idl
 */

package org.mozilla.interfaces;

/**
 * An interface describing a global application service that allows long
 * duration (e.g. 1-7 or more days, weeks or months) timers to be registered
 * and then fired.
 */
public interface nsIUpdateTimerManager extends nsISupports {

  String NS_IUPDATETIMERMANAGER_IID =
    "{0765c92c-6145-4253-9db4-594d8023087e}";

  /**
   * Register an interval with the timer manager. The timer manager
   * periodically checks to see if the interval has expired and if it has
   * calls the specified callback. This is persistent across application
   * restarts and can handle intervals of long durations.
   * @param   id
   *          An id that identifies the interval, used for persistence
   * @param   callback
   *          A nsITimerCallback object that is notified when the interval
   *          expires
   * @param   interval
   *          The length of time, in seconds, of the interval
   */
  void registerTimer(String id, nsITimerCallback callback, long interval);

}