/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedLengthList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedLengthList extends nsISupports {

  String NS_IDOMSVGANIMATEDLENGTHLIST_IID =
    "{bfa6e42b-bc9d-404d-8688-729fdbfff801}";

  nsIDOMSVGLengthList getBaseVal();

  nsIDOMSVGLengthList getAnimVal();

}