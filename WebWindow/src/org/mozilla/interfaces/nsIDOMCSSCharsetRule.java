/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCSSCharsetRule.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCSSCharsetRule extends nsIDOMCSSRule {

  String NS_IDOMCSSCHARSETRULE_IID =
    "{19fe78cc-65ff-4b1d-a5d7-9ea89692cec6}";

  String getEncoding();

  void setEncoding(String aEncoding);

}