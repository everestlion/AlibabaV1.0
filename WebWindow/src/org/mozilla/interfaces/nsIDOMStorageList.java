/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/storage/nsIDOMStorageList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMStorageList extends nsISupports {

  String NS_IDOMSTORAGELIST_IID =
    "{f2166929-91b6-4372-8d5f-c366f47a5f54}";

  /**
   * Returns a storage object for a particular domain.
   *
   * @param domain domain to retrieve
   * @returns a storage area for the given domain
   */
  nsIDOMStorage namedItem(String domain);

}