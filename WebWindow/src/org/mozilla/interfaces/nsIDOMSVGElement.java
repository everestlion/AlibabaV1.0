/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGElement extends nsIDOMElement {

  String NS_IDOMSVGELEMENT_IID =
    "{e0be7cbb-81c1-4663-8f95-109d96a60b6b}";

  String getId();

  void setId(String aId);

  nsIDOMSVGSVGElement getOwnerSVGElement();

  nsIDOMSVGElement getViewportElement();

}