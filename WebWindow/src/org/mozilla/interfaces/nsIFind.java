/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/embedding/components/find/public/nsIFind.idl
 */

package org.mozilla.interfaces;

public interface nsIFind extends nsISupports {

  String NS_IFIND_IID =
    "{75125d55-37ee-4575-b9b5-f33bfa68c2a1}";

  boolean getFindBackwards();

  void setFindBackwards(boolean aFindBackwards);

  boolean getCaseSensitive();

  void setCaseSensitive(boolean aCaseSensitive);

  /**
   * Use "find entire words" mode by setting to a word breaker
   * or null, to disable "entire words" mode.
   */
  nsISupports getWordBreaker();

  /**
   * Use "find entire words" mode by setting to a word breaker
   * or null, to disable "entire words" mode.
   */
  void setWordBreaker(nsISupports aWordBreaker);

  /**
   * Find some text in the current context. The implementation is
   * responsible for performing the find and highlighting the text.
   *
   * @param aPatText     The text to search for.
   * @param aSearchRange A Range specifying domain of search.
   * @param aStartPoint  A Range specifying search start point.
   *                     If not collapsed, we'll start from
   *                     end (forward) or start (backward).
   *                     May be null; if so, we'll start at the start
   *                     (forward) or end (back) of aSearchRange.
   * @param aEndPoint    A Range specifying search end point.
   *                     If not collapsed, we'll end at
   *                     end (forward) or start (backward).
   *                     May be null; if so, we'll end at the end
   *                     (forward) or start (back) of aSearchRange.
   * @retval             A range spanning the match that was found (or null).
   */
  nsIDOMRange find(String aPatText, nsIDOMRange aSearchRange, nsIDOMRange aStartPoint, nsIDOMRange aEndPoint);

}