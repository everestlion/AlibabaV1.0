/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/jsd/idl/jsdIDebuggerService.idl
 */

package org.mozilla.interfaces;

public interface jsdIFilterEnumerator extends nsISupports {

  String JSDIFILTERENUMERATOR_IID =
    "{54382875-ed12-4f90-9a63-1f0498d0a3f2}";

  /**
 * Pass an instance of one of these to jsdIDebuggerService::enumerateFilters.
 */
/**
     * The enumerateFilter method will be called once for every filter the
     * debugger knows about.
     */
  void enumerateFilter(jsdIFilter filter);

}