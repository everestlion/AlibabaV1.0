/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/places/public/nsINavBookmarksService.idl
 */

package org.mozilla.interfaces;

/**
 * Observer for bookmark changes.
 */
public interface nsINavBookmarkObserver extends nsISupports {

  String NS_INAVBOOKMARKOBSERVER_IID =
    "{f9828ba8-9c70-4d95-b926-60d9e4378d7d}";

  /**
   * Notify this observer that a batch transaction has started.
   * Other notifications will be sent during the batch change,
   * but the observer is guaranteed that onEndUpdateBatch() will be called
   * at the completion of changes.
   */
  void onBeginUpdateBatch();

  /**
   * Notify this observer that a batch transaction has ended.
   */
  void onEndUpdateBatch();

  /**
   * Notify this observer that an item was added.  Called after the actual
   * add took place. The items following the index will be shifted down, but
   * no additional notifications will be sent.
   *
   * @param aItemId
   *        The id of the bookmark that was added.
   * @param aFolder
   *        The folder that the item was added to.
   * @param aIndex
   *        The item's index in the folder.
   */
  void onItemAdded(long aItemId, long aFolder, int aIndex);

  /**
   * Notify this observer that an item was removed.  Called after the actual
   * remove took place. The items following the index will be shifted up, but
   * no additional notifications will be sent.
   *
   * @param aItemId
   *        The id of the bookmark that was removed.
   * @param aFolder
   *        The folder that the item was removed from.
   * @param aIndex
   *        The bookmark's index in the folder.
   */
  void onItemRemoved(long aItemId, long aFolder, int aIndex);

  /**
   * Notify this observer that an item's information has changed.  This
   * will be called whenever any attributes like "title" are changed.
   * 
   * @param aItemId
   *        The id of the item that was changed.
   * @param aProperty
   *        The property which changed.
   * @param aIsAnnotationProperty
   *        Is aProperty the name of an item annotation
   *
   * property = "cleartime": (history was deleted, there is no last visit date):
   *                         value = empty string.
   * property = "title": value = new title.
   * property = "favicon": value = new "moz-anno" URL of favicon image
   * property = "uri": value = new uri spec.
   * property = "tags: (tags set for the bookmarked uri have changed)
   *             value = empty string.
   * property = "dateAdded": value = PRTime when the item was first added
   * property = "lastModified": value = PRTime when the item was last modified
   * aIsAnnotationProperty = true: value = empty string.
   */
  void onItemChanged(long aBookmarkId, String aProperty, boolean aIsAnnotationProperty, String aValue);

  /**
   * Notify that the item was visited. Normally in bookmarks we use the last
   * visit date, and normally the time will be a new visit that will be more
   * recent, but this is not guaranteed. You should check to see if it's
   * actually more recent before using this new time.
   *
   * @param aBookmarkId
   *        The id of the bookmark that was visited.
   * @see onItemChanged property = "cleartime" for when all visit dates are
   * deleted for the URI.
   */
  void onItemVisited(long aBookmarkId, long aVisitID, double time);

  /**
   * Notify this observer that an item has been moved.
   *  @param aItemId
   *         The id of the item that was moved.
   *  @param aOldParent
   *         The id of the old parent.
   *  @param aOldIndex
   *         The old index inside aOldParent.
   *  @param aNewParent
   *         The id of the new parent.
   *  @param aNewIndex
   *         The foindex inside aNewParent.
   */
  void onItemMoved(long aItemId, long aOldParent, int aOldIndex, long aNewParent, int aNewIndex);

}