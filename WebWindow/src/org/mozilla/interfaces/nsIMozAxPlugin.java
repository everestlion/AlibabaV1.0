/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/embedding/browser/activex/src/plugin/nsIMozAxPlugin.idl
 */

package org.mozilla.interfaces;

public interface nsIMozAxPlugin extends nsISupports {

  String NS_IMOZAXPLUGIN_IID =
    "{b30c2717-2bbf-4475-9ddf-9e26f893f32a}";

  void invoke(String str);

  void invoke1(String str, nsIVariant a);

  void invoke2(String str, nsIVariant a, nsIVariant b);

  void invoke3(String str, nsIVariant a, nsIVariant b, nsIVariant c);

  void invoke4(String str, nsIVariant a, nsIVariant b, nsIVariant c, nsIVariant d);

  nsIVariant getProperty(String propertyName);

  void setProperty(String propertyName, nsIVariant propertyValue);

}