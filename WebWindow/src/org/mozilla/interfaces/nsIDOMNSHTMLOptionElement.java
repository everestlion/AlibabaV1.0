/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLOptionElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLOptionElement extends nsISupports {

  String NS_IDOMNSHTMLOPTIONELEMENT_IID =
    "{e2dfc89c-7ae0-4651-8aee-7f5edc2aa626}";

  String getText();

  void setText(String aText);

}