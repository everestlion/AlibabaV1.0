/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/base/public/nsIStreamLoader.idl
 */

package org.mozilla.interfaces;

public interface nsIStreamLoaderObserver extends nsISupports {

  String NS_ISTREAMLOADEROBSERVER_IID =
    "{359f7990-d4e9-11d3-a1a5-0050041caf44}";

  /**
     * Called when the entire stream has been loaded.
     *
     * @param loader the stream loader that loaded the stream.
     * @param ctxt the context parameter of the underlying channel
     * @param status the status of the underlying channel
     * @param resultLength the length of the data loaded
     * @param result the data
     *
     * This method will always be called asynchronously by the
     * nsIStreamLoader involved, on the thread that called the
     * loader's init() method.
     */
  void onStreamComplete(nsIStreamLoader loader, nsISupports ctxt, long status, long resultLength, byte[] result);

}