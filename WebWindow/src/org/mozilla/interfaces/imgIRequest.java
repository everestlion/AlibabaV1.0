/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/libpr0n/public/imgIRequest.idl
 */

package org.mozilla.interfaces;

/**
 * imgIRequest interface
 *
 * @author Stuart Parmenter <stuart@mozilla.com>
 * @version 0.1
 * @see imagelib2
 */
public interface imgIRequest extends nsIRequest {

  String IMGIREQUEST_IID =
    "{a297d3fa-5e0c-4e59-9f30-a01c9d4f3f8b}";

  /**
   * the image container...
   * @return the image object associated with the request.
   * @attention NEED DOCS
   */
  imgIContainer getImage();

  /**
   * Bits set in the return value from imageStatus
   * @name statusflags
   */
  int STATUS_NONE = 0;

  int STATUS_SIZE_AVAILABLE = 1;

  int STATUS_LOAD_PARTIAL = 2;

  int STATUS_LOAD_COMPLETE = 4;

  int STATUS_ERROR = 8;

  int STATUS_FRAME_COMPLETE = 16;

  /**
   * something
   * @attention NEED DOCS
   */
  long getImageStatus();

  /**
   * The URI the image load was started with.  Note that this might not be the
   * actual URI for the image (e.g. if HTTP redirects happened during the
   * load).
   */
  nsIURI getURI();

  imgIDecoderObserver getDecoderObserver();

  String getMimeType();

  /**
   * Clone this request; the returned request will have aObserver as the
   * observer.  aObserver will be notified synchronously (before the clone()
   * call returns) with all the notifications that have already been dispatched
   * for this image load.
   */
  imgIRequest _clone(imgIDecoderObserver aObserver);

  /**
   * The principal gotten from the channel the image was loaded from.
   */
  nsIPrincipal getImagePrincipal();

}