/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEComponentTransferElement extends nsIDOMSVGFilterPrimitiveStandardAttributes {

  String NS_IDOMSVGFECOMPONENTTRANSFERELEMENT_IID =
    "{4de6b44a-f909-4948-bc43-5ee2ca6de55b}";

  nsIDOMSVGAnimatedString getIn1();

}