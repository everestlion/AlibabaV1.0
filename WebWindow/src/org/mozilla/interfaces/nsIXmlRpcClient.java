/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/extensions/xml-rpc/idl/nsIXmlRpcClient.idl
 */

package org.mozilla.interfaces;

public interface nsIXmlRpcClient extends nsISupports {

  String NS_IXMLRPCCLIENT_IID =
    "{4d7d15c0-3747-4f7f-b6b3-792a5ea1a9aa}";

  /**
 * Simple XML-RPC client interface.
 */
/**
     * Set server URL. Call this before using this object.
     *
     * @param serverURL URL of server side object on which methods should
     *                  be called.
     */
  void init(String serverURL);

  /**
     * Set authentication info if needed.  Both parameters must be
     * specified for authentication to be enabled.
     *
     * @param username  username to be used if asked to authenticate
     * @param password  password to be used if asked to authenticate
     */
  void setAuthentication(String username, String password);

  /**
     * Clear authentication info.
     */
  void clearAuthentication(String username, String password);

  /**
     * Set character encoding.  The default charset if this function is
     * not called is "UTF-8".
     *
     * @param encoding  encoding charset to be used
     */
  void setEncoding(String encoding);

  /**
     * The URL of the XML-RPC server
     */
  nsIURL getServerURL();

  /**
     * Call remote method methodName asynchronously with given arguments.
     * 
     * Supported arguments are:
     * nsISupportsPRUint8, nsISupportsPRUint16,
     * nsISupportsPRInt16, nsISupportsPRInt32: <i4>
     * nsISupportsPRBool: <boolean>
     * nsISupportsChar, nsISupportsCString: <string>
     * nsISupportsFloat, nsISupportsDouble: <double>
     * nsISupportsPRTime: <dateTime.iso8601>
     * nsIInputStream: <base64>
     * nsISupportsArray: <array>
     * nsIDictionary: <struct>
     *
     * Note that both nsISupportsArray and nsIDictionary can only hold any of
     * the supported input types.
     *
     * Return value will be converted as follows:
     * <i4> or <int>: nsISupportsPRInt32
     * <boolean>: nsISupportsPRBool
     * <string>: nsISupportsCString
     * <double>: nsISupportsDouble
     * <dateTime.iso8601>: nsISupportsPRTime
     * <base64>: nsISupportsCString
     * <array>: nsISupportsArray
     * <struct>: nsIDictionary
     *
     * <fault>s (server side errors) are indicated by returning
     * NS_ERROR_FAILURE. Via nsIXPConnect::GetPendingException()->data a
     * nsIXmlRpcFault object can be retreieved with more information on the
     * fault.
     *
     * @param listener          A nsIXmlRpcClientListener that will get notified
     *                          of XML-RPC events.
     * @param context           A context to be passed on to the listener.
     * @param methodName        Remote method to call.
     * @param arguments         Array of arguments to pass to remote method.
     * @return                  Return value of remote method.
     */
  void asyncCall(nsIXmlRpcClientListener listener, nsISupports ctxt, String methodName, nsISupports[] arguments, long count);

  /**
     * Whether or not a call is in progress
     */
  boolean getInProgress();

  /**
     * The most recent XML-RPC fault from returned from this server.
     * null if the last call didn't return an XML-RPC fault.
     */
  nsIXmlRpcFault getFault();

  /**
     * The most recent XML-RPC call result returned from this server.
     * null if the last call didn't return a valid result
     */
  nsISupports getResult();

  /**
     * The most recent HTTP status code returned from this server
     * null if the server was unreachable or not yet contacted.
     */
  long getResponseStatus();

  long getResponseString();

  /**
     * Convenience: return the correct nsISupportsPrimitive for a given XML-RPC
     * type, or nsISupportsArray or nsIDictionary.  'base64' isn't supported.
     * No QueryInterface call is needed, the object has been QueryInterfaced to
     * the correct interface.
     *
     * @param type              One of the listed constants.
     * @param uuid              The uuid of the returned object. No QI call 
     *                          needed.
     * @param nsQIResult        The apropriate XPCOM object.
     */
  nsISupports createType(long type, String[] uuid);

  long INT = 1L;

  long BOOLEAN = 2L;

  long STRING = 3L;

  long DOUBLE = 4L;

  long DATETIME = 5L;

  long ARRAY = 6L;

  long STRUCT = 7L;

}