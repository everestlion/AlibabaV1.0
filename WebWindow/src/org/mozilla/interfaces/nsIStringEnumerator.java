/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsIStringEnumerator.idl
 */

package org.mozilla.interfaces;

/**
 * Used to enumerate over an ordered list of strings.
 */
public interface nsIStringEnumerator extends nsISupports {

  String NS_ISTRINGENUMERATOR_IID =
    "{50d3ef6c-9380-4f06-9fb2-95488f7d141c}";

  boolean hasMore();

  String getNext();

}