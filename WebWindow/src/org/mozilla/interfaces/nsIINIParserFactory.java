/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsIINIParser.idl
 */

package org.mozilla.interfaces;

public interface nsIINIParserFactory extends nsISupports {

  String NS_IINIPARSERFACTORY_IID =
    "{ccae7ea5-1218-4b51-aecb-c2d8ecd46af9}";

  /**
   * Create an iniparser instance from a local file.
   */
  nsIINIParser createINIParser(nsILocalFile aINIFile);

}