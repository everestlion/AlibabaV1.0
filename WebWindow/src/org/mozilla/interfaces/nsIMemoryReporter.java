/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/base/nsIMemoryReporter.idl
 */

package org.mozilla.interfaces;

public interface nsIMemoryReporter extends nsISupports {

  String NS_IMEMORYREPORTER_IID =
    "{d298b942-3e66-4cd3-9ff5-46abc69147a7}";

  String getPath();

  String getDescription();

  long getMemoryUsed();

}