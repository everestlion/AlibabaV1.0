/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/xtf/public/nsIXTFPrivate.idl
 */

package org.mozilla.interfaces;

public interface nsIXTFPrivate extends nsISupports {

  String NS_IXTFPRIVATE_IID =
    "{13ef3d54-1dd1-4a5c-a8d5-a04a327fb9b6}";

  nsISupports getInner();

}