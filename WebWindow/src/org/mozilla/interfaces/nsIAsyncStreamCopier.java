/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/base/public/nsIAsyncStreamCopier.idl
 */

package org.mozilla.interfaces;

public interface nsIAsyncStreamCopier extends nsIRequest {

  String NS_IASYNCSTREAMCOPIER_IID =
    "{72e515de-a91e-4154-bb78-e5244cbaae74}";

  /**
     * Initialize the stream copier.
     *
     * @param aSource
     *        contains the data to be copied.
     * @param aSink
     *        specifies the destination for the data.
     * @param aTarget
     *        specifies the thread on which the copy will occur.  a null value
     *        is permitted and will cause the copy to occur on an unspecified
     *        background thread.
     * @param aSourceBuffered
     *        true if aSource implements ReadSegments.
     * @param aSinkBuffered
     *        true if aSink implements WriteSegments.
     * @param aChunkSize
     *        specifies how many bytes to read/write at a time.  this controls
     *        the granularity of the copying.  it should match the segment size
     *        of the "buffered" streams involved.
     *
     * NOTE: at least one of the streams must be buffered.
     */
  void init(nsIInputStream aSource, nsIOutputStream aSink, nsIEventTarget aTarget, boolean aSourceBuffered, boolean aSinkBuffered, long aChunkSize);

  /**
     * asyncCopy triggers the start of the copy.  The observer will be notified
     * when the copy completes.
     *
     * @param aObserver
     *        receives notifications.
     * @param aObserverContext
     *        passed to observer methods.
     */
  void asyncCopy(nsIRequestObserver aObserver, nsISupports aObserverContext);

}