/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for unsigned 16-bit integers
 * 
 * @status FROZEN
 */
public interface nsISupportsPRUint16 extends nsISupportsPrimitive {

  String NS_ISUPPORTSPRUINT16_IID =
    "{dfacb090-4a1c-11d3-9890-006008962422}";

  int getData();

  void setData(int aData);

  String toString();

}