/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLImageElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLImageElement extends nsISupports {

  String NS_IDOMNSHTMLIMAGEELEMENT_IID =
    "{a6cf90c7-15b3-11d2-932e-00805f8add32}";

  String getLowsrc();

  void setLowsrc(String aLowsrc);

  boolean getComplete();

  int getNaturalHeight();

  int getNaturalWidth();

  int getX();

  int getY();

}