/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsIVariant.idl
 */

package org.mozilla.interfaces;

/**
 * XPConnect has magic to transparently convert between nsIVariant and JS types.
 * We mark the interface [scriptable] so that JS can use methods
 * that refer to this interface. But we mark all the methods and attributes
 * [noscript] since any nsIVariant object will be automatically converted to a
 * JS type anyway.
 */
public interface nsIVariant extends nsISupports {

  String NS_IVARIANT_IID =
    "{6c9eb060-8c6a-11d5-90f3-0010a4e73d9a}";

}