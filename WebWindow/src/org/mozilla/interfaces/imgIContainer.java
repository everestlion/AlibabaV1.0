/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/libpr0n/public/imgIContainer.idl
 */

package org.mozilla.interfaces;

/**
 * gfxIImageContainer interface
 *
 * @author Stuart Parmenter <pavlov@netscape.com>
 * @version 0.1
 * @see "gfx2"
 */
public interface imgIContainer extends nsISupports {

  String IMGICONTAINER_IID =
    "{1a6290e6-8285-4e10-963d-d001f8d327b8}";

  /** 
   * "Disposal" method indicates how the image should be handled before the
   *  subsequent image is displayed.
   *  Don't change these without looking at the implementations using them,
   *  struct gif_struct::disposal_method and gif_write() in particular.
   */
  int kDisposeClearAll = -1;

  int kDisposeNotSpecified = 0;

  int kDisposeKeep = 1;

  int kDisposeClear = 2;

  int kDisposeRestorePrevious = 3;

  int kBlendSource = 0;

  int kBlendOver = 1;

  /**
   * Create a new \a aWidth x \a aHeight sized image container.
   *
   * @param aWidth The width of the container in which all the
   *               gfxIImageFrame children will fit.
   * @param aHeight The height of the container in which all the
   *                gfxIImageFrame children will fit.
   * @param aObserver Observer to send animation notifications to.
   */
  void init(int aWidth, int aHeight, imgIContainerObserver aObserver);

  int getPreferredAlphaChannelFormat();

  /**
   * The width of the container rectangle.
   */
  int getWidth();

  /**
   * The height of the container rectangle.
   */
  int getHeight();

  /**
   * Get the current frame that would be drawn if the image was to be drawn now
   */
  gfxIImageFrame getCurrentFrame();

  long getNumFrames();

  /**
   * Animation mode Constants
   *   0 = normal
   *   1 = don't animate
   *   2 = loop once
   */
  short kNormalAnimMode = 0;

  short kDontAnimMode = 1;

  short kLoopOnceAnimMode = 2;

  int getAnimationMode();

  void setAnimationMode(int aAnimationMode);

  gfxIImageFrame getFrameAt(long index);

  /**
   * Adds \a item to the end of the list of frames.
   * @param item frame to add.
   */
  void appendFrame(gfxIImageFrame item);

  void removeFrame(gfxIImageFrame item);

  void endFrameDecode(long framenumber, long timeout);

  void decodingComplete();

  void clear();

  void startAnimation();

  void stopAnimation();

  void resetAnimation();

  /**
   * number of times to loop the image.
   * @note -1 means forever.
   */
  int getLoopCount();

  /**
   * number of times to loop the image.
   * @note -1 means forever.
   */
  void setLoopCount(int aLoopCount);

}