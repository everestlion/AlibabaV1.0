/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/embedding/components/commandhandler/public/nsPICommandUpdater.idl
 */

package org.mozilla.interfaces;

public interface nsPICommandUpdater extends nsISupports {

  String NS_PICOMMANDUPDATER_IID =
    "{b135f602-0bfe-11d5-a73c-f0e420e8293c}";

  void init(nsIDOMWindow aWindow);

  void commandStatusChanged(String aCommandName);

}