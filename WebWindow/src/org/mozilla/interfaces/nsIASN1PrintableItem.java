/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsIASN1PrintableItem.idl
 */

package org.mozilla.interfaces;

public interface nsIASN1PrintableItem extends nsIASN1Object {

  String NS_IASN1PRINTABLEITEM_IID =
    "{114e1142-1dd2-11b2-ac26-b6db19d9184a}";

}