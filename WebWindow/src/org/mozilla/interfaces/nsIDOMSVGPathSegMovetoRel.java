/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegMovetoRel extends nsISupports {

  String NS_IDOMSVGPATHSEGMOVETOREL_IID =
    "{58ca7e86-661a-473a-96de-89682e7e24d6}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

}