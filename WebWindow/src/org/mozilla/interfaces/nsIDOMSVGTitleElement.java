/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGTitleElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGTitleElement extends nsIDOMSVGElement {

  String NS_IDOMSVGTITLEELEMENT_IID =
    "{524564b0-13a3-459b-bbec-ad66aea9f789}";

}