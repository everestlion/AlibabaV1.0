/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessNode.idl
 */

package org.mozilla.interfaces;

/**
 * An interface used by in-process accessibility clients
 * to get style, window, markup and other information about
 * a DOM node. When accessibility is active in Gecko,
 * every DOM node can have one nsIAccessNode for each
 * pres shell the DOM node is rendered in.
 * The nsIAccessNode implementations are instantiated lazily.
 * The nsIAccessNode tree for a given dom window
 * has a one to one relationship to the DOM tree.
 * If the DOM node for this access node is "accessible",
 * then a QueryInterface to nsIAccessible will succeed.
 *
 * @status UNDER_REVIEW
 */
public interface nsIAccessNode extends nsISupports {

  String NS_IACCESSNODE_IID =
    "{71a3b4e7-e83d-45cf-a20e-9ce292bcf19f}";

  /**
   * The DOM node this nsIAccessNode is associated with.
   */
  nsIDOMNode getDOMNode();

  /**
   * The number of DOM children for the DOM node, which
   * matches the number of nsIAccessNode children for this
   * nsIAccessNode.
   */
  int getNumChildren();

  /**
   * Get the nth child of this node
   * @param childNum Zero-based child index
   * @return The nth nsIAccessNode child
   */
  nsIAccessNode getChildNodeAt(int childNum);

  /**
   * The parent nsIAccessNode
   */
  nsIAccessNode getParentNode();

  /**
   * The first nsIAccessNode child
   */
  nsIAccessNode getFirstChildNode();

  /**
   * The last nsIAccessNode child
   */
  nsIAccessNode getLastChildNode();

  /**
   * The previous nsIAccessNode sibling
   */
  nsIAccessNode getPreviousSiblingNode();

  /**
   * The next nsIAccessNode sibling
   */
  nsIAccessNode getNextSiblingNode();

  /**
   * The nsIAccessibleDocument that this nsIAccessNode
   * resides in.
   */
  nsIAccessibleDocument getAccessibleDocument();

  /**
   * The innerHTML for the DOM node
   * This is a text string of all the markup inside the DOM
   * node, not including the start and end tag for the node.
   */
  String getInnerHTML();

  /**
   * Makes an object visible on screen.
   *
   * @param scrollType - defines where the object should be placed on
   *                     the screen (see nsIAccessibleScrollType for
   *                     available constants).
   */
  void scrollTo(long aScrollType);

  /**
   * Moves the top left of an object to a specified location.
   *
   * @param coordinateType - specifies whether the coordinates are relative to
   *                         the screen or the parent object (for available
   *                         constants refer to nsIAccessibleCoordinateType)
   * @param aX - defines the x coordinate
   * @param aY - defines the y coordinate
  */
  void scrollToPoint(long aCoordinateType, int aX, int aY);

  /**
   * Retrieve the computed style value for this DOM node, if it is a DOM element.
   * Note: the meanings of width, height and other size measurements depend
   * on the version of CSS being used. Therefore, for bounds information, 
   * it is better to use nsIAccessible::accGetBounds.
   * @param pseudoElt The pseudo element to retrieve style for, or NULL
   *                  for general computed style information for this node.
   * @param propertyName Retrieve the computed style value for this property name,
   *                     for example "border-bottom".
   */
  String getComputedStyleValue(String pseudoElt, String propertyName);

  /**
   * The method is similar to getComputedStyleValue() excepting that this one
   * returns nsIDOMCSSPrimitiveValue.
   */
  nsIDOMCSSPrimitiveValue getComputedStyleCSSValue(String pseudoElt, String propertyName);

  /**
   * The language for the current DOM node, e.g. en, de, etc.
   */
  String getLanguage();

}