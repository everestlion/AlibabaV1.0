/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/events/nsIDOMNSUIEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSUIEvent extends nsISupports {

  String NS_IDOMNSUIEVENT_IID =
    "{a6cf90c4-15b3-11d2-932e-00805f8add32}";

  int SCROLL_PAGE_UP = -32768;

  int SCROLL_PAGE_DOWN = 32768;

  boolean getPreventDefault();

  int getLayerX();

  int getLayerY();

  int getPageX();

  int getPageY();

  long getWhich();

  nsIDOMNode getRangeParent();

  int getRangeOffset();

  boolean getCancelBubble();

  void setCancelBubble(boolean aCancelBubble);

  boolean getIsChar();

}