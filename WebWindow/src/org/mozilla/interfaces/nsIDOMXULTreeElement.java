/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULTreeElement.idl
 */

package org.mozilla.interfaces;

/**
 * @status UNDER_DEVELOPMENT
 */
public interface nsIDOMXULTreeElement extends nsIDOMXULElement {

  String NS_IDOMXULTREEELEMENT_IID =
    "{1f8111b2-d44d-4d11-845a-a70ae06b7d04}";

  nsITreeColumns getColumns();

  nsITreeView getView();

  void setView(nsITreeView aView);

  nsIDOMElement getBody();

  boolean getEditable();

  void setEditable(boolean aEditable);

  nsIDOMXULTextBoxElement getInputField();

}