/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/base/public/nsIFrameLoader.idl
 */

package org.mozilla.interfaces;

public interface nsIFrameLoaderOwner extends nsISupports {

  String NS_IFRAMELOADEROWNER_IID =
    "{feaf9285-05ac-4898-a69f-c3bd350767e4}";

  nsIFrameLoader getFrameLoader();

}