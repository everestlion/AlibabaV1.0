/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLAreaElement2.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLAreaElement2 extends nsIDOMNSHTMLAreaElement {

  String NS_IDOMNSHTMLAREAELEMENT2_IID =
    "{1859b16a-7c16-4ab7-bdb9-52792ba16cc1}";

  String getPing();

  void setPing(String aPing);

}