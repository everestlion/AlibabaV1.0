/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegLinetoHorizontalRel extends nsISupports {

  String NS_IDOMSVGPATHSEGLINETOHORIZONTALREL_IID =
    "{8693268c-5180-43fd-acc3-5b9c09f43386}";

  float getX();

  void setX(float aX);

}