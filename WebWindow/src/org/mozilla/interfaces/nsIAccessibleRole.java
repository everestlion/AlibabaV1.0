/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleRole.idl
 */

package org.mozilla.interfaces;

/**
 * Defines cross platform (Gecko) roles.
 *
 * @note - When adding a new role, be sure to also add it to nsRoleMap.h for
 *         each platform.
 */
public interface nsIAccessibleRole extends nsISupports {

  String NS_IACCESSIBLEROLE_IID =
    "{8c0f68f8-164a-4078-a9ee-36a7d180f0e4}";

  /**
   * Used when accessible hans't strong defined role.
   */
  long ROLE_NOTHING = 0L;

  /**
   * Represents a title or caption bar for a window. It is used by MSAA only,
   * supported automatically by MS Windows.
   */
  long ROLE_TITLEBAR = 1L;

  /**
   * Represents the menu bar (positioned beneath the title bar of a window)
   * from which menus are selected by the user. The role is used by
   * xul:menubar or role="menubar".
   */
  long ROLE_MENUBAR = 2L;

  /**
   * Represents a vertical or horizontal scroll bar, which is part of the client
   * area or used in a control.
   */
  long ROLE_SCROLLBAR = 3L;

  /**
   * Represents a special mouse pointer, which allows a user to manipulate user
   * interface elements such as windows. For example, a user clicks and drags
   * a sizing grip in the lower-right corner of a window to resize it.
   */
  long ROLE_GRIP = 4L;

  /**
   * Represents a system sound, which is associated with various system events.
   */
  long ROLE_SOUND = 5L;

  /**
   * Represents the system mouse pointer.
   */
  long ROLE_CURSOR = 6L;

  /**
   * Represents the system caret. The role is supported for caret.
   */
  long ROLE_CARET = 7L;

  /**
   * Represents an alert or a condition that a user should be notified about.
   * Assistive Technologies typically respond to the role by reading the entire
   * onscreen contents of containers advertising this role. Should be used for
   * warning dialogs, etc. The role is used by xul:browsermessage,
   * role="alert", xforms:message.
   */
  long ROLE_ALERT = 8L;

  /**
   * Represents the window frame, which contains child objects such as
   * a title bar, client, and other objects contained in a window. The role
   * is supported automatically by MS Windows.
   */
  long ROLE_WINDOW = 9L;

  /**
   * A sub-document (<frame> or <iframe>)
   */
  long ROLE_INTERNAL_FRAME = 10L;

  /**
   * Represents a menu, which presents a list of options from which the user can
   * make a selection to perform an action. It is used for role="menu".
   */
  long ROLE_MENUPOPUP = 11L;

  /**
   * Represents a menu item, which is an entry in a menu that a user can choose
   * to carry out a command, select an option. It is used for xul:menuitem,
   * role="menuitem".
   */
  long ROLE_MENUITEM = 12L;

  /**
   * Represents a ToolTip that provides helpful hints.
   */
  long ROLE_TOOLTIP = 13L;

  /**
   * Represents a main window for an application. It is used for
   * role="application". Also refer to ROLE_APP_ROOT
   */
  long ROLE_APPLICATION = 14L;

  /**
   * Represents a document window. A document window is always contained within
   * an application window. It is used for role="document".
   */
  long ROLE_DOCUMENT = 15L;

  /**
   * Represents a pane within a frame or document window. Users can navigate
   * between panes and within the contents of the current pane, but cannot
   * navigate between items in different panes. Thus, panes represent a level
   * of grouping lower than frame windows or documents, but above individual
   * controls. It is used for the first child of a <frame> or <iframe>.
   */
  long ROLE_PANE = 16L;

  /**
   * Represents a graphical image used to represent data.
   */
  long ROLE_CHART = 17L;

  /**
   * Represents a dialog box or message box. It is used for xul:dialog, 
   * role="dialog".
   */
  long ROLE_DIALOG = 18L;

  /**
   * Represents a window border.
   */
  long ROLE_BORDER = 19L;

  /**
   * Logically groups other objects. There is not always a parent-child
   * relationship between the grouping object and the objects it contains. It
   * is used for html:textfield, xul:groupbox, role="group".
   */
  long ROLE_GROUPING = 20L;

  /**
   * Used to visually divide a space into two regions, such as a separator menu
   * item or a bar that divides split panes within a window. It is used for
   * xul:separator, html:hr, role="separator".
   */
  long ROLE_SEPARATOR = 21L;

  /**
   * Represents a toolbar, which is a grouping of controls (push buttons or
   * toggle buttons) that provides easy access to frequently used features. It
   * is used for xul:toolbar, role="toolbar".
   */
  long ROLE_TOOLBAR = 22L;

  /**
   * Represents a status bar, which is an area at the bottom of a window that
   * displays information about the current operation, state of the application,
   * or selected object. The status bar has multiple fields, which display
   * different kinds of information. It is used for xul:statusbar.
   */
  long ROLE_STATUSBAR = 23L;

  /**
   * Represents a table that contains rows and columns of cells, and optionally,
   * row headers and column headers. It is used for html:table,
   * role="grid". Also refer to the following roles: ROLE_COLUMNHEADER,
   * ROLE_ROWHEADER, ROLE_COLUMN, ROLE_ROW, ROLE_CELL.
   */
  long ROLE_TABLE = 24L;

  /**
   * Represents a column header, providing a visual label for a column in
   * a table. It is used for XUL tree column headers, html:th,
   * role="colheader". Also refer to ROLE_TABLE.
   */
  long ROLE_COLUMNHEADER = 25L;

  /**
   * Represents a row header, which provides a visual label for a table row.
   * It is used for role="rowheader". Also, see ROLE_TABLE.
   */
  long ROLE_ROWHEADER = 26L;

  /**
   * Represents a column of cells within a table. Also, see ROLE_TABLE.
   */
  long ROLE_COLUMN = 27L;

  /**
   * Represents a row of cells within a table. Also, see ROLE_TABLE.
   */
  long ROLE_ROW = 28L;

  /**
   * Represents a cell within a table. Is is used for html:td,
   * role="gridcell". Also, see ROLE_TABLE.
   */
  long ROLE_CELL = 29L;

  /**
   * Represents a link to something else. This object might look like text or
   * a graphic, but it acts like a button. It is used for
   * xul:label@class="text-link", html:a, html:area,
   * xforms:trigger@appearance="minimal".
   */
  long ROLE_LINK = 30L;

  /**
   * Displays a Help topic in the form of a ToolTip or Help balloon.
   */
  long ROLE_HELPBALLOON = 31L;

  /**
   * Represents a cartoon-like graphic object, such as Microsoft Office
   * Assistant, which is displayed to provide help to users of an application.
   */
  long ROLE_CHARACTER = 32L;

  /**
   * Represents a list box, allowing the user to select one or more items. It
   * is used for xul:listbox, html:select@size, role="list". See also
   * ROLE_LIST_ITEM.
   */
  long ROLE_LIST = 33L;

  /**
   * Represents an item in a list. See also ROLE_LIST.
   */
  long ROLE_LISTITEM = 34L;

  /**
   * Represents an outline or tree structure, such as a tree view control,
   * that displays a hierarchical list and allows the user to expand and
   * collapse branches. Is is used for role="tree".
   */
  long ROLE_OUTLINE = 35L;

  /**
   * Represents an item in an outline or tree structure. It is used for
   * role="treeitem".
   */
  long ROLE_OUTLINEITEM = 36L;

  /**
   * Represents a page tab, it is a child of a page tab list. It is used for
   * xul:tab, role="treeitem". Also refer to ROLE_PAGETABLIST.
   */
  long ROLE_PAGETAB = 37L;

  /**
   * Represents a property sheet. It is used for xul:tabpanel,
   * role="tabpanel".
   */
  long ROLE_PROPERTYPAGE = 38L;

  /**
   * Represents an indicator, such as a pointer graphic, that points to the
   * current item.
   */
  long ROLE_INDICATOR = 39L;

  /**
   * Represents a picture. Is is used for xul:image, html:img.
   */
  long ROLE_GRAPHIC = 40L;

  /**
   * Represents read-only text, such as labels for other controls or
   * instructions in a dialog box. Static text cannot be modified or selected.
   * Is is used for xul:label, xul:description, html:lablel,
   * role="label" or role="description", xforms:output.
   */
  long ROLE_STATICTEXT = 41L;

  /**
   * Represents selectable text that allows edits or is designated read-only.
   */
  long ROLE_TEXT_LEAF = 42L;

  /**
   * Represents a push button control. It is used for xul:button, html:button,
   * role="button", xforms:trigger, xforms:submit.
   */
  long ROLE_PUSHBUTTON = 43L;

  /**
   * Represents a check box control. It is used for xul:checkbox,
   * html:input@type="checkbox", role="checkbox", boolean xforms:input.
   */
  long ROLE_CHECKBUTTON = 44L;

  /**
   * Represents an option button, also called a radio button. It is one of a
   * group of mutually exclusive options. All objects sharing a single parent
   * that have this attribute are assumed to be part of single mutually
   * exclusive group. It is used for xul:radio, html:input@type="radio",
   * role="radio".
   */
  long ROLE_RADIOBUTTON = 45L;

  /**
   * Represents a combo box; an edit control with an associated list box that
   * provides a set of predefined choices. It is used for html:select,
   * xul:menulist, role="combobox".
   */
  long ROLE_COMBOBOX = 46L;

  /**
   * Represents the calendar control. It is used for date xforms:input.
   */
  long ROLE_DROPLIST = 47L;

  /**
   * Represents a progress bar, dynamically showing the user the percent
   * complete of an operation in progress. It is used for xul:progressmeter,
   * role="progressbar".
   */
  long ROLE_PROGRESSBAR = 48L;

  /**
   * Represents a dial or knob whose purpose is to allow a user to set a value.
   */
  long ROLE_DIAL = 49L;

  /**
   * Represents a hot-key field that allows the user to enter a combination or
   * sequence of keystrokes.
   */
  long ROLE_HOTKEYFIELD = 50L;

  /**
   * Represents a slider, which allows the user to adjust a setting in given
   * increments between minimum and maximum values. It is used by xul:scale,
   * role="slider", xforms:range.
   */
  long ROLE_SLIDER = 51L;

  /**
   * Represents a spin box, which is a control that allows the user to increment
   * or decrement the value displayed in a separate "buddy" control associated
   * with the spin box. It is used for xul:spinbuttons.
   */
  long ROLE_SPINBUTTON = 52L;

  /**
   * Represents a graphical image used to diagram data. It is used for svg:svg.
   */
  long ROLE_DIAGRAM = 53L;

  /**
   * Represents an animation control, which contains content that changes over
   * time, such as a control that displays a series of bitmap frames.
   */
  long ROLE_ANIMATION = 54L;

  /**
   * Represents a mathematical equation. It is used by MATHML, where there is a
   * rich DOM subtree for an equation. Use ROLE_FLAT_EQUATION for <img role="math" alt="[TeX]"/>
   */
  long ROLE_EQUATION = 55L;

  /**
   * Represents a button that drops down a list of items.
   */
  long ROLE_BUTTONDROPDOWN = 56L;

  /**
   * Represents a button that drops down a menu.
   */
  long ROLE_BUTTONMENU = 57L;

  /**
   * Represents a button that drops down a grid. It is used for xul:colorpicker.
   */
  long ROLE_BUTTONDROPDOWNGRID = 58L;

  /**
   * Represents blank space between other objects.
   */
  long ROLE_WHITESPACE = 59L;

  /**
   * Represents a container of page tab controls. Is it used for xul:tabs,
   * DHTML: role="tabs". Also refer to ROLE_PAGETAB.
   */
  long ROLE_PAGETABLIST = 60L;

  /**
   * Represents a control that displays time.
   */
  long ROLE_CLOCK = 61L;

  /**
   * Represents a button on a toolbar that has a drop-down list icon directly
   * adjacent to the button.
   */
  long ROLE_SPLITBUTTON = 62L;

  /**
   * Represents an edit control designed for an Internet Protocol (IP) address.
   * The edit control is divided into sections for the different parts of the
   * IP address.
   */
  long ROLE_IPADDRESS = 63L;

  /**
   * Represents a label control that has an accelerator.
   */
  long ROLE_ACCEL_LABEL = 64L;

  /**
   * Represents an arrow in one of the four cardinal directions.
   */
  long ROLE_ARROW = 65L;

  /**
   * Represents a control that can be drawn into and is used to trap events.
   * It is used for html:canvas.
   */
  long ROLE_CANVAS = 66L;

  /**
   * Represents a menu item with a check box.
   */
  long ROLE_CHECK_MENU_ITEM = 67L;

  /**
   * Represents a specialized dialog that lets the user choose a color.
   */
  long ROLE_COLOR_CHOOSER = 68L;

  /**
   * Represents control whose purpose is to allow a user to edit a date.
   */
  long ROLE_DATE_EDITOR = 69L;

  /**
   * An iconified internal frame in an ROLE_DESKTOP_PANE. Also refer to
   * ROLE_INTERNAL_FRAME.
   */
  long ROLE_DESKTOP_ICON = 70L;

  /**
   * A desktop pane. A pane that supports internal frames and iconified
   * versions of those internal frames.
   */
  long ROLE_DESKTOP_FRAME = 71L;

  /**
   * A directory pane. A pane that allows the user to navigate through
   * and select the contents of a directory. May be used by a file chooser.
   * Also refer to ROLE_FILE_CHOOSER.
   */
  long ROLE_DIRECTORY_PANE = 72L;

  /**
   * A file chooser. A specialized dialog that displays the files in the
   * directory and lets the user select a file, browse a different directory,
   * or specify a filename. May use the directory pane to show the contents of
   * a directory. Also refer to ROLE_DIRECTORY_PANE.
   */
  long ROLE_FILE_CHOOSER = 73L;

  /**
   * A font chooser. A font chooser is a component that lets the user pick
   * various attributes for fonts.
   */
  long ROLE_FONT_CHOOSER = 74L;

  /**
   * Frame role. A top level window with a title bar, border, menu bar, etc.
   * It is often used as the primary window for an application.
   */
  long ROLE_CHROME_WINDOW = 75L;

  /**
   *  A glass pane. A pane that is guaranteed to be painted on top of all
   * panes beneath it. Also refer to ROLE_ROOT_PANE.
   */
  long ROLE_GLASS_PANE = 76L;

  /**
   * A document container for HTML, whose children represent the document
   * content.
   */
  long ROLE_HTML_CONTAINER = 77L;

  /**
   * A small fixed size picture, typically used to decorate components.
   */
  long ROLE_ICON = 78L;

  /**
   * Presents an icon or short string in an interface.
   */
  long ROLE_LABEL = 79L;

  /**
   * A layered pane. A specialized pane that allows its children to be drawn
   * in layers, providing a form of stacking order. This is usually the pane
   * that holds the menu bar as  well as the pane that contains most of the
   * visual components in a window. Also refer to ROLE_GLASS_PANE and
   * ROLE_ROOT_PANE.
   */
  long ROLE_LAYERED_PANE = 80L;

  /**
   * A specialized pane whose primary use is inside a dialog.
   */
  long ROLE_OPTION_PANE = 81L;

  /**
   * A text object uses for passwords, or other places where the text content
   * is not shown visibly to the user.
   */
  long ROLE_PASSWORD_TEXT = 82L;

  /**
   * A temporary window that is usually used to offer the user a list of
   * choices, and then hides when the user selects one of those choices.
   */
  long ROLE_POPUP_MENU = 83L;

  /**
   * A radio button that is a menu item.
   */
  long ROLE_RADIO_MENU_ITEM = 84L;

  /**
   * A root pane. A specialized pane that has a glass pane and a layered pane
   * as its children. Also refer to ROLE_GLASS_PANE and ROLE_LAYERED_PANE.
   */
  long ROLE_ROOT_PANE = 85L;

  /**
   * A scroll pane. An object that allows a user to incrementally view a large
   * amount of information.  Its children can include scroll bars and a
   * viewport. Also refer to ROLE_VIEW_PORT.
   */
  long ROLE_SCROLL_PANE = 86L;

  /**
   * A split pane. A specialized panel that presents two other panels at the
   * same time. Between the two panels is a divider the user can manipulate to
   * make one panel larger and the other panel smaller.
   */
  long ROLE_SPLIT_PANE = 87L;

  /**
   * The header for a column of a table.
   * XXX: it looks this role is dupe of ROLE_COLUMNHEADER.
   */
  long ROLE_TABLE_COLUMN_HEADER = 88L;

  /**
   * The header for a row of a table.
   * XXX: it looks this role is dupe of ROLE_ROWHEADER
   */
  long ROLE_TABLE_ROW_HEADER = 89L;

  /**
   * A menu item used to tear off and reattach its menu.
   */
  long ROLE_TEAR_OFF_MENU_ITEM = 90L;

  /**
   * Represents an accessible terminal.
   */
  long ROLE_TERMINAL = 91L;

  /**
   * Collection of objects that constitute a logical text entity.
   */
  long ROLE_TEXT_CONTAINER = 92L;

  /**
   * A toggle button. A specialized push button that can be checked or
   * unchecked, but does not provide a separate indicator for the current state.
   */
  long ROLE_TOGGLE_BUTTON = 93L;

  /**
   * Representas a control that is capable of expanding and collapsing rows as
   * well as showing multiple columns of data.
   * XXX: it looks like this role is dupe of ROLE_OUTLINE.
   */
  long ROLE_TREE_TABLE = 94L;

  /**
   * A viewport. An object usually used in a scroll pane. It represents the
   * portion of the entire data that the user can see. As the user manipulates
   * the scroll bars, the contents of the viewport can change. Also refer to
   * ROLE_SCROLL_PANE.
   */
  long ROLE_VIEWPORT = 95L;

  /**
   * Header of a document page. Also refer to ROLE_FOOTER.
   */
  long ROLE_HEADER = 96L;

  /**
   * Footer of a document page. Also refer to ROLE_HEADER.
   */
  long ROLE_FOOTER = 97L;

  /**
   * A paragraph of text.
   */
  long ROLE_PARAGRAPH = 98L;

  /**
   * A ruler such as those used in word processors.
   */
  long ROLE_RULER = 99L;

  /**
   * A text entry having dialog or list containing items for insertion into
   * an entry widget, for instance a list of words for completion of a
   * text entry. It is used for xul:textbox@autocomplete
   */
  long ROLE_AUTOCOMPLETE = 100L;

  /**
   *  An editable text object in a toolbar.
   */
  long ROLE_EDITBAR = 101L;

  /**
   * An control whose textual content may be entered or modified by the user.
   */
  long ROLE_ENTRY = 102L;

  /**
   * A caption describing another object.
   */
  long ROLE_CAPTION = 103L;

  /**
   * A visual frame or container which contains a view of document content.
   * Document frames may occur within another Document instance, in which case
   * the second document may be said to be embedded in the containing instance.
   * HTML frames are often ROLE_DOCUMENT_FRAME. Either this object, or a
   * singleton descendant, should implement the Document interface.
   */
  long ROLE_DOCUMENT_FRAME = 104L;

  /**
   * Heading.
   */
  long ROLE_HEADING = 105L;

  /**
   * An object representing a page of document content.  It is used in documents
   * which are accessed by the user on a page by page basis.
   */
  long ROLE_PAGE = 106L;

  /**
   * A container of document content.  An example of the use of this role is to
   * represent an html:div.
   */
  long ROLE_SECTION = 107L;

  /**
   * An object which is redundant with another object in the accessible
   * hierarchy. ATs typically ignore objects with this role.
   */
  long ROLE_REDUNDANT_OBJECT = 108L;

  /**
   * A container of form controls. An example of the use of this role is to
   * represent an html:form.
   */
  long ROLE_FORM = 109L;

  /**
   * An object which is used to allow input of characters not found on a
   * keyboard, such as the input of Chinese characters on a Western keyboard.
   */
  long ROLE_IME = 110L;

  /**
   * XXX: document this.
   */
  long ROLE_APP_ROOT = 111L;

  /**
   * Represents a menu item, which is an entry in a menu that a user can choose
   * to display another menu.
   */
  long ROLE_PARENT_MENUITEM = 112L;

  /**
   * A calendar that allows the user to select a date.
   */
  long ROLE_CALENDAR = 113L;

  /**
   * A list of items that is shown by combobox.
   */
  long ROLE_COMBOBOX_LIST = 114L;

  /**
   * A item of list that is shown by combobox;
   */
  long ROLE_COMBOBOX_OPTION = 115L;

  /**
   * An image map -- has child links representing the areas
   */
  long ROLE_IMAGE_MAP = 116L;

  /**
   * An option in a listbox
   */
  long ROLE_OPTION = 117L;

  /**
   * A rich option in a listbox, it can have other widgets as children
   */
  long ROLE_RICH_OPTION = 118L;

  /**
   * A list of options
   */
  long ROLE_LISTBOX = 119L;

  /**
   * Represents a mathematical equation in the accessible name
   */
  long ROLE_FLAT_EQUATION = 120L;

  /**
   * It's not role actually. This contanst is important to help ensure
   * nsRoleMap's are synchronized.
   */
  long ROLE_LAST_ENTRY = 121L;

}