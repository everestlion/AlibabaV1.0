/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleProvider.idl
 */

package org.mozilla.interfaces;

/**
 * nsIAccessibleProvider interface is used to link element and accessible
   object. For that XBL binding of element should implement the interface.
 */
public interface nsIAccessibleProvider extends nsISupports {

  String NS_IACCESSIBLEPROVIDER_IID =
    "{3f7f9194-c625-4a85-8148-6d92d34897fa}";

  /**
   * Constants set of common use.
   */
/** Do not create an accessible for this object
   * This is useful if an ancestor binding already implements nsIAccessibleProvider,
   * but no accessible is desired for the inheriting binding
   */
  int NoAccessible = 0;

  /** For elements that spawn a new document. For example now it is used by
    <xul:iframe>, <xul:browser> and <xul:editor>. */
  int OuterDoc = 1;

  /**
   * Constants set is used by XUL controls.
   */
  int XULAlert = 4097;

  int XULButton = 4098;

  int XULCheckbox = 4099;

  int XULColorPicker = 4100;

  int XULColorPickerTile = 4101;

  int XULCombobox = 4102;

  int XULDropmarker = 4103;

  int XULGroupbox = 4104;

  int XULImage = 4105;

  int XULLink = 4106;

  int XULListbox = 4107;

  int XULListCell = 4134;

  int XULListHead = 4132;

  int XULListHeader = 4133;

  int XULListitem = 4108;

  int XULMenubar = 4109;

  int XULMenuitem = 4110;

  int XULMenupopup = 4111;

  int XULMenuSeparator = 4112;

  int XULPane = 4113;

  int XULProgressMeter = 4114;

  int XULScale = 4115;

  int XULStatusBar = 4116;

  int XULRadioButton = 4117;

  int XULRadioGroup = 4118;

  /** The single tab in a dialog or tabbrowser/editor interface */
  int XULTab = 4119;

  /** A combination of a tabs object and a tabpanels object */
  int XULTabBox = 4120;

  /** The collection of tab objects, useable in the TabBox and independant of
   as well */
  int XULTabs = 4121;

  int XULText = 4122;

  int XULTextBox = 4123;

  int XULThumb = 4124;

  int XULTree = 4125;

  int XULTreeColumns = 4126;

  int XULTreeColumnItem = 4127;

  int XULToolbar = 4128;

  int XULToolbarSeparator = 4129;

  int XULTooltip = 4130;

  int XULToolbarButton = 4131;

  /**
   * Constants set is used by XForms elements.
   */
/** Used for xforms elements that provide accessible object for itself as
   * well for anonymous content. This property are used for upload,
   * input[type="xsd:gDay"] and input[type="xsd:gMonth"] */
  int XFormsContainer = 8192;

  /** Used for label element */
  int XFormsLabel = 8193;

  /** Used for output element */
  int XFormsOuput = 8194;

  /** Used for trigger and submit elements */
  int XFormsTrigger = 8195;

  /** Used for input and textarea elements */
  int XFormsInput = 8196;

  /** Used for input[xsd:boolean] element */
  int XFormsInputBoolean = 8197;

  /** Used for input[xsd:date] element */
  int XFormsInputDate = 8198;

  /** Used for secret element */
  int XFormsSecret = 8199;

  /** Used for range element represented by slider */
  int XFormsSliderRange = 8200;

  /** Used for select and select1 that are implemented using host document's
   * native widget. For example, a select1 in a xhtml document may be
   * represented by the native html control html:select */
  int XFormsSelect = 8201;

  /** Used for xforms choices element */
  int XFormsChoices = 8208;

  /** Used for xforms full select/select1 elements that may be represented by
   * group of checkboxes and radiogroup */
  int XFormsSelectFull = 8209;

  /** Used for xforms item element that is used inside xforms select elements
   * represented by group of checkboxes */
  int XFormsItemCheckgroup = 8210;

  /** Used for xforms item element that is used inside xforms select1 elements
   * represented by radio group */
  int XFormsItemRadiogroup = 8211;

  /** Used for xforms select1 element that is represented by combobox */
  int XFormsSelectCombobox = 8212;

  /** Used for xforms item element that is used inside xforms select1
   * elements represented by combobox */
  int XFormsItemCombobox = 8213;

  /** Used for dropmarker widget that is used by xforms elements */
  int XFormsDropmarkerWidget = 8449;

  /** Used for calendar widget that is used by xforms elements */
  int XFormsCalendarWidget = 8450;

  /** Used for popup widget that is used by xforms minimal select1 elements */
  int XFormsComboboxPopupWidget = 8451;

  /**
   * Return one of constants declared above.
   */
  int getAccessibleType();

}