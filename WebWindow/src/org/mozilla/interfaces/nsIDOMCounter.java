/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCounter.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCounter extends nsISupports {

  String NS_IDOMCOUNTER_IID =
    "{31adb439-0055-402d-9b1d-d5ca94f3f55b}";

  String getIdentifier();

  String getListStyle();

  String getSeparator();

}