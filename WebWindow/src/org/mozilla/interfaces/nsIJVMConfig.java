/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/oji/public/nsIJVMConfigManager.idl
 */

package org.mozilla.interfaces;

/**
 * This interface contains information for the Java installation.
 */
public interface nsIJVMConfig extends nsISupports {

  String NS_IJVMCONFIG_IID =
    "{3e333e20-b190-42d8-b993-d5fa435e46c4}";

  String getVersion();

  String getType();

  String getOS();

  String getArch();

  nsIFile getPath();

  nsIFile getMozillaPluginPath();

  String getDescription();

}