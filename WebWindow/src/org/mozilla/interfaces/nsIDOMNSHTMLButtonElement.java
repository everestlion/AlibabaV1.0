/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLButtonElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLButtonElement extends nsISupports {

  String NS_IDOMNSHTMLBUTTONELEMENT_IID =
    "{c914d7a4-63b3-4d40-943f-91a3c7ab0d4d}";

  void blur();

  void focus();

  void click();

  String getType();

  void setType(String aType);

}