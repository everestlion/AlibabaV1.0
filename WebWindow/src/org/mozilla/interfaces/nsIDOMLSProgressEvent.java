/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/ls/nsIDOMLSProgressEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLSProgressEvent extends nsIDOMEvent {

  String NS_IDOMLSPROGRESSEVENT_IID =
    "{b9a2371f-70e9-4657-b0e8-28e15b40857e}";

  nsIDOMLSInput getInput();

  long getPosition();

  long getTotalSize();

}