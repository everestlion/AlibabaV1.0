/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/pki/public/nsIPKIParamBlock.idl
 */

package org.mozilla.interfaces;

public interface nsIPKIParamBlock extends nsISupports {

  String NS_IPKIPARAMBLOCK_IID =
    "{b6fe3d78-1dd1-11b2-9058-ced9016984c8}";

  void setISupportAtIndex(int index, nsISupports object);

  nsISupports getISupportAtIndex(int index);

}