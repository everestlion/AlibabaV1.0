/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/events/nsIDOMEventGroup.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMEventGroup extends nsISupports {

  String NS_IDOMEVENTGROUP_IID =
    "{33347bee-6620-4841-8152-36091ae80c7e}";

  /**
 * The nsIDOMEventTarget interface is the interface implemented by all
 * event targets in the Document Object Model.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-3-Events/
 */
  boolean isSameEventGroup(nsIDOMEventGroup other);

}