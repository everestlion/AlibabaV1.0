/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGGradientElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGLinearGradientElement extends nsIDOMSVGGradientElement {

  String NS_IDOMSVGLINEARGRADIENTELEMENT_IID =
    "{7e15fce5-b208-43e1-952a-c570ebad0619}";

  nsIDOMSVGAnimatedLength getX1();

  nsIDOMSVGAnimatedLength getY1();

  nsIDOMSVGAnimatedLength getX2();

  nsIDOMSVGAnimatedLength getY2();

}