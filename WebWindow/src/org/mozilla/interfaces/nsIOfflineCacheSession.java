/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/cache/public/nsIOfflineCacheSession.idl
 */

package org.mozilla.interfaces;

/**
 * The offline cache is meant to reliably store resources for
 * offline use.  The expected semantics are:
 *
 * a) Once populated, the cache will not evict an application resource
 *    unless explicitly asked.
 *
 * b) Resources no longer in use by the application should be evicted.
 *
 * c) If the cache fills up, new entries should be rejected rather
 *    than throwing out old ones.
 *
 * The offline cache uses domains to concretely represent an
 * application.  It maintains a list of resources to be pinned for
 * each domain.  This list is separate from actual cache
 * population - the caller is still responsible for placing items
 * in the cache, and ownership can be declared without a
 * corresponding entry.
 *
 * A key can optionally be associated with a specific URI within
 * the domain.
 */
public interface nsIOfflineCacheSession extends nsISupports {

  String NS_IOFFLINECACHESESSION_IID =
    "{3a33e268-4175-4440-a933-89d461c86c5f}";

  /**
     * Gets the list of owner domains in the cache.
     *
     * @param count
     *        The number of domains returned
     * @param uris
     *        The domains that own resources in the cache
     */
  void getOwnerDomains(long[] count, String[][] domains);

  /**
     * Gets the list of owner URIs associated with a domain.
     *
     * @param ownerAsciiDomain
     *        The domain to query
     *        !! IMPORTANT !! : This must be ascii encoded host - nsIURI.asciiHost
     * @param count
     *        The number of uris returned
     * @param uris
     *        The uris in this domain that own resources
     */
  void getOwnerURIs(String ownerAsciiDomain, long[] count, String[][] uris);

  /**
     * Sets the resources owned by a given domain/URI pair.
     *
     * Setting a list will remove any resources previously owned by this
     * domain/URI pair.
     *
     * A key can be added while there is no associated entry.  When
     * an entry is created with this key, it will be owned by the
     * domain/URI pair.
     *
     * @param ownerAsciiDomain
     *        The domain that owns the resources
     *        !! IMPORTANT !! : This must be ascii encoded host - nsIURI.asciiHost
     * @param ownerAsciiKey
     *        The specific key that owns the resources.  You may use 
     *        ascii encoded URI spec of the owner - nsIURI.asciiSpec.
     *        This can be empty if none specifically owns the resources.
     * @param count
     *        The number of keys in keys.
     * @param keys
     *        The keys that the domain/URI pair own.  This can be empty to
     *        clear ownership for the domain/URI pair.
     */
  void setOwnedKeys(String ownerAsciiDomain, String ownerAsciiKey, long count, String[] keys);

  /**
     * Gets the list of resources owned by a given domain/URI pair.
     *
     * @param ownerAsciiDomain
     *        The domain that owns the resources
     *        !! IMPORTANT !! : This must be ascii encoded host - nsIURI.asciiHost
     * @param ownerAsciiKey
     *        The specific key that owns the resources.  You may use 
     *        ascii encoded URI spec of the owner - nsIURI.asciiSpec.
     *        This can be empty if none specifically owns the resources.
     * @param count
     *        The number of keys in keys.
     * @param keys
     *        The keys that the domain/URI pair own.
     */
  void getOwnedKeys(String ownerAsciiDomain, String ownerAsciiKey, long[] count, String[][] keys);

  /**
     * Adds an owned key to a domain/URI pair.
     *
     * A key can be added while there is no associated entry.  When
     * an entry is created with this key, it will be owned by the
     * domain/URI pair.
     *
     * @param ownerAsciiDomain
     *        The domain that owns the resources
     *        !! IMPORTANT !! : This must be ascii encoded host - nsIURI.asciiHost
     * @param ownerAsciiKey
     *        The specific key that owns the resources.  You may use 
     *        ascii encoded URI spec of the owner - nsIURI.asciiSpec.
     *        This can be empty if none specifically owns the resources.
     * @param key
     *        The key to add.
     */
  void addOwnedKey(String ownerAsciiDomain, String ownerAsciiKey, String key);

  /**
     * Removes an owned key from a domain/URI pair.
     *
     * If the key does not exist, an NS_ERROR_NOT_AVAILABLE exception
     * will be thrown.
     *
     * @param ownerAsciiDomain
     *        The domain that owns the resources
     *        !! IMPORTANT !! : This must be ascii encoded host - nsIURI.asciiHost
     * @param ownerAsciiKey
     *        The specific key that owns the resources.  You may use 
     *        ascii encoded URI spec of the owner - nsIURI.asciiSpec.
     *        This can be empty if none specifically owns the resources.
     * @param key The key to remove.
     */
  void removeOwnedKey(String ownerAsciiDomain, String ownerAsciiKey, String key);

  /**
     * Checks whether a key is owned by a given domain/URI pair.
     *
     * @param ownerAsciiDomain
     *        The domain that owns the resources
     *        !! IMPORTANT !! : This must be ascii encoded host - nsIURI.asciiHost
     * @param ownerAsciiKey
     *        The specific key that owns the resources.  You may use 
     *        ascii encoded URI spec of the owner - nsIURI.asciiSpec.
     *        This can be empty if none specifically owns the resources.
     * @param key The key to check
     */
  boolean keyIsOwned(String ownerAsciiDomain, String ownerAsciiKey, String key);

  /**
     * Remove all keys owned by a domain, including keys owned by
     * a specific URI.
     *
     * @param ownerAsciiDomain
     *        The domain for which keys should be removed
     *        !! IMPORTANT !! : This must be ascii encoded host - nsIURI.asciiHost
     */
  void clearKeysOwnedByDomain(String ownerAsciiDomain);

  /**
     * Get the number of bytes used in the cache by a domain.
     *
     * @param domain The domain to check.
     */
  long getDomainUsage(String ownerDomain);

  /**
     * Evict all entries that are not owned by a domain.
     */
  void evictUnownedEntries();

  /**
     * Merge the items from a temporary clientID in to this client.  This lets
     * offline cache updates accumulate in a temporary client and be moved
     * in all at once.
     *
     * Entries in the temporary client will replace any entries in this client
     * with the same cache key.
     *
     * Ownership lists for a given domain/URI pair from the temporary client
     * will replace ownership lists for the same domain/URI pair.
     */
  void mergeTemporaryClientID(String temporaryClientID);

}