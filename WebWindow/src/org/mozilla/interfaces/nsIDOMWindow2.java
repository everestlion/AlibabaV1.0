/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMWindow2.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMWindow2 extends nsIDOMWindow {

  String NS_IDOMWINDOW2_IID =
    "{73c5fa35-3add-4c87-a303-a850ccf4d65a}";

  /**
   * Get the application cache object for this window.
   */
  nsIDOMOfflineResourceList getApplicationCache();

}