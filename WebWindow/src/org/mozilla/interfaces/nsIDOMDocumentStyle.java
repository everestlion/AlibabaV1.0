/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/stylesheets/nsIDOMDocumentStyle.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMDocumentStyle interface is an interface to a document
 * object that supports style sheets in the Document Object Model.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-Style
 *
 * @status FROZEN
 */
public interface nsIDOMDocumentStyle extends nsISupports {

  String NS_IDOMDOCUMENTSTYLE_IID =
    "{3d9f4973-dd2e-48f5-b5f7-2634e09eadd9}";

  nsIDOMStyleSheetList getStyleSheets();

}