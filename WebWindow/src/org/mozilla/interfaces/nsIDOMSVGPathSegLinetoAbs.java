/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegLinetoAbs extends nsISupports {

  String NS_IDOMSVGPATHSEGLINETOABS_IID =
    "{5c7ba7b0-c7c5-4a7b-bc1c-2d784153be77}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

}