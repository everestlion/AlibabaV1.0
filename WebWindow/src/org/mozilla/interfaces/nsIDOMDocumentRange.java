/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/range/nsIDOMDocumentRange.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMDocumentRange interface is an interface to a document
 * object that supports ranges in the Document Object Model.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-Traversal-Range/
 *
 * @status FROZEN
 */
public interface nsIDOMDocumentRange extends nsISupports {

  String NS_IDOMDOCUMENTRANGE_IID =
    "{7b9badc6-c9bc-447a-8670-dbd195aed24b}";

  nsIDOMRange createRange();

}