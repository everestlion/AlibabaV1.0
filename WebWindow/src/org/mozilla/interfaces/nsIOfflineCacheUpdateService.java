/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/uriloader/prefetch/nsIOfflineCacheUpdate.idl
 */

package org.mozilla.interfaces;

public interface nsIOfflineCacheUpdateService extends nsISupports {

  String NS_IOFFLINECACHEUPDATESERVICE_IID =
    "{3abee04b-5bbb-4405-b659-35f780e38da0}";

  /**
     * Constants for the offline-app permission.
     *
     * XXX: This isn't a great place for this, but it's really the only
     * private offline-app-related interface
     */
/**
     * Allow the domain to use offline APIs, and don't warn about excessive
     * usage.
     */
  long ALLOW_NO_WARN = 3L;

  /**
     * Access to the list of cache updates that have been scheduled.
     */
  long getNumUpdates();

  nsIOfflineCacheUpdate getUpdate(long index);

  /**
     * Schedule a cache update for a given offline manifest.  If an
     * existing update is scheduled or running, that update will be returned.
     * Otherwise a new update will be scheduled.
     */
  nsIOfflineCacheUpdate scheduleUpdate(nsIURI aManifestURI, nsIURI aDocumentURI);

  /**
     * Schedule a cache update for a manifest when the document finishes
     * loading.
     */
  void scheduleOnDocumentStop(nsIURI aManifestURI, nsIURI aDocumentURI, nsIDOMDocument aDocument);

}