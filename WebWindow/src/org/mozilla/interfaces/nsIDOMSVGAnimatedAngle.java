/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedAngle.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMSVGAnimatedAngle interface is the interface to an SVG
 * animated angle.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/SVG11/types.html#InterfaceSVGAnimatedAngle
 *
 */
public interface nsIDOMSVGAnimatedAngle extends nsISupports {

  String NS_IDOMSVGANIMATEDANGLE_IID =
    "{c6ab8b9e-32db-464a-ae33-8691d44bc60a}";

  nsIDOMSVGAngle getBaseVal();

  nsIDOMSVGAngle getAnimVal();

}