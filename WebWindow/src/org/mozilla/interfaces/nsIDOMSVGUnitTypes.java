/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGUnitTypes.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMSVGUnitTypes interface is the interface to SVG unit types.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/SVG11/types.html
 *
 */
public interface nsIDOMSVGUnitTypes extends nsISupports {

  String NS_IDOMSVGUNITTYPES_IID =
    "{154b572f-3d0b-49c0-8b5d-8864d05bd3d1}";

  int SVG_UNIT_TYPE_UNKNOWN = 0;

  int SVG_UNIT_TYPE_USERSPACEONUSE = 1;

  int SVG_UNIT_TYPE_OBJECTBOUNDINGBOX = 2;

}