/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCSSImportRule.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCSSImportRule extends nsIDOMCSSRule {

  String NS_IDOMCSSIMPORTRULE_IID =
    "{a6cf90cf-15b3-11d2-932e-00805f8add32}";

  String getHref();

  nsIDOMMediaList getMedia();

  nsIDOMCSSStyleSheet getStyleSheet();

}