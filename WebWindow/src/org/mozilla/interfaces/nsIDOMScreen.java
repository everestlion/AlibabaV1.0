/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMScreen.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMScreen extends nsISupports {

  String NS_IDOMSCREEN_IID =
    "{77947960-b4af-11d2-bd93-00805f8ae3f4}";

  int getTop();

  int getLeft();

  int getWidth();

  int getHeight();

  int getPixelDepth();

  int getColorDepth();

  int getAvailWidth();

  int getAvailHeight();

  int getAvailLeft();

  int getAvailTop();

}