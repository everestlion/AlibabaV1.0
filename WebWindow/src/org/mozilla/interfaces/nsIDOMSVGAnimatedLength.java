/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedLength.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedLength extends nsISupports {

  String NS_IDOMSVGANIMATEDLENGTH_IID =
    "{a52f0322-7f4d-418d-af6d-a7b14abd5cdf}";

  nsIDOMSVGLength getBaseVal();

  nsIDOMSVGLength getAnimVal();

}