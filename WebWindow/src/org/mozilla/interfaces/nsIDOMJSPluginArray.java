/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMPluginArray.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMJSPluginArray extends nsISupports {

  String NS_IDOMJSPLUGINARRAY_IID =
    "{ee753352-1dd1-11b2-b18d-b0b7320a28c3}";

  void refresh();

}