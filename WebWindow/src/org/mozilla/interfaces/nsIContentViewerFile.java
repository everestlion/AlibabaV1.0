/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/docshell/base/nsIContentViewerFile.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDocShellFile    
 */
public interface nsIContentViewerFile extends nsISupports {

  String NS_ICONTENTVIEWERFILE_IID =
    "{6317f32c-9bc7-11d3-bccc-0060b0fc76bd}";

  boolean getPrintable();

}