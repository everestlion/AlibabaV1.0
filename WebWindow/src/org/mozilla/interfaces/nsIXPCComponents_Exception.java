/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpccomponents.idl
 */

package org.mozilla.interfaces;

/**
* interface of Components.Exception
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIXPCComponents_Exception extends nsISupports {

  String NS_IXPCCOMPONENTS_EXCEPTION_IID =
    "{5bf039c0-e028-11d3-8f5d-0010a4e73d9a}";

}