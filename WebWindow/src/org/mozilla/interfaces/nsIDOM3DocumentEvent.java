/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/events/nsIDOM3DocumentEvent.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMDocumentEvent interface is the interface to the event
 * factory method on a DOM document object.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-3-Events/
 */
public interface nsIDOM3DocumentEvent extends nsISupports {

  String NS_IDOM3DOCUMENTEVENT_IID =
    "{090ecc19-b7cb-4f47-ae47-ed68d4926249}";

  nsIDOMEventGroup createEventGroup();

}