/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLModElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLModElement interface is the interface to a [X]HTML
 * ins and del element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLModElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLMODELEMENT_IID =
    "{a6cf90a9-15b3-11d2-932e-00805f8add32}";

  String getCite();

  void setCite(String aCite);

  String getDateTime();

  void setDateTime(String aDateTime);

}