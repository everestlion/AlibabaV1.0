/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPoint.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPoint extends nsISupports {

  String NS_IDOMSVGPOINT_IID =
    "{45f18f8f-1315-4447-a7d5-8aeca77bdcaf}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  nsIDOMSVGPoint matrixTransform(nsIDOMSVGMatrix matrix);

}