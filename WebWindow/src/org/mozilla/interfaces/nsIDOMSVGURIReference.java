/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGURIReference.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGURIReference extends nsISupports {

  String NS_IDOMSVGURIREFERENCE_IID =
    "{8092b5f3-dc8a-459c-94f1-92f8011f2438}";

  nsIDOMSVGAnimatedString getHref();

}