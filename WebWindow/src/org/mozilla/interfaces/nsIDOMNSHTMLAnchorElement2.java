/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLAnchorElement2.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLAnchorElement2 extends nsIDOMNSHTMLAnchorElement {

  String NS_IDOMNSHTMLANCHORELEMENT2_IID =
    "{d7627eda-6ec0-4326-87c4-c3067fe6e324}";

  String getPing();

  void setPing(String aPing);

}