/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAElement extends nsIDOMSVGElement {

  String NS_IDOMSVGAELEMENT_IID =
    "{35d3365a-3e6f-4cdf-983d-fdaed1564478}";

  nsIDOMSVGAnimatedString getTarget();

}