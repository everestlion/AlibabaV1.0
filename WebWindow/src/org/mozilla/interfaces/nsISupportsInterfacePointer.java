/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for other XPCOM objects
 * 
 * @status FROZEN
 */
public interface nsISupportsInterfacePointer extends nsISupportsPrimitive {

  String NS_ISUPPORTSINTERFACEPOINTER_IID =
    "{995ea724-1dd1-11b2-9211-c21bdd3e7ed0}";

  nsISupports getData();

  void setData(nsISupports aData);

  String getDataIID();

  void setDataIID(String aDataIID);

  String toString();

}