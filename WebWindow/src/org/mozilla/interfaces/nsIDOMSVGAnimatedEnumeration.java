/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedEnum.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedEnumeration extends nsISupports {

  String NS_IDOMSVGANIMATEDENUMERATION_IID =
    "{73b101bd-797b-470f-9308-c24c64278bcd}";

  int getBaseVal();

  void setBaseVal(int aBaseVal);

  int getAnimVal();

}