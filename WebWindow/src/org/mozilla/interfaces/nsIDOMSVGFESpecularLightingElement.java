/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFESpecularLightingElement extends nsIDOMSVGFilterPrimitiveStandardAttributes {

  String NS_IDOMSVGFESPECULARLIGHTINGELEMENT_IID =
    "{49c38287-a7c2-4895-a630-86d2b45df23c}";

  nsIDOMSVGAnimatedString getIn1();

  nsIDOMSVGAnimatedNumber getSurfaceScale();

  nsIDOMSVGAnimatedNumber getSpecularConstant();

  nsIDOMSVGAnimatedNumber getSpecularExponent();

  nsIDOMSVGAnimatedNumber getKernelUnitLengthX();

  nsIDOMSVGAnimatedNumber getKernelUnitLengthY();

}