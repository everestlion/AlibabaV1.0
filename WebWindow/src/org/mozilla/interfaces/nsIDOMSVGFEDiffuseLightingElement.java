/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEDiffuseLightingElement extends nsIDOMSVGFilterPrimitiveStandardAttributes {

  String NS_IDOMSVGFEDIFFUSELIGHTINGELEMENT_IID =
    "{2e9eb422-2398-4be9-a9b8-b1cc7aa9dd6f}";

  nsIDOMSVGAnimatedString getIn1();

  nsIDOMSVGAnimatedNumber getSurfaceScale();

  nsIDOMSVGAnimatedNumber getDiffuseConstant();

  nsIDOMSVGAnimatedNumber getKernelUnitLengthX();

  nsIDOMSVGAnimatedNumber getKernelUnitLengthY();

}