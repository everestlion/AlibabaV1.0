/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpccomponents.idl
 */

package org.mozilla.interfaces;

/**
* interface of object returned by Components.utils.Sandbox.
*/
public interface nsIXPCComponents_utils_Sandbox extends nsISupports {

  String NS_IXPCCOMPONENTS_UTILS_SANDBOX_IID =
    "{4f8ae0dc-d266-4a32-875b-6a9de71a8ce9}";

}