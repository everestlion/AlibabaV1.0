/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleHyperLink.idl
 */

package org.mozilla.interfaces;

/**
 * A cross-platform interface that supports hyperlink-specific properties and
 * methods.  Anchors, image maps, xul:labels with class="text-link" implement this interface.
 */
public interface nsIAccessibleHyperLink extends nsISupports {

  String NS_IACCESSIBLEHYPERLINK_IID =
    "{38c60bfa-6040-4bfe-93f2-acd6a909bb60}";

  /**
   * Returns the offset of the link within the parent accessible.
   */
  int getStartIndex();

  /**
   * Returns the end index of the link within the parent accessible.
   *
   * @note  The link itself is represented by one embedded character within the
   * parent text, so the endIndex should be startIndex + 1.
   */
  int getEndIndex();

  /**
   * Determines whether the link is valid (e. g. points to a valid URL).
   *
   * @note  XXX Currently only used with ARIA links, and the author has to
   * specify that the link is invalid via the aria-invalid="true" attribute.
   * In all other cases, TRUE is returned.
   */
  boolean getValid();

  /**
   * Determines whether the element currently has the focus, e. g. after
   * returning from the destination page.
   *
   * @note  ARIA links can only be focused if they have the tabindex
   * attribute set.  Also, state_focused should then be set on the accessible
   * for this link.
   */
  boolean getSelected();

  /**
   * The numbber of anchors within this Hyperlink. Is normally 1 for anchors.
   * This anchor is, for example, the visible output of the html:a tag.
   * With an Image Map, reflects the actual areas within the map.
   */
  int getAnchorCount();

  /**
   * Returns the URI at the given index.
   *
   * @note  ARIA hyperlinks do not have an URI to point to, since clicks are
   * processed via JavaScript. Therefore this property does not work on ARIA
   * links.
   *
   * @param index  The 0-based index of the URI to be returned.
   *
   * @return the nsIURI object containing the specifications for the URI.
   */
  nsIURI getURI(int index);

  /**
   * Returns a reference to the object at the given index.
   *
   * @param index  The 0-based index whose object is to be returned.
   *
   * @return the nsIAccessible object at the desired index.
   */
  nsIAccessible getAnchor(int index);

}