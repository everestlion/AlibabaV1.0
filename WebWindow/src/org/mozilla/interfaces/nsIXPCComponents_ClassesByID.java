/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpccomponents.idl
 */

package org.mozilla.interfaces;

/**
* interface of Components.classesByID
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIXPCComponents_ClassesByID extends nsISupports {

  String NS_IXPCCOMPONENTS_CLASSESBYID_IID =
    "{336a9590-4d19-11d3-9893-006008962422}";

}