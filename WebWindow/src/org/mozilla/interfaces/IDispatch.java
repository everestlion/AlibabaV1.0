/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/XPCIDispatch.idl
 */

package org.mozilla.interfaces;

/**
 * This interface is not to be used directly, it is to be used internally
 * for XPConnect's IDispatch support
 */
public interface IDispatch extends nsISupports {

  String IDISPATCH_IID =
    "{00020400-0000-0000-c000-000000000046}";

}