/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/cookie/public/nsICookie2.idl
 */

package org.mozilla.interfaces;

/** 
 * Main cookie object interface for use by consumers:
 * extends nsICookie, a frozen interface for external
 * access of cookie objects
 */
public interface nsICookie2 extends nsICookie {

  String NS_ICOOKIE2_IID =
    "{736619fe-8d09-4e59-8223-32f176c22977}";

  /**
     * the host (possibly fully qualified) of the cookie,
     * without a leading dot to represent if it is a
     * domain cookie.
     */
  String getRawHost();

  /**
     * true if the cookie is a session cookie.
     * note that expiry time will also be honored
     * for session cookies (see below); thus, whichever is
     * the more restrictive of the two will take effect.
     */
  boolean getIsSession();

  /**
     * the actual expiry time of the cookie, in seconds
     * since midnight (00:00:00), January 1, 1970 UTC.
     *
     * this is distinct from nsICookie::expires, which
     * has different and obsolete semantics.
     */
  long getExpiry();

  /**
     * true if the cookie is an http only cookie
     */
  boolean getIsHttpOnly();

}