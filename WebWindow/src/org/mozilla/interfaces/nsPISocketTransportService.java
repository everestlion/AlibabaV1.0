/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/base/public/nsPISocketTransportService.idl
 */

package org.mozilla.interfaces;

/**
 * This is a private interface used by the internals of the networking library.
 * It will never be frozen.  Do not use it in external code.
 */
public interface nsPISocketTransportService extends nsISocketTransportService {

  String NS_PISOCKETTRANSPORTSERVICE_IID =
    "{6f704e69-a5fb-11d9-8ce8-0011246ecd24}";

  /**
   * init/shutdown routines.
   */
  void init();

  void shutdown();

  /**
   * controls whether or not the socket transport service should poke
   * the autodialer on connection failure.
   */
  boolean getAutodialEnabled();

  /**
   * controls whether or not the socket transport service should poke
   * the autodialer on connection failure.
   */
  void setAutodialEnabled(boolean aAutodialEnabled);

}