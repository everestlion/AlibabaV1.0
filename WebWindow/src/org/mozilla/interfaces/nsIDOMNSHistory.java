/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMNSHistory.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHistory extends nsISupports {

  String NS_IDOMNSHISTORY_IID =
    "{5fb96f46-1dd2-11b2-a5dc-998ca8636ea9}";

  /**
   * go() can be called with an integer argument or no arguments (a
   * nop) from JS
   */
  void go();

}