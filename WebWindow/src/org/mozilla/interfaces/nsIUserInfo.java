/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/startup/public/nsIUserInfo.idl
 */

package org.mozilla.interfaces;

public interface nsIUserInfo extends nsISupports {

  String NS_IUSERINFO_IID =
    "{6c1034f0-1dd2-11b2-aa14-e6657ed7bb0b}";

  String getFullname();

  String getEmailAddress();

  String getUsername();

  String getDomain();

}