/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/xul/base/public/nsIIFrameBoxObject.idl
 */

package org.mozilla.interfaces;

/**
 * @deprecated Please consider using nsIContainerBoxObject.
 */
public interface nsIIFrameBoxObject extends nsIContainerBoxObject {

  String NS_IIFRAMEBOXOBJECT_IID =
    "{30114c44-d398-44a5-9e01-b48b711291cd}";

}