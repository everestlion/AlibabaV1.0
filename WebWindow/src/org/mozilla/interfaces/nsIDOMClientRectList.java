/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMClientRectList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMClientRectList extends nsISupports {

  String NS_IDOMCLIENTRECTLIST_IID =
    "{917da19d-62f5-441d-b47e-9e35f05639c9}";

  long getLength();

  nsIDOMClientRect item(long index);

}