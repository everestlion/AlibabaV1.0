/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/plugin/base/public/nsIPluginHost.idl
 */

package org.mozilla.interfaces;

public interface nsIPluginHost extends nsIFactory {

  String NS_IPLUGINHOST_IID =
    "{2af1c32d-38dd-4f72-b0ab-24697d836e61}";

  nsIPluginTag[] getPluginTags(long[] aPluginCount);

}