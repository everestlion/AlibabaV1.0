/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/xul/base/public/nsIBrowserBoxObject.idl
 */

package org.mozilla.interfaces;

/**
 * @deprecated Please consider using nsIContainerBoxObject.
 */
public interface nsIBrowserBoxObject extends nsIContainerBoxObject {

  String NS_IBROWSERBOXOBJECT_IID =
    "{db436f2f-c656-4754-b0fa-99bc353bd63f}";

}