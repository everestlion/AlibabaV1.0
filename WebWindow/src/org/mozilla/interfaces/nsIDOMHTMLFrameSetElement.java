/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLFrameSetElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLFrameSetElement interface is the interface to a
 * [X]HTML frameset element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLFrameSetElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLFRAMESETELEMENT_IID =
    "{a6cf90b8-15b3-11d2-932e-00805f8add32}";

  String getCols();

  void setCols(String aCols);

  String getRows();

  void setRows(String aRows);

}