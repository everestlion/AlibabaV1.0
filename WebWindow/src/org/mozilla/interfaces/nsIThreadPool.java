/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/threads/nsIThreadPool.idl
 */

package org.mozilla.interfaces;

/**
 * An interface to a thread pool.  A thread pool creates a limited number of
 * anonymous (unnamed) worker threads.  An event dispatched to the thread pool
 * will be run on the next available worker thread.
 */
public interface nsIThreadPool extends nsIEventTarget {

  String NS_ITHREADPOOL_IID =
    "{394c29f0-225f-487f-86d3-4c259da76cab}";

  /**
   * Shutdown the thread pool.  This method may not be executed from any thread
   * in the thread pool.  Instead, it is meant to be executed from another
   * thread (usually the thread that created this thread pool).  When this
   * function returns, the thread pool and all of its threads will be shutdown,
   * and it will no longer be possible to dispatch tasks to the thread pool.
   */
  void shutdown();

  /**
   * Get/set the maximum number of threads allowed at one time in this pool.
   */
  long getThreadLimit();

  /**
   * Get/set the maximum number of threads allowed at one time in this pool.
   */
  void setThreadLimit(long aThreadLimit);

  /**
   * Get/set the maximum number of idle threads kept alive.
   */
  long getIdleThreadLimit();

  /**
   * Get/set the maximum number of idle threads kept alive.
   */
  void setIdleThreadLimit(long aIdleThreadLimit);

  /**
   * Get/set the amount of time in milliseconds before an idle thread is
   * destroyed.
   */
  long getIdleThreadTimeout();

  /**
   * Get/set the amount of time in milliseconds before an idle thread is
   * destroyed.
   */
  void setIdleThreadTimeout(long aIdleThreadTimeout);

}