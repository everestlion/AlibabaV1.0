/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedPoints.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedPoints extends nsISupports {

  String NS_IDOMSVGANIMATEDPOINTS_IID =
    "{ebf334b3-86ef-4bf3-8a92-d775c72defa4}";

  nsIDOMSVGPointList getPoints();

  nsIDOMSVGPointList getAnimatedPoints();

}