/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLFormElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLFormElement extends nsISupports {

  String NS_IDOMNSHTMLFORMELEMENT_IID =
    "{a6cf90c6-15b3-11d2-932e-00805f8add32}";

  String getEncoding();

  void setEncoding(String aEncoding);

}