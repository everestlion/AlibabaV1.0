/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/events/nsIDOMCustomEvent.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMEventTarget interface is the interface implemented by all
 * event targets in the Document Object Model.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-3-Events/
 */
public interface nsIDOMCustomEvent extends nsIDOMEvent {

  String NS_IDOMCUSTOMEVENT_IID =
    "{55c7af7b-1a64-40bf-87eb-2c2cbee0491b}";

  void setCurrentTarget(nsIDOMNode target);

  void setEventPhase(int phase);

}