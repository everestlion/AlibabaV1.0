/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/storage/public/mozIStorageStatementWrapper.idl
 */

package org.mozilla.interfaces;

public interface mozIStorageStatementParams extends nsISupports {

  String MOZISTORAGESTATEMENTPARAMS_IID =
    "{e65fe6e2-2643-463c-97e2-27665efe2386}";

}