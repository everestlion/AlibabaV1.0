/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMRect.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMRect extends nsISupports {

  String NS_IDOMRECT_IID =
    "{71735f62-ac5c-4236-9a1f-5ffb280d531c}";

  nsIDOMCSSPrimitiveValue getTop();

  nsIDOMCSSPrimitiveValue getRight();

  nsIDOMCSSPrimitiveValue getBottom();

  nsIDOMCSSPrimitiveValue getLeft();

}