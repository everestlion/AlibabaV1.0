/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/extensions/spellcheck/idl/mozISpellI18NManager.idl
 */

package org.mozilla.interfaces;

/**
 * This interface contains various I18N related code used in various places
 * by the spell checker.
 */
public interface mozISpellI18NManager extends nsISupports {

  String MOZISPELLI18NMANAGER_IID =
    "{aeb8936f-219c-4d3c-8385-d9382daa551a}";

  /**
   * Get a mozISpellI18NUtil interface that best matches the given language.
   */
  mozISpellI18NUtil getUtil(String language);

}