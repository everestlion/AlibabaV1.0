/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/traversal/nsIDOMDocumentTraversal.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMDocumentTraversal extends nsISupports {

  String NS_IDOMDOCUMENTTRAVERSAL_IID =
    "{13f236c0-47f8-11d5-b6a3-009027446e84}";

  nsIDOMNodeIterator createNodeIterator(nsIDOMNode root, long whatToShow, nsIDOMNodeFilter filter, boolean entityReferenceExpansion);

  nsIDOMTreeWalker createTreeWalker(nsIDOMNode root, long whatToShow, nsIDOMNodeFilter filter, boolean entityReferenceExpansion);

}