/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEFuncBElement extends nsIDOMSVGComponentTransferFunctionElement {

  String NS_IDOMSVGFEFUNCBELEMENT_IID =
    "{8b139fe7-5d21-4af3-beda-414aa089b3fb}";

}