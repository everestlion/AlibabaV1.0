/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/storage/public/mozIStorageDataSet.idl
 */

package org.mozilla.interfaces;

public interface mozIStorageDataSet extends nsISupports {

  String MOZISTORAGEDATASET_IID =
    "{57826606-3c8a-4243-9f2f-cb3fe6e91148}";

  /**
   * All the rows in this data set, as a nsIArray
   */
  nsIArray getDataRows();

  /**
   * Get an enumerator for the result set rows.
   * @returns a nsISimpleEnumerator of mozIStorageValueArray.
   */
  nsISimpleEnumerator getRowEnumerator();

}