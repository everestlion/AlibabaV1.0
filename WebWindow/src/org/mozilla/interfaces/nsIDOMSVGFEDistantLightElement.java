/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEDistantLightElement extends nsIDOMSVGElement {

  String NS_IDOMSVGFEDISTANTLIGHTELEMENT_IID =
    "{02141672-7f2c-412a-a7d7-4caa194842e9}";

  nsIDOMSVGAnimatedNumber getAzimuth();

  nsIDOMSVGAnimatedNumber getElevation();

}