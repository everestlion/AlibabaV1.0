/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGDescElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGDescElement extends nsIDOMSVGElement {

  String NS_IDOMSVGDESCELEMENT_IID =
    "{56f539b7-0b3d-4bac-b60d-9efe220216ea}";

}