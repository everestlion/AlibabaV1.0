/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegCurvetoQuadraticSmoothRel extends nsISupports {

  String NS_IDOMSVGPATHSEGCURVETOQUADRATICSMOOTHREL_IID =
    "{ac0b2007-04e4-4e70-a0e0-294f374b29c4}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

}