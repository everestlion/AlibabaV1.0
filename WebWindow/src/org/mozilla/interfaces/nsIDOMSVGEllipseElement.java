/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGEllipseElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGEllipseElement extends nsIDOMSVGElement {

  String NS_IDOMSVGELLIPSEELEMENT_IID =
    "{5d1cd1e6-4a14-4056-acc0-2f78c1672898}";

  nsIDOMSVGAnimatedLength getCx();

  nsIDOMSVGAnimatedLength getCy();

  nsIDOMSVGAnimatedLength getRx();

  nsIDOMSVGAnimatedLength getRy();

}