/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/range/nsIDOMNSRange.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSRange extends nsISupports {

  String NS_IDOMNSRANGE_IID =
    "{59188642-23b4-41d6-bde1-302c3906d1f0}";

  nsIDOMDocumentFragment createContextualFragment(String fragment);

  boolean isPointInRange(nsIDOMNode parent, int offset);

  short comparePoint(nsIDOMNode parent, int offset);

}