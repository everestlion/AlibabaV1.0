/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsICipherInfo.idl
 */

package org.mozilla.interfaces;

public interface nsICipherInfoService extends nsISupports {

  String NS_ICIPHERINFOSERVICE_IID =
    "{766d47cb-6d8c-4e71-b6b7-336917629a69}";

  nsICipherInfo getCipherInfoByPrefString(String aPrefString);

}