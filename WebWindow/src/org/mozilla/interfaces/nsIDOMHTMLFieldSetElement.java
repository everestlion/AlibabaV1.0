/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLFieldSetElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLFieldSetElement interface is the interface to a
 * [X]HTML fieldset element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLFieldSetElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLFIELDSETELEMENT_IID =
    "{a6cf9097-15b3-11d2-932e-00805f8add32}";

  nsIDOMHTMLFormElement getForm();

}