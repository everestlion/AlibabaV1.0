/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/traversal/nsIDOMNodeIterator.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNodeIterator extends nsISupports {

  String NS_IDOMNODEITERATOR_IID =
    "{354b5f02-1dd2-11b2-b053-b8c2997022a0}";

  nsIDOMNode getRoot();

  long getWhatToShow();

  nsIDOMNodeFilter getFilter();

  boolean getExpandEntityReferences();

  nsIDOMNode nextNode();

  nsIDOMNode previousNode();

  void detach();

}