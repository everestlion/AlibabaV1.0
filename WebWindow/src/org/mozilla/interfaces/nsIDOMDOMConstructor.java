/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMConstructor.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMDOMConstructor extends nsISupports {

  String NS_IDOMDOMCONSTRUCTOR_IID =
    "{0ccbcf19-d1b4-489e-984c-cd8c43672bb9}";

  String toString();

}