/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpccomponents.idl
 */

package org.mozilla.interfaces;

/**
* interface of Components.results
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIXPCComponents_Results extends nsISupports {

  String NS_IXPCCOMPONENTS_RESULTS_IID =
    "{2fc229a0-5860-11d3-9899-006008962422}";

}