/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleStateChangeEvent extends nsIAccessibleEvent {

  String NS_IACCESSIBLESTATECHANGEEVENT_IID =
    "{444db51a-05fd-4576-8a64-32dbb2a83884}";

  /**
   * Returns the state of accessible (see constants declared
   * in nsIAccessibleStates).
   */
  long getState();

  /**
   * Returns true if the state is extra state.
   */
  boolean isExtraState();

  /**
   * Returns true if the state is turned on.
   */
  boolean isEnabled();

}