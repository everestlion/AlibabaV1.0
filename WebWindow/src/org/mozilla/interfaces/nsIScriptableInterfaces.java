/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/nsIScriptableInterfaces.idl
 */

package org.mozilla.interfaces;

/**
* Interface used as Components.interfaces (and elsewhere)
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIScriptableInterfaces extends nsISupports {

  String NS_ISCRIPTABLEINTERFACES_IID =
    "{01c78c65-31da-456b-90bf-da39d09fdcbf}";

}