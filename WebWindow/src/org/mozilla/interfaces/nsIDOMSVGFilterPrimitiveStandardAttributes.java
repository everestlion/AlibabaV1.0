/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFilterPrimitiveStandardAttributes extends nsIDOMSVGElement {

  String NS_IDOMSVGFILTERPRIMITIVESTANDARDATTRIBUTES_IID =
    "{ab68567a-b830-4c46-9f2f-a28513a9e980}";

  nsIDOMSVGAnimatedLength getX();

  nsIDOMSVGAnimatedLength getY();

  nsIDOMSVGAnimatedLength getWidth();

  nsIDOMSVGAnimatedLength getHeight();

  nsIDOMSVGAnimatedString getResult();

}