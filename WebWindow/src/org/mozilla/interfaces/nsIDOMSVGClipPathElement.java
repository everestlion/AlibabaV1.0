/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGClipPathElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGClipPathElement extends nsIDOMSVGElement {

  String NS_IDOMSVGCLIPPATHELEMENT_IID =
    "{0c3f45a4-e6d0-44e7-a2f8-d128ecf1db9b}";

  nsIDOMSVGAnimatedEnumeration getClipPathUnits();

}