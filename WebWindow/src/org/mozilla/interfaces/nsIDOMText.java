/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMText.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMText extends nsIDOMCharacterData {

  String NS_IDOMTEXT_IID =
    "{a6cf9082-15b3-11d2-932e-00805f8add32}";

  /**
 * The nsIDOMText interface inherits from nsIDOMCharacterData and represents 
 * the textual content (termed character data in XML) of an Element or Attr.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */
  nsIDOMText splitText(long offset);

}