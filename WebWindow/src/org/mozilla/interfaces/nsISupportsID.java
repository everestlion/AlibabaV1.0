/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for nsID structures
 *
 * @status FROZEN
 */
public interface nsISupportsID extends nsISupportsPrimitive {

  String NS_ISUPPORTSID_IID =
    "{d18290a0-4a1c-11d3-9890-006008962422}";

  String getData();

  void setData(String aData);

  String toString();

}