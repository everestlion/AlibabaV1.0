/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for unsigned 32-bit integers
 * 
 * @status FROZEN
 */
public interface nsISupportsPRUint32 extends nsISupportsPrimitive {

  String NS_ISUPPORTSPRUINT32_IID =
    "{e01dc470-4a1c-11d3-9890-006008962422}";

  long getData();

  void setData(long aData);

  String toString();

}