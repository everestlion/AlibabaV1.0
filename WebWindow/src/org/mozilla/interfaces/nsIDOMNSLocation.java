/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMNSLocation.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSLocation extends nsISupports {

  String NS_IDOMNSLOCATION_IID =
    "{a6cf9108-15b3-11d2-932e-00805f8add32}";

  void reload();

}