/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/oji/public/nsIJVMManager.idl
 */

package org.mozilla.interfaces;

public interface nsIJVMManager extends nsISupports {

  String NS_IJVMMANAGER_IID =
    "{a1e5ed50-aa4a-11d1-85b2-00805f0e4dfe}";

  /**
	 * Called from Java Console menu item.
	 */
  void showJavaConsole();

  /**
	 * isAllPermissionGranted [Deprecated Sep-10-2000]
	 */
  boolean isAllPermissionGranted(String lastFingerprint, String lastCommonName, String rootFingerprint, String rootCommonName);

  /**
	 * isAppletTrusted
	 */
  nsIPrincipal isAppletTrusted(String aRSABuf, long aRSABufLen, String aPlaintext, long aPlaintextLen, boolean[] isTrusted);

  /**
     * The JavaEnabled variable to see if Java is Enabled or not
     */
  boolean getJavaEnabled();

}