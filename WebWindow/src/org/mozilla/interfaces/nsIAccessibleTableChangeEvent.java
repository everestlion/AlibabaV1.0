/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleTableChangeEvent extends nsIAccessibleEvent {

  String NS_IACCESSIBLETABLECHANGEEVENT_IID =
    "{a9485c7b-5861-4695-8441-fab0235b205d}";

  /**
   * Return the row or column index.
   */
  int getRowOrColIndex();

  /**
   * Return the number of rows or cols
   */
  int getNumRowsOrCols();

}