/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/libpr0n/public/imgIContainerObserver.idl
 */

package org.mozilla.interfaces;

/**
 * imgIContainerObserver interface
 *
 * @author Stuart Parmenter <pavlov@netscape.com>
 * @version 0.1
 */
public interface imgIContainerObserver extends nsISupports {

  String IMGICONTAINEROBSERVER_IID =
    "{53102f15-0f53-4939-957e-aea353ad2700}";

}