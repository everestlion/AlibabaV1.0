/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedInteger.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMSVGAnimatedNumber interface is the interface to an SVG
 * Animated Integer.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/SVG11/types.html
 *
 */
public interface nsIDOMSVGAnimatedInteger extends nsISupports {

  String NS_IDOMSVGANIMATEDINTEGER_IID =
    "{7b196db6-955e-4a9f-8f42-645ebc2ce938}";

  int getBaseVal();

  void setBaseVal(int aBaseVal);

  int getAnimVal();

}