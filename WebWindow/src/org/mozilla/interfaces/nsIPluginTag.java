/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/plugin/base/public/nsIPluginTag.idl
 */

package org.mozilla.interfaces;

public interface nsIPluginTag extends nsISupports {

  String NS_IPLUGINTAG_IID =
    "{af36bf4d-5652-413f-a78c-745b702f2381}";

  String getDescription();

  String getFilename();

  String getName();

  boolean getDisabled();

  void setDisabled(boolean aDisabled);

  boolean getBlocklisted();

  void setBlocklisted(boolean aBlocklisted);

}