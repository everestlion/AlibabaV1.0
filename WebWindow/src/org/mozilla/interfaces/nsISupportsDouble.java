/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for doubles
 * 
 * @status FROZEN
 */
public interface nsISupportsDouble extends nsISupportsPrimitive {

  String NS_ISUPPORTSDOUBLE_IID =
    "{b32523a0-4ac0-11d3-baea-00805f8a5dd7}";

  double getData();

  void setData(double aData);

  String toString();

}