/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCSSFontFaceRule.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCSSFontFaceRule extends nsIDOMCSSRule {

  String NS_IDOMCSSFONTFACERULE_IID =
    "{a6cf90bb-15b3-11d2-932e-00805f8add32}";

  nsIDOMCSSStyleDeclaration getStyle();

}