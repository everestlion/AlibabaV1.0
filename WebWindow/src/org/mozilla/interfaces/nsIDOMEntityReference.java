/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMEntityReference.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMEntityReference extends nsIDOMNode {

  String NS_IDOMENTITYREFERENCE_IID =
    "{a6cf907a-15b3-11d2-932e-00805f8add32}";

}