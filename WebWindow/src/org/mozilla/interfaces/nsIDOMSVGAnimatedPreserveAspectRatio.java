/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimPresAspRatio.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedPreserveAspectRatio extends nsISupports {

  String NS_IDOMSVGANIMATEDPRESERVEASPECTRATIO_IID =
    "{afcd7cd4-d74d-492f-b3b1-d71bfa36874f}";

  nsIDOMSVGPreserveAspectRatio getBaseVal();

  nsIDOMSVGPreserveAspectRatio getAnimVal();

}