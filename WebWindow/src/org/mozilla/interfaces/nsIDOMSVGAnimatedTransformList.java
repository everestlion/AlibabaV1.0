/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimTransformList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedTransformList extends nsISupports {

  String NS_IDOMSVGANIMATEDTRANSFORMLIST_IID =
    "{fd54c8c4-2eb4-4849-8df6-79985c2491da}";

  nsIDOMSVGTransformList getBaseVal();

  nsIDOMSVGTransformList getAnimVal();

}