/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGDocument.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGDocument extends nsIDOMDocument {

  String NS_IDOMSVGDOCUMENT_IID =
    "{12d3b664-1dd2-11b2-a7cf-ceee7e90f396}";

  String getTitle();

  String getReferrer();

  String getDomain();

  String getURL();

  nsIDOMSVGSVGElement getRootElement();

}