/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCSSUnknownRule.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCSSUnknownRule extends nsIDOMCSSRule {

  String NS_IDOMCSSUNKNOWNRULE_IID =
    "{a6cf90d0-15b3-11d2-932e-00805f8add32}";

}