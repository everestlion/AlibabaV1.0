/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleTypes.idl
 */

package org.mozilla.interfaces;

/**
 * These constants define which coordinate system a point is located in.
 */
public interface nsIAccessibleCoordinateType extends nsISupports {

  String NS_IACCESSIBLECOORDINATETYPE_IID =
    "{c9fbdf10-619e-436f-bf4b-8566686f1577}";

  /**
   * The coordinates are relative to the screen.
   */
  long COORDTYPE_SCREEN_RELATIVE = 0L;

  /**
   * The coordinates are relative to the window.
   */
  long COORDTYPE_WINDOW_RELATIVE = 1L;

  /**
   * The coordinates are relative to the upper left corner of the bounding box
   * of the immediate parent.
   */
  long COORDTYPE_PARENT_RELATIVE = 2L;

}