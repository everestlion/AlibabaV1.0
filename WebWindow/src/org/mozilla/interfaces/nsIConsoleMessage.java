/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/base/nsIConsoleMessage.idl
 */

package org.mozilla.interfaces;

/**
 * This is intended as a base interface; implementations may want to
 * provide an object that can be qi'ed to provide more specific
 * message information.
 */
public interface nsIConsoleMessage extends nsISupports {

  String NS_ICONSOLEMESSAGE_IID =
    "{41bd8784-1dd2-11b2-9553-8606958fffe1}";

  String getMessage();

}