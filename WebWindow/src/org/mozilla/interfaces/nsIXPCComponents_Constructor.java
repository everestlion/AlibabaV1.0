/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpccomponents.idl
 */

package org.mozilla.interfaces;

/**
* interface of Components.Constructor
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIXPCComponents_Constructor extends nsISupports {

  String NS_IXPCCOMPONENTS_CONSTRUCTOR_IID =
    "{88655640-e028-11d3-8f5d-0010a4e73d9a}";

}