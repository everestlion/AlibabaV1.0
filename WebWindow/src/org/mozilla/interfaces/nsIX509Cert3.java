/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsIX509Cert3.idl
 */

package org.mozilla.interfaces;

/**
 * Extending nsIX509Cert
 */
public interface nsIX509Cert3 extends nsIX509Cert2 {

  String NS_IX509CERT3_IID =
    "{aa67eb02-ccc8-4f55-84da-bcafff9265ae}";

  /**
   *  Constants for specifying the chain mode when exporting a certificate
   */
  long CMS_CHAIN_MODE_CertOnly = 1L;

  long CMS_CHAIN_MODE_CertChain = 2L;

  long CMS_CHAIN_MODE_CertChainWithRoot = 3L;

  /**
   *  Async version of nsIX509Cert::getUsagesArray()
   *
   *  Will not block, will request results asynchronously,
   *  availability of results will be notified.
   */
  void requestUsagesArrayAsync(nsICertVerificationListener cvl);

  /**
   *  Obtain the certificate wrapped in a PKCS#7 SignedData structure,
   *  with or without the certificate chain
   *
   *  @param chainMode Whether to include the chain (with or without the root),
                       see CMS_CHAIN_MODE constants.
   *  @param length The number of bytes of the PKCS#7 data.
   *  @param data The bytes representing the PKCS#7 wrapped certificate.
   */
  byte[] exportAsCMS(long chainMode, long[] length);

  boolean getIsSelfSigned();

}