/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/system/nsICrashReporter.idl
 */

package org.mozilla.interfaces;

/**
 * Provides access to crash reporting functionality.
 * @status UNSTABLE - This interface is not frozen and will probably change in
 *                    future releases.
 */
public interface nsICrashReporter extends nsISupports {

  String NS_ICRASHREPORTER_IID =
    "{4d6449fe-d642-45e5-9773-41af5793c99a}";

  /**
   * Add some extra data to be submitted with a crash report.
   * @param key
   *        Name of the data to be added.
   * @param data
   *        Data to be added.
   *
   * @throw NS_ERROR_NOT_INITIALIZED if crash reporting not initialized
   * @throw NS_ERROR_INVALID_ARG if key or data contain invalid characters.
   *                             Invalid characters for key are '=' and
   *                             '\n'.  Invalid character for data is '\0'.
   */
  void annotateCrashReport(String key, String data);

}