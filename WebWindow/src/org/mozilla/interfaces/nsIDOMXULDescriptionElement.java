/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULDescriptionElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULDescriptionElement extends nsIDOMXULElement {

  String NS_IDOMXULDESCRIPTIONELEMENT_IID =
    "{c7b0b43c-1dd1-11b2-9e1c-ce5f6a660630}";

  boolean getDisabled();

  void setDisabled(boolean aDisabled);

  boolean getCrop();

  void setCrop(boolean aCrop);

  String getValue();

  void setValue(String aValue);

}