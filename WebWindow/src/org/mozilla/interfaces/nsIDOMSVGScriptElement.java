/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGScriptElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGScriptElement extends nsIDOMSVGElement {

  String NS_IDOMSVGSCRIPTELEMENT_IID =
    "{bbe0d0ee-e9ed-4f84-a6e4-e58f66530caa}";

  String getType();

  void setType(String aType);

}