/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLFrameElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLFrameElement extends nsISupports {

  String NS_IDOMNSHTMLFRAMEELEMENT_IID =
    "{d67bd267-f984-4993-b378-95851b71f0a3}";

  nsIDOMWindow getContentWindow();

}