/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/canvas/nsIDOMCanvasRenderingContext2D.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCanvasGradient extends nsISupports {

  String NS_IDOMCANVASGRADIENT_IID =
    "{bbb20a59-524e-4662-981e-5e142814b20c}";

  void addColorStop(float offset, String color);

}