/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegCurvetoCubicRel extends nsISupports {

  String NS_IDOMSVGPATHSEGCURVETOCUBICREL_IID =
    "{0e661233-0c4f-4e0d-94d3-fbc460ad1f88}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  float getX1();

  void setX1(float aX1);

  float getY1();

  void setY1(float aY1);

  float getX2();

  void setX2(float aX2);

  float getY2();

  void setY2(float aY2);

}