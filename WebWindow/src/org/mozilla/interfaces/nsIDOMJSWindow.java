/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMJSWindow.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMJSWindow extends nsISupports {

  String NS_IDOMJSWINDOW_IID =
    "{c8188620-1dd1-11b2-bc88-df8440498add}";

  void dump(String str);

  /**
   * These methods take typeless arguments and optional arguments, the
   * first argument is either a function or a string, the second
   * argument must be a number (ms) and the rest of the arguments (2
   * ... n) are passed to the callback function
   */
  int setTimeout();

  int setInterval();

  /**
   * These methods take one optional argument that's the timer ID to
   * clear. Often in existing code these methods are passed undefined,
   * which is a nop so we need to support that as well.
   */
  void clearTimeout();

  void clearInterval();

  /**
   * This method is here for backwards compatibility with 4.x only,
   * its implementation is a no-op
   */
  void setResizable(boolean resizable);

  /**
   * @deprecated These are old Netscape 4 methods. Do not use,
   *             the implementation is no-op.
   */
  void captureEvents(int eventFlags);

  void releaseEvents(int eventFlags);

  void routeEvent(nsIDOMEvent evt);

  void enableExternalCapture();

  void disableExternalCapture();

  /**
   * The prompt method takes up to four arguments, the arguments are
   * message, initial prompt value, title and a save password flag
   */
  String prompt();

  /**
   * These are the scriptable versions of nsIDOMWindowInternal::open() and
   * nsIDOMWindowInternal::openDialog() that take 3 optional arguments.  Unlike
   * the nsIDOMWindowInternal methods, these methods assume that they are
   * called from JavaScript and hence will look on the JS context stack to
   * determine the caller and hence correct security context for doing their
   * search for an existing named window.  Also, these methods will set the
   * default charset on the newly opened window based on the current document
   * charset in the caller.
   */
  nsIDOMWindow open();

  nsIDOMWindow openDialog();

  /**
   * window.frames in Netscape 4.x and IE is just a reference to the
   * window itself (i.e. window.frames === window), but this doesn't
   * make sense from a generic API point of view so that's why this is
   * JS specific.
   *
   * This property is "replaceable" in JavaScript.
   */
  nsIDOMWindow getFrames();

  boolean find();

}