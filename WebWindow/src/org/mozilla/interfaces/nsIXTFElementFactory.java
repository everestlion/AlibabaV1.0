/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/xtf/public/nsIXTFElementFactory.idl
 */

package org.mozilla.interfaces;

public interface nsIXTFElementFactory extends nsISupports {

  String NS_IXTFELEMENTFACTORY_IID =
    "{27c10dca-2efc-416b-ae36-9794380a661e}";

  nsIXTFElement createElement(String tagName);

}