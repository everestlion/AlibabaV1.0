/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleTypes.idl
 */

package org.mozilla.interfaces;

/**
 * These constants control the scrolling of an object or substring into a
 * window. Note, keep them synchronized with IA2ScrollType.
 */
public interface nsIAccessibleScrollType extends nsISupports {

  String NS_IACCESSIBLESCROLLTYPE_IID =
    "{05cd38b1-94b3-4cdf-8371-3935a9611405}";

  /**
   * Scroll the top left of the object or substring to the top left of the
   * window (or as close as possible).
   */
  long SCROLL_TYPE_TOP_LEFT = 0L;

  /**
   * Scroll the bottom right of the object or substring to the bottom right of
   * the window (or as close as possible).
   */
  long SCROLL_TYPE_BOTTOM_RIGHT = 1L;

  /**
   * Scroll the top edge of the object or substring to the top edge of the
   * window (or as close as possible).
   */
  long SCROLL_TYPE_TOP_EDGE = 2L;

  /**
   * Scroll the bottom edge of the object or substring to the bottom edge of
   * the window (or as close as possible).
   */
  long SCROLL_TYPE_BOTTOM_EDGE = 3L;

  /**
   * Scroll the left edge of the object or substring to the left edge of the
   * window (or as close as possible).
   */
  long SCROLL_TYPE_LEFT_EDGE = 4L;

  /**
   * Scroll the right edge of the object or substring to the right edge of the
   * window (or as close as possible).
   */
  long SCROLL_TYPE_RIGHT_EDGE = 5L;

  /**
   * Scroll an object the minimum amount necessary in order for the entire
   * frame to be visible (if possible).
   */
  long SCROLL_TYPE_ANYWHERE = 6L;

}