/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for floating point numbers
 * 
 * @status FROZEN
 */
public interface nsISupportsFloat extends nsISupportsPrimitive {

  String NS_ISUPPORTSFLOAT_IID =
    "{abeaa390-4ac0-11d3-baea-00805f8a5dd7}";

  float getData();

  void setData(float aData);

  String toString();

}