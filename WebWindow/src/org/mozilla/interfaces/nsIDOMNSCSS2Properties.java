/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCSS2Properties.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSCSS2Properties extends nsIDOMCSS2Properties {

  String NS_IDOMNSCSS2PROPERTIES_IID =
    "{c9339b8c-9bdd-4d2a-a61a-55ca609b92bd}";

  String getMozAppearance();

  void setMozAppearance(String aMozAppearance);

  String getMozBackgroundClip();

  void setMozBackgroundClip(String aMozBackgroundClip);

  String getMozBackgroundInlinePolicy();

  void setMozBackgroundInlinePolicy(String aMozBackgroundInlinePolicy);

  String getMozBackgroundOrigin();

  void setMozBackgroundOrigin(String aMozBackgroundOrigin);

  String getMozBinding();

  void setMozBinding(String aMozBinding);

  String getMozBorderBottomColors();

  void setMozBorderBottomColors(String aMozBorderBottomColors);

  String getMozBorderLeftColors();

  void setMozBorderLeftColors(String aMozBorderLeftColors);

  String getMozBorderRightColors();

  void setMozBorderRightColors(String aMozBorderRightColors);

  String getMozBorderTopColors();

  void setMozBorderTopColors(String aMozBorderTopColors);

  String getMozBorderRadius();

  void setMozBorderRadius(String aMozBorderRadius);

  String getMozBorderRadiusTopleft();

  void setMozBorderRadiusTopleft(String aMozBorderRadiusTopleft);

  String getMozBorderRadiusTopright();

  void setMozBorderRadiusTopright(String aMozBorderRadiusTopright);

  String getMozBorderRadiusBottomleft();

  void setMozBorderRadiusBottomleft(String aMozBorderRadiusBottomleft);

  String getMozBorderRadiusBottomright();

  void setMozBorderRadiusBottomright(String aMozBorderRadiusBottomright);

  String getMozBoxAlign();

  void setMozBoxAlign(String aMozBoxAlign);

  String getMozBoxDirection();

  void setMozBoxDirection(String aMozBoxDirection);

  String getMozBoxFlex();

  void setMozBoxFlex(String aMozBoxFlex);

  String getMozBoxOrient();

  void setMozBoxOrient(String aMozBoxOrient);

  String getMozBoxOrdinalGroup();

  void setMozBoxOrdinalGroup(String aMozBoxOrdinalGroup);

  String getMozBoxPack();

  void setMozBoxPack(String aMozBoxPack);

  String getMozBoxSizing();

  void setMozBoxSizing(String aMozBoxSizing);

  String getMozColumnCount();

  void setMozColumnCount(String aMozColumnCount);

  String getMozColumnWidth();

  void setMozColumnWidth(String aMozColumnWidth);

  String getMozColumnGap();

  void setMozColumnGap(String aMozColumnGap);

  String getMozFloatEdge();

  void setMozFloatEdge(String aMozFloatEdge);

  String getMozForceBrokenImageIcon();

  void setMozForceBrokenImageIcon(String aMozForceBrokenImageIcon);

  String getMozImageRegion();

  void setMozImageRegion(String aMozImageRegion);

  String getMozMarginEnd();

  void setMozMarginEnd(String aMozMarginEnd);

  String getMozMarginStart();

  void setMozMarginStart(String aMozMarginStart);

  String getMozOpacity();

  void setMozOpacity(String aMozOpacity);

  String getMozOutline();

  void setMozOutline(String aMozOutline);

  String getMozOutlineColor();

  void setMozOutlineColor(String aMozOutlineColor);

  String getMozOutlineRadius();

  void setMozOutlineRadius(String aMozOutlineRadius);

  String getMozOutlineRadiusTopleft();

  void setMozOutlineRadiusTopleft(String aMozOutlineRadiusTopleft);

  String getMozOutlineRadiusTopright();

  void setMozOutlineRadiusTopright(String aMozOutlineRadiusTopright);

  String getMozOutlineRadiusBottomleft();

  void setMozOutlineRadiusBottomleft(String aMozOutlineRadiusBottomleft);

  String getMozOutlineRadiusBottomright();

  void setMozOutlineRadiusBottomright(String aMozOutlineRadiusBottomright);

  String getMozOutlineStyle();

  void setMozOutlineStyle(String aMozOutlineStyle);

  String getMozOutlineWidth();

  void setMozOutlineWidth(String aMozOutlineWidth);

  String getMozOutlineOffset();

  void setMozOutlineOffset(String aMozOutlineOffset);

  String getMozPaddingEnd();

  void setMozPaddingEnd(String aMozPaddingEnd);

  String getMozPaddingStart();

  void setMozPaddingStart(String aMozPaddingStart);

  String getMozUserFocus();

  void setMozUserFocus(String aMozUserFocus);

  String getMozUserInput();

  void setMozUserInput(String aMozUserInput);

  String getMozUserModify();

  void setMozUserModify(String aMozUserModify);

  String getMozUserSelect();

  void setMozUserSelect(String aMozUserSelect);

  String getOpacity();

  void setOpacity(String aOpacity);

  String getOutlineOffset();

  void setOutlineOffset(String aOutlineOffset);

  String getOverflowX();

  void setOverflowX(String aOverflowX);

  String getOverflowY();

  void setOverflowY(String aOverflowY);

  String getImeMode();

  void setImeMode(String aImeMode);

  String getMozBorderEnd();

  void setMozBorderEnd(String aMozBorderEnd);

  String getMozBorderEndColor();

  void setMozBorderEndColor(String aMozBorderEndColor);

  String getMozBorderEndStyle();

  void setMozBorderEndStyle(String aMozBorderEndStyle);

  String getMozBorderEndWidth();

  void setMozBorderEndWidth(String aMozBorderEndWidth);

  String getMozBorderStart();

  void setMozBorderStart(String aMozBorderStart);

  String getMozBorderStartColor();

  void setMozBorderStartColor(String aMozBorderStartColor);

  String getMozBorderStartStyle();

  void setMozBorderStartStyle(String aMozBorderStartStyle);

  String getMozBorderStartWidth();

  void setMozBorderStartWidth(String aMozBorderStartWidth);

}