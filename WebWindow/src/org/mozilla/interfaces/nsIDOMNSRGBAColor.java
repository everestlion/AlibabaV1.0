/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMNSRGBAColor.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSRGBAColor extends nsIDOMRGBColor {

  String NS_IDOMNSRGBACOLOR_IID =
    "{742dc816-5134-4214-adfa-cad9dd3377cd}";

  nsIDOMCSSPrimitiveValue getAlpha();

}