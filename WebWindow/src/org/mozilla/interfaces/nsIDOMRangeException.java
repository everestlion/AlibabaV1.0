/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/range/nsIDOMRangeException.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMRangeException extends nsISupports {

  String NS_IDOMRANGEEXCEPTION_IID =
    "{0f807301-39d2-11d6-a7f2-8f504ff870dc}";

  int BAD_BOUNDARYPOINTS_ERR = 1;

  int INVALID_NODE_TYPE_ERR = 2;

  int getCode();

}