/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/system/nsICrashReporter.idl
 */

package org.mozilla.interfaces;

public interface nsICrashReporter_MOZILLA_1_9_BRANCH extends nsISupports {

  String NS_ICRASHREPORTER_MOZILLA_1_9_BRANCH_IID =
    "{5d0c6cc1-e1e7-42ce-ae99-ff7b9466a909}";

  /**
   * Append some data to the "Notes" field, to be submitted with a crash report.
   * Unlike annotateCrashReport, this method will append to existing data.
   *
   * @param data
   *        Data to be added.
   *
   * @throw NS_ERROR_NOT_INITIALIZED if crash reporting not initialized
   * @throw NS_ERROR_INVALID_ARG if or data contains invalid characters.
   *                             The only invalid character is '\0'.
   */
  void appendAppNotesToCrashReport(String data);

}