/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/views/nsIDOMDocumentView.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMDocumentView extends nsISupports {

  String NS_IDOMDOCUMENTVIEW_IID =
    "{1acdb2ba-1dd2-11b2-95bc-9542495d2569}";

  /**
 * The nsIDOMDocumentView interface is a datatype for a document that
 * supports views in the Document Object Model.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-Views
 *
 * @status FROZEN
 */
  nsIDOMAbstractView getDefaultView();

}