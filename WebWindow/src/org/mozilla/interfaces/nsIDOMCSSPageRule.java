/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCSSPageRule.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCSSPageRule extends nsIDOMCSSRule {

  String NS_IDOMCSSPAGERULE_IID =
    "{a6cf90bd-15b3-11d2-932e-00805f8add32}";

  String getSelectorText();

  void setSelectorText(String aSelectorText);

  nsIDOMCSSStyleDeclaration getStyle();

}