/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsIPKCS11Module.idl
 */

package org.mozilla.interfaces;

public interface nsIPKCS11Module extends nsISupports {

  String NS_IPKCS11MODULE_IID =
    "{8a44bdf9-d1a5-4734-bd5a-34ed7fe564c2}";

  String getName();

  String getLibName();

  nsIPKCS11Slot findSlotByName(String name);

  nsIEnumerator listSlots();

}