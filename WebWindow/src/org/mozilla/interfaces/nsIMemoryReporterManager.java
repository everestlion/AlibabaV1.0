/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/base/nsIMemoryReporter.idl
 */

package org.mozilla.interfaces;

public interface nsIMemoryReporterManager extends nsISupports {

  String NS_IMEMORYREPORTERMANAGER_IID =
    "{63fc8fbd-509b-4fdb-93b4-2e6caeeddab1}";

  nsISimpleEnumerator enumerateReporters();

  void registerReporter(nsIMemoryReporter reporter);

  void unregisterReporter(nsIMemoryReporter reporter);

}