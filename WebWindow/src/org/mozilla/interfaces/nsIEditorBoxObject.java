/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/xul/base/public/nsIEditorBoxObject.idl
 */

package org.mozilla.interfaces;

/**
 * @deprecated Please consider using nsIContainerBoxObject.
 */
public interface nsIEditorBoxObject extends nsIContainerBoxObject {

  String NS_IEDITORBOXOBJECT_IID =
    "{e3800a23-5b83-49aa-b18c-efa1ac5416e0}";

}