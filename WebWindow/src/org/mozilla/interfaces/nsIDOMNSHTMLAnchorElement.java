/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLAnchorElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLAnchorElement extends nsISupports {

  String NS_IDOMNSHTMLANCHORELEMENT_IID =
    "{a6cf911c-15b3-11d2-932e-00805f8add32}";

  String getProtocol();

  void setProtocol(String aProtocol);

  String getHost();

  void setHost(String aHost);

  String getHostname();

  void setHostname(String aHostname);

  String getPathname();

  void setPathname(String aPathname);

  String getSearch();

  void setSearch(String aSearch);

  String getPort();

  void setPort(String aPort);

  String getHash();

  void setHash(String aHash);

  String getText();

  String toString();

}