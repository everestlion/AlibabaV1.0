/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegClosePath extends nsISupports {

  String NS_IDOMSVGPATHSEGCLOSEPATH_IID =
    "{4970505f-2cc0-4afa-92e6-0cf4bdbf5a53}";

}