/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/base/nsIDebug.idl
 */

package org.mozilla.interfaces;

/**
 * @status DEPRECATED  Replaced by the NS_DebugBreak function.
 * @status FROZEN
 */
public interface nsIDebug extends nsISupports {

  String NS_IDEBUG_IID =
    "{3bf0c3d7-3bd9-4cf2-a971-33572c503e1e}";

  void assertion(String aStr, String aExpr, String aFile, int aLine);

  void warning(String aStr, String aFile, int aLine);

  void _break(String aFile, int aLine);

  void abort(String aFile, int aLine);

}