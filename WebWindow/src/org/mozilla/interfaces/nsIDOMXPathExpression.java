/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xpath/nsIDOMXPathExpression.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXPathExpression extends nsISupports {

  String NS_IDOMXPATHEXPRESSION_IID =
    "{75506f82-b504-11d5-a7f2-ca108ab8b6fc}";

  nsISupports evaluate(nsIDOMNode contextNode, int type, nsISupports result);

}