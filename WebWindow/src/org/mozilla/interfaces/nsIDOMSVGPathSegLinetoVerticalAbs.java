/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegLinetoVerticalAbs extends nsISupports {

  String NS_IDOMSVGPATHSEGLINETOVERTICALABS_IID =
    "{fd5ffb7b-7279-4c09-abfd-b733dc872e80}";

  float getY();

  void setY(float aY);

}