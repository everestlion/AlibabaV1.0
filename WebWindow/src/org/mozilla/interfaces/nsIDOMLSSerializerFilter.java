/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/ls/nsIDOMLSSerializerFilter.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLSSerializerFilter extends nsIDOMNodeFilter {

  String NS_IDOMLSSERIALIZERFILTER_IID =
    "{b9b6ec85-f69f-4a5a-a96a-8a7a8f07e2b4}";

  long getWhatToShow();

}