/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/chrome/public/nsIToolkitChromeRegistry.idl
 */

package org.mozilla.interfaces;

public interface nsIToolkitChromeRegistry extends nsIXULChromeRegistry {

  String NS_ITOOLKITCHROMEREGISTRY_IID =
    "{94490b3f-f094-418e-b1b9-73878d29bff3}";

  /**
   * The "canonical" manifest is a plaintext file which sits outside of a
   * JAR file. To provide backwards-compatibility with contents.rdf, we provide
   * this function which reads a contents.rdf manifest and writes it to a file.
   *
   * @param aOldManifestURI  The URI of an old manifest to read, without
   *                         the trailing "contents.rdf", e.g.
   *                         "jar:resource:///chrome/foo.jar!/content/foo/" or
   *                         "file://path/to/contents/rdf/"
   * @param aFile            The URI of a manifest file to write. It's a good
   *                         idea to use a resource: URI if possible.
   * @param aBaseURI         The base URI for relative path creation
   *                         "jar:resource:///chrome/foo.jar!/content/foo/"
   *                         this is a separate param from aOldManifestURI so
   *                         the "contents.rdf" can be read outside of the jar
   *                         to keep the zipreader cache from holding it open.
   * @param aAppend          Whether we should append to an existing manifest
   *                         or truncate and start empty.
   * @param aSkinOnly        Only allow skin packages.
   */
  void processContentsManifest(nsIURI aOldManifestURI, nsIURI aFile, nsIURI aBaseURI, boolean aAppend, boolean aSkinOnly);

  /**
   * If the OS has a "high-visibility" or "disabled-friendly" theme set,
   * we want to force mozilla into the classic theme, which (for the most part
   * obeys the system color/font settings. We cannot do this at initialization,
   * because it depends on the toolkit (GTK2) being initialized, which is
   * not the case in some embedding situations. Embedders have to manually
   * call this method during the startup process.
   */
  void checkForOSAccessibility();

  /**
   * Get a list of locales available for the specified package.
   */
  nsIUTF8StringEnumerator getLocalesForPackage(String aPackage);

}