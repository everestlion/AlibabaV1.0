/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/extensions/xml-rpc/idl/nsIXmlRpcClientListener.idl
 */

package org.mozilla.interfaces;

public interface nsIXmlRpcClientListener extends nsISupports {

  String NS_IXMLRPCCLIENTLISTENER_IID =
    "{27e60cd8-6d63-4d87-b7d1-82c09e0c7363}";

  /**
     * Called when XML-RPC call has succeeded.
     *
     * @param client    The originating XML-RPC client.
     * @param context   The context passed in to the asyncCall method.
     * @param result    The result of the XML-RPC call.
     */
  void onResult(nsIXmlRpcClient client, nsISupports ctxt, nsISupports result);

  /**
     * Called when the XML-RPC server returned a Fault
     *
     * @param client    The originating XML-RPC client.
     * @param context   The context passed in to the asyncCall method.
     * @param fault     The XML-RPC fault as returned by the server.
     */
  void onFault(nsIXmlRpcClient client, nsISupports ctxt, nsIXmlRpcFault fault);

  /**
     * Called when a transport or other error occurs.
     *
     * @param client    The originating XML-RPC client.
     * @param context   The context passed in to the asyncCall method.
     * @param status    The status code of the error.
     * @param errorMsg  A human readable error message.
     */
  void onError(nsIXmlRpcClient client, nsISupports ctxt, long status, String errorMsg);

}