/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLImageElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLImageElement interface is the interface to a [X]HTML
 * img element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLImageElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLIMAGEELEMENT_IID =
    "{a6cf90ab-15b3-11d2-932e-00805f8add32}";

  String getName();

  void setName(String aName);

  String getAlign();

  void setAlign(String aAlign);

  String getAlt();

  void setAlt(String aAlt);

  String getBorder();

  void setBorder(String aBorder);

  int getHeight();

  void setHeight(int aHeight);

  int getHspace();

  void setHspace(int aHspace);

  boolean getIsMap();

  void setIsMap(boolean aIsMap);

  String getLongDesc();

  void setLongDesc(String aLongDesc);

  String getSrc();

  void setSrc(String aSrc);

  String getUseMap();

  void setUseMap(String aUseMap);

  int getVspace();

  void setVspace(int aVspace);

  int getWidth();

  void setWidth(int aWidth);

}