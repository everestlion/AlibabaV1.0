/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleStates.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleStates extends nsISupports {

  String NS_IACCESSIBLESTATES_IID =
    "{7fe1ee90-edaa-43f1-9f3b-071099b51f08}";

  /**
   * MSAA State flags - used for bitfield. More than 1 allowed.
   */
  long STATE_UNAVAILABLE = 1L;

  long STATE_SELECTED = 2L;

  long STATE_FOCUSED = 4L;

  long STATE_PRESSED = 8L;

  long STATE_CHECKED = 16L;

  long STATE_MIXED = 32L;

  long STATE_READONLY = 64L;

  long STATE_HOTTRACKED = 128L;

  long STATE_DEFAULT = 256L;

  long STATE_EXPANDED = 512L;

  long STATE_COLLAPSED = 1024L;

  long STATE_BUSY = 2048L;

  long STATE_FLOATING = 4096L;

  long STATE_MARQUEED = 8192L;

  long STATE_ANIMATED = 16384L;

  long STATE_INVISIBLE = 32768L;

  long STATE_OFFSCREEN = 65536L;

  long STATE_SIZEABLE = 131072L;

  long STATE_MOVEABLE = 262144L;

  long STATE_SELFVOICING = 524288L;

  long STATE_FOCUSABLE = 1048576L;

  long STATE_SELECTABLE = 2097152L;

  long STATE_LINKED = 4194304L;

  long STATE_TRAVERSED = 8388608L;

  long STATE_MULTISELECTABLE = 16777216L;

  long STATE_EXTSELECTABLE = 33554432L;

  long STATE_ALERT_LOW = 67108864L;

  long STATE_ALERT_MEDIUM = 134217728L;

  long STATE_ALERT_HIGH = 268435456L;

  long STATE_PROTECTED = 536870912L;

  long STATE_HASPOPUP = 1073741824L;

  long STATE_REQUIRED = 67108864L;

  long STATE_IMPORTANT = 134217728L;

  long STATE_INVALID = 268435456L;

  long STATE_CHECKABLE = 8192L;

  /**
 * Extended state flags (for now non-MSAA, for Java and Gnome/ATK support)
 * "Extended state flags" has separate value space from "MSAA State flags".
 */
  long EXT_STATE_SUPPORTS_AUTOCOMPLETION = 1L;

  long EXT_STATE_DEFUNCT = 2L;

  long EXT_STATE_SELECTABLE_TEXT = 4L;

  long EXT_STATE_EDITABLE = 8L;

  long EXT_STATE_ACTIVE = 16L;

  long EXT_STATE_MODAL = 32L;

  long EXT_STATE_MULTI_LINE = 64L;

  long EXT_STATE_HORIZONTAL = 128L;

  long EXT_STATE_OPAQUE = 256L;

  long EXT_STATE_SINGLE_LINE = 512L;

  long EXT_STATE_TRANSIENT = 1024L;

  long EXT_STATE_VERTICAL = 2048L;

  long EXT_STATE_STALE = 4096L;

  long EXT_STATE_ENABLED = 8192L;

  long EXT_STATE_SENSITIVE = 16384L;

  long EXT_STATE_EXPANDABLE = 32768L;

}