/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsISMimeCert.idl
 */

package org.mozilla.interfaces;

public interface nsISMimeCert extends nsISupports {

  String NS_ISMIMECERT_IID =
    "{66710f97-a4dd-49f1-a906-fe0ebc5924c0}";

  void saveSMimeProfile();

}