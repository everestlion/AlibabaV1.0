/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/editor/idl/nsPIEditorTransaction.idl
 */

package org.mozilla.interfaces;

public interface nsPIEditorTransaction extends nsISupports {

  String NS_PIEDITORTRANSACTION_IID =
    "{4f18ada2-0ddc-11d5-9d3a-0060b0f8baff}";

  String getTxnDescription();

}