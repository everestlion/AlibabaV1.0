/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGDefsElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGDefsElement extends nsIDOMSVGElement {

  String NS_IDOMSVGDEFSELEMENT_IID =
    "{a2e86036-f04c-4013-9f74-e7090a0aac0a}";

}