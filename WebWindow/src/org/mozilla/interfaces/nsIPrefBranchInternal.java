/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/libpref/public/nsIPrefBranchInternal.idl
 */

package org.mozilla.interfaces;

/**
 * An empty interface to provide backwards compatibility for existing code that
 * bsmedberg didn't want to break all at once. Don't use me!
 *
 * @status NON-FROZEN interface WHICH WILL PROBABLY GO AWAY.
 */
public interface nsIPrefBranchInternal extends nsIPrefBranch2 {

  String NS_IPREFBRANCHINTERNAL_IID =
    "{d1d412d9-15d6-4a6a-9533-b949dc175ff5}";

}