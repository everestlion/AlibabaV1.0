/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/libjar/nsIJARProtocolHandler.idl
 */

package org.mozilla.interfaces;

public interface nsIJARProtocolHandler extends nsIProtocolHandler {

  String NS_IJARPROTOCOLHANDLER_IID =
    "{92c3b42c-98c4-11d3-8cd9-0060b0fc14a3}";

  /**
     * JARCache contains the collection of open jar files.
     */
  nsIZipReaderCache getJARCache();

}