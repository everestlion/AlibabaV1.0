/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/oji/public/nsIJVMPluginInstance.idl
 */

package org.mozilla.interfaces;

public interface nsIJVMPluginInstance extends nsISupports {

  String NS_IJVMPLUGININSTANCE_IID =
    "{a0c057d0-01c1-11d2-815b-006008119d7a}";

}