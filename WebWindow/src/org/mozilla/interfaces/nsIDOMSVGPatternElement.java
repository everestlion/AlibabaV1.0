/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPatternElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPatternElement extends nsIDOMSVGElement {

  String NS_IDOMSVGPATTERNELEMENT_IID =
    "{bc435244-b748-4e14-9e4c-219d5d3cb218}";

  nsIDOMSVGAnimatedEnumeration getPatternUnits();

  nsIDOMSVGAnimatedEnumeration getPatternContentUnits();

  nsIDOMSVGAnimatedTransformList getPatternTransform();

  nsIDOMSVGAnimatedLength getX();

  nsIDOMSVGAnimatedLength getY();

  nsIDOMSVGAnimatedLength getWidth();

  nsIDOMSVGAnimatedLength getHeight();

}