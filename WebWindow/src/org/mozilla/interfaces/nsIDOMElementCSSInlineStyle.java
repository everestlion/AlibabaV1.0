/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMElementCSSInlineStyle.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMElementCSSInlineStyle interface allows access to the inline
 * style information for elements.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-Style
 *
 * @status FROZEN
 */
public interface nsIDOMElementCSSInlineStyle extends nsISupports {

  String NS_IDOMELEMENTCSSINLINESTYLE_IID =
    "{99715845-95fc-4a56-aa53-214b65c26e22}";

  nsIDOMCSSStyleDeclaration getStyle();

}