/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/inspector/public/inIDeepTreeWalker.idl
 */

package org.mozilla.interfaces;

public interface inIDeepTreeWalker extends nsIDOMTreeWalker {

  String INIDEEPTREEWALKER_IID =
    "{91fca0e9-99d6-406b-9d78-4c96f11e9ee4}";

  boolean getShowAnonymousContent();

  void setShowAnonymousContent(boolean aShowAnonymousContent);

  boolean getShowSubDocuments();

  void setShowSubDocuments(boolean aShowSubDocuments);

  void init(nsIDOMNode aRoot, long aWhatToShow);

}