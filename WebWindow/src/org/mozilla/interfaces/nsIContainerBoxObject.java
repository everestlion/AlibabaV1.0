/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/xul/base/public/nsIContainerBoxObject.idl
 */

package org.mozilla.interfaces;

public interface nsIContainerBoxObject extends nsISupports {

  String NS_ICONTAINERBOXOBJECT_IID =
    "{35d4c04b-3bd3-4375-92e2-a818b4b4acb6}";

  nsIDocShell getDocShell();

}