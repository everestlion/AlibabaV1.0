/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGStyleElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGStyleElement extends nsIDOMSVGElement {

  String NS_IDOMSVGSTYLEELEMENT_IID =
    "{9af0d129-b366-4aa8-b7d8-8dce93148d91}";

  String getXmlspace();

  void setXmlspace(String aXmlspace);

  String getType();

  void setType(String aType);

  String getMedia();

  void setMedia(String aMedia);

  String getTitle();

  void setTitle(String aTitle);

}