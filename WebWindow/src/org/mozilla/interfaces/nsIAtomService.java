/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsIAtomService.idl
 */

package org.mozilla.interfaces;

public interface nsIAtomService extends nsISupports {

  String NS_IATOMSERVICE_IID =
    "{8c0e6018-6a06-47f5-bfa1-2e051705c9de}";

  /**
   * Version of NS_NewAtom that doesn't require linking against the
   * XPCOM library.  See nsIAtom.idl.
   */
  nsIAtom getAtom(String value);

  /**
   * Version of NS_NewPermanentAtom that doesn't require linking against
   * the XPCOM library.  See nsIAtom.idl.
   */
  nsIAtom getPermanentAtom(String value);

}