/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/offline/nsIDOMLoadStatusList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLoadStatusList extends nsISupports {

  String NS_IDOMLOADSTATUSLIST_IID =
    "{d58bc0cf-e35c-4d22-9e21-9ada8fc4203a}";

  long getLength();

  nsIDOMLoadStatus item(long index);

}