/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/cache/public/nsICacheEntryDescriptor.idl
 */

package org.mozilla.interfaces;

public interface nsICacheMetaDataVisitor extends nsISupports {

  String NS_ICACHEMETADATAVISITOR_IID =
    "{22f9a49c-3cf8-4c23-8006-54efb11ac562}";

  /**
     * Called for each key/value pair in the meta data for a cache entry
     */
  boolean visitMetaDataElement(String key, String value);

}