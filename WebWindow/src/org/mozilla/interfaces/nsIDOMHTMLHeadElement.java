/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLHeadElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLHeadElement interface is the interface to a [X]HTML
 * head element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLHeadElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLHEADELEMENT_IID =
    "{a6cf9087-15b3-11d2-932e-00805f8add32}";

  String getProfile();

  void setProfile(String aProfile);

}