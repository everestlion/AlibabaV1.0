/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleText.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleText extends nsISupports {

  String NS_IACCESSIBLETEXT_IID =
    "{caa4f543-070e-4705-8428-2e53575c41bb}";

  int TEXT_OFFSET_END_OF_TEXT = -1;

  int TEXT_OFFSET_CARET = -2;

  int BOUNDARY_CHAR = 0;

  int BOUNDARY_WORD_START = 1;

  int BOUNDARY_WORD_END = 2;

  int BOUNDARY_SENTENCE_START = 3;

  int BOUNDARY_SENTENCE_END = 4;

  int BOUNDARY_LINE_START = 5;

  int BOUNDARY_LINE_END = 6;

  int BOUNDARY_ATTRIBUTE_RANGE = 7;

  /**
   * The current current caret offset.
   * If set < 0 then caret will be placed  at the end of the text
   */
  int getCaretOffset();

  /**
   * The current current caret offset.
   * If set < 0 then caret will be placed  at the end of the text
   */
  void setCaretOffset(int aCaretOffset);

  int getCharacterCount();

  int getSelectionCount();

  /**
      * String methods may need to return multibyte-encoded strings,
      * since some locales can't be encoded using 16-bit chars.
      * So the methods below might return UTF-16 strings, or they could
      * return "string" values which are UTF-8.
      */
  String getText(int startOffset, int endOffset);

  String getTextAfterOffset(int offset, int boundaryType, int[] startOffset, int[] endOffset);

  String getTextAtOffset(int offset, int boundaryType, int[] startOffset, int[] endOffset);

  String getTextBeforeOffset(int offset, int boundaryType, int[] startOffset, int[] endOffset);

  /**
      * It would be better to return an unsigned long here,
      * to allow unicode chars > 16 bits
      */
  char getCharacterAtOffset(int offset);

  /**
   * Get the accessible and start/end offsets around the given offset.
   * This accessible get return the DOM node and layout frame
   * with the uniform attributes for this range of text
   */
  nsIAccessible getAttributeRange(int offset, int[] rangeStartOffset, int[] rangeEndOffset);

  /**
   * Returns the bounding box of the specified position.
   *
   * The virtual character after the last character of the represented text,
   * i.e. the one at position length is a special case. It represents the
   * current input position and will therefore typically be queried by AT more
   * often than other positions. Because it does not represent an existing
   * character its bounding box is defined in relation to preceding characters.
   * It should be roughly equivalent to the bounding box of some character when
   * inserted at the end of the text. Its height typically being the maximal
   * height of all the characters in the text or the height of the preceding
   * character, its width being at least one pixel so that the bounding box is
   * not degenerate.
   *
   * @param offset - Index of the character for which to return its bounding
   *                  box. The valid range is 0..length.
   * @param x - X coordinate of the bounding box of the referenced character.
   * @param y - Y coordinate of the bounding box of the referenced character.
   * @param width - Width of the bounding box of the referenced character.
   * @param height - Height of the bounding box of the referenced character.
   * @param coordType - Specifies if the coordinates are relative to the screen
   *                    or to the parent window (see constants declared in
   *                    nsIAccessibleCoordinateType).
  */
  void getCharacterExtents(int offset, int[] x, int[] y, int[] width, int[] height, long coordType);

  void getRangeExtents(int startOffset, int endOffset, int[] x, int[] y, int[] width, int[] height, long coordType);

  /**
   * Get the text offset at the given point, or return -1
   * if no character exists at that point
   *
   * @param x - The position's x value for which to look up the index of the
   *            character that is rendered on to the display at that point.
   * @param y - The position's y value for which to look up the index of the
   *            character that is rendered on to the display at that point.
   * @param coordType - Screen coordinates or window coordinates (see constants
   *                    declared in nsIAccessibleCoordinateType).
   * @return offset - Index of the character under the given point or -1 if
   *                  the point is invalid or there is no character under
   *                  the point.
   */
  int getOffsetAtPoint(int x, int y, long coordType);

  void getSelectionBounds(int selectionNum, int[] startOffset, int[] endOffset);

  /**
   * Set the bounds for the given selection range
   */
  void setSelectionBounds(int selectionNum, int startOffset, int endOffset);

  void addSelection(int startOffset, int endOffset);

  void removeSelection(int selectionNum);

  /**
   * Makes a specific part of string visible on screen.
   *
   * @param startIndex  0-based character offset
   * @param endIndex    0-based character offset - the offset of the
   *                    character just past the last character of the
   *                    string
   * @param scrollType  defines how to scroll (see nsIAccessibleScrollType for
   *                    available constants)
   */
  void scrollSubstringTo(int startIndex, int endIndex, long scrollType);

  /**
   * Moves the top left of a substring to a specified location.
   *
   * @param startIndex      0-based character offset
   * @param endIndex        0-based character offset - the offset of the
   *                        character just past the last character of
   *                        the string
   * @param coordinateType  specifies the coordinates origin (for available
   *                        constants refer to nsIAccessibleCoordinateType)
   * @param x               defines the x coordinate
   * @param y               defines the y coordinate
   */
  void scrollSubstringToPoint(int startIndex, int endIndex, long coordinateType, int x, int y);

}