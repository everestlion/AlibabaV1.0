/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/offline/nsIDOMOfflineResourceList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMOfflineResourceList extends nsISupports {

  String NS_IDOMOFFLINERESOURCELIST_IID =
    "{8449bce2-0d8c-4c74-ab79-b41b8d81f1c4}";

  /**
   * Enumerate the list of dynamically-managed entries.
   */
  long getLength();

  String item(long index);

  /**
   * Add an item to the list of dynamically-managed entries.  The resource
   * will be fetched into the application cache.
   *
   * @param uri
   *        The resource to add.
   */
  void add(String uri);

  /**
   * Remove an item from the list of dynamically-managed entries.  If this
   * was the last reference to a URI in the application cache, the cache
   * entry will be removed.
   *
   * @param uri
   *        The resource to remove.
   */
  void remove(String uri);

  /**
   * State of the application cache this object is associated with.
   */
  int UNCACHED = 0;

  int IDLE = 1;

  int CHECKING = 2;

  int DOWNLOADING = 3;

  /**
   * There is a new version of the application cache available
   *
   * Versioned application caches are not currently implemented, so this
   * value will not yet be returned
   */
  int UPDATEREADY = 4;

  int getStatus();

  /**
   * Begin the application update process on the associated application cache.
   */
  void update();

  /**
   * Swap in the newest version of the application cache.
   *
   * Versioned application caches are not currently implemented, so this
   * method will throw an exception.
   */
  void swapCache();

  nsIDOMEventListener getOnchecking();

  void setOnchecking(nsIDOMEventListener aOnchecking);

  nsIDOMEventListener getOnerror();

  void setOnerror(nsIDOMEventListener aOnerror);

  nsIDOMEventListener getOnnoupdate();

  void setOnnoupdate(nsIDOMEventListener aOnnoupdate);

  nsIDOMEventListener getOndownloading();

  void setOndownloading(nsIDOMEventListener aOndownloading);

  nsIDOMEventListener getOnprogress();

  void setOnprogress(nsIDOMEventListener aOnprogress);

  nsIDOMEventListener getOnupdateready();

  void setOnupdateready(nsIDOMEventListener aOnupdateready);

  nsIDOMEventListener getOncached();

  void setOncached(nsIDOMEventListener aOncached);

}