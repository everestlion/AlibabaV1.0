/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGException.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGException extends nsISupports {

  String NS_IDOMSVGEXCEPTION_IID =
    "{64e6f0e1-af99-4bb9-ab25-7e56012f0021}";

  int SVG_WRONG_TYPE_ERR = 0;

  int SVG_INVALID_VALUE_ERR = 1;

  int SVG_MATRIX_NOT_INVERTABLE = 2;

  int getCode();

}