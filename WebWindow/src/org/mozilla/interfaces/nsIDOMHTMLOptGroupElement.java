/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLOptGroupElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLOptGroupElement interface is the interface to a
 * [X]HTML optgroup element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLOptGroupElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLOPTGROUPELEMENT_IID =
    "{a6cf9091-15b3-11d2-932e-00805f8add32}";

  boolean getDisabled();

  void setDisabled(boolean aDisabled);

  String getLabel();

  void setLabel(String aLabel);

}