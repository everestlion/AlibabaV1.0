/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegArcRel extends nsISupports {

  String NS_IDOMSVGPATHSEGARCREL_IID =
    "{49d0360d-bb66-4ab9-b9b0-f49b93398595}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  float getR1();

  void setR1(float aR1);

  float getR2();

  void setR2(float aR2);

  float getAngle();

  void setAngle(float aAngle);

  boolean getLargeArcFlag();

  void setLargeArcFlag(boolean aLargeArcFlag);

  boolean getSweepFlag();

  void setSweepFlag(boolean aSweepFlag);

}