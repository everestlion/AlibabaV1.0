/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFitToViewBox.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFitToViewBox extends nsISupports {

  String NS_IDOMSVGFITTOVIEWBOX_IID =
    "{089410f3-9777-44f1-a882-ab4225696434}";

  nsIDOMSVGAnimatedRect getViewBox();

  nsIDOMSVGAnimatedPreserveAspectRatio getPreserveAspectRatio();

}