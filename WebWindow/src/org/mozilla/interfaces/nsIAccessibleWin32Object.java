/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/msaa/nsIAccessibleWin32Object.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleWin32Object extends nsISupports {

  String NS_IACCESSIBLEWIN32OBJECT_IID =
    "{ca7a3a93-822f-4cdf-8cb4-c52d16b9afc7}";

}