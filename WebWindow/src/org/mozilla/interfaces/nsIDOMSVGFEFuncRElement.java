/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEFuncRElement extends nsIDOMSVGComponentTransferFunctionElement {

  String NS_IDOMSVGFEFUNCRELEMENT_IID =
    "{85719a5d-9688-4c5f-bad5-c21847515200}";

}