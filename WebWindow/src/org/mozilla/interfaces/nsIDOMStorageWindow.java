/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/storage/nsIDOMStorageWindow.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMStorageWindow extends nsISupports {

  String NS_IDOMSTORAGEWINDOW_IID =
    "{55e9c181-2476-47cf-97f8-efdaaf7b6f7a}";

  /**
   * Session storage for the current browsing context.
   */
  nsIDOMStorage getSessionStorage();

  /**
   * Global storage, accessible by domain.
   */
  nsIDOMStorageList getGlobalStorage();

}