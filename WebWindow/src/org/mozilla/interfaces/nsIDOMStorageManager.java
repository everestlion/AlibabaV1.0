/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/storage/nsIDOMStorageManager.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMStorageManager extends nsISupports {

  String NS_IDOMSTORAGEMANAGER_IID =
    "{6e4bc25e-f056-4c6c-b27e-89152ca91834}";

  /**
   * Return the amount of disk space used by a domain.  Usage is checked
   * against the domain of the page that set the key (the owner domain), not
   * the domain of the storage object.
   *
   * @param aOwnerDomain The domain to check.
   * @returns the space usage of the domain, in bytes.
   */
  int getUsage(String aOwnerDomain);

  /**
   * Clear keys owned by offline applications.  All data owned by a domain
   * with the "offline-app" permission will be removed from the database.
   */
  void clearOfflineApps();

}