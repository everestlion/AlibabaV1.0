/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/stylesheets/nsIDOMLinkStyle.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLinkStyle extends nsISupports {

  String NS_IDOMLINKSTYLE_IID =
    "{24d89a65-f598-481e-a297-23cc02599bbd}";

  nsIDOMStyleSheet getSheet();

}