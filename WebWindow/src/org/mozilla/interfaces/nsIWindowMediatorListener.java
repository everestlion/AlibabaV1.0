/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpfe/appshell/public/nsIWindowMediatorListener.idl
 */

package org.mozilla.interfaces;

public interface nsIWindowMediatorListener extends nsISupports {

  String NS_IWINDOWMEDIATORLISTENER_IID =
    "{2f276982-0d60-4377-a595-d350ba516395}";

  void onWindowTitleChange(nsIXULWindow window, String newTitle);

  void onOpenWindow(nsIXULWindow window);

  void onCloseWindow(nsIXULWindow window);

}