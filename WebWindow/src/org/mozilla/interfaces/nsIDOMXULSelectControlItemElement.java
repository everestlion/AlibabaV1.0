/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULSelectCntrlItemEl.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULSelectControlItemElement extends nsIDOMXULElement {

  String NS_IDOMXULSELECTCONTROLITEMELEMENT_IID =
    "{6aaaa30d-54ab-434a-8ae8-6d29a566d870}";

  boolean getDisabled();

  void setDisabled(boolean aDisabled);

  String getCrop();

  void setCrop(String aCrop);

  String getImage();

  void setImage(String aImage);

  String getLabel();

  void setLabel(String aLabel);

  String getAccessKey();

  void setAccessKey(String aAccessKey);

  String getCommand();

  void setCommand(String aCommand);

  String getValue();

  void setValue(String aValue);

  boolean getSelected();

  nsIDOMXULSelectControlElement getControl();

}