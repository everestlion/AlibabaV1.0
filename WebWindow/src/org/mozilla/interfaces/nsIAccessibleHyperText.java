/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleHyperText.idl
 */

package org.mozilla.interfaces;

/**
 * A cross-platform interface that deals with text which contains hyperlinks.
 */
public interface nsIAccessibleHyperText extends nsISupports {

  String NS_IACCESSIBLEHYPERTEXT_IID =
    "{d56bd454-8ff3-4edc-b266-baeada00267b}";

  /**
   * Returns the number of links contained within this hypertext object.
   */
  int getLinkCount();

  int getLinkIndex(int charIndex);

  /**
   * Retrieves the nsIAccessibleHyperLink object at the given link index.
   *
   * @param linkIndex  0-based index of the link that is to be retrieved.
   * This can be retrieved via getLinkIndex (see above).
   *
   * @returns nsIAccessibleHyperLink  Object representing the link properties
   * or NS_ERROR_INVALID_ARG if there is no link at that index.
   */
  nsIAccessibleHyperLink getLink(int linkIndex);

}