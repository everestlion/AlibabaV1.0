/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLSelectElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLSelectElement extends nsISupports {

  String NS_IDOMNSHTMLSELECTELEMENT_IID =
    "{a6cf9105-15b3-11d2-932e-00805f8add32}";

  nsIDOMNode item(long index);

  nsIDOMNode namedItem(String name);

}