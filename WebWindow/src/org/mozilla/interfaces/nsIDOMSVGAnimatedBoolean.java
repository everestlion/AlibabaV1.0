/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedBoolean.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMSVGAnimatedBoolean interface is the interface to an SVG
 * animated boolean.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/SVG11/types.html
 *
 */
public interface nsIDOMSVGAnimatedBoolean extends nsISupports {

  String NS_IDOMSVGANIMATEDBOOLEAN_IID =
    "{7e325385-cc82-4763-bd14-e2c92edd5462}";

  boolean getBaseVal();

  void setBaseVal(boolean aBaseVal);

  boolean getAnimVal();

}