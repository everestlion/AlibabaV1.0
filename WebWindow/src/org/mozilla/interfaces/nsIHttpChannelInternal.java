/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/protocol/http/public/nsIHttpChannelInternal.idl
 */

package org.mozilla.interfaces;

/** 
 * Dumping ground for http.  This interface will never be frozen.  If you are 
 * using any feature exposed by this interface, be aware that this interface 
 * will change and you will be broken.  You have been warned.
 */
public interface nsIHttpChannelInternal extends nsISupports {

  String NS_IHTTPCHANNELINTERNAL_IID =
    "{602e8cf0-c387-4598-9e6c-aa4b1551ed1c}";

  /**
     * An http channel can own a reference to the document URI
     */
  nsIURI getDocumentURI();

  /**
     * An http channel can own a reference to the document URI
     */
  void setDocumentURI(nsIURI aDocumentURI);

  /**
     * Get the major/minor version numbers for the request
     */
  void getRequestVersion(long[] major, long[] minor);

  /**
     * Get the major/minor version numbers for the response
     */
  void getResponseVersion(long[] major, long[] minor);

  /**
     * Helper method to set a cookie with a consumer-provided
     * cookie header, _but_ using the channel's other information
     * (URI's, prompters, date headers etc).
     *
     * @param aCookieHeader
     *        The cookie header to be parsed.
     */
  void setCookie(String aCookieHeader);

}