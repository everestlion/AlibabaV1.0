/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGTextElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGTextElement extends nsIDOMSVGTextPositioningElement {

  String NS_IDOMSVGTEXTELEMENT_IID =
    "{6d43b1b4-efb6-426d-9e65-4420c3e24688}";

}