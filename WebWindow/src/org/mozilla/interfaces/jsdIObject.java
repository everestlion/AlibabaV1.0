/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/jsd/idl/jsdIDebuggerService.idl
 */

package org.mozilla.interfaces;

public interface jsdIObject extends nsISupports {

  String JSDIOBJECT_IID =
    "{d500e8b8-1dd1-11b2-89a1-cdf55d91cbbd}";

  /**
     * The URL (filename) that contains the script which caused this object
     * to be created.
     */
  String getCreatorURL();

  /**
     * Line number in the creatorURL where this object was created.
     */
  long getCreatorLine();

  /**
     * The URL (filename) that contains the script which defined the constructor
     * used to create this object.
     */
  String getConstructorURL();

  /**
     * Line number in the creatorURL where this object was created.
     */
  long getConstructorLine();

  /**
     * jsdIValue for this object.
     */
  jsdIValue getValue();

}