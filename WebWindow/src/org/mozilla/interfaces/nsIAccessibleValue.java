/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleValue.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleValue extends nsISupports {

  String NS_IACCESSIBLEVALUE_IID =
    "{f4abbc2f-0f28-47dc-a9e9-f7a1719ab2be}";

  double getMaximumValue();

  double getMinimumValue();

  double getCurrentValue();

  void setCurrentValue(double aCurrentValue);

  double getMinimumIncrement();

}