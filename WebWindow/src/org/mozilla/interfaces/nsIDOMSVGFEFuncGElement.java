/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEFuncGElement extends nsIDOMSVGComponentTransferFunctionElement {

  String NS_IDOMSVGFEFUNCGELEMENT_IID =
    "{28555e78-c6c2-4a98-af53-bfc2c6944295}";

}