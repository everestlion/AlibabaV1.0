/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/editor/idl/nsIEditorIMESupport.idl
 */

package org.mozilla.interfaces;

public interface nsIEditorIMESupport extends nsISupports {

  String NS_IEDITORIMESUPPORT_IID =
    "{ce1c0424-c3c0-44b0-97d6-df12deb19d45}";

  /**
   * endComposition() Handles the end of inline input composition.
   */
  void endComposition();

  /**
   * forceCompositionEnd() force the composition end
   */
  void forceCompositionEnd();

  /**
   * Notify for IME when the editor got focus.
   */
  void notifyIMEOnFocus();

  /**
   * Notify for IME when the editor lost focus.
   */
  void notifyIMEOnBlur();

  /**
   * whether this editor has active IME transaction
   */
  boolean getComposing();

}