/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleRetrieval.idl
 */

package org.mozilla.interfaces;

/**
 * An interface for in-process accessibility clients
 * wishing to get an nsIAccessible or nsIAccessNode for
 * a given DOM node.
 * More documentation at:
 *   http://www.mozilla.org/projects/ui/accessibility
 *
 * @status UNDER_REVIEW
 */
public interface nsIAccessibleRetrieval extends nsISupports {

  String NS_IACCESSIBLERETRIEVAL_IID =
    "{244e4c67-a1d3-44f2-9cab-cdaa31b68046}";

  /**
   * Return an nsIAccessible for a DOM node in pres shell 0.
   * Create a new accessible of the appropriate type if necessary,
   * or use one from the accessibility cache if it already exists.
   * @param aNode The DOM node to get an accessible for.
   * @return The nsIAccessible for the given DOM node.
   */
  nsIAccessible getAccessibleFor(nsIDOMNode aNode);

  /**
   * The same like getAccessibleFor method except it returns accessible only if
   * it is attached, i.e. accessible is certified to be a descendent of the root
   * accessible.
   *
   * @param aNode - the DOM node to get an accessible for.
   *
   * @return - the accessible for the given DOM node.
   */
  nsIAccessible getAttachedAccessibleFor(nsIDOMNode aNode);

  /**
   * Return an DOM node that is relevant to attached accesible check. This
   * node is either from bindings chain if given node is anonymous and owner
   * binding denies accessible in anonymous content or given node (it's not
   * important whether it is accessible or not). This method doesn't create
   * accessible object for returned node.
   *
   * @param aNode - the DOM node to get relevant content node.
   *
   * @return - the DOM node for parent attached accessible
   */
  nsIDOMNode getRelevantContentNodeFor(nsIDOMNode aNode);

  /**
   * Return an nsIAccessible for a DOM node in pres shell for this DOM window.
   * Create a new accessible of the appropriate type if necessary,
   * or use one from the accessibility cache if it already exists.
   * @param aNode   The DOM node to get an accessible for.
   * @param aDOMWin The DOM window containing the node.
   * @return The nsIAccessible for the given DOM node.
   */
  nsIAccessible getAccessibleInWindow(nsIDOMNode aNode, nsIDOMWindow aDOMWin);

  /**
   * Return an nsIAccessible for a DOM node in the given weak shell.
   * Create a new accessible of the appropriate type if necessary,
   * or use one from the accessibility cache if it already exists.
   * @param aNode      The DOM node to get an accessible for.
   * @param aPresShell The presentation shell which contains layout info for the DOM node.
   * @return The nsIAccessible for the given DOM node.
   */
  nsIAccessible getAccessibleInWeakShell(nsIDOMNode aNode, nsISupports aPresShell);

  /**
   * Return an nsIAccessible for a DOM node in the given pres shell.
   * Create a new accessible of the appropriate type if necessary,
   * or use one from the accessibility cache if it already exists.
   * @param aNode      The DOM node to get an accessible for.
   * @param aPresShell The presentation shell which contains layout info for the DOM node.
   * @return The nsIAccessible for the given DOM node.
   */
  nsIAccessible getAccessibleInShell(nsIDOMNode aNode, nsISupports aPresShell);

  /**
    * Return an nsIAccessNode for an already created DOM node in the given weak shell.
    * Does not create a new one -- only returns cached access nodes.
    * @param aNode      The DOM node to get an access node for.
    * @param aPresShell The presentation shell which contains layout info for the DOM node.
    * @return The nsIAccessNode for the given DOM node or null if
    *         an access node does not already exist for this DOM node.
    */
  nsIAccessNode getCachedAccessNode(nsIDOMNode aNode, nsISupports aShell);

  /**
    * Return an nsIAccessible for an already created DOM node in the given weak shell.
    * Does not create a new one -- only returns cached accessibles.
    * @param aNode      The DOM node to get an accessible for.
    * @param aPresShell The presentation shell which contains layout info for the DOM node.
    * @return The nsIAccessible for the given DOM node or null if
    *         an accessible does not already exist for this DOM node.
    */
  nsIAccessible getCachedAccessible(nsIDOMNode aNode, nsISupports aShell);

  /**
    * Returns accessible role as a string.
    *
    * @param aRole - the accessible role constants.
    */
  String getStringRole(long aRole);

  /**
    * Returns list which contains accessible states as a strings.
    *
    * @param aStates - accessible states.
    * @param aExtraStates - accessible extra states.
    */
  nsIDOMDOMStringList getStringStates(long aStates, long aExtraStates);

  /**
   * Get the type of accessible event as a string.
   *
   * @param aEventType - the accessible event type constant
   * @return - accessible event type presented as human readable string
   */
  String getStringEventType(long aEventType);

  /**
   * Get the type of accessible relation as a string.
   *
   * @param aRelationType - the accessible relation type constant
   * @return - accessible relation type presented as human readable string
   */
  String getStringRelationType(long aRelationType);

}