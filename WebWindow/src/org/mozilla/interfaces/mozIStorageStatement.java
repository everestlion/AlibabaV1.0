/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/storage/public/mozIStorageStatement.idl
 */

package org.mozilla.interfaces;

public interface mozIStorageStatement extends mozIStorageValueArray {

  String MOZISTORAGESTATEMENT_IID =
    "{42fad13e-c67d-4b2c-bd61-2c5b17186772}";

  /**
   * Initialize this query with the given SQL statement.
   *
   */
  void initialize(mozIStorageConnection aDBConnection, String aSQLStatement);

  /**
   * Finalizes a statement so you can successfully close a database connection.
   * This method does not need to be used from native callers since you can just
   * set the statement to null, but is extremely useful to JS callers.
   */
  void _finalize();

  /**
   * Create a clone of this statement, by initializing a new statement
   * with the same connection and same SQL statement as this one.  It
   * does not preserve statement state; that is, if a statement is
   * being executed when it is cloned, the new statement will not be
   * executing.
   */
  mozIStorageStatement _clone();

  long getParameterCount();

  /**
   * Name of nth parameter, if given
   */
  String getParameterName(long aParamIndex);

  /**
   * Returns the index of the named parameter.
   *
   * @param aName The name of the parameter you want the index for.
   * @return The index of the named parameter.
   */
  long getParameterIndex(String aName);

  /**
   * Number of columns returned
   */
  long getColumnCount();

  /**
   * Name of nth column
   */
  String getColumnName(long aColumnIndex);

  /**
   * Obtains the index of the column with the specified name.
   *
   * @param aName The name of the column.
   * @return The index of the column with the specified name.
   */
  long getColumnIndex(String aName);

  /**
   * Obtains the declared column type of a prepared statement.
   *
   * @param aParamIndex The zero-based index of the column who's declared type
   *                    we are interested in.
   * @returns the declared index type.
   */
  String getColumnDecltype(long aParamIndex);

  /**
   * Reset parameters/statement execution
   */
  void reset();

  /**
   * Bind the given value to the parameter at aParamIndex. Index 0
   * denotes the first numbered argument or ?1.
   */
  void bindUTF8StringParameter(long aParamIndex, String aValue);

  void bindStringParameter(long aParamIndex, String aValue);

  void bindDoubleParameter(long aParamIndex, double aValue);

  void bindInt32Parameter(long aParamIndex, int aValue);

  void bindInt64Parameter(long aParamIndex, long aValue);

  void bindNullParameter(long aParamIndex);

  void bindBlobParameter(long aParamIndex, byte[] aValue, long aValueSize);

  /**
   * Execute the query, ignoring any results.  This is accomplished by
   * calling step() once, and then calling reset().
   *
   * Error and last insert info, etc. are available from
   * the mozStorageConnection.
   */
  void execute();

  /**
   * Execute a query, using any currently-bound parameters.  Reset
   * must be called on the statement after the last call of
   * executeStep.
   *
   * @returns a boolean indicating whether there are more rows or not;
   * row data may be accessed using mozIStorageValueArray methods on
   * the statement.
   *
   */
  boolean executeStep();

  /**
   * The current state.  Row getters are only valid while
   * the statement is in the "executing" state.
   */
  int MOZ_STORAGE_STATEMENT_INVALID = 0;

  int MOZ_STORAGE_STATEMENT_READY = 1;

  int MOZ_STORAGE_STATEMENT_EXECUTING = 2;

  int getState();

  /**
   * Escape a string for SQL LIKE search.
   *
   * @param     aValue the string to escape for SQL LIKE 
   * @param     aEscapeChar the escape character
   * @returns   an AString of an escaped version of aValue
   *            (%, _ and the escape char are escaped with the escape char)
   *            For example, we will convert "foo/bar_baz%20cheese" 
   *            into "foo//bar/_baz/%20cheese" (if the escape char is '/').
   * @note      consumers will have to use same escape char
   *            when doing statements such as:   ...LIKE '?1' ESCAPE '/'...
   */
  String escapeStringForLIKE(String aValue, char aEscapeChar);

}