/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/extensions/cookie/nsICookieAcceptDialog.idl
 */

package org.mozilla.interfaces;

public interface nsICookieAcceptDialog extends nsISupports {

  String NS_ICOOKIEACCEPTDIALOG_IID =
    "{3f2f0d2c-bdea-4b5a-afc6-fcf18f66b97e}";

  short ACCEPT_COOKIE = 0;

  short REMEMBER_DECISION = 1;

  short HOSTNAME = 2;

  short COOKIESFROMHOST = 3;

  short CHANGINGCOOKIE = 4;

}