/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for 64-bit integers
 * 
 * @status FROZEN
 */
public interface nsISupportsPRUint64 extends nsISupportsPrimitive {

  String NS_ISUPPORTSPRUINT64_IID =
    "{e13567c0-4a1c-11d3-9890-006008962422}";

  double getData();

  void setData(double aData);

  String toString();

}