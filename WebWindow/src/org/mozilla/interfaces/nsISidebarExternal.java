/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/sidebar/nsISidebar.idl
 */

package org.mozilla.interfaces;

public interface nsISidebarExternal extends nsISupports {

  String NS_ISIDEBAREXTERNAL_IID =
    "{4350fb73-9305-41df-a669-11d26222d420}";

  void addSearchProvider(String aDescriptionURL);

  long isSearchProviderInstalled(String aSearchURL);

}