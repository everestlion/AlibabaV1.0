/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/base/public/nsIStandardURL.idl
 */

package org.mozilla.interfaces;

/**
 * nsIStandardURL defines the interface to an URL with the standard
 * file path format common to protocols like http, ftp, and file.
 * It supports initialization from a relative path and provides
 * some customization on how URLs are normalized.
 *
 * @status UNDER_REVIEW
 */
public interface nsIStandardURL extends nsIMutable {

  String NS_ISTANDARDURL_IID =
    "{babd6cca-ebe7-4329-967c-d6b9e33caa81}";

  /**
     * blah:foo/bar    => blah://foo/bar
     * blah:/foo/bar   => blah:///foo/bar
     * blah://foo/bar  => blah://foo/bar
     * blah:///foo/bar => blah:///foo/bar
     */
  long URLTYPE_STANDARD = 1L;

  /**
     * blah:foo/bar    => blah://foo/bar
     * blah:/foo/bar   => blah://foo/bar
     * blah://foo/bar  => blah://foo/bar
     * blah:///foo/bar => blah://foo/bar
     */
  long URLTYPE_AUTHORITY = 2L;

  /**
     * blah:foo/bar    => blah:///foo/bar
     * blah:/foo/bar   => blah:///foo/bar
     * blah://foo/bar  => blah://foo/bar
     * blah:///foo/bar => blah:///foo/bar
     */
  long URLTYPE_NO_AUTHORITY = 3L;

  /**
     * Initialize a standard URL.
     *
     * @param aUrlType       - one of the URLTYPE_ flags listed above.
     * @param aDefaultPort   - if the port parsed from the URL string matches
     *                         this port, then the port will be removed from the
     *                         canonical form of the URL.
     * @param aSpec          - URL string.
     * @param aOriginCharset - the charset from which this URI string
     *                         originated.  this corresponds to the charset
     *                         that should be used when communicating this
     *                         URI to an origin server, for example.  if
     *                         null, then provide aBaseURI implements this
     *                         interface, the origin charset of aBaseURI will
     *                         be assumed, otherwise defaulting to UTF-8 (i.e.,
     *                         no charset transformation from aSpec).
     * @param aBaseURI       - if null, aSpec must specify an absolute URI.
     *                         otherwise, aSpec will be resolved relative
     *                         to aBaseURI.
     */
  void init(long aUrlType, int aDefaultPort, String aSpec, String aOriginCharset, nsIURI aBaseURI);

}