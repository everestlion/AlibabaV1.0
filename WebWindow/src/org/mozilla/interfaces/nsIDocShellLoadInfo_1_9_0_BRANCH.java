/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/docshell/base/nsIDocShellLoadInfo.idl
 */

package org.mozilla.interfaces;

public interface nsIDocShellLoadInfo_1_9_0_BRANCH extends nsIDocShellLoadInfo {

  String NS_IDOCSHELLLOADINFO_1_9_0_BRANCH_IID =
    "{b66d94ba-4457-41b1-9507-79c1cd873da0}";

  /** If this attribute is true only ever use the owner specify by
     *  the owner and inheritOwner attributes.
     *  If there are security reasons for why this is unsafe, such
     *  as trying to use a systemprincipal owner for a content docshell
     *  the load fails.
     */
  boolean getOwnerIsExplicit();

  /** If this attribute is true only ever use the owner specify by
     *  the owner and inheritOwner attributes.
     *  If there are security reasons for why this is unsafe, such
     *  as trying to use a systemprincipal owner for a content docshell
     *  the load fails.
     */
  void setOwnerIsExplicit(boolean aOwnerIsExplicit);

}