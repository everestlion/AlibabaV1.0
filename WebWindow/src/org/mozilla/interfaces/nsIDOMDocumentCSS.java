/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMDocumentCSS.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMDocumentCSS extends nsIDOMDocumentStyle {

  String NS_IDOMDOCUMENTCSS_IID =
    "{39f76c23-45b2-428a-9240-a981e5abf148}";

  nsIDOMCSSStyleDeclaration getOverrideStyle(nsIDOMElement elt, String pseudoElt);

}