/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleEditableText.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleEditableText extends nsISupports {

  String NS_IACCESSIBLEEDITABLETEXT_IID =
    "{52837507-202d-4e72-a482-5f068a1fd720}";

  /**
   * Sets the attributes for the text between the two given indices. The old
   * attributes are replaced by the new list of attributes. For example,
   * sets font styles, such as italic, bold...
   *
   * @param startPos - start index of the text whose attributes are modified.
   * @param endPos - end index of the text whose attributes are modified.
   * @param attributes - set of attributes that replaces the old list of
   *                     attributes of the specified text portion.
   */
  void setAttributes(int startPos, int endPos, nsISupports attributes);

  /**
   * Replaces the text represented by this object by the given text.
   */
  void setTextContents(String text);

  /**
   * Inserts text at the specified position.
   *
   * @param text - text that is inserted.
   * @param position - index at which to insert the text.
   */
  void insertText(String text, int position);

  /**
   * Copies the text range into the clipboard.
   *
   * @param startPos - start index of the text to moved into the clipboard.
   * @param endPos - end index of the text to moved into the clipboard.
   */
  void copyText(int startPos, int endPos);

  /**
   * Deletes a range of text and copies it to the clipboard.
   *
   * @param startPos - start index of the text to be deleted.
   * @param endOffset - end index of the text to be deleted.
   */
  void cutText(int startPos, int endPos);

  /**
   * Deletes a range of text.
   *
   * @param startPos - start index of the text to be deleted.
   * @param endPos - end index of the text to be deleted.
   */
  void deleteText(int startPos, int endPos);

  /**
   * Pastes text from the clipboard.
   *
   * @param position - index at which to insert the text from the system
   *                   clipboard into the text represented by this object.
   */
  void pasteText(int position);

}