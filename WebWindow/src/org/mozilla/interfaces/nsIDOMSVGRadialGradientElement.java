/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGGradientElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMSVGRadialGradientElement interface is the interface to an SVG
 * radial gradient element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/SVG11/pservers.html
 *
 */
public interface nsIDOMSVGRadialGradientElement extends nsIDOMSVGGradientElement {

  String NS_IDOMSVGRADIALGRADIENTELEMENT_IID =
    "{d0262ae1-31a4-44be-b82e-85e4cfe280fd}";

  nsIDOMSVGAnimatedLength getCx();

  nsIDOMSVGAnimatedLength getCy();

  nsIDOMSVGAnimatedLength getR();

  nsIDOMSVGAnimatedLength getFx();

  nsIDOMSVGAnimatedLength getFy();

}