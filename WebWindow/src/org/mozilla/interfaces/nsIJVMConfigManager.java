/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/oji/public/nsIJVMConfigManager.idl
 */

package org.mozilla.interfaces;

/**
 * This interface is a manager that can get information about Java
 * installations.
 */
public interface nsIJVMConfigManager extends nsISupports {

  String NS_IJVMCONFIGMANAGER_IID =
    "{ca29fff1-a677-493c-9d80-3dc60432212b}";

  /**
     * This function returns a list of existing Java installations.
     */
  nsIArray getJVMConfigList();

  /**
     * This function tells the browser to use a specific Java installation.
     */
  void setCurrentJVMConfig(nsIJVMConfig jvmConfig);

}