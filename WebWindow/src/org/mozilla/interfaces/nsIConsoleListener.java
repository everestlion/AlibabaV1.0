/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/base/nsIConsoleListener.idl
 */

package org.mozilla.interfaces;

public interface nsIConsoleListener extends nsISupports {

  String NS_ICONSOLELISTENER_IID =
    "{eaaf61d6-1dd1-11b2-bc6e-8fc96480f20d}";

  void observe(nsIConsoleMessage aMessage);

}