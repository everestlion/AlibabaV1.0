/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/base/nsIProgrammingLanguage.idl
 */

package org.mozilla.interfaces;

/**
 * Enumeration of Programming Languages
 * @status FROZEN
 */
public interface nsIProgrammingLanguage extends nsISupports {

  String NS_IPROGRAMMINGLANGUAGE_IID =
    "{ea604e90-40ba-11d5-90bb-0010a4e73d9a}";

  /**
     * Identifiers for programming languages.
     */
  long UNKNOWN = 0L;

  long CPLUSPLUS = 1L;

  long JAVASCRIPT = 2L;

  long PYTHON = 3L;

  long PERL = 4L;

  long JAVA = 5L;

  long ZX81_BASIC = 6L;

  long JAVASCRIPT2 = 7L;

  long RUBY = 8L;

  long PHP = 9L;

  long TCL = 10L;

  long MAX = 10L;

}