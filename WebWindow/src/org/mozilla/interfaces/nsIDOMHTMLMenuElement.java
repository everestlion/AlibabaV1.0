/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLMenuElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLMenuElement interface is the interface to a [X]HTML
 * menu element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLMenuElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLMENUELEMENT_IID =
    "{a6cf909d-15b3-11d2-932e-00805f8add32}";

  boolean getCompact();

  void setCompact(boolean aCompact);

}