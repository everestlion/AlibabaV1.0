/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/inspector/public/inISearchObserver.idl
 */

package org.mozilla.interfaces;

public interface inISearchObserver extends nsISupports {

  String INISEARCHOBSERVER_IID =
    "{46226d9b-e398-4106-8d9b-225d4d0589f5}";

  short IN_SUCCESS = 1;

  short IN_INTERRUPTED = 2;

  short IN_ERROR = 3;

  void onSearchStart(inISearchProcess aModule);

  void onSearchResult(inISearchProcess aModule);

  void onSearchEnd(inISearchProcess aModule, short aResult);

  void onSearchError(inISearchProcess aModule, String aMessage);

}