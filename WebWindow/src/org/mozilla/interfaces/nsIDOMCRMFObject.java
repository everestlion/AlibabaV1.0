/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMCRMFObject.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCRMFObject extends nsISupports {

  String NS_IDOMCRMFOBJECT_IID =
    "{16da46c0-208d-11d4-8a7c-006008c844c3}";

  String getRequest();

}