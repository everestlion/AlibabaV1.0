/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMDOMStringList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMDOMStringList extends nsISupports {

  String NS_IDOMDOMSTRINGLIST_IID =
    "{0bbae65c-1dde-11d9-8c46-000a95dc234c}";

  String item(long index);

  long getLength();

  boolean contains(String str);

}