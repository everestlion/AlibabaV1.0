/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/ls/nsIDOMLSLoadEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLSLoadEvent extends nsIDOMEvent {

  String NS_IDOMLSLOADEVENT_IID =
    "{6c16a810-a37d-4859-b557-337341631aee}";

  nsIDOMDocument getNewDocument();

  nsIDOMLSInput getInput();

}