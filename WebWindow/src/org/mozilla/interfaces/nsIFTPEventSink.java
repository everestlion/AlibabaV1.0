/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/protocol/ftp/public/nsIFTPChannel.idl
 */

package org.mozilla.interfaces;

/**
 * This interface may be defined as a notification callback on the FTP
 * channel.  It allows a consumer to receive a log of the FTP control
 * connection conversation.
 */
public interface nsIFTPEventSink extends nsISupports {

  String NS_IFTPEVENTSINK_IID =
    "{455d4234-0330-43d2-bbfb-99afbecbfeb0}";

  /**
     * XXX document this method!  (see bug 328915)
     */
  void onFTPControlLog(boolean server, String msg);

}