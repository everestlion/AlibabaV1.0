/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsIStringEnumerator.idl
 */

package org.mozilla.interfaces;

public interface nsIUTF8StringEnumerator extends nsISupports {

  String NS_IUTF8STRINGENUMERATOR_IID =
    "{9bdf1010-3695-4907-95ed-83d0410ec307}";

  boolean hasMore();

  String getNext();

}