/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGMetadataElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGMetadataElement extends nsIDOMSVGElement {

  String NS_IDOMSVGMETADATAELEMENT_IID =
    "{94558064-140c-41a1-9cc9-4e9cdbf5c124}";

}