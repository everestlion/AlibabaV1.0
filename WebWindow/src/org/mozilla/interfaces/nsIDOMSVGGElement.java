/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGGElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGGElement extends nsIDOMSVGElement {

  String NS_IDOMSVGGELEMENT_IID =
    "{0bd57cbd-a090-44aa-a61b-2fb876841194}";

}