/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/base/public/nsIDocumentEncoder.idl
 */

package org.mozilla.interfaces;

public interface nsIDocumentEncoderNodeFixup extends nsISupports {

  String NS_IDOCUMENTENCODERNODEFIXUP_IID =
    "{e770c650-b3d3-11da-a94d-0800200c9a66}";

  /**
   * Create a fixed up version of a node. This method is called before
   * each node in a document is about to be persisted. The implementor
   * may return a new node with fixed up attributes or null. If null is
   * returned the node should be used as-is.
   * @param aNode Node to fixup.
   * @return The resulting fixed up node.
   */
  nsIDOMNode fixupNode(nsIDOMNode aNode);

}