/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGTextPositionElem.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGTextPositioningElement extends nsIDOMSVGTextContentElement {

  String NS_IDOMSVGTEXTPOSITIONINGELEMENT_IID =
    "{5d052835-8cb0-442c-9754-a8e616db1f89}";

  nsIDOMSVGAnimatedLengthList getX();

  nsIDOMSVGAnimatedLengthList getY();

  nsIDOMSVGAnimatedLengthList getDx();

  nsIDOMSVGAnimatedLengthList getDy();

  nsIDOMSVGAnimatedNumberList getRotate();

}