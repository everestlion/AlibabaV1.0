/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/widget/public/nsIScreenManager.idl
 */

package org.mozilla.interfaces;

public interface nsIScreenManager extends nsISupports {

  String NS_ISCREENMANAGER_IID =
    "{e224bd44-252b-4b66-b869-99738250904a}";

  nsIScreen screenForRect(int left, int top, int width, int height);

  nsIScreen getPrimaryScreen();

  long getNumberOfScreens();

}