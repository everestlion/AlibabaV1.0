/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/editor/idl/nsIDocumentStateListener.idl
 */

package org.mozilla.interfaces;

public interface nsIDocumentStateListener extends nsISupports {

  String NS_IDOCUMENTSTATELISTENER_IID =
    "{050cdc00-3b8e-11d3-9ce4-a458f454fcbc}";

  void notifyDocumentCreated();

  void notifyDocumentWillBeDestroyed();

  void notifyDocumentStateChanged(boolean nowDirty);

}