/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLTableCaptionElem.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMHTMLTableCaptionElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLTABLECAPTIONELEMENT_IID =
    "{a6cf90b3-15b3-11d2-932e-00805f8add32}";

  /**
 * The nsIDOMHTMLTableCaptionElement interface is the interface to a
 * [X]HTML caption element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
  String getAlign();

  /**
 * The nsIDOMHTMLTableCaptionElement interface is the interface to a
 * [X]HTML caption element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
  void setAlign(String aAlign);

}