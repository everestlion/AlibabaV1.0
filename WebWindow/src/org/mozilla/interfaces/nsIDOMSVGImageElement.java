/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGImageElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGImageElement extends nsIDOMSVGElement {

  String NS_IDOMSVGIMAGEELEMENT_IID =
    "{43ae4efe-2610-4cce-8242-279e556a78fa}";

  nsIDOMSVGAnimatedLength getX();

  nsIDOMSVGAnimatedLength getY();

  nsIDOMSVGAnimatedLength getWidth();

  nsIDOMSVGAnimatedLength getHeight();

  nsIDOMSVGAnimatedPreserveAspectRatio getPreserveAspectRatio();

}