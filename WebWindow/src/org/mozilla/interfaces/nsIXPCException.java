/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpcexception.idl
 */

package org.mozilla.interfaces;

public interface nsIXPCException extends nsIException {

  String NS_IXPCEXCEPTION_IID =
    "{b2a34010-3983-11d3-9888-006008962422}";

  void initialize(String aMessage, long aResult, String aName, nsIStackFrame aLocation, nsISupports aData, nsIException aInner);

}