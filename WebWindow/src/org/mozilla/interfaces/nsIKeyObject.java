/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsIKeyModule.idl
 */

package org.mozilla.interfaces;

public interface nsIKeyObject extends nsISupports {

  String NS_IKEYOBJECT_IID =
    "{4b31f4ed-9424-4710-b946-79b7e33cf3a8}";

  short SYM_KEY = 1;

  short PRIVATE_KEY = 2;

  short PUBLIC_KEY = 3;

  short RC4 = 1;

  short AES_CBC = 2;

  short HMAC = 257;

  short getType();

}