/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedPathData.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedPathData extends nsISupports {

  String NS_IDOMSVGANIMATEDPATHDATA_IID =
    "{6ef2b400-dbf4-4c12-8787-fe15caac5648}";

  nsIDOMSVGPathSegList getPathSegList();

  nsIDOMSVGPathSegList getNormalizedPathSegList();

  nsIDOMSVGPathSegList getAnimatedPathSegList();

  nsIDOMSVGPathSegList getAnimatedNormalizedPathSegList();

}