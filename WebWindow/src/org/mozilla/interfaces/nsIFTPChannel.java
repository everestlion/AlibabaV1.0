/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/protocol/ftp/public/nsIFTPChannel.idl
 */

package org.mozilla.interfaces;

/**
 * This interface may be used to determine if a channel is a FTP channel.
 */
public interface nsIFTPChannel extends nsISupports {

  String NS_IFTPCHANNEL_IID =
    "{2315d831-8b40-446a-9138-fe09ebb1b720}";

}