/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/base/public/nsICryptoFIPSInfo.idl
 */

package org.mozilla.interfaces;

public interface nsICryptoFIPSInfo extends nsISupports {

  String NS_ICRYPTOFIPSINFO_IID =
    "{99e81922-7318-4431-b3aa-78b3cb4119bb}";

  boolean getIsFIPSModeActive();

}