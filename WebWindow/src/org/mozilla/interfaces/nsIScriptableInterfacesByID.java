/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/nsIScriptableInterfaces.idl
 */

package org.mozilla.interfaces;

/**
* interface of Components.interfacesByID
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIScriptableInterfacesByID extends nsISupports {

  String NS_ISCRIPTABLEINTERFACESBYID_IID =
    "{c99cffac-5aed-4267-ad2f-f4a4c9d4a081}";

}