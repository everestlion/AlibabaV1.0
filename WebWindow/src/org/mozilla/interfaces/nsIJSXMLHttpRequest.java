/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/base/public/nsIXMLHttpRequest.idl
 */

package org.mozilla.interfaces;

public interface nsIJSXMLHttpRequest extends nsISupports {

  String NS_IJSXMLHTTPREQUEST_IID =
    "{261676b4-d508-43bf-b099-74635a0ee2e9}";

  /**
   * Meant to be a script-only mechanism for setting a load event listener.
   * The attribute is expected to be JavaScript function object. When
   * the load event occurs, the function is invoked.
   * This attribute should not be used from native code!!
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?   
   *
   * Call open() before setting an onload listener.
   *
   * Mozilla only.
   */
  nsIDOMEventListener getOnload();

  /**
   * Meant to be a script-only mechanism for setting a load event listener.
   * The attribute is expected to be JavaScript function object. When
   * the load event occurs, the function is invoked.
   * This attribute should not be used from native code!!
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?   
   *
   * Call open() before setting an onload listener.
   *
   * Mozilla only.
   */
  void setOnload(nsIDOMEventListener aOnload);

  /**
   * Meant to be a script-only mechanism for setting an error event listener.
   * The attribute is expected to be JavaScript function object. When
   * the error event occurs, the function is invoked.
   * This attribute should not be used from native code!!
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?   
   *
   * Call open() before setting an onerror listener.
   *
   * Mozilla only.
   */
  nsIDOMEventListener getOnerror();

  /**
   * Meant to be a script-only mechanism for setting an error event listener.
   * The attribute is expected to be JavaScript function object. When
   * the error event occurs, the function is invoked.
   * This attribute should not be used from native code!!
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?   
   *
   * Call open() before setting an onerror listener.
   *
   * Mozilla only.
   */
  void setOnerror(nsIDOMEventListener aOnerror);

  /**
   * Meant to be a script-only mechanism for setting a progress event listener.
   * The attribute is expected to be JavaScript function object. When
   * the error event occurs, the function is invoked.
   * This attribute should not be used from native code!!
   * This event listener may be called multiple times during the open request.
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?
   *
   * This event listener must be set BEFORE calling open().
   *
   * Mozilla only.
   */
  nsIDOMEventListener getOnprogress();

  /**
   * Meant to be a script-only mechanism for setting a progress event listener.
   * The attribute is expected to be JavaScript function object. When
   * the error event occurs, the function is invoked.
   * This attribute should not be used from native code!!
   * This event listener may be called multiple times during the open request.
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?
   *
   * This event listener must be set BEFORE calling open().
   *
   * Mozilla only.
   */
  void setOnprogress(nsIDOMEventListener aOnprogress);

  /**
   * Meant to be a script-only mechanism for setting an upload progress event
   * listener.
   * This attribute should not be used from native code!!
   * This event listener may be called multiple times during the upload..
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?
   *
   * This event listener must be set BEFORE calling open().
   *
   * Mozilla only.
   */
  nsIDOMEventListener getOnuploadprogress();

  /**
   * Meant to be a script-only mechanism for setting an upload progress event
   * listener.
   * This attribute should not be used from native code!!
   * This event listener may be called multiple times during the upload..
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?
   *
   * This event listener must be set BEFORE calling open().
   *
   * Mozilla only.
   */
  void setOnuploadprogress(nsIDOMEventListener aOnuploadprogress);

  /**
   * Meant to be a script-only mechanism for setting a callback function.
   * The attribute is expected to be JavaScript function object. When the
   * readyState changes, the callback function will be called.
   * This attribute should not be used from native code!!
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?   
   *
   * Call open() before setting an onreadystatechange listener.
   */
  nsIDOMEventListener getOnreadystatechange();

  /**
   * Meant to be a script-only mechanism for setting a callback function.
   * The attribute is expected to be JavaScript function object. When the
   * readyState changes, the callback function will be called.
   * This attribute should not be used from native code!!
   *
   * After the initial response, all event listeners will be cleared.
   * // XXXbz what does that mean, exactly?   
   *
   * Call open() before setting an onreadystatechange listener.
   */
  void setOnreadystatechange(nsIDOMEventListener aOnreadystatechange);

}