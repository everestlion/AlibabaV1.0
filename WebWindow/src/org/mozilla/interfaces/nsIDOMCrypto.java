/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMCrypto.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCrypto extends nsISupports {

  String NS_IDOMCRYPTO_IID =
    "{d2b675a5-f05b-4172-bac2-24cc39ffd398}";

  String getVersion();

  boolean getEnableSmartCardEvents();

  void setEnableSmartCardEvents(boolean aEnableSmartCardEvents);

  nsIDOMCRMFObject generateCRMFRequest();

  String importUserCertificates(String nickname, String cmmfResponse, boolean doForcedBackup);

  String popChallengeResponse(String challenge);

  String random(int numBytes);

  String signText(String stringToSign, String caOption);

  void alert(String message);

  void logout();

  void disableRightClick();

}