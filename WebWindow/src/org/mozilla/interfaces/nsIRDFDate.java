/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/rdf/base/idl/nsIRDFLiteral.idl
 */

package org.mozilla.interfaces;

/**
 * A literal node in the graph, whose value is a date
 */
public interface nsIRDFDate extends nsIRDFNode {

  String NS_IRDFDATE_IID =
    "{e13a24e1-c77a-11d2-80be-006097b76b8e}";

  /**
     * The date value of the literal
     */
  long getValue();

}