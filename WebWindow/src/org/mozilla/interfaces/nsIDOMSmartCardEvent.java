/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/events/nsIDOMSmartCardEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSmartCardEvent extends nsIDOMEvent {

  String NS_IDOMSMARTCARDEVENT_IID =
    "{52bdc7ca-a934-4a40-a2e2-ac83a70b4019}";

  String getTokenName();

}