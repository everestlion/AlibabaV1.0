/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/jsd/idl/jsdIDebuggerService.idl
 */

package org.mozilla.interfaces;

public interface jsdIScriptEnumerator extends nsISupports {

  String JSDISCRIPTENUMERATOR_IID =
    "{4c2f706e-1dd2-11b2-9ebc-85a06e948830}";

  /**
 * Pass an instance of one of these to jsdIDebuggerService::enumerateScripts.
 */
/**
     * The enumerateScript method will be called once for every script the
     * debugger knows about.
     */
  void enumerateScript(jsdIScript script);

}