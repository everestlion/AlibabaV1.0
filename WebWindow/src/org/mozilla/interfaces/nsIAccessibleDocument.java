/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleDocument.idl
 */

package org.mozilla.interfaces;

/**
 * An interface for in-process accessibility clients
 * that wish to retrieve information about a document.
 * When accessibility is turned on in Gecko,
 * there is an nsIAccessibleDocument for each document
 * whether it is XUL, HTML or whatever.
 * You can QueryInterface to nsIAccessibleDocument from
 * the nsIAccessible or nsIAccessNode for the root node
 * of a document. You can also get one from 
 * nsIAccessNode::GetAccessibleDocument() or 
 * nsIAccessibleEvent::GetAccessibleDocument()
 *
 * @status UNDER_REVIEW
 */
public interface nsIAccessibleDocument extends nsISupports {

  String NS_IACCESSIBLEDOCUMENT_IID =
    "{b7ae45bd-21e9-4ed5-a67e-86448b25d56b}";

  /**
   * The URL of the document
   */
  String getURL();

  /**
   * The title of the document, as specified in the document.
   */
  String getTitle();

  /**
   * The mime type of the document
   */
  String getMimeType();

  /**
   * The doc type of the document, as specified in the document.
   */
  String getDocType();

  /**
   * The nsIDOMDocument interface associated with this document.
   */
  nsIDOMDocument getDocument();

  /**
   * The nsIDOMWindow that the document resides in.
   */
  nsIDOMWindow getWindow();

  /**
   * The namespace for each ID that is handed back.
   */
  String getNameSpaceURIForID(short nameSpaceID);

  /**
   * Returns the first accessible parent of a DOM node.
   * Guaranteed not to return nsnull if the DOM node is in a document.
   * @param aDOMNode The DOM node we need an accessible for.
   * @param aCanCreate Can accessibles be created or must it be the first 
   *                   cached accessible in the parent chain?
   * @return An first nsIAccessible found by crawling up the DOM node
   *         to the document root.
   */
  nsIAccessible getAccessibleInParentChain(nsIDOMNode aDOMNode, boolean aCanCreate);

}