/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpccomponents.idl
 */

package org.mozilla.interfaces;

/**
* interface of Components.classes
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIXPCComponents_Classes extends nsISupports {

  String NS_IXPCCOMPONENTS_CLASSES_IID =
    "{978ff520-d26c-11d2-9842-006008962422}";

}