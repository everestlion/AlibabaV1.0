/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGTSpanElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGTSpanElement extends nsIDOMSVGTextPositioningElement {

  String NS_IDOMSVGTSPANELEMENT_IID =
    "{4a23cb1f-cf1e-437e-9524-8756f7928b2e}";

}