/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/layout/printing/nsIPrintProgressParams.idl
 */

package org.mozilla.interfaces;

public interface nsIPrintProgressParams extends nsISupports {

  String NS_IPRINTPROGRESSPARAMS_IID =
    "{ca89b55b-6faf-4051-9645-1c03ef5108f8}";

  String getDocTitle();

  void setDocTitle(String aDocTitle);

  String getDocURL();

  void setDocURL(String aDocURL);

}