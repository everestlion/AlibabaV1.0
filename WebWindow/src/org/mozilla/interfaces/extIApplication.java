/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/exthelper/extIApplication.idl
 */

package org.mozilla.interfaces;

public interface extIApplication extends nsISupports {

  String EXTIAPPLICATION_IID =
    "{ba9442ee-7070-44fb-8157-c111e1fa70b6}";

  /**
   * The id of the application.
   */
  String getId();

  /**
   * The name of the application.
   */
  String getName();

  /**
   * The version number of the application.
   */
  String getVersion();

  /**
   * The console object for the application.
   */
  extIConsole getConsole();

  /**
   * The extensions object for the application. Contains a list
   * of all installed extensions.
   */
  extIExtensions getExtensions();

  /**
   * The preferences object for the application. Defaults to an empty
   * root branch.
   */
  extIPreferenceBranch getPrefs();

  /**
   * The storage object for the application.
   */
  extISessionStorage getStorage();

  /**
   * The events object for the application.
   * supports: "load", "ready", "quit", "unload"
   */
  extIEvents getEvents();

}