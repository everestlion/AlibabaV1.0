/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMNavigator.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNavigator extends nsISupports {

  String NS_IDOMNAVIGATOR_IID =
    "{8e150a70-3e51-45df-bee3-77505fbe016c}";

  String getAppCodeName();

  String getAppName();

  String getAppVersion();

  String getLanguage();

  nsIDOMMimeTypeArray getMimeTypes();

  String getPlatform();

  String getOscpu();

  String getVendor();

  String getVendorSub();

  String getProduct();

  String getProductSub();

  nsIDOMPluginArray getPlugins();

  String getSecurityPolicy();

  String getUserAgent();

  boolean getCookieEnabled();

  boolean getOnLine();

  String getBuildID();

  boolean javaEnabled();

  boolean taintEnabled();

}