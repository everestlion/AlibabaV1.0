/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpcjsid.idl
 */

package org.mozilla.interfaces;

public interface nsIJSCID extends nsIJSID {

  String NS_IJSCID_IID =
    "{26b2a374-6eaf-46d4-acaf-1c6be152d36b}";

  nsISupports createInstance();

  nsISupports getService();

}