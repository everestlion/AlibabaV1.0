/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsPIAccessNode.idl
 */

package org.mozilla.interfaces;

public interface nsPIAccessNode extends nsISupports {

  String NS_PIACCESSNODE_IID =
    "{b3507946-4a44-4e40-b66c-f23e320997c5}";

}