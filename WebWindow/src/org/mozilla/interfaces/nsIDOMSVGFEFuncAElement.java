/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEFuncAElement extends nsIDOMSVGComponentTransferFunctionElement {

  String NS_IDOMSVGFEFUNCAELEMENT_IID =
    "{fa48511c-283a-437f-9507-c309ac6f0f57}";

}