/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGLineElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGLineElement extends nsIDOMSVGElement {

  String NS_IDOMSVGLINEELEMENT_IID =
    "{4ea07ef3-ed66-4b41-8119-4afc6d0ed5af}";

  nsIDOMSVGAnimatedLength getX1();

  nsIDOMSVGAnimatedLength getY1();

  nsIDOMSVGAnimatedLength getX2();

  nsIDOMSVGAnimatedLength getY2();

}