/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/extensions/xml-rpc/idl/nsIXmlRpcClient.idl
 */

package org.mozilla.interfaces;

public interface nsIXmlRpcFault extends nsISupports {

  String NS_IXMLRPCFAULT_IID =
    "{691cb864-0a7e-448c-98ee-4a7f359cf145}";

  /**
 * An XML-RPC exception.
 * XML-RPC server fault codes are returned wrapped in this Access via
 * nsIXPConnect::GetPendingException()->data
 */
  int getFaultCode();

  String getFaultString();

  void init(int faultCode, String faultString);

  String toString();

}