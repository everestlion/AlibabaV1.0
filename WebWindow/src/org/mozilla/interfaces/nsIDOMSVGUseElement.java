/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGUseElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGUseElement extends nsIDOMSVGElement {

  String NS_IDOMSVGUSEELEMENT_IID =
    "{d49a3ac7-e779-46c8-ae92-214420aa1b71}";

  nsIDOMSVGAnimatedLength getX();

  nsIDOMSVGAnimatedLength getY();

  nsIDOMSVGAnimatedLength getWidth();

  nsIDOMSVGAnimatedLength getHeight();

}