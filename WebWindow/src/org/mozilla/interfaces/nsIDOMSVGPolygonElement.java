/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPolygonElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPolygonElement extends nsIDOMSVGElement {

  String NS_IDOMSVGPOLYGONELEMENT_IID =
    "{9de04775-77c5-48b5-9f4a-8996a936bfb2}";

}