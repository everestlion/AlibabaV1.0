/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLDirectoryElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLDirectoryElement interface is the interface to a
 * [X]HTML dir element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLDirectoryElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLDIRECTORYELEMENT_IID =
    "{a6cf909c-15b3-11d2-932e-00805f8add32}";

  boolean getCompact();

  void setCompact(boolean aCompact);

}