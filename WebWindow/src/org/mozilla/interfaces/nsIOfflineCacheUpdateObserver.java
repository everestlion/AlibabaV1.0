/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/uriloader/prefetch/nsIOfflineCacheUpdate.idl
 */

package org.mozilla.interfaces;

public interface nsIOfflineCacheUpdateObserver extends nsISupports {

  String NS_IOFFLINECACHEUPDATEOBSERVER_IID =
    "{0aa38757-999c-44d6-bdb4-7dd32634fa83}";

  /**
   * There was an error updating the cache.
   *
   * @param aUpdate
   *        The nsIOfflineCacheUpdate being processed.
   */
  void error(nsIOfflineCacheUpdate aUpdate);

  /**
   * The manifest is being checked for updates
   *
   * @param aUpdate
   *        The nsIOfflineCacheUpdate being processed.
   */
  void checking(nsIOfflineCacheUpdate aUpdate);

  /**
   * No update was necessary.
   *
   * @param aUpdate
   *        The nsIOfflineCacheUpdate being processed.
   */
  void noUpdate(nsIOfflineCacheUpdate aUpdate);

  /**
   * Starting to download resources
   *
   * @param aUpdate
   *        The nsIOfflineCacheUpdate being processed.
   */
  void downloading(nsIOfflineCacheUpdate aUpdate);

  /**
   * An item has started downloading.
   *
   * @param aUpdate
   *        The nsIOfflineCacheUpdate being processed.
   * @param aItem
   *        load status for the item that is being downloaded.
   */
  void itemStarted(nsIOfflineCacheUpdate aUpdate, nsIDOMLoadStatus aItem);

  /**
   * An item has finished loading.
   *
   * @param aUpdate
   *        The nsIOfflineCacheUpdate being processed.
   * @param aItem
   *         load status for the item that completed.
   */
  void itemCompleted(nsIOfflineCacheUpdate aUpdate, nsIDOMLoadStatus aItem);

}