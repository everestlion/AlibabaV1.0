/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/modules/plugin/base/public/nsIPluginManager.idl
 */

package org.mozilla.interfaces;

public interface nsIPluginManager extends nsISupports {

  String NS_IPLUGINMANAGER_IID =
    "{da58ad80-4eb6-11d2-8164-006008119d7a}";

  /**
     * Causes the plugins directory to be searched again for new plugin 
     * libraries.
     *
     * (Corresponds to NPN_ReloadPlugins.)
     *
     * @param reloadPages - indicates whether currently visible pages should 
     * also be reloaded
     */
  void reloadPlugins(boolean reloadPages);

}