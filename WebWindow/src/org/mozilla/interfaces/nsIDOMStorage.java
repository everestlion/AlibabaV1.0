/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/storage/nsIDOMStorage.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMStorage extends nsISupports {

  String NS_IDOMSTORAGE_IID =
    "{95cc1383-3b62-4b89-aaef-1004a513ef47}";

  /**
   * The number of keys stored.
   */
  long getLength();

  /**
   * Retrieve the name of the key at a particular index.
   *
   * @param index index of the item to retrieve
   * @returns the key at index
   * @throws INDEX_SIZE_ERR if there is no key at that index
   */
  String key(long index);

  /**
   * Retrieve an item with a given key
   *
   * @param key key to retrieve
   * @returns found item or null if the key was not found
   */
  nsIDOMStorageItem getItem(String key);

  /**
   * Assign a value with a key. If the key does not exist already, a new
   * key is added associated with that value. If the key already exists,
   * then the existing value is replaced with a new value.
   *
   * @param key key to set
   * @param data data to associate with the key
   * @returns found item or null if the key was not found
   */
  void setItem(String key, String data);

  /**
   * Remove a key and its corresponding value.
   *
   * @param key key to remove
   */
  void removeItem(String key);

}