/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleTable.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleTable extends nsISupports {

  String NS_IACCESSIBLETABLE_IID =
    "{dcc1e5c3-966e-45b2-b30a-839d35432b24}";

  nsIAccessible getCaption();

  String getSummary();

  int getColumns();

  nsIAccessibleTable getColumnHeader();

  int getRows();

  nsIAccessibleTable getRowHeader();

  /**
   * Returns the accessible object at the specified row and column in the table.
   * If both row and column index are valid then the corresponding accessible
   * object is returned that represents the requested cell regardless of whether
   * the cell is currently visible (on the screen).
   *
   * @param row - The row index for which to retrieve the cell.
   * @param column - The column index for which to retrieve the cell.
   */
  nsIAccessible cellRefAt(int row, int column);

  /**
   * Translates the given row and column indices into the corresponding cell
   * index.
   *
   * @param row - index of the row of the table for which to return the cell
   *              index.
   * @param column - index of the column of the table for which to return
   *                 the cell index.
   */
  int getIndexAt(int row, int column);

  /**
   * Translates the given child index into the corresponding column index.
   *
   * @param index - index of the child of the table for which to return
   *                the column index.
   */
  int getColumnAtIndex(int index);

  /**
   * Translates the given child index into the corresponding row index.
   *
   * @param index - index of the child of the table for which to return
   *                the row index.
   */
  int getRowAtIndex(int index);

  /**
   * Returns the number of columns occupied by the accessible object
   * at the specified row and column in the table. The result differs from 1
   * if the specified cell spans multiple columns.
   *
   * @param row - Row index of the accessible for which to return
   *              the column extent.
   * @param column - Column index of the accessible for which to return
   *                 the column extent.
   */
  int getColumnExtentAt(int row, int column);

  /**
   * Returns the number of rows occupied by the accessible oject
   * at the specified row and column in the table. The result differs from 1
   * if the specified cell spans multiple rows.
   *
   * @param row  - Row index of the accessible for which to return
   *               the column extent.
   * @param column - Column index of the accessible for which to return
   *                 the column extent.
   */
  int getRowExtentAt(int row, int column);

  /**
   * Returns the description text of the specified column in the table.
   * @param column - The index of the column for which to retrieve
   *                 the description.
   */
  String getColumnDescription(int column);

  /**
   * Returns the description text of the specified row in the table.
   * @param row - The index of the row for which to retrieve the description.
   */
  String getRowDescription(int row);

  /**
   * Returns a boolean value indicating whether the specified column is
   * completely selected.
   *
   * @param column  - Index of the column for which to determine whether it is
   *                   selected.
   */
  boolean isColumnSelected(int column);

  /**
   * Returns a boolean value indicating whether the specified row is completely
   * selected.
   *
   * @param row - Index of the row for which to determine whether it is
   *              selected.
   */
  boolean isRowSelected(int row);

  /**
   * Returns a boolean value indicating whether the specified cell is selected.
   *
   * @param row - Index of the row for the cell to determine whether it is
   *              selected.
   * @param column - Index of the column for the cell to determine whether it
   *                 is selected.
   */
  boolean isCellSelected(int row, int column);

  /**
   * Returns the total number of selected cells.
   */
  long getSelectedCellsCount();

  /**
   * Returns the total number of selected columns.
   */
  long getSelectedColumnsCount();

  /**
   * Returns the total number of selected rows.
   */
  long getSelectedRowsCount();

  /**
   * Returns a list of cells indexes currently selected.
   *
   * @param cellsSize - length of array
   * @param cells - array of indexes of selected cells
   */
  int[] getSelectedCells(long[] cellsSize);

  /**
   * Returns a list of column indexes currently selected.
   *
   * @param columnsSize - Length of array
   * @param columns - Array of indexes of selected columns
   */
  int[] getSelectedColumns(long[] columnsSize);

  /**
   * Returns a list of row indexes currently selected.
   *
   * @param rowsSize - Length of array
   * @param rows - Array of indexes of selected rows
   */
  int[] getSelectedRows(long[] rowsSize);

  /**
   * Selects a row and unselects all previously selected rows.
   *
   * @param row - Index of the row to be selected.
   */
  void selectRow(int row);

  /**
   * Selects a column and unselects all previously selected columns.
   *
   * @param column - Index of the column to be selected.
   */
  void selectColumn(int column);

  /**
   * Unselects one row, leaving other selected rows selected (if any).
   *
   * @param row  - Index of the row to be selected.
  */
  void unselectRow(int row);

  /**
   * Unselects one column, leaving other selected columns selected (if any).
   *
   * @param column - Index of the column to be selected.
   */
  void unselectColumn(int column);

  /**
   * Use heuristics to determine if table is most likely used for layout.
   */
  boolean isProbablyForLayout();

}