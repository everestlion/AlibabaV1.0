/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFETileElement extends nsIDOMSVGFilterPrimitiveStandardAttributes {

  String NS_IDOMSVGFETILEELEMENT_IID =
    "{ed042a81-39fc-4c89-9385-75758a2434b5}";

  nsIDOMSVGAnimatedString getIn1();

}