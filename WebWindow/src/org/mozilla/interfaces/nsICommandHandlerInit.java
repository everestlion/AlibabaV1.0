/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/embedding/browser/webBrowser/nsICommandHandler.idl
 */

package org.mozilla.interfaces;

public interface nsICommandHandlerInit extends nsISupports {

  String NS_ICOMMANDHANDLERINIT_IID =
    "{731c6c50-67d6-11d4-9529-0020183bf181}";

  nsIDOMWindow getWindow();

  void setWindow(nsIDOMWindow aWindow);

}