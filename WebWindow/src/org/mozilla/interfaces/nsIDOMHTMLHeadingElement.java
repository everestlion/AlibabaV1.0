/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLHeadingElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLHeadingElement interface is the interface to a
 * [X]HTML h1, h2, h3, ... element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLHeadingElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLHEADINGELEMENT_IID =
    "{a6cf90a2-15b3-11d2-932e-00805f8add32}";

  String getAlign();

  void setAlign(String aAlign);

}