/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGRectElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGRectElement extends nsIDOMSVGElement {

  String NS_IDOMSVGRECTELEMENT_IID =
    "{1695ca39-e40d-44dc-81db-a51b6fd234fa}";

  nsIDOMSVGAnimatedLength getX();

  nsIDOMSVGAnimatedLength getY();

  nsIDOMSVGAnimatedLength getWidth();

  nsIDOMSVGAnimatedLength getHeight();

  nsIDOMSVGAnimatedLength getRx();

  nsIDOMSVGAnimatedLength getRy();

}