/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xpath/nsIDOMXPathNamespace.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXPathNamespace extends nsIDOMNode {

  String NS_IDOMXPATHNAMESPACE_IID =
    "{75506f87-b504-11d5-a7f2-ca108ab8b6fc}";

  int XPATH_NAMESPACE_NODE = 13;

  nsIDOMElement getOwnerElement();

}