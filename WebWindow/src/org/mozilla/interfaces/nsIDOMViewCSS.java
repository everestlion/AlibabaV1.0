/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMViewCSS.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMViewCSS extends nsIDOMAbstractView {

  String NS_IDOMVIEWCSS_IID =
    "{0b9341f3-95d4-4fa4-adcd-e119e0db2889}";

  nsIDOMCSSStyleDeclaration getComputedStyle(nsIDOMElement elt, String pseudoElt);

}