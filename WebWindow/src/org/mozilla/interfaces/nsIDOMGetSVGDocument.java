/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMGetSVGDocument.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMGetSVGDocument extends nsISupports {

  String NS_IDOMGETSVGDOCUMENT_IID =
    "{0401f299-685b-43a1-82b4-ce1a0011598c}";

  nsIDOMSVGDocument getSVGDocument();

}