/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegLinetoVerticalRel extends nsISupports {

  String NS_IDOMSVGPATHSEGLINETOVERTICALREL_IID =
    "{d3ef2128-8de3-4aac-a6b4-13c7563119a6}";

  float getY();

  void setY(float aY);

}