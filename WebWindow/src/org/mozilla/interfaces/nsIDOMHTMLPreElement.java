/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLPreElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLPreElement interface is the interface to a [X]HTML
 * pre element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLPreElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLPREELEMENT_IID =
    "{a6cf90a4-15b3-11d2-932e-00805f8add32}";

  int getWidth();

  void setWidth(int aWidth);

}