/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/extensions/xml-rpc/idl/nsIDictionary.idl
 */

package org.mozilla.interfaces;

/**
 * A simple mutable table of objects, accessed by key.
 */
public interface nsIDictionary extends nsISupports {

  String NS_IDICTIONARY_IID =
    "{1dd0cb45-aea3-4a52-8b29-01429a542863}";

  /**
     * Check if a given key is present in the dictionary.
     *
     * @param key       Key to check for
     * @return          true if present, false if absent.
     */
  boolean hasKey(String key);

  /**
     * Retrieve all keys in the dictionary.
     *
     * @return          array of all keys, unsorted.
     */
  String[] getKeys(long[] count);

  /**
     * Find the value indicated by the key.
     *
     * @param key       The lookup key indicating the value.
     * @return          Value indicated by key. If the key doesn't exist,
     *                  NS_ERROR_FAILURE will be returned.
     */
  nsISupports getValue(String key);

  /**
     * Add the key-value pair to the dictionary.
     * If the key is already present, replace the old value
     * with the new.
     *
     * @param key       The key by which the value can be accessed
     * @param value     The value to be stored.
     */
  void setValue(String key, nsISupports value);

  /**
     * Delete the indicated key-value pair.
     *
     * @param key       The key indicating the pair to be removed.
     * @return          The removed value. If the key doesn't exist,
     *                  NS_ERROR_FAILURE will be returned.
     */
  nsISupports deleteValue(String key);

  /**
     * Delete all key-value pairs from the dictionary.
     */
  void clear();

}