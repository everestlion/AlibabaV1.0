/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMCSS2Properties.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCSS2Properties extends nsISupports {

  String NS_IDOMCSS2PROPERTIES_IID =
    "{529b987a-cb21-4d58-99d7-9586e7662801}";

  String getAzimuth();

  void setAzimuth(String aAzimuth);

  String getBackground();

  void setBackground(String aBackground);

  String getBackgroundAttachment();

  void setBackgroundAttachment(String aBackgroundAttachment);

  String getBackgroundColor();

  void setBackgroundColor(String aBackgroundColor);

  String getBackgroundImage();

  void setBackgroundImage(String aBackgroundImage);

  String getBackgroundPosition();

  void setBackgroundPosition(String aBackgroundPosition);

  String getBackgroundRepeat();

  void setBackgroundRepeat(String aBackgroundRepeat);

  String getBorder();

  void setBorder(String aBorder);

  String getBorderCollapse();

  void setBorderCollapse(String aBorderCollapse);

  String getBorderColor();

  void setBorderColor(String aBorderColor);

  String getBorderSpacing();

  void setBorderSpacing(String aBorderSpacing);

  String getBorderStyle();

  void setBorderStyle(String aBorderStyle);

  String getBorderTop();

  void setBorderTop(String aBorderTop);

  String getBorderRight();

  void setBorderRight(String aBorderRight);

  String getBorderBottom();

  void setBorderBottom(String aBorderBottom);

  String getBorderLeft();

  void setBorderLeft(String aBorderLeft);

  String getBorderTopColor();

  void setBorderTopColor(String aBorderTopColor);

  String getBorderRightColor();

  void setBorderRightColor(String aBorderRightColor);

  String getBorderBottomColor();

  void setBorderBottomColor(String aBorderBottomColor);

  String getBorderLeftColor();

  void setBorderLeftColor(String aBorderLeftColor);

  String getBorderTopStyle();

  void setBorderTopStyle(String aBorderTopStyle);

  String getBorderRightStyle();

  void setBorderRightStyle(String aBorderRightStyle);

  String getBorderBottomStyle();

  void setBorderBottomStyle(String aBorderBottomStyle);

  String getBorderLeftStyle();

  void setBorderLeftStyle(String aBorderLeftStyle);

  String getBorderTopWidth();

  void setBorderTopWidth(String aBorderTopWidth);

  String getBorderRightWidth();

  void setBorderRightWidth(String aBorderRightWidth);

  String getBorderBottomWidth();

  void setBorderBottomWidth(String aBorderBottomWidth);

  String getBorderLeftWidth();

  void setBorderLeftWidth(String aBorderLeftWidth);

  String getBorderWidth();

  void setBorderWidth(String aBorderWidth);

  String getBottom();

  void setBottom(String aBottom);

  String getCaptionSide();

  void setCaptionSide(String aCaptionSide);

  String getClear();

  void setClear(String aClear);

  String getClip();

  void setClip(String aClip);

  String getColor();

  void setColor(String aColor);

  String getContent();

  void setContent(String aContent);

  String getCounterIncrement();

  void setCounterIncrement(String aCounterIncrement);

  String getCounterReset();

  void setCounterReset(String aCounterReset);

  String getCue();

  void setCue(String aCue);

  String getCueAfter();

  void setCueAfter(String aCueAfter);

  String getCueBefore();

  void setCueBefore(String aCueBefore);

  String getCursor();

  void setCursor(String aCursor);

  String getDirection();

  void setDirection(String aDirection);

  String getDisplay();

  void setDisplay(String aDisplay);

  String getElevation();

  void setElevation(String aElevation);

  String getEmptyCells();

  void setEmptyCells(String aEmptyCells);

  String getCssFloat();

  void setCssFloat(String aCssFloat);

  String getFont();

  void setFont(String aFont);

  String getFontFamily();

  void setFontFamily(String aFontFamily);

  String getFontSize();

  void setFontSize(String aFontSize);

  String getFontSizeAdjust();

  void setFontSizeAdjust(String aFontSizeAdjust);

  String getFontStretch();

  void setFontStretch(String aFontStretch);

  String getFontStyle();

  void setFontStyle(String aFontStyle);

  String getFontVariant();

  void setFontVariant(String aFontVariant);

  String getFontWeight();

  void setFontWeight(String aFontWeight);

  String getHeight();

  void setHeight(String aHeight);

  String getLeft();

  void setLeft(String aLeft);

  String getLetterSpacing();

  void setLetterSpacing(String aLetterSpacing);

  String getLineHeight();

  void setLineHeight(String aLineHeight);

  String getListStyle();

  void setListStyle(String aListStyle);

  String getListStyleImage();

  void setListStyleImage(String aListStyleImage);

  String getListStylePosition();

  void setListStylePosition(String aListStylePosition);

  String getListStyleType();

  void setListStyleType(String aListStyleType);

  String getMargin();

  void setMargin(String aMargin);

  String getMarginTop();

  void setMarginTop(String aMarginTop);

  String getMarginRight();

  void setMarginRight(String aMarginRight);

  String getMarginBottom();

  void setMarginBottom(String aMarginBottom);

  String getMarginLeft();

  void setMarginLeft(String aMarginLeft);

  String getMarkerOffset();

  void setMarkerOffset(String aMarkerOffset);

  String getMarks();

  void setMarks(String aMarks);

  String getMaxHeight();

  void setMaxHeight(String aMaxHeight);

  String getMaxWidth();

  void setMaxWidth(String aMaxWidth);

  String getMinHeight();

  void setMinHeight(String aMinHeight);

  String getMinWidth();

  void setMinWidth(String aMinWidth);

  String getOrphans();

  void setOrphans(String aOrphans);

  String getOutline();

  void setOutline(String aOutline);

  String getOutlineColor();

  void setOutlineColor(String aOutlineColor);

  String getOutlineStyle();

  void setOutlineStyle(String aOutlineStyle);

  String getOutlineWidth();

  void setOutlineWidth(String aOutlineWidth);

  String getOverflow();

  void setOverflow(String aOverflow);

  String getPadding();

  void setPadding(String aPadding);

  String getPaddingTop();

  void setPaddingTop(String aPaddingTop);

  String getPaddingRight();

  void setPaddingRight(String aPaddingRight);

  String getPaddingBottom();

  void setPaddingBottom(String aPaddingBottom);

  String getPaddingLeft();

  void setPaddingLeft(String aPaddingLeft);

  String getPage();

  void setPage(String aPage);

  String getPageBreakAfter();

  void setPageBreakAfter(String aPageBreakAfter);

  String getPageBreakBefore();

  void setPageBreakBefore(String aPageBreakBefore);

  String getPageBreakInside();

  void setPageBreakInside(String aPageBreakInside);

  String getPause();

  void setPause(String aPause);

  String getPauseAfter();

  void setPauseAfter(String aPauseAfter);

  String getPauseBefore();

  void setPauseBefore(String aPauseBefore);

  String getPitch();

  void setPitch(String aPitch);

  String getPitchRange();

  void setPitchRange(String aPitchRange);

  String getPosition();

  void setPosition(String aPosition);

  String getQuotes();

  void setQuotes(String aQuotes);

  String getRichness();

  void setRichness(String aRichness);

  String getRight();

  void setRight(String aRight);

  String getSize();

  void setSize(String aSize);

  String getSpeak();

  void setSpeak(String aSpeak);

  String getSpeakHeader();

  void setSpeakHeader(String aSpeakHeader);

  String getSpeakNumeral();

  void setSpeakNumeral(String aSpeakNumeral);

  String getSpeakPunctuation();

  void setSpeakPunctuation(String aSpeakPunctuation);

  String getSpeechRate();

  void setSpeechRate(String aSpeechRate);

  String getStress();

  void setStress(String aStress);

  String getTableLayout();

  void setTableLayout(String aTableLayout);

  String getTextAlign();

  void setTextAlign(String aTextAlign);

  String getTextDecoration();

  void setTextDecoration(String aTextDecoration);

  String getTextIndent();

  void setTextIndent(String aTextIndent);

  String getTextShadow();

  void setTextShadow(String aTextShadow);

  String getTextTransform();

  void setTextTransform(String aTextTransform);

  String getTop();

  void setTop(String aTop);

  String getUnicodeBidi();

  void setUnicodeBidi(String aUnicodeBidi);

  String getVerticalAlign();

  void setVerticalAlign(String aVerticalAlign);

  String getVisibility();

  void setVisibility(String aVisibility);

  String getVoiceFamily();

  void setVoiceFamily(String aVoiceFamily);

  String getVolume();

  void setVolume(String aVolume);

  String getWhiteSpace();

  void setWhiteSpace(String aWhiteSpace);

  String getWidows();

  void setWidows(String aWidows);

  String getWidth();

  void setWidth(String aWidth);

  String getWordSpacing();

  void setWordSpacing(String aWordSpacing);

  String getZIndex();

  void setZIndex(String aZIndex);

}