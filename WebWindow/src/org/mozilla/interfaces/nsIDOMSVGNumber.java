/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGNumber.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGNumber extends nsISupports {

  String NS_IDOMSVGNUMBER_IID =
    "{98575762-a936-4ecf-a226-b74c3a2981b4}";

  float getValue();

  void setValue(float aValue);

}