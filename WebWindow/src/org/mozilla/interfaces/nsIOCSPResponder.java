/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsIOCSPResponder.idl
 */

package org.mozilla.interfaces;

public interface nsIOCSPResponder extends nsISupports {

  String NS_IOCSPRESPONDER_IID =
    "{96b2f5ae-4334-11d5-ba27-00108303b117}";

  String getResponseSigner();

  String getServiceURL();

}