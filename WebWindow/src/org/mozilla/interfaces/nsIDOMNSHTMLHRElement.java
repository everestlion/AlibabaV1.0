/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLHRElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMNSHTMLHRElement interface contains extensions to the
 * interface for [X]HTML hr elements, for compatibility with IE.
 */
public interface nsIDOMNSHTMLHRElement extends nsISupports {

  String NS_IDOMNSHTMLHRELEMENT_IID =
    "{19b5879f-c125-447c-aaaf-719de3ef221a}";

  String getColor();

  void setColor(String aColor);

}