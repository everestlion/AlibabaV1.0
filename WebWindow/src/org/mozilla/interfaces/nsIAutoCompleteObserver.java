/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/autocomplete/public/nsIAutoCompleteSearch.idl
 */

package org.mozilla.interfaces;

public interface nsIAutoCompleteObserver extends nsISupports {

  String NS_IAUTOCOMPLETEOBSERVER_IID =
    "{18c36504-9a4c-4ac3-8494-bd05e00ae27f}";

  void onSearchResult(nsIAutoCompleteSearch search, nsIAutoCompleteResult result);

}