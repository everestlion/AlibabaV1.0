/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMChromeWindow.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMChromeWindow extends nsISupports {

  String NS_IDOMCHROMEWINDOW_IID =
    "{847fac33-48a8-4360-bfbc-6bb96245404d}";

  int STATE_MAXIMIZED = 1;

  int STATE_MINIMIZED = 2;

  int STATE_NORMAL = 3;

  String getTitle();

  void setTitle(String aTitle);

  int getWindowState();

  /**
   * browserDOMWindow provides access to yet another layer of
   * utility functions implemented by chrome script. It will be null
   * for DOMWindows not corresponding to browsers.
   */
  nsIBrowserDOMWindow getBrowserDOMWindow();

  /**
   * browserDOMWindow provides access to yet another layer of
   * utility functions implemented by chrome script. It will be null
   * for DOMWindows not corresponding to browsers.
   */
  void setBrowserDOMWindow(nsIBrowserDOMWindow aBrowserDOMWindow);

  void getAttention();

  void getAttentionWithCycleCount(int aCycleCount);

  void setCursor(String cursor);

  void maximize();

  void minimize();

  void restore();

}