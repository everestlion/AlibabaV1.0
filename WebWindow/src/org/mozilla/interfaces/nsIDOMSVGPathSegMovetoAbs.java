/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegMovetoAbs extends nsISupports {

  String NS_IDOMSVGPATHSEGMOVETOABS_IID =
    "{30cf7749-bf1f-4f9c-9558-8ee24da3a22c}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

}