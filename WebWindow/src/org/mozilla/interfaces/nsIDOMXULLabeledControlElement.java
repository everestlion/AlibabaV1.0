/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULLabeledControlEl.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULLabeledControlElement extends nsIDOMXULControlElement {

  String NS_IDOMXULLABELEDCONTROLELEMENT_IID =
    "{a457ea70-1dd1-11b2-9089-8fd894122084}";

  String getCrop();

  void setCrop(String aCrop);

  String getImage();

  void setImage(String aImage);

  String getLabel();

  void setLabel(String aLabel);

  String getAccessKey();

  void setAccessKey(String aAccessKey);

  String getCommand();

  void setCommand(String aCommand);

}