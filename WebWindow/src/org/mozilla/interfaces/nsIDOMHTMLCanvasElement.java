/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLCanvasElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLCanvasElement interface is the interface to a HTML
 * <canvas> element.
 *
 * For more information on this interface, please see
 * http://www.whatwg.org/specs/web-apps/current-work/#graphics
 *
 * @status UNDER_DEVELOPMENT
 */
public interface nsIDOMHTMLCanvasElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLCANVASELEMENT_IID =
    "{0583a2ea-ab19-40e1-8be4-5e9b2f275560}";

  int getWidth();

  void setWidth(int aWidth);

  int getHeight();

  void setHeight(int aHeight);

  nsISupports getContext(String contextId);

  String toDataURL();

}