/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleEvent.idl
 */

package org.mozilla.interfaces;

/**
 * An interface for accessibility events listened to
 * by in-process accessibility clients, which can be used
 * to find out how to get accessibility and DOM interfaces for
 * the event and its target. To listen to in-process accessibility invents,
 * make your object an nsIObserver, and listen for accessible-event by 
 * using code something like this:
 *   nsCOMPtr<nsIObserverService> observerService = 
 *     do_GetService("@mozilla.org/observer-service;1", &rv);
 *   if (NS_SUCCEEDED(rv)) 
 *     rv = observerService->AddObserver(this, "accessible-event", PR_TRUE);
 *
 * @status UNDER_REVIEW
 */
public interface nsIAccessibleEvent extends nsISupports {

  String NS_IACCESSIBLEEVENT_IID =
    "{ba448f0e-a761-48c8-a0f5-1f25e23d4fe4}";

  /**
   * An object has been created.
   */
  long EVENT_DOM_CREATE = 1L;

  /**
   * An object has been destroyed.
   */
  long EVENT_DOM_DESTROY = 2L;

  /**
   * An object's properties or content have changed significantly so that the
   * type of object has really changed, and therefore the accessible should be 
   * destroyed or recreated.
   */
  long EVENT_DOM_SIGNIFICANT_CHANGE = 3L;

  /**
   * A hidden object is shown -- this is a layout occurance and is thus asynchronous
   */
  long EVENT_ASYNCH_SHOW = 4L;

  /**
   * An object is hidden -- this is a layout occurance and is thus asynchronous
   */
  long EVENT_ASYNCH_HIDE = 5L;

  /**
   * An object had a significant layout change which could affect
   * the type of accessible object -- this is a layout occurance and is thus asynchronous
   */
  long EVENT_ASYNCH_SIGNIFICANT_CHANGE = 6L;

  /**
   * The active descendant of a component has changed. The active descendant
   * is used in objects with transient children.
   */
  long EVENT_ACTIVE_DECENDENT_CHANGED = 7L;

  /**
   * An object has received the keyboard focus.
   */
  long EVENT_FOCUS = 8L;

  /**
   * An object's state has changed.
   */
  long EVENT_STATE_CHANGE = 9L;

  /**
   * An object has changed location, shape, or size.
   */
  long EVENT_LOCATION_CHANGE = 10L;

  /**
   * An object's Name property has changed.
   */
  long EVENT_NAME_CHANGE = 11L;

  /**
   * An object's Description property has changed.
   */
  long EVENT_DESCRIPTION_CHANGE = 12L;

  /**
   * An object's Value property has changed.
   */
  long EVENT_VALUE_CHANGE = 13L;

  /**
   * An object's help has changed.
   */
  long EVENT_HELP_CHANGE = 14L;

  /**
   * An object's default action has changed.
   */
  long EVENT_DEFACTION_CHANGE = 15L;

  /**
   * An object's action has changed.
   */
  long EVENT_ACTION_CHANGE = 16L;

  /**
   * An object's keyboard shortcut has changed.
   */
  long EVENT_ACCELERATOR_CHANGE = 17L;

  /**
   * The selection within a container object has changed.
   */
  long EVENT_SELECTION = 18L;

  /**
   * An item within a container object has been added to the selection.
   */
  long EVENT_SELECTION_ADD = 19L;

  /**
   * An item within a container object has been removed from the selection.
   */
  long EVENT_SELECTION_REMOVE = 20L;

  /**
   * Numerous selection changes have occurred within a container object.
   */
  long EVENT_SELECTION_WITHIN = 21L;

  /**
   * An alert has been generated. Server applications send this event when a
   * user needs to know that a user interface element has changed.
   */
  long EVENT_ALERT = 22L;

  /**
   * The foreground window has changed.
   */
  long EVENT_FOREGROUND = 23L;

  /**
   * A menu item on the menu bar has been selected.
   */
  long EVENT_MENU_START = 24L;

  /**
   * A menu from the menu bar has been closed.
   */
  long EVENT_MENU_END = 25L;

  /**
   * A pop-up menu has been displayed.
   */
  long EVENT_MENUPOPUP_START = 26L;

  /**
   * A pop-up menu has been closed.
   */
  long EVENT_MENUPOPUP_END = 27L;

  /**
   * A window has received mouse capture.
   */
  long EVENT_CAPTURE_START = 28L;

  /**
   * A window has lost mouse capture.
   */
  long EVENT_CAPTURE_END = 29L;

  /**
   * A window is being moved or resized.
   */
  long EVENT_MOVESIZE_START = 30L;

  /**
  * The movement or resizing of a window has finished
  */
  long EVENT_MOVESIZE_END = 31L;

  /**
   * A window has entered context-sensitive Help mode
   */
  long EVENT_CONTEXTHELP_START = 32L;

  /**
   * A window has exited context-sensitive Help mode
   */
  long EVENT_CONTEXTHELP_END = 33L;

  /**
   * An application is about to enter drag-and-drop mode
   */
  long EVENT_DRAGDROP_START = 34L;

  /**
   * An application is about to exit drag-and-drop mode
   */
  long EVENT_DRAGDROP_END = 35L;

  /**
   * A dialog box has been displayed
   */
  long EVENT_DIALOG_START = 36L;

  /**
   * A dialog box has been closed
   */
  long EVENT_DIALOG_END = 37L;

  /**
   * Scrolling has started on a scroll bar
   */
  long EVENT_SCROLLING_START = 38L;

  /**
   * Scrolling has ended on a scroll bar
   */
  long EVENT_SCROLLING_END = 39L;

  /**
   * A window object is about to be minimized or maximized
   */
  long EVENT_MINIMIZE_START = 40L;

  /**
   * A window object has been minimized or maximized
   */
  long EVENT_MINIMIZE_END = 41L;

  /**
   * XXX:
   */
  long EVENT_DOCUMENT_LOAD_START = 42L;

  /**
   * The loading of the document has completed.
   */
  long EVENT_DOCUMENT_LOAD_COMPLETE = 43L;

  /**
   * The document contents are being reloaded.
   */
  long EVENT_DOCUMENT_RELOAD = 44L;

  /**
   * The loading of the document was interrupted.
   */
  long EVENT_DOCUMENT_LOAD_STOPPED = 45L;

  /**
   * The document wide attributes of the document object have changed.
   */
  long EVENT_DOCUMENT_ATTRIBUTES_CHANGED = 46L;

  /**
   * The contents of the document have changed.
   */
  long EVENT_DOCUMENT_CONTENT_CHANGED = 47L;

  long EVENT_PROPERTY_CHANGED = 48L;

  long EVENT_SELECTION_CHANGED = 49L;

  /**
   * A text object's attributes changed.
   * Also see EVENT_OBJECT_ATTRIBUTE_CHANGED.
   */
  long EVENT_TEXT_ATTRIBUTE_CHANGED = 50L;

  /**
   * The caret has moved to a new position.
   */
  long EVENT_TEXT_CARET_MOVED = 51L;

  /**
   * This event indicates general text changes, i.e. changes to text that is
   * exposed through the IAccessibleText and IAccessibleEditableText interfaces.
   */
  long EVENT_TEXT_CHANGED = 52L;

  /**
   * Text was inserted.
   */
  long EVENT_TEXT_INSERTED = 53L;

  /**
   * Text was removed.
   */
  long EVENT_TEXT_REMOVED = 54L;

  /**
   * Text was updated.
   */
  long EVENT_TEXT_UPDATED = 55L;

  /**
   * The text selection changed.
   */
  long EVENT_TEXT_SELECTION_CHANGED = 56L;

  /**
   * A visibile data event indicates the change of the visual appearance
   * of an accessible object.  This includes for example most of the
   * attributes available via the IAccessibleComponent interface.
   */
  long EVENT_VISIBLE_DATA_CHANGED = 57L;

  /**
   * The caret moved from one column to the next.
   */
  long EVENT_TEXT_COLUMN_CHANGED = 58L;

  /**
   * The caret moved from one section to the next.
   */
  long EVENT_SECTION_CHANGED = 59L;

  /**
   * A table caption changed.
   */
  long EVENT_TABLE_CAPTION_CHANGED = 60L;

  /**
   * A table's data changed.
   */
  long EVENT_TABLE_MODEL_CHANGED = 61L;

  /**
   * A table's summary changed.
   */
  long EVENT_TABLE_SUMMARY_CHANGED = 62L;

  /**
   * A table's row description changed.
   */
  long EVENT_TABLE_ROW_DESCRIPTION_CHANGED = 63L;

  /**
   * A table's row header changed.
   */
  long EVENT_TABLE_ROW_HEADER_CHANGED = 64L;

  long EVENT_TABLE_ROW_INSERT = 65L;

  long EVENT_TABLE_ROW_DELETE = 66L;

  long EVENT_TABLE_ROW_REORDER = 67L;

  /**
   * A table's column description changed.
   */
  long EVENT_TABLE_COLUMN_DESCRIPTION_CHANGED = 68L;

  /**
   * A table's column header changed.
   */
  long EVENT_TABLE_COLUMN_HEADER_CHANGED = 69L;

  long EVENT_TABLE_COLUMN_INSERT = 70L;

  long EVENT_TABLE_COLUMN_DELETE = 71L;

  long EVENT_TABLE_COLUMN_REORDER = 72L;

  long EVENT_WINDOW_ACTIVATE = 73L;

  long EVENT_WINDOW_CREATE = 74L;

  long EVENT_WINDOW_DEACTIVATE = 75L;

  long EVENT_WINDOW_DESTROY = 76L;

  long EVENT_WINDOW_MAXIMIZE = 77L;

  long EVENT_WINDOW_MINIMIZE = 78L;

  long EVENT_WINDOW_RESIZE = 79L;

  long EVENT_WINDOW_RESTORE = 80L;

  /**
   * The ending index of this link within the containing string has changed.
   */
  long EVENT_HYPERLINK_END_INDEX_CHANGED = 81L;

  /**
   * The number of anchors assoicated with this hyperlink object has changed.
   */
  long EVENT_HYPERLINK_NUMBER_OF_ANCHORS_CHANGED = 82L;

  /**
   * The hyperlink selected state changed from selected to unselected or
   * from unselected to selected.
   */
  long EVENT_HYPERLINK_SELECTED_LINK_CHANGED = 83L;

  /**
   * One of the links associated with the hypertext object has been activated.
   */
  long EVENT_HYPERTEXT_LINK_ACTIVATED = 84L;

  /**
   * One of the links associated with the hypertext object has been selected.
   */
  long EVENT_HYPERTEXT_LINK_SELECTED = 85L;

  /**
   * The starting index of this link within the containing string has changed.
   */
  long EVENT_HYPERLINK_START_INDEX_CHANGED = 86L;

  /**
   * Focus has changed from one hypertext object to another, or focus moved
   * from a non-hypertext object to a hypertext object, or focus moved from a
   * hypertext object to a non-hypertext object.
   */
  long EVENT_HYPERTEXT_CHANGED = 87L;

  /**
   * The number of hyperlinks associated with a hypertext object changed.
   */
  long EVENT_HYPERTEXT_NLINKS_CHANGED = 88L;

  /**
   * An object's attributes changed. Also see EVENT_TEXT_ATTRIBUTE_CHANGED.
   */
  long EVENT_OBJECT_ATTRIBUTE_CHANGED = 89L;

  /**
   * A slide changed in a presentation document or a page boundary was
   * crossed in a word processing document.
   */
  long EVENT_PAGE_CHANGED = 90L;

  /**
   * Used internally in Gecko.
   */
  long EVENT_INTERNAL_LOAD = 91L;

  /**
   * An object's children have changed
   */
  long EVENT_REORDER = 92L;

  /**
   * Help make sure event map does not get out-of-line.
   */
  long EVENT_LAST_ENTRY = 93L;

  /**
   * The type of event, based on the enumerated event values
   * defined in this interface.
   */
  long getEventType();

  /**
   * The nsIAccessible associated with the event.
   * May return null if no accessible is available
   */
  nsIAccessible getAccessible();

  /**
   * The nsIAccessibleDocument that the event target nsIAccessible
   * resides in. This can be used to get the DOM window,
   * the DOM document and the window handler, among other things.
   */
  nsIAccessibleDocument getAccessibleDocument();

  /**
   * The nsIDOMNode associated with the event
   * May return null if accessible for event has been shut down
   */
  nsIDOMNode getDOMNode();

  /**
   * Returns true if the event was caused by explicit user input,
   * as opposed to purely originating from a timer or mouse movement
   */
  boolean getIsFromUserInput();

  /**
   * Returns true if the event was caused by explicit user input,
   * as opposed to purely originating from a timer or mouse movement
   */
  void setIsFromUserInput(boolean aIsFromUserInput);

}