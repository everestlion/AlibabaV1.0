/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMHistory.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMHistory extends nsISupports {

  String NS_IDOMHISTORY_IID =
    "{896d1d20-b4c4-11d2-bd93-00805f8ae3f4}";

  int getLength();

  String getCurrent();

  String getPrevious();

  String getNext();

  void back();

  void forward();

  String item(long index);

}