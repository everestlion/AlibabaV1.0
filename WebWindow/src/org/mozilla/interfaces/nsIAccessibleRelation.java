/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleRelation.idl
 */

package org.mozilla.interfaces;

/**
 * This interface gives access to an accessible's set of relations.
 * Be carefull, do not change constants until ATK has a structure to map gecko
 * constants into ATK constants.
 */
public interface nsIAccessibleRelation extends nsISupports {

  String NS_IACCESSIBLERELATION_IID =
    "{f42a1589-70ab-4704-877f-4a9162bbe188}";

  long RELATION_NUL = 0L;

  /**
   * Some attribute of this object is affected by a target object.
   */
  long RELATION_CONTROLLED_BY = 1L;

  long RELATION_FIRST = 1L;

  /**
   * This object is interactive and controls some attribute of a target object.
   */
  long RELATION_CONTROLLER_FOR = 2L;

  /**
   * This object is label for a target object.
   */
  long RELATION_LABEL_FOR = 3L;

  /**
   * This object is labelled by a target object.
   */
  long RELATION_LABELLED_BY = 4L;

  /**
   * This object is a member of a group of one or more objects. When there is
   * more than one object in the group each member may have one and the same
   * target, e.g. a grouping object.  It is also possible that each member has
   * multiple additional targets, e.g. one for every other member in the group.
   */
  long RELATION_MEMBER_OF = 5L;

  /**
   * This object is a child of a target object.
   */
  long RELATION_NODE_CHILD_OF = 6L;

  /**
   * Content flows from this object to a target object, i.e. has content that
   * flows logically to another object in a sequential way, e.g. text flow.
   */
  long RELATION_FLOWS_TO = 7L;

  /**
   * Content flows to this object from a target object, i.e. has content that
   * flows logically from another object in a sequential way, e.g. text flow.
   */
  long RELATION_FLOWS_FROM = 8L;

  /**
   * This object is a sub window of a target object.
   */
  long RELATION_SUBWINDOW_OF = 9L;

  /**
   * This object embeds a target object. This relation can be used on the
   * OBJID_CLIENT accessible for a top level window to show where the content
   * areas are.
   */
  long RELATION_EMBEDS = 10L;

  /**
   * This object is embedded by a target object.
   */
  long RELATION_EMBEDDED_BY = 11L;

  /**
   * This object is a transient component related to the target object. When
   * this object is activated the target object doesn't loose focus.
   */
  long RELATION_POPUP_FOR = 12L;

  /**
   * This object is a parent window of the target object.
   */
  long RELATION_PARENT_WINDOW_OF = 13L;

  /**
   * This object is described by the target object.
   */
  long RELATION_DESCRIBED_BY = 14L;

  /**
   * This object is describes the target object.
   */
  long RELATION_DESCRIPTION_FOR = 15L;

  long RELATION_LAST = 15L;

  /**
   * Part of a form/dialog with a related default button. It is used for
   * MSAA only, no for IA2 nor ATK.
   */
  long RELATION_DEFAULT_BUTTON = 16384L;

  /**
   * Returns the type of the relation.
   */
  long getRelationType();

  /**
   * Returns the number of targets for this relation.
   */
  long getTargetsCount();

  /**
   * Returns one accessible relation target.
   * @param index - 0 based index of relation target.
   */
  nsIAccessible getTarget(long index);

  /**
   * Returns multiple accessible relation targets.
   */
  nsIArray getTargets();

}