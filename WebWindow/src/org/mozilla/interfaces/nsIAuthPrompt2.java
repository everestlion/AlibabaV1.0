/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/netwerk/base/public/nsIAuthPrompt2.idl
 */

package org.mozilla.interfaces;

/**
 * An interface allowing to prompt for a username and password. This interface
 * is usually acquired using getInterface on notification callbacks or similar.
 * It can be used to prompt users for authentication information, either
 * synchronously or asynchronously.
 */
public interface nsIAuthPrompt2 extends nsISupports {

  String NS_IAUTHPROMPT2_IID =
    "{447fc780-1d28-412a-91a1-466d48129c65}";

  /** @name Security Levels */
/**
   * The password will be sent unencrypted. No security provided.
   */
  long LEVEL_NONE = 0L;

  /**
   * Password will be sent encrypted, but the connection is otherwise
   * insecure.
   */
  long LEVEL_PW_ENCRYPTED = 1L;

  /**
   * The connection, both for password and data, is secure.
   */
  long LEVEL_SECURE = 2L;

  /**
   * Requests a username and a password. Implementations will commonly show a
   * dialog with a username and password field, depending on flags also a
   * domain field.
   *
   * @param aChannel
   *        The channel that requires authentication.
   * @param level
   *        One of the level constants from above. See there for descriptions
   *        of the levels.
   * @param authInfo
   *        Authentication information object. The implementation should fill in
   *        this object with the information entered by the user before
   *        returning.
   *
   * @retval true
   *         Authentication can proceed using the values in the authInfo
   *         object.
   * @retval false
   *         Authentication should be cancelled, usually because the user did
   *         not provide username/password.
   *
   * @note   Exceptions thrown from this function will be treated like a
   *         return value of false.
   */
  boolean promptAuth(nsIChannel aChannel, long level, nsIAuthInformation authInfo);

  /**
   * Asynchronously prompt the user for a username and password.
   * This has largely the same semantics as promptUsernameAndPassword(),
   * but must return immediately after calling and return the entered
   * data in a callback.
   *
   * If the user closes the dialog using a cancel button or similar,
   * the callback's nsIAuthPromptCallback::onAuthCancelled method must be
   * called.
   * Calling nsICancelable::cancel on the returned object SHOULD close the
   * dialog and MUST call nsIAuthPromptCallback::onAuthCancelled on the provided
   * callback.
   *
   * @throw NS_ERROR_NOT_IMPLEMENTED
   *        Asynchronous authentication prompts are not supported;
   *        the caller should fall back to promptUsernameAndPassword().
   */
  nsICancelable asyncPromptAuth(nsIChannel aChannel, nsIAuthPromptCallback aCallback, nsISupports aContext, long level, nsIAuthInformation authInfo);

}