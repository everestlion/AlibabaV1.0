/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/threads/nsIRunnable.idl
 */

package org.mozilla.interfaces;

public interface nsIRunnable extends nsISupports {

  String NS_IRUNNABLE_IID =
    "{4a2abaf0-6886-11d3-9382-00104ba0fd40}";

  void run();

}