/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLTitleElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLTitleElement interface is the interface to a [X]HTML
 * title element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLTitleElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLTITLEELEMENT_IID =
    "{a6cf9089-15b3-11d2-932e-00805f8add32}";

  String getText();

  void setText(String aText);

}