/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULLabelElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULLabelElement extends nsIDOMXULDescriptionElement {

  String NS_IDOMXULLABELELEMENT_IID =
    "{c987629e-6370-45f5-86ec-aa765fa861cd}";

  String getAccessKey();

  void setAccessKey(String aAccessKey);

  String getControl();

  void setControl(String aControl);

}