/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpccomponents.idl
 */

package org.mozilla.interfaces;

/**
* interface of Components.ID
* (interesting stuff only reflected into JavaScript)
*/
public interface nsIXPCComponents_ID extends nsISupports {

  String NS_IXPCCOMPONENTS_ID_IID =
    "{7994a6e0-e028-11d3-8f5d-0010a4e73d9a}";

}