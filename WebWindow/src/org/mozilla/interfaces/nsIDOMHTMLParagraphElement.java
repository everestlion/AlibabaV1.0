/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLParagraphElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLParagraphElement interface is the interface to a
 * [X]HTML p element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLParagraphElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLPARAGRAPHELEMENT_IID =
    "{a6cf90a1-15b3-11d2-932e-00805f8add32}";

  String getAlign();

  void setAlign(String aAlign);

}