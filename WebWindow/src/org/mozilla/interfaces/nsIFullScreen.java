/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/widget/public/nsIFullScreen.idl
 */

package org.mozilla.interfaces;

public interface nsIFullScreen extends nsISupports {

  String NS_IFULLSCREEN_IID =
    "{9854976e-1dd1-11b2-8350-e6d35099fbce}";

  /**
   * Hide all registered OS chrome components
   */
  void hideAllOSChrome();

  /**
   * Show all registered OS chrome components
   */
  void showAllOSChrome();

  /**
   * Return an enumerator of all registered OS chrome components
   */
  nsISimpleEnumerator getChromeItems();

}