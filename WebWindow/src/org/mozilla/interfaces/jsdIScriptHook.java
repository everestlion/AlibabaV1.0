/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/jsd/idl/jsdIDebuggerService.idl
 */

package org.mozilla.interfaces;

public interface jsdIScriptHook extends nsISupports {

  String JSDISCRIPTHOOK_IID =
    "{ae89a7e2-1dd1-11b2-8c2f-af82086291a5}";

  /**
 * Set jsdIDebuggerService::scriptHook to an instance of one of these.
 */
/**
     * Called when scripts are created.
     */
  void onScriptCreated(jsdIScript script);

  /**
     * Called when the JavaScript engine destroys a script.  The jsdIScript
     * object passed in will already be invalidated.
     */
  void onScriptDestroyed(jsdIScript script);

}