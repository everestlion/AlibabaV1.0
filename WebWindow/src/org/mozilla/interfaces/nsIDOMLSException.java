/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/ls/nsIDOMLSException.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLSException extends nsISupports {

  String NS_IDOMLSEXCEPTION_IID =
    "{1cc8e4b3-1dbb-4adc-a913-1527bf67748c}";

  int PARSE_ERR = 81;

  int SERIALIZE_ERR = 82;

  int getCode();

}