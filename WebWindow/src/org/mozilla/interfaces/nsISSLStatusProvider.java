/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/boot/public/nsISSLStatusProvider.idl
 */

package org.mozilla.interfaces;

public interface nsISSLStatusProvider extends nsISupports {

  String NS_ISSLSTATUSPROVIDER_IID =
    "{8de811f0-1dd2-11b2-8bf1-e9aa324984b2}";

  nsISupports getSSLStatus();

}