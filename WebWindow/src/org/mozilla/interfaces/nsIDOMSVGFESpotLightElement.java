/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFESpotLightElement extends nsIDOMSVGElement {

  String NS_IDOMSVGFESPOTLIGHTELEMENT_IID =
    "{5515dd05-3d9d-4d6c-8460-a04aaf5afe15}";

  nsIDOMSVGAnimatedNumber getX();

  nsIDOMSVGAnimatedNumber getY();

  nsIDOMSVGAnimatedNumber getZ();

  nsIDOMSVGAnimatedNumber getPointsAtX();

  nsIDOMSVGAnimatedNumber getPointsAtY();

  nsIDOMSVGAnimatedNumber getPointsAtZ();

  nsIDOMSVGAnimatedNumber getSpecularExponent();

  nsIDOMSVGAnimatedNumber getLimitingConeAngle();

}