/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMNavigator.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMJSNavigator extends nsISupports {

  String NS_IDOMJSNAVIGATOR_IID =
    "{4b4f8316-1dd2-11b2-b265-9a857376d159}";

  void preference();

}