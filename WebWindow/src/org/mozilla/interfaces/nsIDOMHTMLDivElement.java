/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLDivElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLDivElement interface is the interface to a [X]HTML
 * div element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLDivElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLDIVELEMENT_IID =
    "{a6cf90a0-15b3-11d2-932e-00805f8add32}";

  String getAlign();

  void setAlign(String aAlign);

}