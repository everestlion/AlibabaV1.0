/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedNumberList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedNumberList extends nsISupports {

  String NS_IDOMSVGANIMATEDNUMBERLIST_IID =
    "{93ebb030-f82d-4f8e-b133-d1b5abb73cf3}";

  nsIDOMSVGNumberList getBaseVal();

  nsIDOMSVGNumberList getAnimVal();

}