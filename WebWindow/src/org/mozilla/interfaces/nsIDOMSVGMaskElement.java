/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGMaskElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGMaskElement extends nsIDOMSVGElement {

  String NS_IDOMSVGMASKELEMENT_IID =
    "{fdd7039c-35b6-465a-b7a3-c98a815b583e}";

  nsIDOMSVGAnimatedEnumeration getMaskUnits();

  nsIDOMSVGAnimatedEnumeration getMaskContentUnits();

  nsIDOMSVGAnimatedLength getX();

  nsIDOMSVGAnimatedLength getY();

  nsIDOMSVGAnimatedLength getWidth();

  nsIDOMSVGAnimatedLength getHeight();

}