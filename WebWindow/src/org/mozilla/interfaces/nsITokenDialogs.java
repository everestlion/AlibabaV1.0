/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/security/manager/ssl/public/nsITokenDialogs.idl
 */

package org.mozilla.interfaces;

public interface nsITokenDialogs extends nsISupports {

  String NS_ITOKENDIALOGS_IID =
    "{bb4bae9c-39c5-11d5-ba26-00108303b117}";

  void chooseToken(nsIInterfaceRequestor ctx, String[] tokenNameList, long count, String[] tokenName, boolean[] canceled);

  /**
    * displayProtectedAuth - displays notification dialog to the user 
    * that he is expected to authenticate to the token using its
    * "protected authentication path" feature
    */
  void displayProtectedAuth(nsIInterfaceRequestor ctx, nsIProtectedAuthThread runnable);

}