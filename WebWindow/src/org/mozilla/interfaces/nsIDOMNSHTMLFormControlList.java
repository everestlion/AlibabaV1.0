/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLFormControlList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLFormControlList extends nsISupports {

  String NS_IDOMNSHTMLFORMCONTROLLIST_IID =
    "{a6cf911a-15b3-11d2-932e-00805f8add32}";

  nsISupports namedItem(String name);

}