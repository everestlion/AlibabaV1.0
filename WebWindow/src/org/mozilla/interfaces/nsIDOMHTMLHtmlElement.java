/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLHtmlElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLHtmlElement interface is the interface to a [X]HTML
 * html element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLHtmlElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLHTMLELEMENT_IID =
    "{a6cf9086-15b3-11d2-932e-00805f8add32}";

  String getVersion();

  void setVersion(String aVersion);

}