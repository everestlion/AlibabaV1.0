/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/uriloader/prefetch/nsIPrefetchService.idl
 */

package org.mozilla.interfaces;

public interface nsIPrefetchService extends nsISupports {

  String NS_IPREFETCHSERVICE_IID =
    "{cba513eb-c457-4b93-832c-1a979e66edd1}";

  /**
     * Enqueue a request to prefetch the specified URI.
     *
     * @param aURI the URI of the document to prefetch
     * @param aReferrerURI the URI of the referring page
     * @param aSource the DOM node (such as a <link> tag) that requested this
     *        fetch, or null if the prefetch was not requested by a DOM node.
     * @param aExplicit the link element has an explicit prefetch link type
     */
  void prefetchURI(nsIURI aURI, nsIURI aReferrerURI, nsIDOMNode aSource, boolean aExplicit);

  /**
     * Enqueue a request to prefetch the specified URI, making sure it's in the
     * offline cache.
     *
     * @param aURI the URI of the document to prefetch
     * @param aReferrerURI the URI of the referring page
     * @param aSource the DOM node (such as a <link> tag) that requested this
     *        fetch, or null if the prefetch was not requested by a DOM node.
     * @param aExplicit the link element has an explicit offline link type
     */
  void prefetchURIForOfflineUse(nsIURI aURI, nsIURI aReferrerURI, nsIDOMNode aSource, boolean aExplicit);

  /**
     * Enumerate the items in the prefetch queue.  Each element in the
     * enumeration is an nsIDOMLoadStatus.
     *
     * @param aIncludeNormalItems include normal prefetch items in the list.
     * @param aIncludeOfflineItems include items being fetched for offline
     *        use.
     */
  nsISimpleEnumerator enumerateQueue(boolean aIncludeNormalItems, boolean aIncludeOfflineItems);

}