/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleCaretMoveEvent extends nsIAccessibleEvent {

  String NS_IACCESSIBLECARETMOVEEVENT_IID =
    "{b9076dce-4cd3-4e3d-a7f6-7f33a7f40c31}";

  /**
   * Return caret offset.
   */
  int getCaretOffset();

}