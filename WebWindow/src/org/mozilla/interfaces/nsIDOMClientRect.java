/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMClientRect.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMClientRect extends nsISupports {

  String NS_IDOMCLIENTRECT_IID =
    "{f8583bbc-c6de-4646-b39f-df7e766442e9}";

  float getLeft();

  float getTop();

  float getRight();

  float getBottom();

}