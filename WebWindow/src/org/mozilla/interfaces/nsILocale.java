/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/intl/locale/idl/nsILocale.idl
 */

package org.mozilla.interfaces;

public interface nsILocale extends nsISupports {

  String NS_ILOCALE_IID =
    "{21035ee0-4556-11d3-91cd-00105aa3f7dc}";

  String getCategory(String category);

}