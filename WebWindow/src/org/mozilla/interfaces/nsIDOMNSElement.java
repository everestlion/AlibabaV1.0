/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMNSElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSElement extends nsISupports {

  String NS_IDOMNSELEMENT_IID =
    "{cea6f919-7fe6-4bdd-9db6-158d9283f8d3}";

  nsIDOMNodeList getElementsByClassName(String classes);

  nsIDOMClientRectList getClientRects();

  /**
   * Returns the union of all rectangles in the getClientRects() list. Empty
   * rectangles are ignored, except that if all rectangles are empty,
   * we return an empty rectangle positioned at the top-left of the first
   * rectangle in getClientRects().
   */
  nsIDOMClientRect getBoundingClientRect();

}