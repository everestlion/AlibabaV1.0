/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLElement extends nsISupports {

  String NS_IDOMNSHTMLELEMENT_IID =
    "{eac0a4ee-2e4f-403c-9b77-5cf32cfb42f7}";

  int getOffsetTop();

  int getOffsetLeft();

  int getOffsetWidth();

  int getOffsetHeight();

  nsIDOMElement getOffsetParent();

  String getInnerHTML();

  void setInnerHTML(String aInnerHTML);

  int getScrollTop();

  void setScrollTop(int aScrollTop);

  int getScrollLeft();

  void setScrollLeft(int aScrollLeft);

  int getScrollHeight();

  int getScrollWidth();

  int getClientTop();

  int getClientLeft();

  int getClientHeight();

  int getClientWidth();

  int getTabIndex();

  void setTabIndex(int aTabIndex);

  String getContentEditable();

  void setContentEditable(String aContentEditable);

  void blur();

  void focus();

  void scrollIntoView(boolean top);

  boolean getSpellcheck();

  void setSpellcheck(boolean aSpellcheck);

}