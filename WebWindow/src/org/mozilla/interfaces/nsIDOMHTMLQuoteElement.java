/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLQuoteElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLQuoteElement interface is the interface to a [X]HTML
 * q element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLQuoteElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLQUOTEELEMENT_IID =
    "{a6cf90a3-15b3-11d2-932e-00805f8add32}";

  String getCite();

  void setCite(String aCite);

}