/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/jsd/idl/jsdIDebuggerService.idl
 */

package org.mozilla.interfaces;

public interface jsdINestCallback extends nsISupports {

  String JSDINESTCALLBACK_IID =
    "{88bea60f-9b5d-4b39-b08b-1c3a278782c6}";

  /**
 * Pass an instance of one of these to jsdIDebuggerService::enterNestedEventLoop.
 */
/**
     * This method will be called after pre-nesting work has completed, such
     * as pushing the js context and network event queue, but before the new
     * event loop starts.
     */
  void onNest();

}