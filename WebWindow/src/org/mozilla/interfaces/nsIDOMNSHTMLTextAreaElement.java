/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLTextAreaElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLTextAreaElement extends nsISupports {

  String NS_IDOMNSHTMLTEXTAREAELEMENT_IID =
    "{ca066b44-9ddf-11d3-bccc-0060b0fc76bd}";

  nsIControllers getControllers();

  int getTextLength();

  int getSelectionStart();

  void setSelectionStart(int aSelectionStart);

  int getSelectionEnd();

  void setSelectionEnd(int aSelectionEnd);

  void setSelectionRange(int selectionStart, int selectionEnd);

}