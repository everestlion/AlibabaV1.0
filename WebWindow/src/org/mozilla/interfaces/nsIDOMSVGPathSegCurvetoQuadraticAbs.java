/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegCurvetoQuadraticAbs extends nsISupports {

  String NS_IDOMSVGPATHSEGCURVETOQUADRATICABS_IID =
    "{b7aef0f0-2830-4145-b04f-fe05789ccf8a}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  float getX1();

  void setX1(float aX1);

  float getY1();

  void setY1(float aY1);

}