/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedString.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedString extends nsISupports {

  String NS_IDOMSVGANIMATEDSTRING_IID =
    "{4afb42f4-9969-483d-9b90-59ddba047492}";

  String getBaseVal();

  void setBaseVal(String aBaseVal);

  String getAnimVal();

}