/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessible.idl
 */

package org.mozilla.interfaces;

/**
 * A cross-platform interface that supports platform-specific 
 * accessibility APIs like MSAA and ATK. Contains the sum of what's needed
 * to support IAccessible as well as ATK's generic accessibility objects.
 * Can also be used by in-process accessibility clients to get information
 * about objects in the accessible tree. The accessible tree is a subset of 
 * nodes in the DOM tree -- such as documents, focusable elements and text.
 * Mozilla creates the implementations of nsIAccessible on demand.
 * See http://www.mozilla.org/projects/ui/accessibility for more information.
 *
 * @status UNDER_REVIEW
 */
public interface nsIAccessible extends nsISupports {

  String NS_IACCESSIBLE_IID =
    "{004b6882-2df1-49df-bb5f-0fb81a5b1edf}";

  /**
   * Parent node in accessible tree.
   */
  nsIAccessible getParent();

  /**
   * Next sibling in accessible tree
   */
  nsIAccessible getNextSibling();

  /**
   * Previous sibling in accessible tree
   */
  nsIAccessible getPreviousSibling();

  /**
   * First child in accessible tree
   */
  nsIAccessible getFirstChild();

  /**
   * Last child in accessible tree
   */
  nsIAccessible getLastChild();

  /**
   * Array of all this element's children.
   */
  nsIArray getChildren();

  /**
   * Number of accessible children
   */
  int getChildCount();

  /**
   * The 0-based index of this accessible in its parent's list of children,
   * or -1 if this accessible does not have a parent.
   */
  int getIndexInParent();

  /**
   * Accessible name -- the main text equivalent for this node
   */
  String getName();

  /**
   * Accessible name -- the main text equivalent for this node
   */
  void setName(String aName);

  /**
   * Accessible value -- a number or a secondary text equivalent for this node
   * Widgets that use role attribute can force a value using the valuenow attribute
   */
  String getValue();

  /**
   * Accessible description -- long text associated with this node
   */
  String getDescription();

  /**
   * Provides localized string of accesskey name, such as Alt+D.
   * The modifier may be affected by user and platform preferences.
   * Usually alt+letter, or just the letter alone for menu items. 
   */
  String getKeyboardShortcut();

  /**
   * Provides localized string of global keyboard accelerator for default
   * action, such as Ctrl+O for Open file
   */
  String getDefaultKeyBinding();

  /**
   * Provides array of localized string of global keyboard accelerator for
   * the given action index supported by accessible.
   *
   * @param aActionIndex - index of the given action
   */
  nsIDOMDOMStringList getKeyBindings(short aActionIndex);

  /**
   * Natural enumerated accessible role for the associated element.
   * The values depend on platform because of variations.
   * See the ROLE_* constants defined in nsIAccessibleRole.
   * This does not take into account role attribute as the finalRole does.
   */
  long getRole();

  /**
   * Enumerated accessible role. The values depend on platform because of variations.
   * See the ROLE_* constants defined in nsIAccessibleRole.
   * Widgets can use role attribute to force the final role
   */
  long getFinalRole();

  /**
   * Accessible states -- bit fields which describe boolean properties of node.
   * Many states are only valid given a certain role attribute that supports
   * them.
   *
   * @param aState - the first bit field (see nsIAccessibleStates::STATE_*
   *                 constants)
   * @param aExtraState - the second bit field
   *                      (see nsIAccessibleStates::EXT_STATE_* constants)
   */
  void getFinalState(long[] aState, long[] aExtraState);

  /**
   * Help text associated with node
   */
  String getHelp();

  /**
   * Focused accessible child of node
   */
  nsIAccessible getFocusedChild();

  /**
   * Attributes of accessible
   */
  nsIPersistentProperties getAttributes();

  /**
   * Returns grouping information. Used for tree items, list items, tab panel
   * labels, radio buttons, etc. Also used for collectons of non-text objects.
   *
   * @param groupLevel - 1-based, similar to ARIA 'level' property
   * @param similarItemsInGroup - 1-based, similar to ARIA 'setsize' property,
   *                              inclusive of the current item
   * @param positionInGroup - 1-based, similar to ARIA 'posinset' property
   */
  void groupPosition(int[] aGroupLevel, int[] aSimilarItemsInGroup, int[] aPositionInGroup);

  /**
   * Accessible child which contains the coordinate at (x, y) in screen pixels.
   * If the point is in the current accessible but not in a child, the
   * current accessible will be returned.
   * If the point is in neither the current accessible or a child, then
   * null will be returned.
   */
  nsIAccessible getChildAtPoint(int x, int y);

  /**
   * Nth accessible child using zero-based index or last child if index less than zero
   */
  nsIAccessible getChildAt(int aChildIndex);

  /**
   * Accessible node geometrically to the right of this one
   */
  nsIAccessible getAccessibleToRight();

  /**
   * Accessible node geometrically to the left of this one
   */
  nsIAccessible getAccessibleToLeft();

  /**
   * Accessible node geometrically above this one
   */
  nsIAccessible getAccessibleAbove();

  /**
   * Accessible node geometrically below this one
   */
  nsIAccessible getAccessibleBelow();

  /**
   * Return accessible related to this one by the given relation type (see.
   * constants defined in nsIAccessibleRelation).
   */
  nsIAccessible getAccessibleRelated(long aRelationType);

  /**
   * Returns the number of accessible relations for this object.
   */
  long getRelationsCount();

  /**
   * Returns one accessible relation for this object.
   *
   * @param index - relation index (0-based)
   */
  nsIAccessibleRelation getRelation(long index);

  /**
   * Returns multiple accessible relations for this object.
   */
  nsIArray getRelations();

  /**
   * Return accessible's x and y coordinates relative to the screen and
   * accessible's width and height.
   */
  void getBounds(int[] x, int[] y, int[] width, int[] height);

  /**
   * Add or remove this accessible to the current selection
   */
  void setSelected(boolean isSelected);

  /**
   * Extend the current selection from its current accessible anchor node
   * to this accessible
   */
  void extendSelection();

  /**
   * Select this accessible node only
   */
  void takeSelection();

  /**
   * Focus this accessible node,
   * The state STATE_FOCUSABLE indicates whether this node is normally focusable.
   * It is the callers responsibility to determine whether this node is focusable.
   * accTakeFocus on a node that is not normally focusable (such as a table),
   * will still set focus on that node, although normally that will not be visually 
   * indicated in most style sheets.
   */
  void takeFocus();

  /**
   * The number of accessible actions associated with this accessible
   */
  short getNumActions();

  /**
   * The name of the accessible action at the given zero-based index
   */
  String getActionName(short index);

  /**
   * The description of the accessible action at the given zero-based index
   */
  String getActionDescription(short aIndex);

  /**
   * Perform the accessible action at the given zero-based index
   * Action number 0 is the default action
   */
  void doAction(short index);

}