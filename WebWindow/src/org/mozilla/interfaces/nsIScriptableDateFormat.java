/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/intl/locale/idl/nsIScriptableDateFormat.idl
 */

package org.mozilla.interfaces;

public interface nsIScriptableDateFormat extends nsISupports {

  String NS_ISCRIPTABLEDATEFORMAT_IID =
    "{0c89efb0-1aae-11d3-9141-006008a6edf6}";

  int dateFormatNone = 0;

  int dateFormatLong = 1;

  int dateFormatShort = 2;

  int dateFormatYearMonth = 3;

  int dateFormatWeekday = 4;

  int timeFormatNone = 0;

  int timeFormatSeconds = 1;

  int timeFormatNoSeconds = 2;

  int timeFormatSecondsForce24Hour = 3;

  int timeFormatNoSecondsForce24Hour = 4;

  String formatDateTime(String locale, int dateFormatSelector, int timeFormatSelector, int year, int month, int day, int hour, int minute, int second);

  String formatDate(String locale, int dateFormatSelector, int year, int month, int day);

  String formatTime(String locale, int timeFormatSelector, int hour, int minute, int second);

}