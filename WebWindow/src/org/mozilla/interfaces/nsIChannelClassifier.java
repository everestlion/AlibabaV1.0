/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/docshell/base/nsIChannelClassifier.idl
 */

package org.mozilla.interfaces;

/**
 * An nsIChannelClassifier object checks a channel's URI against the
 * URI classifier service, and cancels the channel before OnStartRequest
 * if it is found on a blacklist.
 */
public interface nsIChannelClassifier extends nsISupports {

  String NS_ICHANNELCLASSIFIER_IID =
    "{d17f8f74-d403-4dea-b124-3ace5dbe44dc}";

  /**
   * Checks a channel against the URI classifier service (if it exists).
   *
   * The channel will be suspended while the classifier is checked.  The
   * channel may be cancelled with an error code determined by the classifier
   * before it is resumed.
   *
   * If there is no URI classifier service, NS_ERROR_FACTORY_NOT_REGISTERED
   * will be returned.
   *
   * @param aChannel
   *        The channel to suspend.
   */
  void start(nsIChannel aChannel);

  /**
   * Notify the classifier that the channel was redirected.  The new channel
   * will be suspended pending a new classifier lookup.
   *
   * @param aOldChannel
   *        The channel that's being redirected.
   * @param aNewChannel
   *        The new channel. This channel is not opened yet.
   */
  void onRedirect(nsIChannel aOldChannel, nsIChannel aNewChannel);

  /**
   * Cancel an existing query.  If a channel has been suspended, it will
   * be resumed.
   */
  void cancel();

}