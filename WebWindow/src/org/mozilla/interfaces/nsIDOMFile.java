/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/base/public/nsIDOMFile.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMFile extends nsISupports {

  String NS_IDOMFILE_IID =
    "{4a17d83b-424f-43f3-8a7c-430f406921be}";

  String getFileName();

  double getFileSize();

  String getAsText(String encoding);

  String getAsDataURL();

  String getAsBinary();

}