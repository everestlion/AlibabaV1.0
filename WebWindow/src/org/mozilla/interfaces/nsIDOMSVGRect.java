/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGRect.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGRect extends nsISupports {

  String NS_IDOMSVGRECT_IID =
    "{5b912111-c10e-498f-a44c-c713c1843007}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  float getWidth();

  void setWidth(float aWidth);

  float getHeight();

  void setHeight(float aHeight);

}