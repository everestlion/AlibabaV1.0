/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegCurvetoCubicSmoothAbs extends nsISupports {

  String NS_IDOMSVGPATHSEGCURVETOCUBICSMOOTHABS_IID =
    "{5fa8fea8-bdd1-4315-ac44-a39b3ff347b5}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  float getX2();

  void setX2(float aX2);

  float getY2();

  void setY2(float aY2);

}