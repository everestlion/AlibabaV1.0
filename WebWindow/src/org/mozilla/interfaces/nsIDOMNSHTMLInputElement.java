/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLInputElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLInputElement extends nsISupports {

  String NS_IDOMNSHTMLINPUTELEMENT_IID =
    "{df3dc133-d77a-482f-8364-8e40df978a33}";

  nsIControllers getControllers();

  int getTextLength();

  int getSelectionStart();

  void setSelectionStart(int aSelectionStart);

  int getSelectionEnd();

  void setSelectionEnd(int aSelectionEnd);

  nsIDOMFileList getFiles();

  void setSelectionRange(int selectionStart, int selectionEnd);

}