/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMNSFeatureFactory.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSFeatureFactory extends nsISupports {

  String NS_IDOMNSFEATUREFACTORY_IID =
    "{dc5ba787-b648-4b01-a8e7-b293ffb044ef}";

  /**
   * @param object: usually either nsIDOMNode or nsIDOMDOMImplementation
   * @param feature: the name of the feature
   * @param version: the version of the feature
   */
  boolean hasFeature(nsISupports object, String feature, String version);

  /**
   * @param object: usually either nsIDOMNode or nsIDOMDOMImplementation
   * @param feature: the name of the feature
   * @param version: the version of the feature
   */
  nsISupports getFeature(nsISupports object, String feature, String version);

}