/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMMimeType.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMMimeType extends nsISupports {

  String NS_IDOMMIMETYPE_IID =
    "{f6134682-f28b-11d2-8360-c90899049c3c}";

  String getDescription();

  nsIDOMPlugin getEnabledPlugin();

  String getSuffixes();

  String getType();

}