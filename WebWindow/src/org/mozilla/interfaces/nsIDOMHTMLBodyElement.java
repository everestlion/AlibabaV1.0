/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLBodyElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLBodyElement interface is the interface to a [X]HTML
 * body element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLBodyElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLBODYELEMENT_IID =
    "{a6cf908e-15b3-11d2-932e-00805f8add32}";

  String getALink();

  void setALink(String aALink);

  String getBackground();

  void setBackground(String aBackground);

  String getBgColor();

  void setBgColor(String aBgColor);

  String getLink();

  void setLink(String aLink);

  String getText();

  void setText(String aText);

  String getVLink();

  void setVLink(String aVLink);

}