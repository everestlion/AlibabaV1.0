/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/nsIActiveXSecurityPolicy.idl
 */

package org.mozilla.interfaces;

public interface nsIActiveXSecurityPolicy extends nsISupports {

  String NS_IACTIVEXSECURITYPOLICY_IID =
    "{0a3928d2-76c8-4c25-86a9-9c005ad832f4}";

  /** Host nothing at all. */
  long HOSTING_FLAGS_HOST_NOTHING = 0L;

  /** Allow hosting of safe for scripting objects. */
  long HOSTING_FLAGS_HOST_SAFE_OBJECTS = 1L;

  /** Allow any object to be hosted. */
  long HOSTING_FLAGS_HOST_ALL_OBJECTS = 2L;

  /** Allow objects to be downloaded and installed. */
  long HOSTING_FLAGS_DOWNLOAD_CONTROLS = 4L;

  /** Allow objects marked safe for scripting to be scripted. */
  long HOSTING_FLAGS_SCRIPT_SAFE_OBJECTS = 8L;

  /** Allow any object to be scripted. */
  long HOSTING_FLAGS_SCRIPT_ALL_OBJECTS = 16L;

  /**
     * Return the ActiveX security and hosting flags.
     * @param context The context for which flags are requested. At present the
     *                only valid value is nsnull.
     */
  long getHostingFlags(String aContext);

}