/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULContainerElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULContainerItemElement extends nsIDOMXULElement {

  String NS_IDOMXULCONTAINERITEMELEMENT_IID =
    "{4650e55f-4777-4271-8b62-9603a7dd4614}";

  /**
   * Returns the parent container if any.
   */
  nsIDOMXULContainerElement getParentContainer();

}