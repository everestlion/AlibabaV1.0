/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpfe/components/windowds/nsIWindowDataSource.idl
 */

package org.mozilla.interfaces;

public interface nsIWindowDataSource extends nsISupports {

  String NS_IWINDOWDATASOURCE_IID =
    "{3722a5b9-5323-4ed0-bb1a-8299f27a4e89}";

  /**
     * for the given resource name, return the window
     */
  nsIDOMWindowInternal getWindowForResource(String inResource);

}