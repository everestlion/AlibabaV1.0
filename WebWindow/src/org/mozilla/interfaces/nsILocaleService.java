/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/intl/locale/idl/nsILocaleService.idl
 */

package org.mozilla.interfaces;

public interface nsILocaleService extends nsISupports {

  String NS_ILOCALESERVICE_IID =
    "{c2edc848-4219-4440-abbf-98119882c83f}";

  nsILocale newLocale(String aLocale);

  nsILocale getSystemLocale();

  nsILocale getApplicationLocale();

  nsILocale getLocaleFromAcceptLanguage(String acceptLanguage);

  String getLocaleComponentForUserAgent();

}