/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xpath/nsIDOMXPathException.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXPathException extends nsISupports {

  String NS_IDOMXPATHEXCEPTION_IID =
    "{75506f89-b504-11d5-a7f2-ca108ab8b6fc}";

  int INVALID_EXPRESSION_ERR = 51;

  int TYPE_ERR = 52;

  int getCode();

}