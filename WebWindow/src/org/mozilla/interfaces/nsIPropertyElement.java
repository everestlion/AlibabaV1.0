/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsIPersistentProperties2.idl
 */

package org.mozilla.interfaces;

public interface nsIPropertyElement extends nsISupports {

  String NS_IPROPERTYELEMENT_IID =
    "{283ee646-1aef-11d4-98b3-00c04fa0ce9a}";

  String getKey();

  void setKey(String aKey);

  String getValue();

  void setValue(String aValue);

}