/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/intl/locale/idl/nsICollation.idl
 */

package org.mozilla.interfaces;

public interface nsICollationFactory extends nsISupports {

  String NS_ICOLLATIONFACTORY_IID =
    "{04971e14-d6b3-4ada-8cbb-c3a13842b349}";

  nsICollation createCollation(nsILocale locale);

}