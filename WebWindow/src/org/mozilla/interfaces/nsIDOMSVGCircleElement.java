/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGCircleElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGCircleElement extends nsIDOMSVGElement {

  String NS_IDOMSVGCIRCLEELEMENT_IID =
    "{0f89f2a4-b168-4602-90f5-1874418c0a6a}";

  nsIDOMSVGAnimatedLength getCx();

  nsIDOMSVGAnimatedLength getCy();

  nsIDOMSVGAnimatedLength getR();

}