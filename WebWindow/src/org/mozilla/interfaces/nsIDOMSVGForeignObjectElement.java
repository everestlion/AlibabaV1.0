/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGForeignObjectElem.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGForeignObjectElement extends nsIDOMSVGElement {

  String NS_IDOMSVGFOREIGNOBJECTELEMENT_IID =
    "{fd9c9871-23fd-48eb-a65b-3842e9b0acbd}";

  nsIDOMSVGAnimatedLength getX();

  nsIDOMSVGAnimatedLength getY();

  nsIDOMSVGAnimatedLength getWidth();

  nsIDOMSVGAnimatedLength getHeight();

}