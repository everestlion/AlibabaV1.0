/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMDocumentFragment.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMDocumentFragment extends nsIDOMNode {

  String NS_IDOMDOCUMENTFRAGMENT_IID =
    "{a6cf9076-15b3-11d2-932e-00805f8add32}";

}