/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/jsd/idl/jsdIDebuggerService.idl
 */

package org.mozilla.interfaces;

public interface jsdIContextEnumerator extends nsISupports {

  String JSDICONTEXTENUMERATOR_IID =
    "{912e342a-1dd2-11b2-b09f-cf3af38c15f0}";

  /**
 * Pass an instance of one of these to jsdIDebuggerService::enumerateContexts.
 */
/**
     * The enumerateContext method will be called once for every context
     * currently in use.
     */
  void enumerateContext(jsdIContext executionContext);

}