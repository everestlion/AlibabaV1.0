/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLOptionCollectn.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLOptionCollection extends nsISupports {

  String NS_IDOMNSHTMLOPTIONCOLLECTION_IID =
    "{1181207b-2337-41a7-8ddf-fbe96461256f}";

  int getSelectedIndex();

  void setSelectedIndex(int aSelectedIndex);

}