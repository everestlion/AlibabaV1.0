/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMComment.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMComment extends nsIDOMCharacterData {

  String NS_IDOMCOMMENT_IID =
    "{a6cf9073-15b3-11d2-932e-00805f8add32}";

}