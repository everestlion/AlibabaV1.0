/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/widget/public/nsIPrintSettingsWin.idl
 */

package org.mozilla.interfaces;

/**
 * Simplified PrintSettings for Windows interface 
 *
 * @status UNDER_REVIEW
 */
public interface nsIPrintSettingsWin extends nsISupports {

  String NS_IPRINTSETTINGSWIN_IID =
    "{ff328fe4-41d5-4b78-82ab-6b1fbc7930af}";

}