/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegCurvetoQuadraticSmoothAbs extends nsISupports {

  String NS_IDOMSVGPATHSEGCURVETOQUADRATICSMOOTHABS_IID =
    "{ff5bbb58-b49a-450f-b91b-e50585c34b3d}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

}