/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMBarProp.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMBarProp extends nsISupports {

  String NS_IDOMBARPROP_IID =
    "{9eb2c150-1d56-11d3-8221-0060083a0bcf}";

  /**
 * The nsIDOMBarProp interface is the interface for controlling and
 * accessing the visibility of certain UI items (scrollbars, menubars,
 * toolbars, ...) through the DOM.
 *
 * @status FROZEN
 */
  boolean getVisible();

  /**
 * The nsIDOMBarProp interface is the interface for controlling and
 * accessing the visibility of certain UI items (scrollbars, menubars,
 * toolbars, ...) through the DOM.
 *
 * @status FROZEN
 */
  void setVisible(boolean aVisible);

}