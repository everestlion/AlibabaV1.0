/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIAccessibleTextChangeEvent extends nsIAccessibleEvent {

  String NS_IACCESSIBLETEXTCHANGEEVENT_IID =
    "{50a1e151-8e5f-4bcc-aaaf-a4bed1190e93}";

  /**
   * Returns offset of changed text in accessible.
   */
  int getStart();

  /**
   * Returns length of changed text.
   */
  long getLength();

  /**
   * Returns true if text was inserted, otherwise false.
   */
  boolean isInserted();

  /**
   * The inserted or removed text
   */
  String getModifiedText();

}