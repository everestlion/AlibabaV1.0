/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/autocomplete/public/nsIAutoCompleteController.idl
 */

package org.mozilla.interfaces;

public interface nsIAutoCompleteController extends nsISupports {

  String NS_IAUTOCOMPLETECONTROLLER_IID =
    "{6f08d134-8536-4b28-b456-d150fbaa66a9}";

  int STATUS_NONE = 1;

  int STATUS_SEARCHING = 2;

  int STATUS_COMPLETE_NO_MATCH = 3;

  int STATUS_COMPLETE_MATCH = 4;

  nsIAutoCompleteInput getInput();

  void setInput(nsIAutoCompleteInput aInput);

  int getSearchStatus();

  long getMatchCount();

  void startSearch(String searchString);

  void stopSearch();

  void handleText(boolean aIgnoreSelection);

  boolean handleEnter(boolean aIsPopupSelection);

  boolean handleEscape();

  void handleStartComposition();

  void handleEndComposition();

  void handleTab();

  boolean handleKeyNavigation(long key);

  boolean handleDelete();

  String getValueAt(int index);

  String getCommentAt(int index);

  String getStyleAt(int index);

  String getImageAt(int index);

  String getSearchString();

  void setSearchString(String aSearchString);

}