/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGStopElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGStopElement extends nsIDOMSVGElement {

  String NS_IDOMSVGSTOPELEMENT_IID =
    "{93169940-7663-4eab-af23-94a8a08c2654}";

  nsIDOMSVGAnimatedNumber getOffset();

}