/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedNumber.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedNumber extends nsISupports {

  String NS_IDOMSVGANIMATEDNUMBER_IID =
    "{716e3b11-b03b-49f7-b82d-5383922b0ab3}";

  float getBaseVal();

  void setBaseVal(float aBaseVal);

  float getAnimVal();

}