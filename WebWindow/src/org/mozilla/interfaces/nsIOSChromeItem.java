/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/widget/public/nsIFullScreen.idl
 */

package org.mozilla.interfaces;

public interface nsIOSChromeItem extends nsISupports {

  String NS_IOSCHROMEITEM_IID =
    "{ddd6790a-1dd1-11b2-a804-b522643903b9}";

  String getName();

  boolean getHidden();

  void setHidden(boolean aHidden);

  int getX();

  int getY();

  int getWidth();

  int getHeight();

}