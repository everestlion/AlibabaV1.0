/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOM3Attr.idl
 */

package org.mozilla.interfaces;

public interface nsIDOM3Attr extends nsIDOM3Node {

  String NS_IDOM3ATTR_IID =
    "{a2216ddc-1bcd-4ec2-a292-371e09a6c377}";

  nsIDOM3TypeInfo getSchemaTypeInfo();

  boolean getIsId();

}