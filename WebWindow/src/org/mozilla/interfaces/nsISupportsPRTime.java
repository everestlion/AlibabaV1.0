/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for NSPR date/time values
 * 
 * @status FROZEN
 */
public interface nsISupportsPRTime extends nsISupportsPrimitive {

  String NS_ISUPPORTSPRTIME_IID =
    "{e2563630-4a1c-11d3-9890-006008962422}";

  double getData();

  void setData(double aData);

  String toString();

}