/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSHTMLAreaElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSHTMLAreaElement extends nsISupports {

  String NS_IDOMNSHTMLAREAELEMENT_IID =
    "{3dce9071-f3b9-4280-a6ee-776cdfe3dd9e}";

  String getProtocol();

  void setProtocol(String aProtocol);

  String getHost();

  void setHost(String aHost);

  String getHostname();

  void setHostname(String aHostname);

  String getPathname();

  void setPathname(String aPathname);

  String getSearch();

  void setSearch(String aSearch);

  String getPort();

  void setPort(String aPort);

  String getHash();

  void setHash(String aHash);

  String toString();

}