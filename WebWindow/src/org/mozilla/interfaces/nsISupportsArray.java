/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsArray.idl
 */

package org.mozilla.interfaces;

public interface nsISupportsArray extends nsICollection {

  String NS_ISUPPORTSARRAY_IID =
    "{791eafa0-b9e6-11d1-8031-006008159b5a}";

  int getIndexOf(nsISupports aPossibleElement);

  int getIndexOfStartingAt(nsISupports aPossibleElement, long aStartIndex);

  int getLastIndexOf(nsISupports aPossibleElement);

  void deleteLastElement(nsISupports aElement);

  void deleteElementAt(long aIndex);

  void compact();

  nsISupportsArray _clone();

}