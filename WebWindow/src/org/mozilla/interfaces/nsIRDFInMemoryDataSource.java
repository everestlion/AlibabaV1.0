/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/rdf/base/idl/nsIRDFInMemoryDataSource.idl
 */

package org.mozilla.interfaces;

public interface nsIRDFInMemoryDataSource extends nsISupports {

  String NS_IRDFINMEMORYDATASOURCE_IID =
    "{17c4e0aa-1dd2-11b2-8029-bf6f668de500}";

  void ensureFastContainment(nsIRDFResource aSource);

}