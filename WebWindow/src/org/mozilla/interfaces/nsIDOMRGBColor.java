/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/css/nsIDOMRGBColor.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMRGBColor extends nsISupports {

  String NS_IDOMRGBCOLOR_IID =
    "{6aff3102-320d-4986-9790-12316bb87cf9}";

  nsIDOMCSSPrimitiveValue getRed();

  nsIDOMCSSPrimitiveValue getGreen();

  nsIDOMCSSPrimitiveValue getBlue();

}