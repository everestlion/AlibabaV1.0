/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPolylineElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPolylineElement extends nsIDOMSVGElement {

  String NS_IDOMSVGPOLYLINEELEMENT_IID =
    "{7b6e15cf-9793-41ee-adcc-cc1c206c80e6}";

}