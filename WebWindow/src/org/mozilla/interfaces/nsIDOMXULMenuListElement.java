/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULMenuListElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULMenuListElement extends nsIDOMXULSelectControlElement {

  String NS_IDOMXULMENULISTELEMENT_IID =
    "{3d49950e-04f9-4e35-a9a0-ffd51356a674}";

  boolean getEditable();

  void setEditable(boolean aEditable);

  boolean getOpen();

  void setOpen(boolean aOpen);

  String getLabel();

  String getCrop();

  void setCrop(String aCrop);

  String getImage();

  void setImage(String aImage);

  nsIDOMNode getInputField();

}