/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEPointLightElement extends nsIDOMSVGElement {

  String NS_IDOMSVGFEPOINTLIGHTELEMENT_IID =
    "{557f128a-026b-4fa8-a44c-605df7bfd62e}";

  nsIDOMSVGAnimatedNumber getX();

  nsIDOMSVGAnimatedNumber getY();

  nsIDOMSVGAnimatedNumber getZ();

}