/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/rdf/base/idl/nsIRDFNode.idl
 */

package org.mozilla.interfaces;

public interface nsIRDFNode extends nsISupports {

  String NS_IRDFNODE_IID =
    "{0f78da50-8321-11d2-8eac-00805f29f370}";

  boolean equalsNode(nsIRDFNode aNode);

}