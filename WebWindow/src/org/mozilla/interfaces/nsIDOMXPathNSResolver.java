/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xpath/nsIDOMXPathNSResolver.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXPathNSResolver extends nsISupports {

  String NS_IDOMXPATHNSRESOLVER_IID =
    "{75506f83-b504-11d5-a7f2-ca108ab8b6fc}";

  String lookupNamespaceURI(String prefix);

}