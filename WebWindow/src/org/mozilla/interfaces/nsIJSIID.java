/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/js/src/xpconnect/idl/xpcjsid.idl
 */

package org.mozilla.interfaces;

public interface nsIJSIID extends nsIJSID {

  String NS_IJSIID_IID =
    "{e76ec564-a080-4705-8609-384c755ec91e}";

}