/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/io/nsILocalFileWin.idl
 */

package org.mozilla.interfaces;

public interface nsILocalFileWin extends nsILocalFile {

  String NS_ILOCALFILEWIN_IID =
    "{def38371-73b0-4dfd-85cd-0a7c91afbec6}";

  /**
    * getVersionInfoValue
    *
    * Retrieve a metadata field from the file's VERSIONINFO block.
    * Throws NS_ERROR_FAILURE if no value is found, or the value is empty.
    *
    * @param   aField         The field to look up.
    *
    */
  String getVersionInfoField(String aField);

  /**
     * The canonical path of the file, which avoids short/long
     * pathname inconsistencies. The nsILocalFile persistent
     * descriptor is not guaranteed to be canonicalized (it may
     * persist either the long or the short path name). The format of
     * the canonical path will vary with the underlying file system:
     * it will typically be the short pathname on filesystems that
     * support both short and long path forms.
     */
  String getCanonicalPath();

}