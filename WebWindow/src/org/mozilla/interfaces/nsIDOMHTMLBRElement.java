/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLBRElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLBRElement interface is the interface to a [X]HTML br
 * element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLBRElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLBRELEMENT_IID =
    "{a6cf90a5-15b3-11d2-932e-00805f8add32}";

  String getClear();

  void setClear(String aClear);

}