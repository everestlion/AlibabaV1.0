/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/base/public/nsIDOMFileException.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMFileException extends nsISupports {

  String NS_IDOMFILEEXCEPTION_IID =
    "{b52356e1-45c5-4d61-b61a-fb9bd91690e1}";

  int NOT_FOUND_ERR = 0;

  int NOT_READABLE_ERR = 1;

  int getCode();

}