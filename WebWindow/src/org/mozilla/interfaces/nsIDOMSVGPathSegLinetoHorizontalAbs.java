/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegLinetoHorizontalAbs extends nsISupports {

  String NS_IDOMSVGPATHSEGLINETOHORIZONTALABS_IID =
    "{4a54a4d2-edef-4e19-9600-2330311000f4}";

  float getX();

  void setX(float aX);

}