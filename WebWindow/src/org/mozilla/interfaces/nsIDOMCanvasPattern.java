/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/canvas/nsIDOMCanvasRenderingContext2D.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCanvasPattern extends nsISupports {

  String NS_IDOMCANVASPATTERN_IID =
    "{21dea65c-5c08-4eb1-ac82-81fe95be77b8}";

}