/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/widget/public/nsISound.idl
 */

package org.mozilla.interfaces;

public interface nsISound extends nsISupports {

  String NS_ISOUND_IID =
    "{b01adad7-d937-4738-8508-3bd5946bf9c8}";

  void play(nsIURL aURL);

  /**
   * for playing system sounds
   */
  void playSystemSound(String soundAlias);

  void beep();

  /**
    * Not strictly necessary, but avoids delay before first sound.
    * The various methods on nsISound call Init() if they need to.
	*/
  void init();

}