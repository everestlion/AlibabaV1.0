/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/gfx/idl/gfxIImageFrame.idl
 */

package org.mozilla.interfaces;

/**
 * gfxIImageFrame interface
 *
 * All x, y, width, height values are in pixels.
 *
 * @author Tim Rowley <tor@cs.brown.edu>
 * @author Stuart Parmenter <pavlov@netscape.com>
 * @version 0.1
 */
public interface gfxIImageFrame extends nsISupports {

  String GFXIIMAGEFRAME_IID =
    "{9c37930b-cadd-453c-89e1-9ed456715b9c}";

  /**
   * Create a new \a aWidth x \a aHeight sized image.
   *
   * @param aX The x-offset from the origin of the gfxIImageContainer parent.
   * @param aY The y-offset from the origin of the gfxIImageContainer parent.
   * @param aWidth The width of the image to create.
   * @param aHeight The height of the image to create.
   * @param aFormat the width of the image to create.
   *
   * @note The data in a new image is uninitialized.
   */
  void init(int aX, int aY, int aWidth, int aHeight, int aFormat, int aDepth);

  /**
   * TRUE by default.  When set to FALSE, you will no longer be able to make any modifications
   * to the data of the image.  Any attempts will fail.
   */
  boolean getMutable();

  /**
   * TRUE by default.  When set to FALSE, you will no longer be able to make any modifications
   * to the data of the image.  Any attempts will fail.
   */
  void setMutable(boolean aMutable);

  /**
   * The x-offset of the image.
   */
  int getX();

  /**
   * The y-offset of the image.
   */
  int getY();

  /**
   * The width of the image.
   */
  int getWidth();

  /**
   * The height of the image.
   */
  int getHeight();

  /**
   * The image data format the image was created with.
   * @see gfxIFormats
   */
  int getFormat();

  /**
   * returns whether the image requires the background to be painted
   */
  boolean getNeedsBackground();

  long getImageBytesPerRow();

  /**
   * returns the number of bytes allocated for the image
   */
  long getImageDataLength();

  void getImageData(byte[][] bits, long[] length);

  /**
   * Get Palette data pointer
   */
  void getPaletteData(long[][] palette, long[] length);

  /**
   * Lock image pixels before addressing the data directly
   */
  void lockImageData();

  /**
   * Unlock image pixels
   */
  void unlockImageData();

  /**
   * Represents the number of milliseconds until the next frame should be displayed.
   * @note -1 means that this frame should be displayed forever.
   */
  int getTimeout();

  /**
   * Represents the number of milliseconds until the next frame should be displayed.
   * @note -1 means that this frame should be displayed forever.
   */
  void setTimeout(int aTimeout);

  int getFrameDisposalMethod();

  void setFrameDisposalMethod(int aFrameDisposalMethod);

  int getBlendMethod();

  void setBlendMethod(int aBlendMethod);

}