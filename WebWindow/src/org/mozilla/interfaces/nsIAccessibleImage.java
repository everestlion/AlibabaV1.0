/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/accessible/public/nsIAccessibleImage.idl
 */

package org.mozilla.interfaces;

/**
 *
 * @status UNDER_REVIEW
 */
public interface nsIAccessibleImage extends nsISupports {

  String NS_IACCESSIBLEIMAGE_IID =
    "{09086623-0f09-4310-ac56-c2cda7c29648}";

  /**
   * Returns the coordinates of the image.
   *
   * @param coordType  specifies coordinates origin (for available constants
   *                   refer to nsIAccessibleCoordinateType)
   * @param x          the x coordinate
   * @param y          the y coordinate
   */
  void getImagePosition(long coordType, int[] x, int[] y);

  /**
   * Returns the size of the image.
   *
   * @param width      the heigth
   * @param height     the width
   */
  void getImageSize(int[] width, int[] height);

}