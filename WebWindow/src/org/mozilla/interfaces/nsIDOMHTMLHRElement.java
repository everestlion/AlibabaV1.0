/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLHRElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLHRElement interface is the interface to a [X]HTML hr
 * element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLHRElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLHRELEMENT_IID =
    "{a6cf90a8-15b3-11d2-932e-00805f8add32}";

  String getAlign();

  void setAlign(String aAlign);

  boolean getNoShade();

  void setNoShade(boolean aNoShade);

  String getSize();

  void setSize(String aSize);

  String getWidth();

  void setWidth(String aWidth);

}