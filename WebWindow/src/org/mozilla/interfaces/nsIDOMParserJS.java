/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/base/public/nsIDOMParser.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMParserJS interface provides a scriptable way of calling init().
 * Do NOT use this interface from languages other than JavaScript.
 */
public interface nsIDOMParserJS extends nsISupports {

  String NS_IDOMPARSERJS_IID =
    "{dca92fe9-ae7a-44b7-80aa-d151216698ac}";

  /**
   * Just like nsIDOMParser.init, but callable from JS.  It'll pick up the args
   * from XPConnect.
   */
  void init();

}