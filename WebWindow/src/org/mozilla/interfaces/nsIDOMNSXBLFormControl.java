/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMNSXBLFormControl.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNSXBLFormControl extends nsISupports {

  String NS_IDOMNSXBLFORMCONTROL_IID =
    "{1c28ed66-1dd2-11b2-95af-e2cf10931adb}";

  nsIBoxObject getBoxObject();

}