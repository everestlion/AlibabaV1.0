/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for 16-bit integers
 * 
 * @status FROZEN
 */
public interface nsISupportsPRInt16 extends nsISupportsPrimitive {

  String NS_ISUPPORTSPRINT16_IID =
    "{e30d94b0-4a1c-11d3-9890-006008962422}";

  short getData();

  void setData(short aData);

  String toString();

}