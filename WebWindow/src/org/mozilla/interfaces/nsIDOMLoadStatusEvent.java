/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/offline/nsIDOMLoadStatusEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLoadStatusEvent extends nsIDOMEvent {

  String NS_IDOMLOADSTATUSEVENT_IID =
    "{f14431b1-efb6-436c-a272-312f087b1459}";

  nsIDOMLoadStatus getStatus();

  void initLoadStatusEvent(String typeArg, boolean canBubbleArg, boolean cancellableArg, nsIDOMLoadStatus statusArg);

  void initLoadStatusEventNS(String namespaceURIArg, String typeArg, boolean canBubbleArg, boolean cancellableArg, nsIDOMLoadStatus statusArg);

}