/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsIVariant.idl
 */

package org.mozilla.interfaces;

public interface nsIDataType extends nsISupports {

  String NS_IDATATYPE_IID =
    "{4d12e540-83d7-11d5-90ed-0010a4e73d9a}";

  int VTYPE_INT8 = 0;

  int VTYPE_INT16 = 1;

  int VTYPE_INT32 = 2;

  int VTYPE_INT64 = 3;

  int VTYPE_UINT8 = 4;

  int VTYPE_UINT16 = 5;

  int VTYPE_UINT32 = 6;

  int VTYPE_UINT64 = 7;

  int VTYPE_FLOAT = 8;

  int VTYPE_DOUBLE = 9;

  int VTYPE_BOOL = 10;

  int VTYPE_CHAR = 11;

  int VTYPE_WCHAR = 12;

  int VTYPE_VOID = 13;

  int VTYPE_ID = 14;

  int VTYPE_DOMSTRING = 15;

  int VTYPE_CHAR_STR = 16;

  int VTYPE_WCHAR_STR = 17;

  int VTYPE_INTERFACE = 18;

  int VTYPE_INTERFACE_IS = 19;

  int VTYPE_ARRAY = 20;

  int VTYPE_STRING_SIZE_IS = 21;

  int VTYPE_WSTRING_SIZE_IS = 22;

  int VTYPE_UTF8STRING = 23;

  int VTYPE_CSTRING = 24;

  int VTYPE_ASTRING = 25;

  int VTYPE_EMPTY_ARRAY = 254;

  int VTYPE_EMPTY = 255;

}