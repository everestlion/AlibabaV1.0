/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegCurvetoQuadraticRel extends nsISupports {

  String NS_IDOMSVGPATHSEGCURVETOQUADRATICREL_IID =
    "{c46eb661-9c05-4d46-9b2a-c2ae5b166060}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  float getX1();

  void setX1(float aX1);

  float getY1();

  void setY1(float aY1);

}