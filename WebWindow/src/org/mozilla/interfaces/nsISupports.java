/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/base/nsISupports.idl
 */

package org.mozilla.interfaces;

public interface nsISupports {

  String NS_ISUPPORTS_IID =
    "{00000000-0000-0000-c000-000000000046}";

  nsISupports queryInterface(String uuid);

}