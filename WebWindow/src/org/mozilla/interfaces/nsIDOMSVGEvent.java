/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGEvent.idl
 */

package org.mozilla.interfaces;

/**
 * For more information on this interface please see
 * http://www.w3.org/TR/SVG11/script.html#InterfaceSVGEvent
 */
public interface nsIDOMSVGEvent extends nsIDOMEvent {

  String NS_IDOMSVGEVENT_IID =
    "{13aed1cc-a505-45d5-bbc2-0052c6bf200f}";

}