/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMCDATASection.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCDATASection extends nsIDOMText {

  String NS_IDOMCDATASECTION_IID =
    "{a6cf9071-15b3-11d2-932e-00805f8add32}";

}