/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGSymbolElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGSymbolElement extends nsIDOMSVGElement {

  String NS_IDOMSVGSYMBOLELEMENT_IID =
    "{86092181-a5db-4a89-be03-07dcc14d426e}";

}