/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGPathSeg.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGPathSegCurvetoCubicSmoothRel extends nsISupports {

  String NS_IDOMSVGPATHSEGCURVETOCUBICSMOOTHREL_IID =
    "{dd5b4b00-edaa-493a-b477-bbc2576b4a98}";

  float getX();

  void setX(float aX);

  float getY();

  void setY(float aY);

  float getX2();

  void setX2(float aX2);

  float getY2();

  void setY2(float aY2);

}