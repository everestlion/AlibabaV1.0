/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/base/nsIDOMPlugin.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMPlugin extends nsISupports {

  String NS_IDOMPLUGIN_IID =
    "{f6134681-f28b-11d2-8360-c90899049c3c}";

  String getDescription();

  String getFilename();

  String getName();

  long getLength();

  nsIDOMMimeType item(long index);

  nsIDOMMimeType namedItem(String name);

}