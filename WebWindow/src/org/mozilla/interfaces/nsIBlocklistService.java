/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/mozapps/extensions/public/nsIBlocklistService.idl
 */

package org.mozilla.interfaces;

public interface nsIBlocklistService extends nsISupports {

  String NS_IBLOCKLISTSERVICE_IID =
    "{0c3fe697-d50d-4f42-b747-0c5855cfc60e}";

  /**
   * Determine if an item is blocklisted
   * @param   id
   *          The GUID of the item.
   * @param   version
   *          The item's version.
   * @param   appVersion
   *          The version of the application we are checking in the blocklist.
   *          If this parameter is null, the version of the running application
   *          is used.
   * @param   toolkitVersion
   *          The version of the toolkit we are checking in the blocklist.
   *          If this parameter is null, the version of the running toolkit
   *          is used.
   * @returns true if the item is compatible with this version of the
   *          application or this version of the toolkit, false, otherwise.
   */
  boolean isAddonBlocklisted(String id, String version, String appVersion, String toolkitVersion);

}