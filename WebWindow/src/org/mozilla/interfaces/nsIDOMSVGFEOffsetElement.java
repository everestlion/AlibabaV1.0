/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGFilters.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGFEOffsetElement extends nsIDOMSVGFilterPrimitiveStandardAttributes {

  String NS_IDOMSVGFEOFFSETELEMENT_IID =
    "{c080f191-b22c-4fc0-85d5-a79dc3fa7ec8}";

  nsIDOMSVGAnimatedString getIn1();

  nsIDOMSVGAnimatedNumber getDx();

  nsIDOMSVGAnimatedNumber getDy();

}