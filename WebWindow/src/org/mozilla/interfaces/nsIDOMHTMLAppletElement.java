/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLAppletElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLAppletElement interface is the interface to a [X]HTML
 * applet element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLAppletElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLAPPLETELEMENT_IID =
    "{a6cf90ae-15b3-11d2-932e-00805f8add32}";

  String getAlign();

  void setAlign(String aAlign);

  String getAlt();

  void setAlt(String aAlt);

  String getArchive();

  void setArchive(String aArchive);

  String getCode();

  void setCode(String aCode);

  String getCodeBase();

  void setCodeBase(String aCodeBase);

  String getHeight();

  void setHeight(String aHeight);

  int getHspace();

  void setHspace(int aHspace);

  String getName();

  void setName(String aName);

  String getObject();

  void setObject(String aObject);

  int getVspace();

  void setVspace(int aVspace);

  String getWidth();

  void setWidth(String aWidth);

}