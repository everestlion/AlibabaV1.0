/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/storage/public/mozIStorageStatementWrapper.idl
 */

package org.mozilla.interfaces;

public interface mozIStorageStatementRow extends nsISupports {

  String MOZISTORAGESTATEMENTROW_IID =
    "{02eeaf95-c3db-4182-9340-222c29f68f02}";

}