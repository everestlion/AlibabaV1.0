/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/events/nsIDOMCommandEvent.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMCommandEvent extends nsIDOMEvent {

  String NS_IDOMCOMMANDEVENT_IID =
    "{37fb1798-0f76-4870-af6f-0135b4d973c8}";

  String getCommand();

  void initCommandEvent(String typeArg, boolean canBubbleArg, boolean canCancelArg, String command);

}