/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/core/nsIDOMNameList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMNameList extends nsISupports {

  String NS_IDOMNAMELIST_IID =
    "{faaf1b80-1ddd-11d9-8c46-000a95dc234c}";

  String getName(long index);

  String getNamespaceURI(long index);

  long getLength();

  boolean contains(String str);

  boolean containsNS(String namespaceURI, String name);

}