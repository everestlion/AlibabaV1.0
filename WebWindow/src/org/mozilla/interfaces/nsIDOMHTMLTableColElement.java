/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/html/nsIDOMHTMLTableColElement.idl
 */

package org.mozilla.interfaces;

/**
 * The nsIDOMHTMLTableColElement interface is the interface to a
 * [X]HTML col element.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */
public interface nsIDOMHTMLTableColElement extends nsIDOMHTMLElement {

  String NS_IDOMHTMLTABLECOLELEMENT_IID =
    "{a6cf90b4-15b3-11d2-932e-00805f8add32}";

  String getAlign();

  void setAlign(String aAlign);

  String getCh();

  void setCh(String aCh);

  String getChOff();

  void setChOff(String aChOff);

  int getSpan();

  void setSpan(int aSpan);

  String getVAlign();

  void setVAlign(String aVAlign);

  String getWidth();

  void setWidth(String aWidth);

}