/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/components/places/public/nsINavHistoryService.idl
 */

package org.mozilla.interfaces;

/**
 * Allows clients to observe what is happening to a result as it updates itself
 * according to history and bookmark system events. Register this observer on a
 * result using registerView
 *
 * @see nsINavHistoryResult for where this fits in
 */
public interface nsINavHistoryResultViewer extends nsISupports {

  String NS_INAVHISTORYRESULTVIEWER_IID =
    "{e60f4429-3787-45c8-a8c0-18ef52621bbf}";

  /**
   * Called when 'aItem' is inserted into 'aParent' at index 'aNewIndex'.
   * The item previously at index (if any) and everything below it will have
   * been shifted down by one. The item may be a container or a leaf.
   */
  void itemInserted(nsINavHistoryContainerResultNode aParent, nsINavHistoryResultNode aItem, long aNewIndex);

  /**
   * Called whan 'aItem' is removed from 'aParent' at 'aOldIndex'. The item
   * may be a container or a leaf. This function will be called after the item
   * has been removed from its parent list, but before anything else (including
   * NULLing out the item's parent) has happened.
   */
  void itemRemoved(nsINavHistoryContainerResultNode aParent, nsINavHistoryResultNode aItem, long aOldIndex);

  /**
   * Called whan 'aItem' is moved from 'aOldParent' at 'aOldIndex' to
   * aNewParent at aNewIndex. The item may be a container or a leaf.
   *
   * XXX: at the moment, this method is called only when an item is moved
   * within the same container. When an item is moved between containers,
   * a new node is created for the item, and the itemRemoved/itemAdded methods
   * are used.
   */
  void itemMoved(nsINavHistoryResultNode aItem, nsINavHistoryContainerResultNode aOldParent, long aOldIndex, nsINavHistoryContainerResultNode aNewParent, long aNewIndex);

  /**
   * Called when an item has been changed and should be repainted. This only
   * refers to the specific item. If it is a container, getting this message
   * does not imply anything happened to the children. You'll get separate
   * messages for those. Also, this may be called for container nodes at times
   * when the result thinks it's possible that a twisty mey need to bw redrawn.
   */
  void itemChanged(nsINavHistoryResultNode item);

  /**
   * Called when an item is being replaced with another item at the exact
   * same position.
   */
  void itemReplaced(nsINavHistoryContainerResultNode parent, nsINavHistoryResultNode oldItem, nsINavHistoryResultNode newItem, long index);

  /**
   * Called after a container node went from closed to opened.
   */
  void containerOpened(nsINavHistoryContainerResultNode item);

  /**
   * Called after a container node went from opened to closed. This will be
   * called for the topmost container that is closing, and implies that any
   * child containers have closed as well.
   */
  void containerClosed(nsINavHistoryContainerResultNode item);

  /**
   * Called when something significant has happened within the container. The
   * contents of the container should be re-built.
   */
  void invalidateContainer(nsINavHistoryContainerResultNode item);

  /**
   * Called when something significant is changing that requires everything
   * to be recomputed. For example, changing sorting can affect every row.
   */
  void invalidateAll();

  /**
   * This is called to indicate to the UI that the sort has changed to the
   * given mode. For trees, for example, this would update the column headers
   * to reflect the sorting. For many other types of views, this won't be
   * applicable.
   *
   * @param sortingMode  One of nsINavHistoryQueryOptions.SORT_BY_* that
   *                     indicates the new sorting mode.
   *
   * This only is expected to update the sorting UI. invalidateAll() will also
   * get called if the sorting changes to update everything.
   */
  void sortingChanged(int sortingMode);

  /**
   * Called by the result when this object is set using
   * nsINavHistoryResult.viewer. This will be set to NULL when the result
   * is being deallocated. This should not be set by other code.
   */
  nsINavHistoryResult getResult();

  /**
   * Called by the result when this object is set using
   * nsINavHistoryResult.viewer. This will be set to NULL when the result
   * is being deallocated. This should not be set by other code.
   */
  void setResult(nsINavHistoryResult aResult);

}