/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/xul/nsIDOMXULImageElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMXULImageElement extends nsIDOMXULElement {

  String NS_IDOMXULIMAGEELEMENT_IID =
    "{f73f4d77-a6fb-4ab5-b41e-15045a0cc6ff}";

  String getSrc();

  void setSrc(String aSrc);

}