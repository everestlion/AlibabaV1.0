/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGAnimatedRect.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGAnimatedRect extends nsISupports {

  String NS_IDOMSVGANIMATEDRECT_IID =
    "{ca45959e-f1da-46f6-af19-1ecdc322285a}";

  nsIDOMSVGRect getBaseVal();

  nsIDOMSVGRect getAnimVal();

}