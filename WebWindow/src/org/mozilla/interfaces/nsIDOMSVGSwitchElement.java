/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGSwitchElement.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGSwitchElement extends nsIDOMSVGElement {

  String NS_IDOMSVGSWITCHELEMENT_IID =
    "{7676f306-22c9-427e-bd71-2b1315851c93}";

}