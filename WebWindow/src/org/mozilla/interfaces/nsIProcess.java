/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/threads/nsIProcess.idl
 */

package org.mozilla.interfaces;

public interface nsIProcess extends nsISupports {

  String NS_IPROCESS_IID =
    "{9da0b650-d07e-4617-a18a-250035572ac8}";

  void init(nsIFile executable);

  void initWithPid(long pid);

  void kill();

  /** XXX what charset? **/
/** Executes the file this object was initialized with
         * @param blocking Whether to wait until the process terminates before returning or not
         * @param args An array of arguments to pass to the process
         * @param count The length of the args array
         * @return the PID of the newly spawned process */
  long run(boolean blocking, String[] args, long count);

  nsIFile getLocation();

  long getPid();

  String getProcessName();

  long getProcessSignature();

  int getExitValue();

}