/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/views/nsIDOMAbstractView.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMAbstractView extends nsISupports {

  String NS_IDOMABSTRACTVIEW_IID =
    "{f51ebade-8b1a-11d3-aae7-0010830123b4}";

  /**
 * The nsIDOMAbstractView interface is a datatype for a view in the
 * Document Object Model.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-Views
 *
 * @status FROZEN
 */
  nsIDOMDocumentView getDocument();

}