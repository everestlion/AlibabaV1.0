/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/svg/nsIDOMSVGTransformable.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMSVGTransformable extends nsIDOMSVGLocatable {

  String NS_IDOMSVGTRANSFORMABLE_IID =
    "{b81f6e37-1842-4534-a546-1ab86e59a3c6}";

  nsIDOMSVGAnimatedTransformList getTransform();

}