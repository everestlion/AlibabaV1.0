/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/xpcom/ds/nsISupportsPrimitives.idl
 */

package org.mozilla.interfaces;

/**
 * Scriptable storage for generic pointers
 * 
 * @status FROZEN
 */
public interface nsISupportsVoid extends nsISupportsPrimitive {

  String NS_ISUPPORTSVOID_IID =
    "{464484f0-568d-11d3-baf8-00805f8a5dd7}";

  String toString();

}