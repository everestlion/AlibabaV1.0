/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/dom/public/idl/ls/nsIDOMLSParserFilter.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMLSParserFilter extends nsISupports {

  String NS_IDOMLSPARSERFILTER_IID =
    "{10e8893d-ddf5-45d1-8872-615d72065fb4}";

  short FILTER_ACCEPT = 1;

  short FILTER_REJECT = 2;

  short FILTER_SKIP = 3;

  short FILTER_INTERRUPT = 4;

  int startElement(nsIDOMElement elementArg);

  int acceptNode(nsIDOMNode nodeArg);

  long getWhatToShow();

}