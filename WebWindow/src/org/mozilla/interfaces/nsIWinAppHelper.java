/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/toolkit/xre/nsIWinAppHelper.idl
 */

package org.mozilla.interfaces;

public interface nsIWinAppHelper extends nsISupports {

  String NS_IWINAPPHELPER_IID =
    "{2bd9ec66-05eb-4f63-8825-a83ccf00fc7f}";

  void postUpdate(nsILocalFile logFile);

  boolean getUserCanElevate();

}