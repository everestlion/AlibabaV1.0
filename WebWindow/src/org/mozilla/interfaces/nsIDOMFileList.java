/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/content/base/public/nsIDOMFileList.idl
 */

package org.mozilla.interfaces;

public interface nsIDOMFileList extends nsISupports {

  String NS_IDOMFILELIST_IID =
    "{3bfef9fa-8ad3-4e49-bd62-d6cd75b29298}";

  long getLength();

  nsIDOMFile item(long index);

}