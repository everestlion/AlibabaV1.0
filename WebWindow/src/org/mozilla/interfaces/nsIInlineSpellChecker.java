/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM
 * e:/xr19rel/WINNT_5.2_Depend/mozilla/editor/txtsvc/public/nsIInlineSpellChecker.idl
 */

package org.mozilla.interfaces;

public interface nsIInlineSpellChecker extends nsISupports {

  String NS_IINLINESPELLCHECKER_IID =
    "{f5d1ec9e-4d30-11d8-8053-da0cc7df1f20}";

  nsIEditorSpellCheck getSpellChecker();

  boolean getEnableRealTimeSpell();

  void setEnableRealTimeSpell(boolean aEnableRealTimeSpell);

  void spellCheckAfterEditorChange(int aAction, nsISelection aSelection, nsIDOMNode aPreviousSelectedNode, int aPreviousSelectedOffset, nsIDOMNode aStartNode, int aStartOffset, nsIDOMNode aEndNode, int aEndOffset);

  void spellCheckRange(nsIDOMRange aSelection);

  nsIDOMRange getMispelledWord(nsIDOMNode aNode, int aOffset);

  void replaceWord(nsIDOMNode aNode, int aOffset, String aNewword);

  void addWordToDictionary(String aWord);

  void ignoreWord(String aWord);

  void ignoreWords(String[] aWordsToIgnore, long aCount);

}