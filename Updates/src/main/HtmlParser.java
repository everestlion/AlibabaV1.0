package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.tags.ParagraphTag;
import org.htmlparser.tags.Span;
import org.htmlparser.util.NodeList;

public class HtmlParser {
	public static String getHtmlContent(URL url, String encode) {
		StringBuffer contentBuffer = new StringBuffer();

		int responseCode = -1;
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setRequestProperty("User-Agent",
					"Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");// IE代理进行下载
			con.setConnectTimeout(60000);
			con.setReadTimeout(60000);
			// 获得网页返回信息码
			responseCode = con.getResponseCode();
			if (responseCode == -1) {
				System.out.println(url.toString()
						+ " : connection is failure...");
				con.disconnect();
				return null;
			}
			if (responseCode >= 400) // 请求失败
			{
				System.out.println("请求失败:get response code: " + responseCode);
				con.disconnect();
				return null;
			}

			InputStream inStr = con.getInputStream();
			InputStreamReader istreamReader = new InputStreamReader(inStr,
					encode);
			BufferedReader buffStr = new BufferedReader(istreamReader);

			String str = null;
			while ((str = buffStr.readLine()) != null)
				contentBuffer.append(str);
			inStr.close();
		} catch (IOException e) {
			e.printStackTrace();
			contentBuffer = null;
			System.out.println("error: " + url.toString());
		} finally {
			con.disconnect();
		}
		return contentBuffer.toString();
	}

	public static String getHtmlContent(String url, String encode) {
		if (!url.toLowerCase().startsWith("http://")) {
			url = "http://" + url;
		}
		try {
			URL rUrl = new URL(url);
			return getHtmlContent(rUrl, encode);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Map<String, String> getData(String html) {
		try {
		 Parser parser = Parser.createParser(html, "utf-8");
		 
         try {
             // 通过过滤器过滤出<A>标签   
             NodeList nodeList = parser   
                     .extractAllNodesThatMatch(new NodeFilter() {   
                         /**
						 * 
						 */
						private static final long serialVersionUID = 299745861734512487L;

						//实现该方法,用以过滤标签   
                         public boolean accept(Node node) {   
                             if (node instanceof ParagraphTag) {//<A>标记
                            	 Node firstChild = node.getFirstChild();
                            	 if (firstChild instanceof Span && ((Span)firstChild).getAttribute("class") != null) {
                            		  
                            		 return true; 
                            	 }
                         }
                             return false;   
                         }   
                     });   
             // 打印   
             for (int i = 0; i < nodeList.size(); i++) {   
            	 ParagraphTag n = (ParagraphTag) nodeList.elementAt(i);
            	 Node[] nodeArr = n.getChildrenAsNodeArray();
            	 for (int j = 1; j < nodeArr.length; j++) {
            		 String text = nodeArr[j].getText();
            		 if (text.contains("br")) {
            			 continue;
            		 }
            		 text = text.replaceAll("分享社", "");
            		 text = text.replaceAll("迅雷会员账号", "");
            		 text = text.replaceAll("迅雷会员密码", "==>");
            		 System.out.println(text);
            	 }
//            	 String value = n.getAttributeEx("class") != null ? n.getAttributeEx("class").getValue() : null;
//                 if ((value != null && value.toString().equalsIgnoreCase("wp_keywordlink_affiliate")) == true) {
//                	 System.out.print(n.getNextSibling().getText());   
//                 }
                 
//                 System.out.println(n.extractLink());   
             }   
         } catch (Exception e) {   
             e.printStackTrace();   
         }   
 
     } catch (Exception e) {   
         e.printStackTrace();   
     }    
	Map<String, String> dataMap = new HashMap<String, String>();
		
		return dataMap;
	}

	public static void main(String argsp[]) {
//		System.out.println(getHtmlContent("http://www.fenxs.com/1079.html", "utf-8"));
		getData(getHtmlContent("http://www.fenxs.com/1079.html", "utf-8"));

	}
}