package main;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;





import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.HeadingTag;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.MetaTag;
import org.htmlparser.util.NodeList;

import dao.AlibabaDao;
import dao.DBUtil;
import utils.Utils;
import vo.Item;
import vo.Product;

public class AlibabaInspect {

	// private static final String companyName =
	// "Shenzhen Lex Optoelectronic Co., Ltd.";

	private static String next = null;

	private static boolean gotShowCaseProductsId = false;
	
	private static int configPages = 20;
	private static int pageOrder = 0;
	private static int order = 0;

	private static int donePages = 0;

	private static int i = 0;// 第几个关键词

	private static String curUrl = null;
	
	private static String editUrl = "http://hz.productposting.alibaba.com/product/editing.htm?id=";
	
	private static KeyListener keyListener;
	
	private static String searchKeyword = null;
	
	private static Map<String, Map<String, Product>> gotMap = new HashMap<String, Map<String,Product>>();
	
	private static Map<String, Map<String, Product>> dataMap = new HashMap<String, Map<String,Product>>();
	
	public static String getHtmlContent(URL url, String encode) {
		StringBuffer contentBuffer = new StringBuffer();

		int responseCode = -1;
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");// IE代理进行下载
			con.setConnectTimeout(60000);
			con.setReadTimeout(60000);
			// 获得网页返回信息码
			responseCode = con.getResponseCode();
			if (responseCode == -1) {
				System.out.println(url.toString() + " : connection is failure...");
				con.disconnect();
				return null;
			}
			if (responseCode >= 400) // 请求失败
			{
				System.out.println("请求失败:get response code: " + responseCode);
				con.disconnect();
				return null;
			}

			InputStream inStr = con.getInputStream();
			InputStreamReader istreamReader = new InputStreamReader(inStr, encode);
			BufferedReader buffStr = new BufferedReader(istreamReader);

			String str = null;
			while ((str = buffStr.readLine()) != null)
				contentBuffer.append(str);
			inStr.close();
		} catch (IOException e) {
			e.printStackTrace();
			contentBuffer = null;
			System.out.println("error: " + url.toString());
		} finally {
			con.disconnect();
		}
		return contentBuffer.toString();
	}

	public static String getHtmlContent(String url, String encode) {
		if (!url.toLowerCase().startsWith("http://")) {
			url = "http://" + url;
		}
		try {
			URL rUrl = new URL(url);
			return getHtmlContent(rUrl, encode);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void getOwner(String html, final Item item) {
		try {
			Parser parser = Parser.createParser(html, "utf-8");
			try {
				// 通过过滤器过滤出<A>标签
				@SuppressWarnings("unused")
				NodeList nodeList = parser.extractAllNodesThatMatch(new NodeFilter() {

					/**
							 * 
							 */
					private static final long serialVersionUID = -2906136240488718581L;
					
					// 实现该方法,用以过滤标签
					public boolean accept(Node node) {
						if (node instanceof MetaTag) {
							MetaTag metaTag = (MetaTag)node;
	                   		 if (metaTag.getAttribute("name") != null && metaTag.getAttribute("name").equals("keywords")) {
	                   			 String contentStr = metaTag.getAttribute("content");
	                   			 String keywords = contentStr != null ? contentStr.substring(contentStr.indexOf(" High Quality ") + 14, contentStr.length()) : null;
	                   			 keywords = keywords != null ? keywords.replaceAll(Config.companyName, "") : null;
	                   			if (keywords != null && keywords.length() > 0) {
	                   				String[] tmpkeywords = new String[3];
	                   				String [] tmp = keywords.split(",");
	                   				for (int i = 0; i < tmp.length; i++) {
	                   					tmpkeywords[i] = tmp[i];
	                   					if (i >= 2) {
	                   						break;
	                   					}
	                   				}
	                   				item.setKeywords(tmpkeywords);
	                   			}
                   			 }
						} else if (node instanceof Div && ((Div) node).getAttribute("class") != null && ((Div) node).getAttribute("class").equals("avatar")) {
							String owner = ((ImageTag) ((Div) node).getChild(1)).getAttribute("alt");
							item.setOwner(owner);
							try {
								item.setOwnerImgByteArray(Utils.getUrlFileData(((ImageTag) ((Div) node).getChild(1)).getAttribute("src")));
								return true;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						return false;
					}

				});
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void getData(String html) {
		try {
			Parser parser = Parser.createParser(html, "utf-8");

			try {
				// 通过过滤器过滤出<A>标签
				@SuppressWarnings("unused")
				NodeList nodeList = parser.extractAllNodesThatMatch(new NodeFilter() {
					/**
						 * 
						 */
					private static final long serialVersionUID = 1L;

					/**
						 * 
						 */
					
					// 实现该方法,用以过滤标签
					public boolean accept(Node node) {
						if (node instanceof LinkTag) {
							if (((LinkTag) node).getAttribute("class") != null && ((LinkTag) node).getAttribute("class").equals("next")) {
								next = ((LinkTag) node).getAttribute("href");
							}
							return true;
						} else if (node instanceof Div && "cwrap".equals(((Div) node).getAttribute("class"))) {
							order++;
							Product product = new Product();
							Node corp = ((Div) ((Div) node).getChild(1)).getChild(1);
							Node ahref = ((Div) corp).getChild(1).getFirstChild();
							String linkStr = ((TagNode) ahref).getAttribute("href");
							String titleStr = ((TagNode) ahref).getAttribute("title");
							String pId = ((TagNode) ahref).getAttribute("data-pid");
							product.setProductId(pId);
							product.setHref(linkStr);
							product.setTitle(titleStr);
							product.setOrder(order);
							Node imgDiv = node.getParent().getParent().getLastChild().getPreviousSibling();
							String imgUrl = ((ImageTag) (imgDiv.getFirstChild().getNextSibling().getFirstChild().getNextSibling().getFirstChild()
									.getNextSibling())).getAttribute("image-src");
//							byte[] imgByteArray = Utils.getUrlFileData(imgUrl);
//							item.setImgByteArray(imgByteArray != null ? imgByteArray : defaultImgByteArray);
							
//							getOwner(getHtmlContent(linkStr, "utf-8"), item);
//							dataMap.get(i).add(item);
							System.out.println(linkStr + "---" + titleStr);
							gotMap.get(searchKeyword).put(pId, product);
							AlibabaDao.insertProduct(product, searchKeyword);
							return true;
						}
						return false;
					}
				});
				
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static Map<String, Map<String, List<Product>>> changeMap = new HashMap<String, Map<String,List<Product>>>();
	
	public static void analize() {
		if (dataMap == null || gotMap == null) {
			return;
		}
		for (String e : gotMap.keySet()) {
			Map<String, Product> newMap = gotMap.get(e);
			Map<String, Product> oldMap = dataMap.get(e);
			for (Product p : newMap.values()) {
				Product oldProduct = oldMap.get(p.getProductId());
				if (oldProduct == null && isChange(p, oldProduct)) {
					if (!changeMap.containsKey(p.getSearchKeywords())) {
						changeMap.put(p.getSearchKeywords(), new HashMap<String, List<Product>>());
					}
					List<Product> products = new ArrayList<Product>();
					products.add(p);
					products.add(oldProduct);
					changeMap.get(p.getSearchKeywords()).put(p.getProductId(), products);
				}
			}
		}
	}
	
	public static boolean isChange(Product newProduct, Product oldProduct) {
		if (newProduct.getOrder() != oldProduct.getOrder()) {
			return true;
		}
		if (newProduct.getTitle() != null && !newProduct.getTitle().equals(oldProduct.getTitle())) {
			return true;
		}
		
		return false;
	}

	public static void main(String argsp[]) {
		// System.out.println(getHtmlContent("http://www.fenxs.com/1079.html",
		// "utf-8"));
		keyListener = new KeyListener();
		for (; i < Config.keywords.size(); i++) {

			order = 0;
			donePages = 0;
			searchKeyword = Config.keywords.get(i);
			gotMap.put(searchKeyword, new HashMap<String, Product>());
			System.out.println(searchKeyword);
			try {
				curUrl = Config.searchUrl + URLEncoder.encode(Config.keywords.get(i), "utf-8");
			} catch (Exception e) {
				e.printStackTrace();
			}
			getData(getHtmlContent(curUrl, "utf-8"));
			pageOrder = 0;
			donePages++;
			
			while (donePages < configPages && next != null) {
				curUrl = Config.webSite + next;
				next = null;
				String html = getHtmlContent(curUrl, "utf-8");
				getData(html);
//				if (html != null && html.contains(companyName)) {
//				}
				pageOrder = 0;
				donePages++;
			}
		}
		dataMap = AlibabaDao.selectProductsDB();
		DBUtil.closeConn();
		analize();
		writeToExcel();
	}

	private static void writeToExcel() {
		FileOutputStream fileOut = null;
		BufferedImage bufferImg = null;
		// 先把读进来的图片放到一个ByteArrayOutputStream中，以便产生ByteArray
		try {
			// ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
			// bufferImg = ImageIO.read(new File("F:/temp/test.jpg"));
			// ImageIO.write(bufferImg, "jpg", byteArrayOut);

			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet1 = wb.createSheet("test picture");
//			sheet1.setColumnWidth((short) 1, 50);
			// 画图的顶级管理器，一个sheet只能获取一个（一定要注意这点）
			HSSFPatriarch patriarch = sheet1.createDrawingPatriarch();
			// anchor主要用于设置图片的属性
			int dx1 = 0;
			int dy1 = 0;
			int dx2 = 0;
			int dy2 = 0;
			short col1 = 0;
			int row1 = 1;
			short col2 = 1;
			int row2 = 5;
			HSSFCellStyle cellStyle = wb.createCellStyle();
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
			cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			cellStyle.setWrapText(true);

			HSSFCellStyle hlinkStyle = wb.createCellStyle();
			HSSFFont hlinkFont = wb.createFont();
			hlinkFont.setUnderline(HSSFFont.U_SINGLE);
			hlinkFont.setColor(HSSFColor.BLUE.index);
			hlinkStyle.setFont(hlinkFont);
			hlinkStyle.setAlignment(CellStyle.ALIGN_CENTER);
			hlinkStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			hlinkStyle.setWrapText(true);
			
			HSSFCellStyle keywordStyle = wb.createCellStyle();
			keywordStyle.setAlignment(CellStyle.ALIGN_CENTER);
			keywordStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			keywordStyle.setWrapText(true);
			HSSFFont font = wb.createFont();
			font.setFontName("黑体");
			font.setColor(Font.COLOR_RED);
			keywordStyle.setFont(font);

			for (int i = 0; i < Config.keywords.size(); i++) {
				HSSFRow keywordRow = sheet1.createRow(row1);
				HSSFCell keywordCell = keywordRow.createCell((int)col1);
				keywordCell.setCellValue(Config.keywords.get(i));
				sheet1.addMergedRegion(new CellRangeAddress(row1, row1, col1, col1 + 5));
				keywordRow.setHeight((short)600);
				keywordCell.setCellStyle(keywordStyle);
				
				row1 += 1;
				row2 += 1;
				ArrayList<Item> list = new ArrayList<Item>();
				for (Item e : list) {
					if (e.getImgByteArray() != null) {
						HSSFClientAnchor anchor = new HSSFClientAnchor(dx1, dy1, dx2, dy2, col1, row1, col2, row2);
						anchor.setAnchorType(0);
						patriarch.createPicture(anchor, wb.addPicture(e.getImgByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG));
					} 
					HSSFRow row = sheet1.createRow(row1);
					
					int cellIndex = col1 + 1;
					sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));
					if (Config.showcaseProductIds.contains(e.getProductId())) {
						HSSFCell showcaseCell = row.createCell(cellIndex);
						showcaseCell.setCellStyle(hlinkStyle);
						showcaseCell.setCellValue("橱窗产品");
					}
					cellIndex += 1;
					HSSFCell pageCell = row.createCell(cellIndex);
					pageCell.setCellStyle(hlinkStyle);
					pageCell.setCellValue("第" + e.getPage() + "頁,第" + e.getPageOrder() + "個");
					sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));
					HSSFHyperlink pageLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
					pageLink.setAddress(e.getPageUrl());
					pageCell.setHyperlink(pageLink);
					
					cellIndex += 1;
					HSSFCell titleCell = row.createCell(cellIndex);
					titleCell.setCellStyle(hlinkStyle);
					titleCell.setCellValue(e.getTitle());
					HSSFHyperlink editLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
					editLink.setAddress(editUrl + e.getProductId());
					titleCell.setHyperlink(editLink);
					sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));
					
					cellIndex += 1;
					HSSFCell keywordsCell = row.createCell(cellIndex);
					keywordsCell.setCellStyle(cellStyle);
					keywordsCell.setCellValue(e.getKeywordsStr());
					sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));

					cellIndex += 1;
					HSSFCell urlCell = row.createCell(cellIndex);
					urlCell.setCellStyle(hlinkStyle);
					urlCell.setCellValue(e.getHref());
					HSSFHyperlink link = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
					link.setAddress(e.getHref());
					urlCell.setHyperlink(link);
					sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));

					cellIndex += 1;
					HSSFCell ownerCell = row.createCell(cellIndex);
					ownerCell.setCellStyle(cellStyle);
					ownerCell.setCellValue(e.getOwner());
					sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));

					row1 += 5;
					row2 += 5;
				}
			}
			// 插入图片
			// patriarch.createPicture(anchor,
			// wb.addPicture(byteArrayOut.toByteArray(),
			// HSSFWorkbook.PICTURE_TYPE_JPEG));
			// byte[] byteArray =
			// getUrlFileData("http://i01.i.aliimg.com/photo/v2/60072823787_1/9H_hardness_2_5D_round_edge_tempered.jpg_140x140.jpg");
			String fileName = "F:/temp/AliRank_" + Config.companyName + "_" + System.currentTimeMillis() + ".xls";
			fileOut = new FileOutputStream(fileName);
			// 写入excel文件
			wb.write(fileOut);
			System.out.println("----Excle文件已生成------" + fileName);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
}