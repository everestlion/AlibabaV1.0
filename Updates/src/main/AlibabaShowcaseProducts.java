package main;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;

import utils.Utils;

public class AlibabaShowcaseProducts {
	
	
	public static void main(String[] args) {
//		String html = readHtlm(Config.showcaseHtml);
		String html = Utils.getHtmlContent("http://boddiga.en.alibaba.com/featureproductlist.html", "utf-8");
		getProductIds(html);
		
	}
	
	public static HashSet<String> getProductsIdsFile() {
		String html = readHtlm(Config.showcaseHtml);
		return getProductIds(html);
	}
	
	public static HashSet<String> getProductsIdsUrl(String url) {
		HashSet<String> idsSet = new HashSet<String>();
		String html = Utils.getHtmlContent(url + Config.showCasePage, "utf-8");
		idsSet.addAll(getProductIds(html));
		
		String regex = "<a class=\"ui-pagination-next\" href=\"[\t]+/featureproductlist-[0-9]*\\.html\\?isGallery=Y";
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(html);
		while (html != null && matcher.find()) {
			String pageUrl = matcher.group(0).toString().replaceAll("<a class=\"ui-pagination-next\" href=\"", "").trim();
			html = null;
			html = Utils.getHtmlContent(url + pageUrl, "utf-8");
			matcher = p.matcher(html);
			idsSet.addAll(getProductIds(html));
		}
		
		return idsSet;
	}
	
	
	public static HashSet<String> getProductIds(String html) {
//		String regex = "/product_detail.htm\\?id=[0-9]*";
		HashSet<String> idsSet = new HashSet<String>();
		String regex = "data-role=\"chk-product\" value=\"[0-9]*";
		Pattern p = Pattern.compile(regex);
		Matcher matcher = p.matcher(html);
		while (matcher.find()) {
			String pId = matcher.group(0).toString().replaceAll("data-role=\"chk-product\" value=\"", "");
			idsSet.add(pId);
			System.out.println("showcaseProductIds.add(\"" + pId +"\");");
		}
		return idsSet;
	}
	
	public static String readHtlm(String path) {
		StringBuffer contentBuffer = new StringBuffer();
		InputStreamReader istreamReader = null;
		InputStream inStr = null;
		BufferedReader buffStr = null;
		try {
			inStr = new FileInputStream(path);
			istreamReader = new InputStreamReader(inStr,
					"utf-8");
			buffStr = new BufferedReader(istreamReader);
			
			String str = null;
			while ((str = buffStr.readLine()) != null)
				contentBuffer.append(str);
			inStr.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (istreamReader != null) {
					istreamReader.close();
				}
				if (inStr != null) {
					inStr.close();
				}
				if (buffStr != null) {
					buffStr.close();
				}
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return contentBuffer.toString();
	}

}
