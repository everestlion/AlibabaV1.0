package main;

import java.awt.Button;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyListener extends Frame{

	public int key1 = 0;
	public int key2 = 0;
	
	public static void main(String[] args) {
		new KeyListener();
	}
	
	public KeyListener () {
		init();
	}
	
	public void init() {
		this.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (key1 == 0 && e.getKeyCode() == KeyEvent.VK_CONTROL) {
					key1 = e.getKeyCode();
				} else if (key2 == 0 && e.getKeyCode() == KeyEvent.VK_Z) {
					key2 = e.getKeyCode();
				}
				if (key1 == KeyEvent.VK_CONTROL && key2 == KeyEvent.VK_Z) {
					System.out.println("ok");
				}
				super.keyPressed(e);
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
					key1 = 0;
				} else if (e.getKeyCode() == KeyEvent.VK_Z) {
					key2 = 0;
				}
				super.keyReleased(e);
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				super.keyTyped(e);
			}
			
		});
	}

}
