package main;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.tags.HeadingTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.MetaTag;
import org.htmlparser.tags.TitleTag;
import org.htmlparser.util.NodeList;

import utils.Utils;

public class AlibabaGetKeywords {
	
	private static String next = null;
	
	private static HSSFWorkbook wb;
	private static HSSFSheet sheet;
	
	private static int curRow = 0;
	private static int order = 0;
	
	static {
//		File file = new File("resource/data.xls");
//		try {
//			wb = new HSSFWorkbook(new FileInputStream(file));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	public static void main(String[] args) {
		long beginTime = System.currentTimeMillis();
		wb = new HSSFWorkbook();
		sheet = wb.createSheet("data");
		getData(Utils.getHtmlContent(Config.catelogUrl, "utf-8"));
		
		int donePages = 1;
		String nextUrl = null;
		while ((Config.configCatPages == 0 || donePages < Config.configCatPages) && next != null) {
			nextUrl = Config.webSite + next;
			next = null;
			String html = Utils.getHtmlContent(nextUrl, "utf-8");
			getData(html);
			donePages++;
			System.out.println("========== " + donePages + " =========");
		}
		FileOutputStream fos = null;
		String outFileName = "resource/data" + System.currentTimeMillis() + ".xls";
		try {
			
			fos = new FileOutputStream(outFileName);
			wb.write(fos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("cost time:" + (System.currentTimeMillis() - beginTime));
		System.out.println("outFileName:" + outFileName);
	}
	
	
	public static void getData(String html) {
		try {
		 Parser parser = Parser.createParser(html, "utf-8");
		 
         try {
             // 通过过滤器过滤出<A>标签   
             @SuppressWarnings("unused")
			NodeList nodeList = parser   
                     .extractAllNodesThatMatch(new NodeFilter() {   
						/**
						 * 
						 */
						private static final long serialVersionUID = 248281332878275828L;

						//实现该方法,用以过滤标签   
                         public boolean accept(Node node) {
                        	 if (node instanceof LinkTag) {
                        		 if (((LinkTag)node).getAttribute("class") != null && ((LinkTag)node).getAttribute("class").equals("next")) {
                        			 next = ((LinkTag)node).getAttribute("href");
                        		 }
                        		 return true; 
                        	 } else if (node instanceof HeadingTag) {//<A>标记
                        		 long begin = System.currentTimeMillis();
                        		 HeadingTag headingTag = (HeadingTag)node;
                         		if (headingTag.getAttribute("class") != null && headingTag.getAttribute("class").equals("title")) {
                         			LinkTag linkTag = ((LinkTag)(headingTag.getChild(0)));
	                         		 String linkStr = linkTag.getAttribute("href");
	                         		 String titleStr = linkTag.getAttribute("title");
	                         		 String detailHtml = Utils.getHtmlContent(linkStr, "utf-8");
	                           		getKeywords(detailHtml);
	                           		System.out.println(System.currentTimeMillis() - begin);
	                           	 }
                       		return true; 
                           	 
                        }
                        	 return false;
                         }   
                     });   
             
         } catch (Exception e) {   
             e.printStackTrace();   
         }   
 
     } catch (Exception e) {   
         e.printStackTrace();   
     }    
	}
	
	public static String[] getKeywords(String html) {
		final String content[][] = new String[1][];
        try {
       	 	Parser parser = Parser.createParser(html, "utf-8");
       	 	try {
       	 	 @SuppressWarnings("unused")
 			NodeList nodeList = parser   
                     .extractAllNodesThatMatch(new NodeFilter() {   

 						/**
 						 * 
 						 */
 						private static final long serialVersionUID = 1L;
 						private String title;
 						//实现该方法,用以过滤标签   
                         public boolean accept(Node node) {
                        	 if (node instanceof TitleTag) {
                        		 String str = ((TitleTag) node).getStringText();
                        		 if (str.contains("- Buy")) {
                        			 title = str.substring(0, str.indexOf(" - Buy"));
                        		 } else {
                        			 title = str.substring(0, str.indexOf(" - Alibaba"));
									
								}
//                        		 StackTraceElement[] stacks = new Throwable().getStackTrace();
//                        		 System.out.println("cur num:" + stacks[1].getLineNumber());
                        	 } else if (node instanceof MetaTag) {
                        		 long begin = System.currentTimeMillis();
                        		 MetaTag metaTag = (MetaTag)node;
                        		 if (metaTag.getAttribute("name") != null && metaTag.getAttribute("name").equals("keywords")) {
                        			 String contentStr = metaTag.getAttribute("content");
                        			 boolean replace = title.contains(",");
//                        			 int i = contentStr.indexOf("high quality");
                        			 if (replace) {
                        				 String tempTitle = title.replace(",", ";");
//                        				 System.out.println(title);
                        				 contentStr = contentStr.replaceAll(title, tempTitle);
                        			 }
                        			 contentStr = contentStr.replace(".,", ".;");
                        			 System.out.println(contentStr);
                        			 content[0] = contentStr.split(",");
//                        			 HSSFRow row = sheet.createRow(curRow);
                        			 int shitf = 0;
//                        			 if (content[0][0] != null) {
//                        				 if (replace) {
//                        					 content[0][0] = content[0][0].replaceAll(";", ",");
//                        				 }
//                        				 if (!content[0][0].equalsIgnoreCase(title)) {
//                        					 HSSFCell cell1 = row.createCell(shitf);
//                        					 cell1.setCellValue(title);
//                        					 shitf ++;
//                        				 }
//                        			 }
                        			 order++;
                        			 for (int i = 1; i < content[0].length - 1; i++) {
                        				 HSSFRow row = sheet.createRow(curRow);
                        				 HSSFCell cell0 = row.createCell(0);
                        				 cell0.setCellValue(order);
                        				 HSSFCell cell1 = row.createCell(1);
                        				 cell1.setCellValue(content[0][i].trim());
                        				 curRow++;
                        			 }
//                        			 System.out.println(System.currentTimeMillis() - begin);
                        		 }
                        		 return true; 
                        	 } 
                        	 return false;
                         }   
                     });   
             
         } catch (Exception e) {   
             e.printStackTrace();   
         }   
			} catch (Exception e) {
				// TODO: handle exception
			}
            // 通过过滤器过滤出<A>标签   
           
        return content[0];
    
	}
}
