package main;

import java.util.HashMap;
import java.util.Map;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;

import utils.Utils;

public class AlibabaHtmlParser {
	private static String webSite = "http://www.alibaba.com/";
	
	private static String nextUrl = null;
	
	public static Map<String, String> getData(String html) {
		try {
		 Parser parser = Parser.createParser(html, "utf-8");
		 
         try {
             // 通过过滤器过滤出<A>标签   
             @SuppressWarnings("unused")
			NodeList nodeList = parser   
                     .extractAllNodesThatMatch(new NodeFilter() {   
                         /**
						 * 
						 */
						private static final long serialVersionUID = -8459148400936175773L;

						//实现该方法,用以过滤标签   
                         public boolean accept(Node node) {
                        	 if (node instanceof LinkTag) {
                        		 if (((LinkTag)node).getAttribute("class") != null && ((LinkTag)node).getAttribute("class").equals("next")) {
                        			 nextUrl = ((LinkTag)node).getAttribute("href");
                        		 }
                        		 return true; 
                        	 } else if (node instanceof Div) {//<A>标记
                            	 
                            	 if (((Div)node).getAttribute("class") != null && ((Div)node).getAttribute("class").equals("corp")) {
                            		 Node ahref = ((Div) node).getChild(1).getFirstChild(); 
                            		 String linkStr = ((TagNode) ahref).getAttribute("href");
                            		 String titleStr = ((TagNode) ahref).getAttribute("title");
                            		 System.out.println(linkStr + "-" + titleStr);
                            	 }
                            	 return true; 
                            	 
                         }
                             return false;   
                         }   
                     });   
             // 打印   
//             for (int i = 0; i < nodeList.size(); i++) {   
//            	 ParagraphTag n = (ParagraphTag) nodeList.elementAt(i);
//            	 Node[] nodeArr = n.getChildrenAsNodeArray();
//            	 for (int j = 1; j < nodeArr.length; j++) {
//            		 String text = nodeArr[j].getText();
//            		 if (text.contains("br")) {
//            			 continue;
//            		 }
//            		 text = text.replaceAll("分享社", "");
//            		 text = text.replaceAll("迅雷会员账号", "");
//            		 text = text.replaceAll("迅雷会员密码", "==>");
//            		 System.out.println(text);
//            	 }
////            	 String value = n.getAttributeEx("class") != null ? n.getAttributeEx("class").getValue() : null;
////                 if ((value != null && value.toString().equalsIgnoreCase("wp_keywordlink_affiliate")) == true) {
////                	 System.out.print(n.getNextSibling().getText());   
////                 }
//                 
////                 System.out.println(n.extractLink());   
//             }   
         } catch (Exception e) {   
             e.printStackTrace();   
         }   
 
     } catch (Exception e) {   
         e.printStackTrace();   
     }    
	Map<String, String> dataMap = new HashMap<String, String>();
		
		return dataMap;
	}

	private static int donePages = 0;
	private static int configPages = 10;
	public static void main(String argsp[]) {
//		System.out.println(getHtmlContent("http://www.fenxs.com/1079.html", "utf-8"));
		getData(Utils.getHtmlContent("http://www.alibaba.com/", "utf-8"));
		donePages++;
		String url = null;
		while (donePages < configPages && nextUrl != null) {
			url = webSite + nextUrl;
			nextUrl = null;
			getData(Utils.getHtmlContent(url, "utf-8"));
			donePages++;
		}
	}
}