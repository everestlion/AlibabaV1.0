package main;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;



public class CellMergeTest {
	
	public static void main(String[] args) {
		test();
	}

	public static void test() {
		FileOutputStream fileOut = null; 
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("cell merge test");
		HSSFRow row = sheet.createRow(1);
		HSSFCellStyle cellStyle = wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		HSSFCell cell = row.createCell(1);
		cell.setCellValue("merge test");
		cell.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(1, 2, 1, 1));
		
		String fileName = "F:/temp/Excel merge" + System.currentTimeMillis() + ".xls";
        try {
			fileOut = new FileOutputStream(fileName);
			wb.write(fileOut);   
		} catch (Exception e) {
			e.printStackTrace();
		}  finally{
            if(fileOut != null){
                try {
                   fileOut.close();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       } 
        // д��excel�ļ�   
         System.out.println("----Excle�ļ�������------");
	}
	
}
