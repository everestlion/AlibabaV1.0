package main;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Timestamp;

import javax.imageio.ImageIO;

import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelImageTest {
    public static void main(String[] args) {
         FileOutputStream fileOut = null;   
         BufferedImage bufferImg = null;   
        //�ȰѶ�������ͼƬ�ŵ�һ��ByteArrayOutputStream�У��Ա����ByteArray  
        try {
//            ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();   
//            bufferImg = ImageIO.read(new File("F:/temp/test.jpg"));   
//            ImageIO.write(bufferImg, "jpg", byteArrayOut);
            
            HSSFWorkbook wb = new HSSFWorkbook();   
            HSSFSheet sheet1 = wb.createSheet("test picture");  
            sheet1.setColumnWidth((short)1, 50);
            //��ͼ�Ķ�����������һ��sheetֻ�ܻ�ȡһ����һ��Ҫע����㣩
            HSSFPatriarch patriarch = sheet1.createDrawingPatriarch();   
            //anchor��Ҫ��������ͼƬ������
            HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0,(short) 0, 1, (short) 1, 5);   
            anchor.setAnchorType(0);   
            //����ͼƬ  
//            patriarch.createPicture(anchor, wb.addPicture(byteArrayOut.toByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG)); 
            byte[] byteArray = getUrlFileData("http://i01.i.aliimg.com/photo/v2/60072823787_1/9H_hardness_2_5D_round_edge_tempered.jpg_140x140.jpg");
            patriarch.createPicture(anchor, wb.addPicture(byteArray, HSSFWorkbook.PICTURE_TYPE_JPEG));
            String fileName = "F:/temp/����Excel" + System.currentTimeMillis() + ".xls";
            fileOut = new FileOutputStream(fileName);   
            // д��excel�ļ�   
             wb.write(fileOut);   
             System.out.println("----Excle�ļ�������------");
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(fileOut != null){
                 try {
                    fileOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void main1(String[] args){
		try {
			System.out.println(getUrlDetail("http://www.baidu.com",true));
			saveUrlFile("http://www.baidu.com/img/baidu_jgylogo3.gif", "D:\\1.gif");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//��ȡ�����ļ���ת�浽fileDes�У�fileDes��Ҫ���ļ���׺��
	public static void saveUrlFile(String fileUrl,String fileDes) throws Exception
	{
		File toFile = new File(fileDes);
		if (toFile.exists())
		{
//			throw new Exception("file exist");
			return;
		}
		toFile.createNewFile();
		FileOutputStream outImgStream = new FileOutputStream(toFile);
		outImgStream.write(getUrlFileData(fileUrl));
		outImgStream.close();
	}
	
	//��ȡ���ӵ�ַ�ļ���byte����
	public static byte[] getUrlFileData(String fileUrl) throws Exception
	{
		URL url = new URL(fileUrl);
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		httpConn.connect();
		InputStream cin = httpConn.getInputStream();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = cin.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		cin.close();
		byte[] fileData = outStream.toByteArray();
		outStream.close();
		return fileData;
	}
	
	//��ȡ���ӵ�ַ���ַ����ݣ�wichSep�Ƿ��б��
	public static String getUrlDetail(String urlStr,boolean withSep) throws Exception
	{
		URL url = new URL(urlStr);
		HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
		httpConn.connect();
		InputStream cin = httpConn.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(cin,"UTF-8"));
		StringBuffer sb = new StringBuffer();
		String rl = null;
		while((rl = reader.readLine()) != null)
		{
			if (withSep)
			{
				sb.append(rl).append(System.getProperty("line.separator"));
			}
			else
			{
				sb.append(rl);
			}
		}
		return sb.toString();
	}
}

