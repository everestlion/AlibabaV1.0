package main;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;

public abstract class Config {

	public static String webSite = "http://www.alibaba.com/";
	
	public static final String companyName = "Guangzhou XCL Electronic Technology Co., Limited";
//	public static final String companyName = "Guangzhou Boddiga Leather Co., Ltd.";
//	public static final String companyName = "Polva Electronic Technology Inc.";
//	public static final String companyName = "Guangzhou Sunruo Film Co., Ltd.";
//	public static final String companyName = "Guangzhou Dmax Electronic Technology Co., Limited";
//	public static final String companyName = "Shenzhen New Star Glass &amp; Packing Manufacture Co., Ltd.";
//	public static final String companyName = "Shenzhen Topvision Optical Co., Ltd.";
//	public static final String companyName = "Zhejiang Conrad Sanitaryware Co., Ltd.";
//	public static final String companyName = "Ningbo San Bei Plush Products Company Limited";
	
	//Products › Consumer Electronics › Accessories & Parts › Screen Protector › 
	public static final String catelogUrl = "http://www.alibaba.com/trade/search?fsb=y&IndexArea=product_en&CatId=&SearchText=tempered+glass+screen+protector";
//	public static final String cateLogUrl = "http://www.alibaba.com/catalogs/products/CID4408/101";

	public static final String showCasePage = "/featureproductlist.html";
	
	public static int configPages = 20;//搜索排序，查排名
	
	public static int configCatPages = 300;//类目排序，查关键词
	
	//是否从网络取当前橱窗产品
	public static boolean getCurShowcaseProductsId = true;

	public static final String keywordFileUrl = "resource/keywordFile.txt";
//	public static final String keywordFileUrl = "resource/keywordFile-evening bag.txt";
//	public static final String keywordFileUrl = "resource/keywordFile-polva.txt";
//	public static final String keywordFileUrl = "resource/keywordFile-Charlene.txt";
//	public static final String keywordFileUrl = "resource/keywordFile-Leo.txt";
//	public static final String keywordFileUrl = "resource/keywordFile-showcase.txt";
//	public static final String keywordFileUrl = "resource/keywordFile-Zhejiang Conrad Sanitaryware Co., Ltd..txt";
//	public static final String keywordFileUrl = "resource/keywordFile-Ninbo San Bei Plush Products Company Linited.txt";
	
	public static String searchUrl = "http://www.alibaba.com/trade/search?fsb=y&IndexArea=product_en&CatId=&SearchText=";

	public static String defaultImg = "resource/no-default.jpg";

	public static String showcaseHtml = "resource/showcase_product.html";
	
	public static byte[] defaultImgByteArray;
	
	public static final ArrayList<String> keywords = new ArrayList<String>();
	
	public static Set<String> showcaseProductIds = new HashSet<String>();
	
	static {
		//tempered glass screen protector
//		showcaseProductIds.add("1681752309");
//		showcaseProductIds.add("1684098481");
//		showcaseProductIds.add("1689899011");
//		showcaseProductIds.add("1676478550");
//		showcaseProductIds.add("1944836680");
//		showcaseProductIds.add("1944815182");
//		showcaseProductIds.add("1944853852");
//		showcaseProductIds.add("1944893515");
//		showcaseProductIds.add("1944869947");
//		showcaseProductIds.add("1944830668");
//		showcaseProductIds.add("1944791161");
//		showcaseProductIds.add("1944814720");
//		showcaseProductIds.add("1944782054");
//		showcaseProductIds.add("60133706291");
		
		//evening bag
		showcaseProductIds.add("1995968333");
		showcaseProductIds.add("1933709772");
		showcaseProductIds.add("60049135764");
		showcaseProductIds.add("1933635044");
		showcaseProductIds.add("1965718254");
		showcaseProductIds.add("2009252004");
		showcaseProductIds.add("1972781100");
		showcaseProductIds.add("2002263426");
		showcaseProductIds.add("60049341923");
		showcaseProductIds.add("60049565721");
		showcaseProductIds.add("60049616182");
		showcaseProductIds.add("60049333029");
	}


	public static void readKeywords() {
		File file = new File(keywordFileUrl);
		BufferedReader reader = null;
		try {
			// System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null) {
				if (tempString.trim().length() > 0) {
					keywords.add(tempString);
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
	
	}

	

	static {
		loadDefaultImg(Config.defaultImg);
		Config.readKeywords();
	}

	public static void loadDefaultImg(String url) {
		BufferedImage bufferImg = null;
		ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();   
		try {
			
			bufferImg = ImageIO.read(new File(url));   
			ImageIO.write(bufferImg, "jpg", byteArrayOut);
			defaultImgByteArray = byteArrayOut.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (byteArrayOut != null) {
					byteArrayOut.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
