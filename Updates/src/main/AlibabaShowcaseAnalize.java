package main;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.tags.Div;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.MetaTag;
import org.htmlparser.util.NodeList;

import utils.Utils;
import vo.HotWords;
import vo.Item;
import dao.HotWordsDao;

public class AlibabaShowcaseAnalize {
//
//	private static String webSite = "http://xclelec.en.alibaba.com/";
//	private static String companyName = "Guangzhou XCL Electronic Technology Co., Limited";
	
	private static String webSite = "http://sunruo.en.alibaba.com/";
	private static String companyName = "Guangzhou Sunruo Film Co., Ltd.";
	
	private static String detailUrl = "http://www.alibaba.com/product-detail"; 
	

	private static String next = null;

	private static Map<Integer, ArrayList<Item>> dataMap = new HashMap<Integer, ArrayList<Item>>();
	
	private static byte[] defaultImgByteArray;
	
	private static boolean gotShowCaseProductsId = false;
	
	public static void loadDefaultImg(String url) {
		BufferedImage bufferImg = null;
		ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();   
		try {
			
			bufferImg = ImageIO.read(new File(url));   
			ImageIO.write(bufferImg, "jpg", byteArrayOut);
			defaultImgByteArray = byteArrayOut.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (byteArrayOut != null) {
					byteArrayOut.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getHtmlContent(URL url, String encode) {
		StringBuffer contentBuffer = new StringBuffer();

		int responseCode = -1;
		HttpURLConnection con = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");// IE代理进行下载
			con.setConnectTimeout(60000);
			con.setReadTimeout(60000);
			// 获得网页返回信息码
			responseCode = con.getResponseCode();
			if (responseCode == -1) {
				System.out.println(url.toString() + " : connection is failure...");
				con.disconnect();
				return null;
			}
			if (responseCode >= 400) // 请求失败
			{
				System.out.println("请求失败:get response code: " + responseCode);
				con.disconnect();
				return null;
			}

			InputStream inStr = con.getInputStream();
			InputStreamReader istreamReader = new InputStreamReader(inStr, encode);
			BufferedReader buffStr = new BufferedReader(istreamReader);

			String str = null;
			while ((str = buffStr.readLine()) != null)
				contentBuffer.append(str);
			inStr.close();
		} catch (IOException e) {
			e.printStackTrace();
			contentBuffer = null;
			System.out.println("error: " + url.toString());
		} finally {
			con.disconnect();
		}
		return contentBuffer.toString();
	}

	public static String getHtmlContent(String url, String encode) {
		if (!url.toLowerCase().startsWith("http://")) {
			url = "http://" + url;
		}
		try {
			URL rUrl = new URL(url);
			return getHtmlContent(rUrl, encode);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void getOwner(String html, final Item item) {
		try {
			Parser parser = Parser.createParser(html, "utf-8");
			try {
				// 通过过滤器过滤出<A>标签
				@SuppressWarnings("unused")
				NodeList nodeList = parser.extractAllNodesThatMatch(new NodeFilter() {

					/**
							 * 
							 */
					private static final long serialVersionUID = -2906136240488718581L;
					
					// 实现该方法,用以过滤标签
					public boolean accept(Node node) {
						if (node instanceof MetaTag) {
							MetaTag metaTag = (MetaTag)node;
	                   		 if (metaTag.getAttribute("name") != null && metaTag.getAttribute("name").equals("keywords")) {
	                   			 String contentStr = metaTag.getAttribute("content");
	                   			 String keywords = contentStr != null ? contentStr.substring(contentStr.indexOf(" High Quality ") + 14, contentStr.length()) : null;
	                   			 keywords = keywords != null ? keywords.replaceAll(companyName, "") : null;
	                   			if (keywords != null && keywords.length() > 0) {
	                   				String[] tmpkeywords = new String[3];
	                   				String [] tmp = keywords.split(",");
	                   				String[][] keyWordsOrder = new String[3][];
	                   				for (int i = 0; i < tmp.length; i++) {
	                   					tmpkeywords[i] = tmp[i];
	                   					if (tmpkeywords[i] != null && tmpkeywords[i].length() > 0) {
	                   						keyWordsOrder[i] = checkKeyWordsOrder(tmpkeywords[i], item.getProductId());
	                   					}
	                   					if (i >= 2) {
	                   						break;
	                   					}
	                   				}
	                   				
	                   				item.setKeywords(tmpkeywords);
	                   				item.setKeywordsOrder(keyWordsOrder);
	                   			}
                   			 }
						} else if (node instanceof Div && ((Div) node).getAttribute("class") != null && ((Div) node).getAttribute("class").equals("avatar")) {
							String owner = ((ImageTag) ((Div) node).getChild(1)).getAttribute("alt");
							item.setOwner(owner);
							try {
								item.setOwnerImgByteArray(Utils.getUrlFileData(((ImageTag) ((Div) node).getChild(1)).getAttribute("src")));
								return true;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						return false;
					}

				});
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	private static String checkKeywordNext = null;
	public static String[] checkKeyWordsOrder(String keyword, String productId) {

		int order = 0;
		int donePage = 0;
		String[] result = null;
		try {
			String curUrl = Config.searchUrl + URLEncoder.encode(keyword, "utf-8");
			while (curUrl != null && donePage < Config.configPages) {
				String html = getHtmlContent(curUrl, "utf-8");
				order = checkKeyWordsOrderHtml(html, productId);
				donePage ++;
				if (order > 0) {
					result = new String[2];
					result[0] = donePage + "-" + order;
					result[1] = curUrl;
					break;
				}
				curUrl = checkKeywordNext != null ? Config.webSite + checkKeywordNext : null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	public static int checkKeyWordsOrderHtml(String html, final String productId) {
		final int[] order = new int[2];
		try {
			Parser parser = Parser.createParser(html, "utf-8");
			try {
				// 通过过滤器过滤出<A>标签
				@SuppressWarnings("unused")
				NodeList nodeList = parser.extractAllNodesThatMatch(new NodeFilter() {
					
					/**
					 * 
					 */
					private static final long serialVersionUID = -513985487690337805L;
					
					// 实现该方法,用以过滤标签
					public boolean accept(Node node) {
						if (node instanceof LinkTag) {
							if (((LinkTag) node).getAttribute("class") != null && ((LinkTag) node).getAttribute("class").equals("next")) {
								checkKeywordNext = ((LinkTag) node).getAttribute("href");
							}
							return true;
						} else if (node instanceof Div && ((Div)node).getAttribute("data-ctrdot") != null) {
							order[0]++;
							if (((Div)node).getAttribute("data-ctrdot").equals(productId)) {
								order[1] = order[1] == 0 ? order[0] : order[1];
							}
						}
						return false;
					}
					
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return order[1];
	}

	public static Map<String, String> getData(String html) {
		try {
			Parser parser = Parser.createParser(html, "utf-8");

			try {
				// 通过过滤器过滤出<A>标签
				@SuppressWarnings("unused")
				NodeList nodeList = parser.extractAllNodesThatMatch(new NodeFilter() {
					/**
						 * 
						 */
					private static final long serialVersionUID = 1L;

					/**
						 * 
						 */
					
					// 实现该方法,用以过滤标签
					public boolean accept(Node node) {
						if (node instanceof LinkTag) {
							if (((LinkTag) node).getAttribute("class") != null && ((LinkTag) node).getAttribute("class").equals("ui-pagination-next")) {
								next = ((LinkTag) node).getAttribute("href");
							}
							return true;
						} else if (node instanceof Div) {// <A>标记
							if (((Div) node).getAttribute("class") != null && ((Div) node).getAttribute("class").equals("product-title")) {
								LinkTag link = (LinkTag)((Div) node).getChild(1);
								pageOrder++;
								
								try {
									Item item = new Item();
									item.setPage(donePages + 1);
									item.setPageOrder(pageOrder);
									item.setPageUrl(curUrl);
									String tmp = link.getAttribute("href");
									String pid = tmp.substring(9, tmp.indexOf("-"));
									item.setProductId(pid);
									tmp = tmp.substring(tmp.lastIndexOf("/"), tmp.length());
									String[] words = tmp.split("_", 7);
									StringBuffer sb = new StringBuffer();
									int count = 0;
									for (String e : words) {
										sb.append(e);
										count ++;
										if (count > 5) {
											break;
										}else {
											sb.append("-");
										}
									}
									sb.append("_").append(pid).append(".html?s=p");
									
									String linkStr = detailUrl + sb.toString();
									String titleStr = link.getAttribute("title");
//									String pId = ((TagNode) ahref).getAttribute("data-pid");
//									item.setProductId(pId);
									item.setHref(linkStr);
									item.setTitle(titleStr);
									Node imgDiv = node.getPreviousSibling().getPreviousSibling();
									String imgUrl = ((ImageTag)imgDiv.getFirstChild().getNextSibling().getFirstChild().getNextSibling()).getAttribute("data-src");
									byte[] imgByteArray = Utils.getUrlFileData(imgUrl);
									item.setImgByteArray(imgByteArray != null ? imgByteArray : defaultImgByteArray);
									
									getOwner(getHtmlContent(linkStr, "utf-8"), item);
									dataMap.get(i).add(item);
									System.out.println(linkStr + "---" + titleStr);
								} catch (Exception e) {
									e.printStackTrace();
								}
							
							}
							return true;

						}
						return false;
					}
				});
				
			} catch (Exception e) {
				System.out.println(curUrl);
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		Map<String, String> dataMap = new HashMap<String, String>();

		return dataMap;
	}

	private static int configPages = 20;
	private static int pageOrder = 0;

	private static final ArrayList<String> keywords = new ArrayList<String>();


	public static void readKeywords() {
		File file = new File(Config.keywordFileUrl);
		BufferedReader reader = null;
		try {
			// System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null) {
				if (tempString.trim().length() > 0) {
					keywords.add(tempString);
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}

	}

	static {
		loadDefaultImg(Config.defaultImg);
	}

	private static int donePages = 0;

	private static int i = 0;// 第几个关键词

	private static String curUrl = null;
	
	private static String editUrl = "http://hz.productposting.alibaba.com/product/editing.htm?id=";
	
	
	public static void main(String argsp[]) {
		// System.out.println(getHtmlContent("http://www.fenxs.com/1079.html",
		// "utf-8"));
		try {
			dataMap.put(i, new ArrayList<Item>());
			curUrl = webSite + Config.showCasePage;
		} catch (Exception e) {
			e.printStackTrace();
		}
		getData(getHtmlContent(curUrl, "utf-8"));
		donePages++;
		
		while (next != null) {
			curUrl = Config.webSite + next;
			String html = getHtmlContent(curUrl, "utf-8");
			next = null;
			getData(html);
//				if (html != null && html.contains(companyName)) {
//				}
			donePages++;
		}
		hotWords = HotWordsDao.selectHotWordsDB();
		writeToExcel();
	}
	
	private static Map<String, HotWords> hotWords = new HashMap<String, HotWords>();

	private static void writeToExcel() {
		FileOutputStream fileOut = null;
		BufferedImage bufferImg = null;
		// 先把读进来的图片放到一个ByteArrayOutputStream中，以便产生ByteArray
		try {
			// ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
			// bufferImg = ImageIO.read(new File("F:/temp/test.jpg"));
			// ImageIO.write(bufferImg, "jpg", byteArrayOut);

			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet1 = wb.createSheet("test picture");
//			sheet1.setColumnWidth((short) 1, 50);
			// 画图的顶级管理器，一个sheet只能获取一个（一定要注意这点）
			HSSFPatriarch patriarch = sheet1.createDrawingPatriarch();
			// anchor主要用于设置图片的属性
			int dx1 = 0;
			int dy1 = 0;
			int dx2 = 0;
			int dy2 = 0;
			short col1 = 0;
			int row1 = 1;
			short col2 = 1;
			int row2 = 5;
			HSSFCellStyle cellStyle = wb.createCellStyle();
			cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
			cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			cellStyle.setWrapText(true);

			HSSFCellStyle hlinkStyle = wb.createCellStyle();
			HSSFFont hlinkFont = wb.createFont();
			hlinkFont.setUnderline(HSSFFont.U_SINGLE);
			hlinkFont.setColor(HSSFColor.BLUE.index);
			hlinkStyle.setFont(hlinkFont);
			hlinkStyle.setAlignment(CellStyle.ALIGN_CENTER);
			hlinkStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			hlinkStyle.setWrapText(true);
			
			HSSFCellStyle keywordStyle = wb.createCellStyle();
			keywordStyle.setAlignment(CellStyle.ALIGN_CENTER);
			keywordStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			keywordStyle.setWrapText(true);
			HSSFFont font = wb.createFont();
			font.setFontName("黑体");
			font.setColor(Font.COLOR_RED);
			keywordStyle.setFont(font);
			

			HSSFRow keywordRow = sheet1.createRow(row1);
			HSSFCell keywordCell = keywordRow.createCell((int)col1);
			keywordCell.setCellValue("showcase analize");
			sheet1.addMergedRegion(new CellRangeAddress(row1, row1, col1, col1 + 8));
			keywordRow.setHeight((short)600);
			keywordCell.setCellStyle(keywordStyle);
			
			row1 += 1;
			row2 += 1;
			ArrayList<Item> list = dataMap.get(i);
			for (Item e : list) {
				if (e.getImgByteArray() != null) {
					HSSFClientAnchor anchor = new HSSFClientAnchor(dx1, dy1, dx2, dy2, col1, row1, col2, row2);
					anchor.setAnchorType(0);
					patriarch.createPicture(anchor, wb.addPicture(e.getImgByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG));
				} 
				HSSFRow row = sheet1.createRow(row1);
				
				int cellIndex = col1 + 1;
				sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));
				HSSFCell showcaseCell = row.createCell(cellIndex);
				showcaseCell.setCellStyle(hlinkStyle);
				showcaseCell.setCellValue("橱窗产品");
				
				cellIndex += 1;
				HSSFCell pageCell = row.createCell(cellIndex);
				pageCell.setCellStyle(hlinkStyle);
				pageCell.setCellValue("第" + e.getPage() + "頁,第" + e.getPageOrder() + "個");
				sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));
				HSSFHyperlink pageLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
				pageLink.setAddress(e.getPageUrl());
				pageCell.setHyperlink(pageLink);
				
				cellIndex += 1;
				HSSFCell titleCell = row.createCell(cellIndex);
				titleCell.setCellStyle(hlinkStyle);
				titleCell.setCellValue(e.getTitle());
				HSSFHyperlink editLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
				editLink.setAddress(editUrl + e.getProductId());
				titleCell.setHyperlink(editLink);
				sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));
				
				String[] keywords = e.getKeywords();
				String[][] order = e.getKeywordsOrder();
//				if (e.getProductId().equals("1944869947")) {
//					System.out.println("");
//				}
				HSSFRow orderRow = null;
				HSSFRow dataRow = null;
				for (int k = 0; k < 3; k++) {
					cellIndex += 1;
					HSSFCell keywordsCell = row.createCell(cellIndex);
					sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 1, cellIndex, cellIndex));
					keywordsCell.setCellStyle(cellStyle);
					if (keywords != null && keywords.length > k) {
						keywordsCell.setCellValue(keywords[k]);
						dataRow = dataRow == null ? sheet1.createRow(row1 + 2) : dataRow;
						HSSFCell dataCell = dataRow.createCell(cellIndex);
						dataCell.setCellStyle(cellStyle);
						if (keywords[k] != null) {
							String wordLowercase = keywords[k].toLowerCase().trim();
							dataCell.setCellValue(hotWords.containsKey(wordLowercase) ? hotWords.get(wordLowercase).toString() : "null");
						}
						if (order[k] != null) {
							orderRow = orderRow == null ? sheet1.createRow(row1 + 3) : orderRow;
							HSSFCell orderCell = orderRow.createCell(cellIndex);
							orderCell.setCellStyle(hlinkStyle);
							orderCell.setCellValue(order[k][0]);
							HSSFHyperlink orderLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
							orderLink.setAddress(order[k][1]);
							orderCell.setHyperlink(orderLink);
						}
					}
				}

				cellIndex += 1;
				HSSFCell urlCell = row.createCell(cellIndex);
				urlCell.setCellStyle(hlinkStyle);
				urlCell.setCellValue(e.getHref());
				HSSFHyperlink link = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
				link.setAddress(e.getHref());
				urlCell.setHyperlink(link);
				sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));

				cellIndex += 1;
				HSSFCell ownerCell = row.createCell(cellIndex);
				ownerCell.setCellStyle(cellStyle);
				ownerCell.setCellValue(e.getOwner());
				sheet1.addMergedRegion(new CellRangeAddress(row1, row1 + 3, cellIndex, cellIndex));

				row1 += 5;
				row2 += 5;
			}
		

			// 插入图片
			// patriarch.createPicture(anchor,
			// wb.addPicture(byteArrayOut.toByteArray(),
			// HSSFWorkbook.PICTURE_TYPE_JPEG));
			// byte[] byteArray =
			// getUrlFileData("http://i01.i.aliimg.com/photo/v2/60072823787_1/9H_hardness_2_5D_round_edge_tempered.jpg_140x140.jpg");
			String fileName = "F:/temp/Showcase_" + companyName + "_" + System.currentTimeMillis() + ".xls";
			fileOut = new FileOutputStream(fileName);
			// 写入excel文件
			wb.write(fileOut);
			System.out.println("----Excle文件已生成------" + fileName);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
}