package vo;

public class Product {
	
	private String productId;
	
	private String href;
	
	private String company;
	
	private String title;
	
	private int order;
	
	private String searchKeywords;
	
	private int catlogTyep;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getSearchKeywords() {
		return searchKeywords;
	}

	public void setSearchKeywords(String searchKeywords) {
		this.searchKeywords = searchKeywords;
	}

	public int getCatlogTyep() {
		return catlogTyep;
	}

	public void setCatlogTyep(int catlogTyep) {
		this.catlogTyep = catlogTyep;
	}
}
