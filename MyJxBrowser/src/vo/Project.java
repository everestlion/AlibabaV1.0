package vo;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import utils.Utils;


public class Project {
	
	private String modelId;
	
	private int posted;
	
	private int postPlanNum;
	
	private int interval;
	
	private String createTime;
	
	private String title;
	
	private String imagePath;
	
	private String[] imagePathArr;
	
	private String keywords;

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public int getPosted() {
		return posted;
	}

	public void setPosted(int posted) {
		this.posted = posted;
	}

	public int getPostPlanNum() {
		return postPlanNum;
	}

	public void setPostPlanNum(int postPlanNum) {
		this.postPlanNum = postPlanNum;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String[] getImagePathArr() {
		return imagePathArr;
	}

	public void setImagePathArr(String[] imagePathArr) {
		this.imagePathArr = imagePathArr;
	}

	public static Project getProject() {
		return project;
	}

	public static void setProject(Project project) {
		Project.project = project;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	private static String xmlPath = "resource/xml/project.xml";
	
	private static Project project = new Project();
	
	static {
		loadXml();
	}
	
	@SuppressWarnings("unchecked")
	public static void loadXml() {
		Document document = getDocument();
		if (document == null) {
			System.err.println("load project.xml err");
			return;
		}
		
		Element rootElement = document.getRootElement();
		
		Utils.setValue(rootElement, project, Project.class);
		String imagePath = project.getImagePath();
		if (imagePath != null && imagePath.length() > 0) {
			String[] arr = imagePath.split(",");
			project.setImagePathArr(arr);
		}
		
	}
	
	
	
	public static Document getDocument() {
		File xmlFile = new File(xmlPath);
		Document document = null;
		if (!xmlFile.exists()) {
			document = DocumentHelper.createDocument();
			document.setXMLEncoding("UTF-8");
			document.setRootElement(DocumentHelper.createElement("project"));
		} else {
			SAXReader reader = new SAXReader();
			try {
				document = reader.read(xmlFile);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		return document;
	}
	
	public static void valueChange(Project pm) {
		writeToXml(pm);
	}

	@SuppressWarnings("deprecation")
	public static void writeToXml(Project pm) {
		Document document = getDocument();
		if (document == null) {
			System.err.println("load model.xml err");
			return;
		}
		Element rootElement = document.getRootElement();
		if (rootElement != null) {
			Utils.setXmlValue((Element)rootElement, pm, Project.class);
		} else {
			Element neweElement = DocumentHelper.createElement("project");
			neweElement.addElement("title");
			neweElement.addElement("modelId");
			neweElement.addElement("posted");
			neweElement.addElement("postPlanNum");
			neweElement.addElement("createTime");
			neweElement.addElement("imagePath");
			neweElement.addElement("keywords");
			neweElement.addElement("interval");
			Utils.setXmlValue(neweElement, pm, Project.class);
			document.setRootElement(neweElement);
		}
		
		Utils.writeToXml(document, xmlPath);
		
	}
	

}
