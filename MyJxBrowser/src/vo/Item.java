package vo;

public class Item {
	
	private String productId;

	private byte[] imgByteArray;
	
	private byte[] ownerImgByteArray;
	
	private String href;
	
	private String owner;
	
	private String title;
	
	private int page;
	
	private int pageOrder;
	
	private String pageUrl;
	
	private String[] keywords;
	
	private String[][] keywordsOrder;
	
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public byte[] getImgByteArray() {
		return imgByteArray;
	}

	public void setImgByteArray(byte[] imgByteArray) {
		this.imgByteArray = imgByteArray;
	}
	
	public byte[] getOwnerImgByteArray() {
		return ownerImgByteArray;
	}

	public void setOwnerImgByteArray(byte[] ownerImgByteArray) {
		this.ownerImgByteArray = ownerImgByteArray;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageOrder() {
		return pageOrder;
	}

	public void setPageOrder(int pageOrder) {
		this.pageOrder = pageOrder;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getKeywordsStr() {
		return keywords != null ? keywords[0] + "," + keywords[1] + "," + keywords[2] : null;
	}

	public void setKeywords(String[] keywords) {
		this.keywords = keywords;
	}
	
	public String[] getKeywords() {
		return keywords;
	}

	public String[][] getKeywordsOrder() {
		return keywordsOrder;
	}

	public void setKeywordsOrder(String[][] keywordsOrder) {
		this.keywordsOrder = keywordsOrder;
	}

	
}
