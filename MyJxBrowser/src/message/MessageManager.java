package message;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class MessageManager {
	
	protected static final boolean IS_DEBUGGING_MESSAGES = true;//TODO IS_DEBUGGING_MESSAGES
	
	private static List<Message> messagesList = new LinkedList<Message>();
	
	private static ObjectOutputStream oos;
	
	private static ObjectInputStream ois;
	
	private static Socket socket;
	
	private static int oosByteCount;
	
	 private static final int OOS_RESET_THRESHOLD = 500000;
	 
	 static {
		 openChannel();
		 createReceiveMessageThread();
	 }
	 
	public MessageManager() {
	}
	
	public static void openChannel() {
		int port;
		ServerSocket serverSocket;
        try {
          serverSocket = new ServerSocket();
          serverSocket.setReuseAddress(false);
          serverSocket.bind(new InetSocketAddress(InetAddress.getByName("127.0.0.1"), 0));
          port = serverSocket.getLocalPort();
          serverSocket.close();
          socket = new Socket("127.0.0.1", port+1);
          oos = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()) {
              @Override
              public synchronized void write(int b) throws IOException {
                super.write(b);
                oosByteCount++;
              }
              @Override
              public synchronized void write(byte[] b, int off, int len) throws IOException {
                super.write(b, off, len);
                oosByteCount += len;
              }
            });
          oos.flush();
          ois = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch(IOException e) {
        	e.printStackTrace();
          throw new RuntimeException(e);
        }
        
	}
	
	protected static void closeChannel() {
	    try {
	      oos.close();
	    } catch(Exception e) {
	    }
	    try {
	      ois.close();
	    } catch(Exception e) {
	    }
	    try {
	      socket.close();
	    } catch(Exception e) {
	    }
	    socket = null;
	  }
	
	  protected static void writeMessageToChannel(Message message) throws IOException {
	    synchronized(oos) {
	      oos.writeUnshared(message);
	      oos.flush();
	      // Messages are cached, so we need to reset() from time to time to clean the cache, or else we get an OutOfMemoryError.
	      if(oosByteCount > OOS_RESET_THRESHOLD) {
	        oos.reset();
	        oosByteCount = 0;
	      }
	    }
	  }

	  protected static Message readMessageFromChannel() throws IOException, ClassNotFoundException {
	    Object o = ois.readUnshared();
	    if(o instanceof Message) {
	      Message message = (Message)o;
//	      Object[] obj = (Object[])o;
//	      System.out.println(obj[0]);
	      if(IS_DEBUGGING_MESSAGES) {
	        System.out.println("RECV: " + message.getType() + ", " + message);
	      }
//	      messagesList.add(message);
	      return message;
	    }
	    System.err.println("Unknown message: " + o);
	    return null;
	  }
	  
	  private static Object RECEIVER_LOCK = new Object();

	  public static void processReceivedMessages() {
	    while(true) {
	      Message message;
	      synchronized(RECEIVER_LOCK) {
	        if(!messagesList.isEmpty()) {
	          message = messagesList.remove(0);
	          if (message instanceof CommMessage) {
	        	  try {
	        		  ((CommMessage)message).run();
				} catch (Exception e) {
					e.printStackTrace();
				}
	          }
	        }
	      }
	    }
	  }
	  
	  public static void createReceiveMessageThread() {
		  Thread t = new Thread(new Runnable() {
			
			public void run() {
				Message message = null;
				while (true) {
					try {
						message = readMessageFromChannel();
						if (message != null && message instanceof CommMessage) {
							((CommMessage)message).run();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
		t.setDaemon(true);
		t.start();
	  }
	  
	  public static void sendMessage(Message message) {
		  try {
			  writeMessageToChannel(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	  }

}
