package message;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;

public class RepetProductGroupMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;

	public 	RepetProductGroupMessage() {
		
	}

	@Override
	public Object run() {


//		System.out.println(paramStatusEvent.getText());
//		System.out.println(paramStatusEvent.getBrowser().getHTML());
		try {
			TimeUnit.MILLISECONDS.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Browser browser = Main.getBrowser2();
		if (browser == null) {
			return null;
		}
		
		//页面验证，防止页面已经跳转
		
		DOMDocument document =browser.getDocument();
        DOMElement productList = document.getElementById("productList");
        if (productList != null) {
        	List<DOMNode> repeatTip = document.getElementsByClassName("repeat-tip");
        	List<String> urlList = new LinkedList<String>();
        	for (DOMNode e : repeatTip) {
        		List<DOMNode> tip = e.getElementsByTagName("span");
        		if (tip == null || tip.size() <= 0) {
        			DOMNode itemNode = e.getParent().getParent();
        			if (itemNode != null) {
        				List<DOMNode> editUrlNode = itemNode.getElementsByClassName("J-edit-product");
        				if (editUrlNode != null && editUrlNode.size() > 0) {
        					String editUrl = ((DOMElement)editUrlNode.get(0)).getAttribute("href");
        					if (editUrl != null) {
        						urlList.add(editUrl);
        						System.out.println(editUrl);
        					}
        				}
        			}
        		}
        	}
        	for (String url : urlList) {
        		Main.hideWindowLoad(url);
        		Main.showHideWindow();
        	}

        }
	
	
		return null;
	}

}
