package message;

import java.util.List;

import utils.TimeUtils;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserFunction;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.demo.everest.main.Main;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEvent;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventListener;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventType;

public class SelectProductModelMessage extends CommMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -353375601937670614L;
	
	

	public 	SelectProductModelMessage() {
		
	}

	@Override
	public Object run() {
		TimeUtils.sleep(1000);
		Browser browser = Main.getBrowser3();
		while(browser.isLoading()){
			TimeUtils.sleep(500);
		}
		DOMDocument document = browser.getDocument();
		browser.registerFunction("MyFunction", new BrowserFunction() {
            public JSValue invoke(JSValue... args) {
                for (JSValue arg : args) {
                    System.out.println("arg = " + arg);
                }
                return JSValue.create("Hello!");
            }
        });
		List<DOMNode> bodyNodes = document.getElementsByTagName("body");
		if (bodyNodes.size() > 0) {
			List<DOMNode> checkboxList = document.getElementsByClassName("list-checkbox");
			for (final DOMNode e : checkboxList) {
				e.addEventListener(DOMEventType.OnClick, new DOMEventListener() {
					
					public void handleEvent(DOMEvent arg0) {
						DOMElement checkbox = (DOMElement)e;
						System.out.println(checkbox.getAttribute("productid"));
						String selected = checkbox.getAttribute("selected");
						if (selected == null || selected.length() <= 0 || selected.equalsIgnoreCase("false")) {
							checkbox.setAttribute("selected", "true");
						} else {
							checkbox.removeAttribute("selected");
						}
						System.out.println(checkbox.getAttribute("selected"));
					}
				}, false);
			}
			
			DOMElement body = (DOMElement) bodyNodes.get(0);
			DOMElement header = document.getElementById("guid-1409720357634");
			if (header != null) {
				body.removeChild(header);
			}
			DOMElement page960 = document.getElementById("page960");
			DOMElement maNavigator = document.getElementById("maNavigator");
			if (maNavigator != null) {
				page960.removeChild(maNavigator);
			}
			//去掉左边距
			DOMElement layoutBox = document.getElementById("layoutBox");
			layoutBox.removeAttribute("class");
			
			List<DOMNode> left = document.getElementsByClassName("colL");
			if (left.size() > 0) {
				page960.removeChild(left.get(0));
			}
			
			DOMElement tabbox  = document.getElementById("tab-box");
			DOMElement uitabnav  = document.getElementById("ui-tab-nav");
			if (uitabnav != null) {
				tabbox.removeChild(uitabnav);
			}
			List<DOMNode> ps = tabbox.getElementsByTagName("p");
			for (DOMNode e : ps) {
				tabbox.removeChild(e);
			}
			
			List<DOMNode> uitabbodyList = tabbox.getElementsByClassName("ui-tab-body");
			DOMNode uitabBody = uitabbodyList.get(0);
			List<DOMNode> innerboxwrapList = uitabBody.getElementsByClassName("inner-box-wrap");
			if (innerboxwrapList.size() > 1) {
				uitabBody.removeChild(innerboxwrapList.get(1));
			}
			
			
			List<DOMNode> divs = body.getElementsByTagName("div");
			if (divs.size() > 0) {
				for (DOMNode e : divs) {
					String id = ((DOMElement)e).getAttribute("id");
					if (id != null && id.equalsIgnoreCase("page960")) {
						continue;
					}
					body.removeChild(e);
				}
			}
			DOMElement batchbotbottom = document.getElementById("batch-bot-bottom");
			if (batchbotbottom != null) {
				tabbox.removeChild(batchbotbottom);
			}
			DOMElement footer = document.getElementById("footer");
			if (footer != null) {
				body.removeChild(footer);
			}
			List<DOMNode> uimenubuttonList = document.getElementsByClassName("ui-menubutton");
			if (uimenubuttonList.size() <= 0) {
				TimeUtils.sleep(2000);
				uimenubuttonList = document.getElementsByClassName("ui-menubutton");
			}
			for (final DOMNode e : uimenubuttonList) {
				String href = ((DOMElement)e.getElementsByTagName("a").get(0)).getAttribute("href");
				final String productid = href.substring(href.lastIndexOf("=") + 1, href.length());
				((DOMElement)e).setInnerHTML("<a productid=\"" + productid + "\" onclick=\"alert('1,"+productid+"');\" target=\"_blank\">下载</a>");
				((DOMElement)e).setAttribute("id", productid);
				//改为通过onAlert实现，只能存在一种
//				e.addEventListener(DOMEventType.OnClick, new DOMEventListener() {
//					
//					public void handleEvent(DOMEvent arg0) {
//						MessageManager.sendMessage(new DoSelectProductModelMessage(productid));
//					}
//				}, false);
//				System.out.println(((DOMElement)e).getInnerHTML());
			}
			
			
		}
		
        Main.getSelectProductFram().setVisible(true);
		return null;
	}
	
}
