package test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class Mytrailtable extends JPanel {

	private static final long serialVersionUID = 1L;
	JTable table;

	public Mytrailtable() {
		setLayout(new BorderLayout());
		table = new JTable(new SampleTableModel());
		table.setCellSelectionEnabled(true); // not sure
		table.setShowVerticalLines(false);
		// table.setAutoResizeMode(table.AUTO_RESIZE_OFF);
		TableColumn firsetColumn = table.getColumnModel().getColumn(0);
		firsetColumn.setPreferredWidth(30);
		firsetColumn.setMaxWidth(50);
		firsetColumn.setMinWidth(5);

		TableColumn lastColumn = table.getColumnModel().getColumn(3);
		lastColumn.setPreferredWidth(100);
		lastColumn.setMaxWidth(100);
		lastColumn.setMinWidth(50);

		table.getColumn("Button").setCellRenderer(new MyTableCellRenderer());
		table.getColumn("Label").setCellRenderer(new MyFileCellRenderer());
		JScrollPane pane = new JScrollPane(table);
		add(pane, BorderLayout.CENTER);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.setBounds(150, 150, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mytable trail = new mytable();
		frame.add(trail, BorderLayout.NORTH);

		JPanel panel_content = new JPanel();
		panel_content.setLayout(new BorderLayout());
		frame.add(panel_content, BorderLayout.CENTER);

		JPanel panel_path = new JPanel(); // grid_1 path
		panel_path.add(new JLabel("path: "), FlowLayout.LEFT);
		panel_path.add(new JTextField(40));
		panel_content.add(panel_path, BorderLayout.NORTH);

		panel_content.add(new JPanel(), BorderLayout.WEST); // make left and
															// right rim
		panel_content.add(new JPanel(), BorderLayout.EAST);

		JPanel panel_load = new JPanel(); // grid_3 button up or down load

		JButton change_user = new JButton("changeuser");
		panel_load.add(change_user);
		JButton btn_upload = new JButton("upload");
		panel_load.add(btn_upload);
		JButton btn_download = new JButton("download");
		panel_load.add(btn_download);
		panel_content.add(panel_load, BorderLayout.SOUTH);

		Mytrailtable sample = new Mytrailtable();
		panel_content.add(sample, BorderLayout.CENTER);

		// frame.setSize(400, 200);
		frame.setVisible(true);
	}
}

class mytable extends JPanel {
	private static final long serialVersionUID = 1L;
	private JButton btn_trail[];
	private JButton btn_left;
	private JButton btn_right;

	public mytable() {
		this.setLayout(new BorderLayout());
		btn_left = new JButton("<");
		this.add(btn_left, BorderLayout.WEST);
		// ///////////////////////////// trail
		JPanel panel_trailbtn = new JPanel();
		panel_trailbtn.setLayout(new FlowLayout(FlowLayout.LEFT));
		int len = 6; // tempory --> steamobject.length
		btn_trail = new JButton[len];
		String trailname[] = new String[len];
		for (int i = 0; i < len; i++) {
			trailname[i] = "trail" + i + " ";
			btn_trail[i] = new JButton(trailname[i]);
			panel_trailbtn.add(btn_trail[i]);
			btn_trail[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// trail_action(e);
				}
			});
		}
		this.add(panel_trailbtn, BorderLayout.CENTER);

		// /////////////////////////// right
		btn_right = new JButton(">");
		this.add(btn_right, BorderLayout.EAST);
	}
}

class MyFileCellRenderer extends JLabel implements TableCellRenderer {
	public MyFileCellRenderer() {
		super();
		this.setOpaque(true);
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			super.setForeground(table.getSelectionForeground());
			super.setBackground(table.getSelectionBackground());
		} else {
			super.setForeground(table.getForeground());
			super.setBackground(table.getBackground());
		}
		this.setText((value == null) ? " " : ((JLabel) value).getText());
		return this;
	}
}

class MyTableCellRenderer extends JButton implements TableCellRenderer {

	public MyTableCellRenderer() {
		super();
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		this.setText((value == null) ? " " : ((JButton) value).getText());
		if (isSelected) {

			System.out.println(this.getText());
			super.setBackground(new Color(255, 0, 0));
		}
		if (hasFocus) {
			super.setForeground(table.getSelectionForeground());
			super.setBackground(table.getSelectionBackground());
		} else {
			super.setForeground(table.getForeground());
			super.setBackground(table.getBackground());
		}
		return this;
	}
}

class SampleTableModel extends AbstractTableModel {
	Object data[][] = { { new ImageIcon("QQ.gif"), "Jon ", new JLabel("Apple "), new JButton("Apple ") },
			{ new ImageIcon("QQ.gif"), "Marry ", new JLabel("Pine "), new JButton("Pine ") }, { new ImageIcon("QQ.gif"), "Ben ", new JLabel("Peach "), new JButton("Peach ") },
			{ new ImageIcon("QQ.gif"), "Mike ", new JLabel("Orange "), new JButton("Orange ") },
			{ new ImageIcon("QQ.gif"), "Patty ", new JLabel("Apple "), new JButton("Apple ") }, { new ImageIcon("QQ.gif"), "Jimmy ", new JLabel("Lemon "), new JButton("Lemon ") },
			{ new ImageIcon("QQ.gif"), "Jon ", new JLabel("Apple "), new JButton("Apple ") }, { new ImageIcon("QQ.gif"), "Marry ", new JLabel("Pine "), new JButton("Pine ") },
			{ new ImageIcon("QQ.gif"), "Ben ", new JLabel("Peach "), new JButton("Peach ") }, { new ImageIcon("QQ.gif"), "Mike ", new JLabel("Orange "), new JButton("Orange ") },
			{ new ImageIcon("QQ.gif"), "Patty ", new JLabel("Apple "), new JButton("Apple ") }, { new ImageIcon("QQ.gif"), "Jimmy ", new JLabel("Lemon "), new JButton("Lemon ") }, };
	Object names[] = { "Icon", "NAME", "Label", "Button" };

	public int getRowCount() {
		java.awt.Label label = new Label("");
		return data.length;
	}

	public int getColumnCount() {
		return names.length;
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public String getColumnName(int colIndex) {
		return names[colIndex].toString();
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public boolean isCellEditable(int row, int col) {
		return getColumnClass(col) == String.class;
	}

	public void setValueAt(Object aValue, int row, int col) {
		data[row][col] = aValue;
	}
}