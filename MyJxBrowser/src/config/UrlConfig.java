package config;

public class UrlConfig {
	
	public static final String hotKeywordSearchUrl2 = "http://hz.my.data.alibaba.com/industry/.json?action=CommonAction&iName=searchKeywords";
	public static final String hotKeywordSearchUrl = "http://hz.my.data.alibaba.com/industry/keywords.htm";
	
	public static final String repetProductGroupUrl = "http://hz.productposting.alibaba.com/product/searchdiagnostic/repeat/product_repeat.htm?newClusterId=";
	
	public static final String repetProductGrouListpUrl = "http://hz.productposting.alibaba.com/product/searchdiagnostic/repeat/product_repeat_group.htm";
	
	public static final String editProductUrl = "http://hz.productposting.alibaba.com/product/editing.htm?id=";
	
	public static final String postProductFromImportUrl = "http://hz.productposting.alibaba.com/product/post_product_interface.htm?from=manage&import_product_id=60167500528";
	
	public static final String photoBankUrl = "http://sh.vip.alibaba.com/photobank/photobank.htm";
	
	public static final String selectPhotoUrl = "http://sh.vip.alibaba.com/photobank/image_select.htm?iframe_delete=true&setDomain=true&callback=callBackPhotobankSelect&imageType=dynamic&multiSelect=true&imgCount=";
	
	public static final String selectPhotoCatlogUrl = "http://sh.vip.alibaba.com/photobank/ajaxPhotobank.htm";
	
	public static final String productManageUrl = "http://hz.productposting.alibaba.com/product/products_manage.htm";
	
	public static final String productManageUrl2 = "http://hz.productposting.alibaba.com/product/managementproducts/asyProductScoreInfo.do";

	public static final String loginUrl = "http://login.alibaba.com";
	
	public static final String logoutUrl = "https://login.alibaba.com/xman/xlogout.htm";

	public static final String homepageUrl = "http://www.alibaba.com";
	
	public static final String detailUrl = "http://www.alibaba.com/product-detail/"; 
	
}
