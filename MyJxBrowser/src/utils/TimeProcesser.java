package utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeProcesser {

	public static long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	/**
	 * 获取当前时间�?
	 * 
	 * @return 返回秒数
	 */
	public static int getUnixTime() {
		long unixTime = System.currentTimeMillis() / 1000;
		return (int) unixTime;
	}

	public static String getCUnixTime() {
		long unixTime = System.currentTimeMillis() / 1000;
		return String.valueOf(unixTime);
	}

	public static int getUnixTime(Date date) {
		long unixTime = date.getTime() / 1000;
		return (int) unixTime;
	}

	/**
	 * 由时间格式转化为时间�?
	 * 
	 * @param stringTime
	 *            �?"1970-01-01 08:00:00"
	 * @param stringFormat
	 *            "yyyy-MM-dd HH:mm:ss"
	 * @return 秒数
	 * @throws ParseException
	 */
	public static int getUnixTimeByString(String stringTime, String stringFormat) throws ParseException {
		Date date = new SimpleDateFormat(stringFormat).parse(stringTime);
		return (int) (date.getTime() / 1000);
	}

	public static int getUnixTimeByString(String stringTime) throws ParseException {
		return getUnixTimeByString(stringTime, "yyyy-MM-dd HH:mm:ss");
	}

	public static int getUnixTimeSilent(String stringTime) {
		try {
			return getUnixTimeByString(stringTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static int getUnixTimeByStringShanghai(String stringTime, String stringFormat) throws ParseException {
		String strDate2 = "1970-01-01 08:00:00";
		Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(stringTime);
		Date date2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strDate2);
		int l = (int) ((date1.getTime() - date2.getTime()) / 1000);
		return l;
	}

	/**
	 * 格式化时间戳�?yyyy-MM-dd HH:mm:ss
	 * 
	 * @param time
	 *            毫秒�?
	 * @return
	 */
	public static String getDateByUnixTime(long time) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(time);
		return date;
	}

	public static String getDateByUnixTimeMs(long time) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		String date = dateFormat.format(time);
		return date;
	}

	/**
	 * 格式化时间戳�?yyyy-MM-dd HH:mm:ss
	 * 
	 * @param time
	 *            秒数
	 * @return
	 */
	public static String getDateTimeByUnixTime(int time) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(time * 1000L);
		return date;
	}
	
	public static String getDateByUnixTime(int time) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = dateFormat.format(time * 1000L);
		return date;
	}

	public static long getTime(int unixTime) {
		return (long) unixTime * 1000;
	}

	public static int getMorningTime() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return (int) (cal.getTimeInMillis() / 1000);
	}

	/**
	 * 返回间隔的天�?
	 * 
	 * @param SmallerTime
	 * @param largerTime
	 * @return
	 */
	public static int dayInterval(int smallerTime, int largerTime) {
		Double d = Math.floor(largerTime / 86400) - Math.floor(smallerTime / 86400);
		return d.intValue();
	}

	public static int dayInterval(int smallerTime) {
		return dayInterval(smallerTime, getUnixTime());
	}

	/**
	 * �?��今天是否匹配指定的星�?
	 * 
	 * @param week
	 * @return
	 */
	public static boolean checkWeek(int week) {
		return (week & (1 << Calendar.getInstance().get(Calendar.DAY_OF_WEEK))) > 0;
	}

	/**
	 * 当前时间是否在指定的时间�?
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static boolean isRange(int week, String begin, String end) {
		if (checkWeek(week)) {
			return isRange(begin, end);
		} else {
			return false;
		}
	}

	/**
	 * 当前时间是否在指定的时间�?
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static boolean isRange(String begin, String end) {
		int beginTime = getHourTime(begin);
		int endTime = getHourTime(end);
		int current = getUnixTime();
		return beginTime <= current && current <= endTime;
	}

	public static int getHourTime(String hour) {
		String[] time = hour.split(":");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
		calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return (int) (calendar.getTimeInMillis() / 1000);
	}

	public static Date getHourDateTime(String hour) {
		String[] time = hour.split(":");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
		calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 当前时间是否在指定的时间段内
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static boolean inTimePeriod(int beginSec, int endSec) {
		int curSec = getUnixTime() - getMorningTime();

		if (curSec > beginSec && curSec < endSec) {
			return true;
		}
		return false;
	}

	/**
	 * 汉化时间长度
	 * 
	 * @param seconds
	 * @return
	 */
	public static String fromatTimeLength(int seconds) {
		StringBuilder sb = new StringBuilder();
		int day = seconds / 86400;
		if (day > 0) {
			sb.append(day).append(":");
		}
		int hour = (seconds % 86400) / 3600;
		if (hour > 0) {
			sb.append(hour).append(":");
		}
		int a = seconds % 3600;
		if (a > 0) {
			int m = a / 60;
			int s = a % 60;
			if (m > 0) {
				sb.append(m).append(":");
			}
			if (s > 0) {
				sb.append(s).append("");
			}
		}

		return sb.toString();
	}
}
