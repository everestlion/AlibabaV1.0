package utils;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class NumberProcesser {

	private static Random random = new Random();
	private static int defaultBase = 100;

	public static int between(int value, int min, int max) {
		return Math.min(max, Math.max(min, value));
	}

	private static int getRand(int max) {
		if (max <= 0) {
			return 0;
		}
		return random.nextInt(max + 1);
	}

	/**
	 * 获取随机�?1到max 包含
	 * 
	 * @param max
	 * @return
	 */
	public static int mtRand(int max) {
		return mtRand(1, max);
	}

	/**
	 * 获取随机 包含min max
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int mtRand(int min, int max) {
		if (min >= max) {
			return max;
		}
		int Offset = max - min;
		return getRand(Offset) + min;
	}

	/**
	 * 判断是否达到随机�?
	 * 
	 * @param rate
	 *            几率�?
	 * @param base
	 *            �?��随机�?
	 * 
	 * @return 是否获得
	 */
	public static boolean isWinning(int rate, int base) {
		if (rate <= 0) {
			return false;
		}
		return mtRand(base) <= rate;
	}

	public static boolean isWinning(int rate) {
		return isWinning(rate, defaultBase);

	}

	/**
	 * �?��公约�?
	 * 
	 * @param m
	 * @param n
	 * @return
	 */
	public static int gcd(int m, int n) {
		if (m < n) {
			int t = n;
			n = m;
			m = t;
		}
		int r;
		do {
			r = m % n;
			m = n;
			n = r;
		} while (r != 0);
		return m;
	}

	public static int gcd(List<Integer> nums) {
		int r = nums.get(0);

		for (int i = 1; i < nums.size(); i++) {
			r = gcd(r, nums.get(i));
			if (r == 1) {
				break;
			}
		}
		return r;
	}

	public static int hashKey(int rootId, int typeId) {
		final int prime = 31;
		int result = 1;
		result = prime * result + rootId;
		result = prime * result + typeId;
		return result;
	}

	public static int[] getRandomArray(int num, int size) {
		int[] percent = new int[size];
		Arrays.fill(percent, 1);// 保证�?��的数==1
		int s = num - size;
		int k = 20;// 常数�?=1比较平均的分配，越大越不平均�?
		// Random r = new Random();
		while (s > 0) {
			int x = random.nextInt(size);
			int y = random.nextInt(k > s ? s : k) + 1;
			percent[x] += y;
			s -= y;
		}
		return percent;
	}

	/**
	 * 返回�?��的（�?��近负无穷大）double 值，该�?大于等于参数，并等于某个整数
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static int ceil(int a, int b) {
		double cc = Math.ceil((double) a / (double) b);
		return (int) cc;
	}

	/**
	 * 返回�?��的（�?��近正无穷大）double 值，该�?小于等于参数，并等于某个整数
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static int floor(int a, int b) {
		double cc = Math.floor((double) a / (double) b);
		return (int) cc;
	}

	public static int cycle(int times, int onceTimes) {
		int a = times / onceTimes;
		int b = times % onceTimes;
		if (b > 0) {
			a++;
		}
		return a;
	}

	/**
	 * 计算百分加成
	 * 
	 * @param source
	 * @param percentAdd
	 * @return
	 */
	public static int percentClac(int source, int percentAdd) {
		if (percentAdd == 0 || source == 0) {
			return source;
		}
		int result = source * (100 + percentAdd) / 100;
		return result;
	}

	public static int percentOnlyAddValue(int source, int percentAdd) {
		if (percentAdd == 0 || source == 0) {
			return 0;
		}
		int result = source * percentAdd / 100;
		return result;
	}

	/**
	 * 数组里面是否包含
	 * 
	 * @param sourceArr
	 * @param target
	 * @return
	 */
	public static boolean contains(int[] sourceArr, int target) {
		if (sourceArr == null || sourceArr.length == 0) {
			return false;
		}
		for (int i = 0; i < sourceArr.length; i++) {
			if (sourceArr[i] == target) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取数组里面不超出目标的�?��下标
	 * 
	 * @param sourceArr
	 * @param target
	 * @return
	 */
	public static int nearMaxIndex(int[] sourceArr, int target) {
		int indexId = 0;
		if (sourceArr == null || sourceArr.length == 0) {
			return 0;
		}
		for (int i = 0; i < sourceArr.length; i++) {
			if (target >= sourceArr[i]) {
				indexId = i;
			}
		}
		return indexId;
	}

	public static String format(long source) {
		NumberFormat usFormat = NumberFormat.getIntegerInstance(Locale.US);
		return usFormat.format(source);
	}

	public static int rateIndex(int[] sourceArr) {
		return rateIndex(sourceArr, 100);
	}

	public static int rateIndex(int[] sourceArr, int base) {
		if (sourceArr == null || sourceArr.length == 0) {
			return 0;
		}
		int currentRate = mtRand(base);
		int tmpRate = 0;
		for (int i = 0; i < sourceArr.length; i++) {
			tmpRate += sourceArr[i];
			if (currentRate <= tmpRate) {
				return i;
			}
		}
		return 0;
	}
	
	public static int getRateOne(int[] sourceArr){
		return sourceArr[mtRand(sourceArr.length)-1];
	}
	
}
