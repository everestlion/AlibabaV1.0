package sample;
/*
 * Copyright (c) 2000-2014 TeamDev Ltd. All rights reserved.
 * TeamDev PROPRIETARY and CONFIDENTIAL.
 * Use is subject to license terms.
 */

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserFactory;
import com.teamdev.jxbrowser.chromium.BrowserView;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * The sample demonstrates how to get screen shot of the web page
 * and save it as PNG image file.
 */
public class HTMLToImageSample {
    public static void main(String[] args) throws Exception {
        final Browser browser = BrowserFactory.create();
        // Wait until web page is loaded completely
        invokeAndWaitReady(browser, new Runnable() {
            public void run() {
                browser.loadURL("http://www.teamdev.com");
            }
        });

        int viewWidth = 1280;
        int viewHeight = 1024;
        BrowserView view = browser.getView();
        // Resize view to the required size
        view.updateSize(viewWidth, viewHeight);
        // Get image of the loaded web page
        Image image = view.getImage();
        // Save image as PNG file
        BufferedImage bufferedImage = new BufferedImage(viewWidth, viewHeight,
                BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D graphics = (Graphics2D) bufferedImage.getGraphics();
        // Scale image according to the current device scale factor.
        // This code must be used to support Retina displays.
        Double scale = 1.0 / browser.getView().getDeviceScaleFactor();
        graphics.scale(scale, scale);
        graphics.drawImage(image, 0, 0, null);
        ImageIO.write(bufferedImage, "PNG", new File("teamdev.com.png"));
        browser.dispose();
    }

    public static void invokeAndWaitReady(Browser browser, Runnable runnable) {
        final CountDownLatch latch = new CountDownLatch(1);
        LoadAdapter listener = new LoadAdapter() {
            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent event) {
                if (event.isMainFrame()) {
                    latch.countDown();
                }
            }
        };
        browser.addLoadListener(listener);
        try {
            runnable.run();
            try {
                if (!latch.await(45, TimeUnit.SECONDS)) {
                    throw new RuntimeException(new TimeoutException());
                }
            } catch (InterruptedException ignore) {
                Thread.currentThread().interrupt();
            }
        } finally {
            browser.removeLoadListener(listener);
        }
    }
}
